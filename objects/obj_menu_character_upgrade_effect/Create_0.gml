motherInst	= noone;
purchaseAlpha = 1;
purchaseScale = 1;
	
drawType = 0;
selectedDraw	= function() {
	if(purchaseAlpha > 0)
	{
		var _alpha	= purchaseAlpha;
		var _scale	= purchaseScale;
		with(motherInst)
		{
			if(surface_exists(surface) && instance_exists(obj_menu_character_ability_control))
			{
				var _x	= (1-_scale)*surface_get_width(surface)/2;
				var _y	= (1-_scale)*surface_get_height(surface)/2;
				
				var _inventoryLeft	= obj_menu_character_ability_control.inventoryLeft;
				var _inventoryRight	= obj_menu_character_ability_control.inventoryRight;

				var _left	= 0+max(0,_inventoryLeft-bbox_left);
				var _top	= 0;
				var _right	= surface_get_width(surface)+min(0,_inventoryRight-bbox_right)-_left;
				var _bottom	= surface_get_height(surface);
				gpu_set_blendmode(bm_max);
				draw_surface_part_ext(surface,_left,_top,_right,_bottom,x+_x+_left,y+_y,_scale,_scale,c_white,1);
				gpu_set_blendmode(bm_normal);
			}
		}
	}
	else
	{
		instance_destroy();
	}
	purchaseAlpha -= 0.5;
	purchaseScale += 0.02;
};
purchaseDraw	= function() {
	if(purchaseAlpha > 0)
	{
		var _alpha	= purchaseAlpha;
		var _scale	= purchaseScale;
		with(motherInst)
		{
			if(surface_exists(surface) && instance_exists(obj_menu_character_ability_control))
			{
				var _x	= (1-_scale)*surface_get_width(surface)/2;
				var _y	= (1-_scale)*surface_get_height(surface)/2;
				
				var _left	= 0
				var _top	= 0;
				var _right	= surface_get_width(surface);
				var _bottom	= surface_get_height(surface);
				draw_surface_part_ext(surface,_left,_top,_right,_bottom,x+_x+_left,y+_y,_scale,_scale,c_white,_alpha);
			}
		}
	}
	else
	{
		instance_destroy();
	}
	purchaseAlpha -= 0.05;
	purchaseScale += 0.06;
};

drawMain	= function() {
	if(drawType == 0)
	{
		selectedDraw();
	}
	else if(drawType == 1)
	{
		purchaseDraw();
	}
}
