method_show_debug(["firebase remote config","fetch start"]);

networkErrorMsg		= "Internet connection is required.";
serverNotRespondMsg	= "The server is not responding.";

Firebase_remote_config_init(global.debugMode);
Firebase_RemoteConfig_fetch();

getFetchData	= function() {
	method_show_debug(["get fetch"]);
	
	#region get fetch data
	global.structGameMeta	= json_parse(Firebase_RemoteConfig_getString("metaData"));
	global.structGameText	= json_parse(Firebase_RemoteConfig_getString("gameText"));
	#endregion
	
	#region language code set
	switch(os_get_language()) 
	{
		case "zh":
		{
			var _region = os_get_region();
			if (_region == "HK" || _region == "MO" || _region == "TW")
			{
				langCode	= "zh-tw";
			}
			else
			{
				langCode	= "zh-cn";
			}
			break;
		}
	
		default :
		{
			langCode	= os_get_language();
			global.languageCode	= langCode;
			break;
		}
	}
				
	var _check	= method_struct_get_deep_value(global.structGameText,["common","languageCodeTest",global.languageCode]);
	if(is_undefined(_check))
	{
		global.languageCode	= "en";
	}
	#endregion				
	
	#region next step
	with(obj_room_game_start_manager)
	{
		gameStartProcess("fetch");
	}
	#endregion
}

