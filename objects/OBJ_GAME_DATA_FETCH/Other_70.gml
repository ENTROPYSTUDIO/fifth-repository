// All Async Events for Authentication
var _type,_event,_value,_exists,_listener_id;
if(!ds_exists(async_load,ds_type_map)) { exit; }
_type		= async_load[? "type"];
if(is_undefined(_type)) { exit; }

if(_type == Firebase_asyncEvent)
{
	_event	= async_load[? "event"];
	//_ref	= async_load[? "ref"];
	_value	= async_load[? "value"];
	_exists	= async_load[? "exists"];
	_listener_id		= async_load[? "Id"];
	switch(_event)
	{
		case "RemoteConfig_fetch":
		{
			/*ds_map_destroy(async_load)*/
			method_show_debug(["RemoteConfig fetch Response"]);
			if(_value)
			{
				getFetchData();
			}
			else
			{
				if(tryFetch < 5)
				{
					method_show_debug(["firebase remote config","fetch retry"]);
					tryFetch++;
					Firebase_RemoteConfig_fetch();
				}
				else
				{
					if(Firebase_RemoteConfig_getString("itemData") != "")
					{
						getFetchData();
					}
				}
			}
			break;
		}

	}
}

