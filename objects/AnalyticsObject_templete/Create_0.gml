/// @description Initialize Firebase Analytics

if(global.debugMode)
{
	firebase_analytics_debug_mode(true);
}

//This will start collecting data.
firebase_analytics_init();

//You can assign the following functions in a button.

//firebase_analytics_set_current_screen("Main Screen");

firebase_analytics_set_userid("Martin");
firebase_analytics_set_userproperty("CountryPreference","Turkey");

var scr,level,character;
scr = 100;
level = 2;
character = "Skull Face";
//firebase_analytics_event_post_point(scr,level,character);

//THIS PART IS INTENTIONALLY COMMENTED
/*
This extension does not automatically tracks analytical datas, this is intentionally made for some games
for European Union's GDPR data laws. Once you call firebase_analytics_init, the extension will start tracking
data. You can disable this via firebase_analytics_disable(); 
or enable again via firebase_analytics_enable();

If you don't need to disable/enable it, just use firebase_analytics_init() and everything will be fine.
*/