gc_enable(false);
gc_target_frame_time(10000);
alarm[0]	= 360;
oldCurrentTime	= current_time;
dataInit	= false;
lidUserData		= noone;

#region userDataListener
userDataUpdate	= function(_value,_event,_childKey,_dataType) {
	if(_dataType == enumFbReturnDataType.json)
	{
		switch(_event)
		{
			case "DataBase_onChildAdded": 
			case "DataBase_onChildChanged":
			{
				var _structValue	= json_parse(_value);
				var _oldStruct	= global.structUserData[$ _childKey];
				if(is_struct(_oldStruct)) 
				{ 
					variable_struct_remove(global.structUserData,_childKey);
					
				}
				variable_struct_set(global.structUserData,_childKey,_structValue);
				break;
			}
			case "DataBase_onChildRemoved":
			{
				variable_struct_remove(global.structUserData,_childKey);
				break;
			}
		}
	}
	else
	{
		switch(_event)
		{
			case "DataBase_onChildAdded": 
			case "DataBase_onChildChanged":
			{
				variable_struct_set(global.structUserData,_childKey,_value);
				break;
			}
			case "DataBase_onChildRemoved":
			{
				variable_struct_remove(global.structUserData,_childKey);
				break;
			}
		}
		
		if(_childKey == "connectionTime")
		{
			global.serverTimestamp	= _value;
		}
	}
	if(!dataInit)
	{
		if(_event == "DataBase_onChildChanged")
		{
			dataInit	= true;

			with(obj_room_game_start_manager)
			{
				gameStartProcess("loadUserData");
			}
			firebase_rtdb_remove_listener_child(lidUserData);
		}
	}
}

lidUserData	= firebase_rtdb_listener_child_builder(mac_firebase_ref_my_uid,userDataUpdate,undefined,id);
var _ref1	= method_firebase_ref_addChildren(mac_firebase_ref_my_uid,["connectionTime"]);
var _ref2	= method_firebase_ref_addChildren(mac_firebase_ref_my_uid,["lastOnline"]);
lidFirebaseInfoConnected	= firebase_connection_manager(_ref1,_ref2);
#endregion

#region updatePipeline
global.firebaseUserDataPipeLine	= new firebaseRtdbUpdateMultiPathConstructor();

structUserDataUploader	= {
	delay	: 1000*60,
	time	: current_time,
	process	: false,
	
	regularUpdate	: function(){
		if(current_time > time && !process)
		{
			time		= current_time+delay;
			if(global.firebaseUserDataPipeLine.existsCheck())
			{
				process	= true;
				var _json	= global.firebaseUserDataPipeLine.getJson();
				firebase_rtdb_node_builder(mac_firebase_ref_my_uid,_json,false,true,true,regularDone,regularFail);
			}
		}
	},
	forceUpdate	: function(){
		if(global.firebaseUserDataPipeLine.existsCheck())
		{
			process	= true;
			var _json	= global.firebaseUserDataPipeLine.getJson();
			firebase_rtdb_node_builder(mac_firebase_ref_my_uid,_json,false,true,true,regularDone,regularFail);
		}
	},
	
	regularDone	: function(){
		process		= false;
		global.firebaseUserDataPipeLine.clear();
	},
	regularFail	: function(){
		process		= false;
	},
}
#endregion


