// Setup:
sys_cloudDefault = part_system_create_layer("particleBackground",false);
part_cloudDefault = part_type_create();
part_type_shape(part_cloudDefault, pt_shape_square);
part_type_size(part_cloudDefault, 0.8, 1.8, 0, 0);
part_type_scale(part_cloudDefault, 1, 1);
//part_type_color3(part_cloudDefault, 16777215, 16777215, 16579836);
part_type_alpha3(part_cloudDefault, 0.03, 0.06, 0.01);
part_type_speed(part_cloudDefault, 0.3, 2, 0, 0);
part_type_direction(part_cloudDefault, 0, 359, 0, 0);
part_type_orientation(part_cloudDefault, 0, 0, 0.1, 0, 0);
part_type_blend(part_cloudDefault, 1);
part_type_life(part_cloudDefault, 500, 800);

emit_cloudDefault = part_emitter_create(sys_cloudDefault);
part_emitter_region(sys_cloudDefault, emit_cloudDefault, 0, room_width, 0, room_height, ps_shape_rectangle, ps_distr_linear);
//part_emitter_region(sys_cloudDefault, emit_cloudDefault, x - 0, x + 0, y - 300, y + 300, ps_shape_rectangle, ps_distr_linear);
part_emitter_stream(sys_cloudDefault, emit_cloudDefault, part_cloudDefault, -70);

