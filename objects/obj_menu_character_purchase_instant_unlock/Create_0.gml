forceInput	= false;
myWindowLayer	= enumWindowLayer.slotMenu;
var _text	= method_struct_get_deep_value(global.structGameText,["common","purchase",global.languageCode]);
structTextDraw	= new drawTextConstructor(_text,x,y,fa_center,fa_middle,global.fontMainBold,mac_default_font_scale-0.2,c_white,1);
structSpriteDraw	= new drawSpriteConstructor(sprite_index,0,x,y,1,0,enumColor.skyBlue,1);

slotName	= obj_menu_character_main_control.slotName;
characterUid	= obj_menu_character_main_control.characterUid;
callInstance	= obj_menu_character_main_control.callInstance;
instanceUnlockGetPrice	= obj_menu_character_main_control.instanceUnlockGetPrice;
asyncProcess	= false;

#region purchase function
purchase = function() {
	var _currency	= method_struct_get_deep_value(global.structGameMeta,["unlock",slotName,"unlockInstantCurrency"]);
	var _now		= user_resource_get_real(_currency);
	var _price		= instanceUnlockGetPrice();
	if(!is_undefined(_price))
	{
		if(_now >= _price)
		{
			purchaseDone();
			
			//if(_price > 0)
			//{
			//	var _popupName	= method_struct_get_deep_value(global.structGameText,["common","watingResult",global.languageCode]);
			//	var _functionData	= new firebase_cloud_function_builder("unlockCharacterSlot",{
			//		slotKey	: slotName,
			//		slotType : characterUid
			//	});
	
			//	firebase_waiting_for_cloud_function_result(id,_functionData,_popupName,purchaseDone,purchaseFail);
			//}
			//else
			//{
			//	asyncProcess	= false;
			//	var _text	= method_struct_get_deep_value(global.structGameText,["error","notEnoughResource",global.languageCode]);
			//	popup_message(_text,120);
			//}
		}
		else
		{
			purchaseFail();
		}
	}
	else
	{
		purchaseFail();
	}
	
	//if(os_get_config() == "pc")
	//{
	//	asyncProcess	= false;
	//	global.structUserData.equip.character[$ slotName]	= "empty";
		
	//	var _text	= method_struct_get_deep_value(global.structGameText,["common","unlockSuccess",global.languageCode]);
	//	popup_message(_text,120);
		
	//	with(callInstance)
	//	{
	//		slotManageCreate();
	//	}
	//}
};

purchaseDone	= function() {
	//global.windowLayer	= myWindowLayer;
	//asyncProcess	= false;
	//var _result	= json_parse(_value);
	//var _success	= _result.success;
	//if(_success)
	//{
	//	var _text	= method_struct_get_deep_value(global.structGameText,["common",_result.message,global.languageCode]);
	//	popup_message(_text,120);
		
	//	with(callInstance)
	//	{
	//		slotManageCreate();
	//	}
	//}
	//else
	//{
	//	purchaseFail(_value);
	//}
	
	//delete(_result);
	var _currency	= method_struct_get_deep_value(global.structGameMeta,["unlock",slotName,"unlockInstantCurrency"]);
	var _now		= user_resource_get_real(_currency);
	var _price		= instanceUnlockGetPrice();
	var _finalResource	= string(_now-_price);
	
	global.structUserData.equip.character[$ slotName]	= "empty";
	global.structUserData.resource[$ _currency]	= _finalResource;
	variable_struct_remove(global.structUserData.timestamp.equip.character,slotName);
	
	firebase_userdata_pipeline_add("equip/character/"+slotName,"empty");
	firebase_userdata_pipeline_add("resource/"+_currency,_finalResource);
	firebase_userdata_pipeline_add("timestamp/equip/character/"+slotName,{});
	firebase_userdata_pipeline_upload();
	
	var _text	= method_struct_get_deep_value(global.structGameText,["common","unlockSuccess",global.languageCode]);
	popup_message(_text,120);
	
	with(callInstance)
	{
		slotManageCreate();
	}
};
purchaseFail	= function() {
	var _text	= method_struct_get_deep_value(global.structGameText,["error","notEnoughResource",global.languageCode]);
	popup_message(_text,120);
			
	//global.windowLayer	= myWindowLayer;
	//asyncProcess	= false;
	//if(_value != "error")
	//{
	//	var _result	= json_parse(_value);
	//	var _text	= method_struct_get_deep_value(global.structGameText,["common",_result.message,global.languageCode]);
	//}
	//else
	//{
	//	var _text	= method_struct_get_deep_value(global.structGameText,["common","networkError",global.languageCode]);
	//}
	//popup_message(_text,120);

	//delete(_result);
};
#endregion

