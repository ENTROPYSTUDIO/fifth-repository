if(moveStart)
{
	if(current_time > moveTime)
	{
		var _id	= id;
		with(all)
		{
			if(!persistent && id != _id)
			{
				instance_destroy();
			}
		}
		moveStart	= false;
		room_goto(roomDestination);
	}
}

if(endStart)
{
	alpha	= lerp(alpha,-1.5,0.05);
	if(alpha < 0)
	{
		instance_destroy();
	}
}

