var _view	= view_camera[0];
var _viewLeft	= camera_get_view_x(_view);
var _viewTop	= camera_get_view_y(_view);
var _viewRight	= _viewLeft+camera_get_view_width(_view);
var _viewBottom	= _viewTop+camera_get_view_height(_view);

#region visible
//ds_list_clear(listDepth);
//var _listDepth	= listDepth;
with(obj_parent_stage)
{
	if(bbox_right < _viewLeft || bbox_left > _viewRight || bbox_bottom < _viewTop || bbox_top > _viewBottom)
	{
		visible	= false;
	}
	else
	{
		visible	= true;
		//ds_list_add(_listDepth, (id | y << 32) );
	}
}
//ds_list_sort(listDepth, true);
#endregion

