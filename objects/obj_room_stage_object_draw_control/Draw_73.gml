//draw_set_halign(fa_center);
//draw_set_valign(fa_middle);
//draw_set_color(c_white);
//draw_set_font(global.fontMainBold);
//with(obj_parent_player)
//{
//	if(structDraw.starIcon != noone)
//	{
//		draw_sprite_ext(structDraw.starIcon,0,x+structDraw.starIconX,bbox_top+structDraw.starIconY,0.4,0.4,0,c_white,1);
//		//draw_text_transformed(x,bbox_top+structDraw.heroNameY,structDraw.heroName,0.7,0.7,0);//(structDraw.starIcon,0,x+structDraw.starIconX,bbox_top+structDraw.starIconY,0.5,0.5,0,c_white,1);
//	}
//}

with(obj_parent_human)
{
	if(!visible) { continue; }
	if(!structState.die)
	{
		structDraw.healthBarDraw(x,bbox_top+structDraw.healthBarDrawY,structState.healthRatio,structAbility.healthPoint);
	}
	structDraw.drawDefaultDamage();
	structDraw.drawCriticalDamage();
	structDraw.drawCriticalInfo();
	structDraw.drawAggroSprite();
}


