with(obj_parent_loot)
{
	if(!visible || !structState.ready) { continue; }
	draw_set_alpha(image_alpha);
	draw_sprite_ext(structDraw.shadowSprite,0,x,y,1,1,0,c_white,image_alpha);
}
draw_set_alpha(1);

with(obj_parent_pet)
{
	if(!visible) { continue; }
	draw_sprite_ext(structDraw.auraSprite,0,x,y,structDraw.nowScale,structDraw.nowScale,0,c_white,1);
	draw_sprite(structDraw.shadowSprite,0,x,y);
}
with(obj_parent_human)
{
	if(!visible) { continue; }
	if(!structState.die)
	{
		draw_sprite_ext(structDraw.auraSprite,0,x,y,structDraw.nowScale,structDraw.nowScale,0,c_white,1);
	}
	draw_sprite(structDraw.shadowSprite,0,x,y);
}