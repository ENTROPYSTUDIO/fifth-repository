beforeUserData	= "";
#region createFunction
createFunction	= function(){
	var _characterUid	= global.structUserData.equip.character[$ slotName];
	originalUid	= _characterUid;
	switch(_characterUid)
	{
		case "lock" :
		{
			drawMain	= function() {
				static frameColor	= enumColor.navy;
				static scale	= 80/sprite_get_height(spr_room_stage_avatar_frame);
				draw_sprite_ext(spr_room_stage_avatar_frame,0,x,y,1,1,0,frameColor,1);
				draw_sprite_ext(spr_icon_lock,0,x,y,scale,scale,0,c_white,1);
			};
			break;
		}
		case "empty" :
		{
			drawMain	= function() {
				static frameColor	= enumColor.skyBlue;
				draw_sprite_ext(spr_room_stage_avatar_frame,0,x,y,1,1,0,frameColor,1);
			};
			break;
		}
		case "unlocking" :
		{
			drawMain	= function() {
				static frameColor	= enumColor.navy;
				static scale	= 80/sprite_get_height(spr_room_stage_avatar_frame);
				draw_sprite_ext(spr_room_stage_avatar_frame,0,x,y,1,1,0,frameColor,1);
				draw_sprite_ext(spr_icon_lock,0,x,y,scale,scale,0,c_white,1);
			};
			break;
		}
		default :
		{
			drawMain	= function() {
				var _characterUid	= global.structUserData.equip.character[$ slotName];
				var _grade	= character_get_static_ability(_characterUid,"grade","string");
				var _index	= character_get_static_ability(_characterUid,"index","string");
				var _starIcon	= asset_get_index("spr_character_star_grade_"+_grade);
				var _frameSub	= real(_grade);
				var _avatarSub	= real(_index);
				var _sprAvatar	= asset_get_index("SPR_CHARACTER_HEAD_GRADE_"+_grade);
			
				static frameColor	= enumColor.skyBlue;
				static scale	= 60/sprite_get_height(spr_room_stage_avatar_frame);
				draw_sprite_ext(spr_room_stage_avatar_frame,0,x,y,1,1,0,frameColor,1);
				draw_sprite_ext(_sprAvatar,_avatarSub,x,y-5,scale,scale,0,c_white,1);
				draw_sprite_ext(_starIcon,0,x,bbox_bottom-15,0.45,0.45,0,c_white,1);
			};
			break;
		}
	}
};
#endregion

#region slotChangeDone
slotChangeDone	= function() {
	var _text	= method_struct_get_deep_value(global.structGameText,["common","legendEntrySuccess",global.languageCode]);
	popup_message(_text,120);
	
	var _slotGridIndexY	= slotGridIndexY;
	with(obj_rm_character_inventory_manager)
	{
		structInventory.inventoryUpdateIndividual(global.inventorySelectedIndex);
		if(_slotGridIndexY != -1)
		{
			structInventory.inventoryUpdateIndividual(_slotGridIndexY);
		}
		surfaceUpdate	= true;
	}
	with(obj_rm_character_team_entry_parent)
	{
		instance_destroy();
	}
	instance_create_depth(0,0,0,obj_system_shader_shock_wave);
	ex_camera_flash(MAC_CAMERA_NAME_MAIN, 0.9, 60,method_ease_out_sine, c_white, bm_max,undefined,undefined);
};
#endregion

#region touchEvent
touchEvent	= function(){
	var _characterUid	= global.structUserData.equip.character[$ slotName];
	switch(_characterUid)
	{
		case "lock":
		case "unlocking":
		{
			slotManageCreate();
			break;
		}
		default:
		{
			var _vaild	= sameLegendCheck();
			if(_vaild)
			{
				var _multiUpdate	= new firebaseRtdbUpdateMultiPathConstructor();
				var _newKey	= global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
				var _nowKey	= global.structUserData.equip.character[$ slotName];
				if(_nowKey == "empty")
				{
					global.structUserData.equip.character[$ slotName]	= _newKey;
					global.structUserData.inventory.character[$ _newKey][$ "equip"]	= slotName;
					
					firebase_userdata_pipeline_add("equip/character/"+slotName,_newKey);
					firebase_userdata_pipeline_add("inventory/character/"+_newKey+"/equip",slotName);
				}
				else
				{
					global.structUserData.equip.character[$ slotName]	= _newKey;
					global.structUserData.inventory.character[$ _nowKey][$ "equip"]	= "none";
					global.structUserData.inventory.character[$ _newKey][$ "equip"]	= slotName;
					
					firebase_userdata_pipeline_add("equip/character/"+slotName,_newKey);
					firebase_userdata_pipeline_add("inventory/character/"+_nowKey+"/equip","none");
					firebase_userdata_pipeline_add("inventory/character/"+_newKey+"/equip",slotName);
				}
				
				firebase_userdata_pipeline_upload();
				slotChangeDone();
			}
			else
			{
				var _message	= method_struct_get_deep_value(global.structGameText,["error","sameLegendSlot",global.languageCode]);
				popup_message(_message,120);
			}
			break;
		}
	}

	mouse_clear(mb_left);
};
#endregion

#region stepMain
stepMain	= function(){
	if(method_function_input_mouse_pressed_gui_single(id,myWindowLayer,noone) || (myWindowLayer == global.windowLayer && forceInput))
	{
		mouse_clear(mb_left);
		forceInput	= false;
		touchEvent();
	}
};
#endregion

#region slotManageCreate
slotManageCreate	= function(){
	var _callback	= callbackUnlock;
	var _characterUid		= global.structUserData.equip.character[$ slotName];
	var _slotName	= slotName;
	var _callInstance	= id;
	
	with(obj_menu_character_main_control)
	{
		instance_destroy();
	}
	if(_characterUid != "empty")
	{
		with(instance_create_layer(0,0,"slot_menu",obj_menu_character_main_control))
		{
			callInstance	= _callInstance;
			characterUid	= _characterUid;
			slotName	= _slotName;
			callbackFunc	= _callback;
			event_user(0);
		}
	}
	else
	{
		global.windowLayer	= myWindowLayer;
	}
};
#endregion

#region sameLegendCheck
sameLegendCheck	= function(){
	var _reuturn	= true;
	var _selectedLegend	= global.gridCharInventory[# enumGridCharacterInventory.legendKey,global.inventorySelectedIndex];
	var _slotLegend	= slotGridIndexY > -1 ? global.gridCharInventory[# enumGridCharacterInventory.legendKey,slotGridIndexY] : global.structUserData.equip.character[$ slotName];
	
	var _size	= variable_struct_names_count(global.structUserData.equip.character);
	for(var _i=0; _i<_size; ++_i)
	{
		var _slotName	= mac_equip_character_key_prefix+string(_i);
		var _slotLegend	= global.structUserData.equip.character[$ _slotName];
		if(_slotLegend == "lock" || _slotLegend == "unlocking" || _slotLegend == "empty")
		{
			continue;
		}
		else
		{
			if(_slotLegend == _selectedLegend && _slotName != slotName)
			{
				_reuturn	= false;
				break;
			}
		}
	}
	
	return _reuturn;
};
#endregion

#region callbackFunc
callbackUnlock	= function(){
	with(obj_rm_character_team_entry_slot)
	{
		createFunction();
	}
	with(obj_rm_character_team_entry_manager)
	{
		surfaceUpdate	= true;
	}
	system_shader_create_blur(true,1);
	global.windowLayer	= myWindowLayer;
};
#endregion

#region init
myWindowLayer	= enumWindowLayer.teamEntry;
forceInput	= false;
drawMain	= undefined;
cloudFcInitDataDone	= undefined;
cloudFcInitDataFail	= undefined;
#endregion










