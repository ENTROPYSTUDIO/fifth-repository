var _names	= variable_struct_get_names(structListener);
var _size	= array_length(_names);
for(var _i=0; _i<_size; ++_i)
{
	var _key	= _names[_i];
	if(!instance_exists(structListener[$ _key].ownerId))
	{
		Firebase_DataBase_RemoveListener(real(_key));
		variable_struct_remove(structListener,_key);
	}
}