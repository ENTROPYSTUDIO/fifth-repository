// All Async Events for Authentication
var _type,_event,_value,_exists,_listener_id;
if(!ds_exists(async_load,ds_type_map)) { exit; }
_type		= async_load[? "type"];
if(is_undefined(_type)) { exit; }

if(_type == Firebase_asyncEvent)
{
	_event	= async_load[? "event"];
	_value	= async_load[? "value"];
	_exists	= async_load[? "exists"];
	_listener_id		= async_load[? "Id"];
	switch(_event)
	{
		case "DataBase_onChildAdded":
		case "DataBase_onChildChanged":
		case "DataBase_onChildRemoved":
		{
			var _names	= variable_struct_get_names(structListener);
			var _size	= array_length(_names);
			for(var _i=0; _i<_size; ++_i)
			{
				var _key	= real(_names[_i]);
				var _convertLid	= real(_key);
				if(_listener_id == _convertLid)
				{
					if(_exists)
					{
						var _childKey		= async_load[? "childKey"];
						var _dataType		= async_load[? "dataType"];
						
						var _callbackFunction	= structListener[$ _key].callbackDone;
						if(!is_undefined(_callbackFunction))
						{
							_callbackFunction(_value,_event,_childKey,_dataType);
						}
					}
					else
					{
						var _callbackFunction	= structListener[$ _key].callbackFail;
						if(!is_undefined(_callbackFunction))
						{
							_callbackFunction();
						}
					}
					break;
				}
			}
			break;
		}
	}
}
