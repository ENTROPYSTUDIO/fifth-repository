if(scale_switch == 0)
{
    if(scale < 1.3)
    {
       scale	= lerp(scale,1.4,0.2);
    }
    else
    {
        scale_switch = 1;
    }
}
else if(scale_switch == 1)
{
    if(scale > 1)
    {
       scale	= lerp(scale,0.5,0.2);
    }
    else
    {
        scale_switch    = 2;
        scale	= 1;
    }
}

draw_sprite_ext(sprite_index,0,x,y,scale,scale,0,c_white,alpha);

draw_set_alpha(alpha);
draw_set_font(font);
draw_set_halign(fa_center);
draw_set_valign(fa_middle);
draw_set_color(fontColor);
draw_text_transformed(x,y,stringMessage,font_size*scale,font_size*scale,0);


draw_set_alpha(1);
draw_set_color(c_black);

