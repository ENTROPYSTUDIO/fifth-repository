// Inherit the parent event
event_inherited();
cleanUp	= undefined;

create	= function(_petUid,_slotName){
	switch(_petUid)
	{
		case "lock" :
		{
			instance_destroy();
			break;
		}
		case "unlocking" :
		{
			instance_destroy();
			break;
		}
		case "empty" :
		{
			instance_destroy();
			break;
		}
		default :
		{
			slotName	= _slotName;
			petUid	= _petUid;
			structAbility	= new constructPetAbility(petUid);
			structState		= new constructPetState();
			structDraw		= new constructPetDraw();
			image_xscale	= structState.imageScale;
			image_yscale	= structState.imageScale;
			lerpHsp	= 0;
			lerpVsp	= 0;
			
			cleanUp	= function(){
				if(typeof(structState) == "struct")
				{
					delete(structState);
					structState	= undefined;
				}
				if(typeof(structAbility) == "struct")
				{
					delete(structAbility);
					structAbility	= undefined;
				}
				if(typeof(structDraw) == "struct")
				{
					delete(structDraw);
					structDraw	= undefined;
				}
			};
			
			lootAction	= function(){
				var _multi	= structAbility.multi; _multi--;
				var _target	= structState.target;
				if(!instance_exists(_target)) { return 0; }
				
				if(!_target.structState.die)
				{
					with(_target)
					{
						event_user(0);
					}
				}
				if(_multi > 0)
				{
					var _targetObject	= object_get_parent(_target.object_index);
					var _list	= ds_list_create();
					var _number	= instance_place_list(x,y,_targetObject,_list,false);
					if(_number > 0)
					{
						for(var _i=0; _i<_number; ++_i)
						{
							var _anotherTarget	= _list[| _i];
							if(!_anotherTarget.structState.die)
							{
								with(_anotherTarget)
								{
									event_user(0);
									_multi--;
								}
								if(_multi < 1)
								{
									break;
								}
							}
						}
					}
					ds_list_destroy(_list);
				}
				structState.target	= noone;
			};
			break;
		}
	}
};
