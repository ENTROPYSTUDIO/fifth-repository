beforeWindow	= global.windowLayer;
forceInput	= false;
global.windowLayer	= enumWindowLayer.popupQuestion;
myWindowLayer	= enumWindowLayer.popupQuestion;
surface	= noone;
surfaceUpdate	= false;
questionMessage	= "";

callbackCancle	= undefined;
callbackConfirm	= undefined;

blurCreate	= system_shader_create_blur(true,1);

drawMain	= function(){
	static messageX	= 160;
	static messageY	= 80;
	
	static cancleX	= 80;
	static cancleY	= 181;
	static cancle	= method_struct_get_deep_value(global.structGameText,["common","cancle",global.languageCode]);
	
	static confirmX	= 240;
	static confirmY	= 181;
	static confirm	= method_struct_get_deep_value(global.structGameText,["common","confirm",global.languageCode]);
	
	if(!surface_exists(surface))
	{
		surface	= surface_create(sprite_width,sprite_height);
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		surfaceUpdate	= false;
		surface_set_target(surface);
		draw_clear_alpha(c_black,0);
		
		draw_sprite_ext(sprite_index,0,0,0,1,1,0,c_white,1);
	
		preSetForDrawText(fa_center,fa_middle,c_black,1,global.fontMainMedium);
		var _arrMessage	= method_function_string_display_set(questionMessage,280,120,mac_default_font_scale-0.2,global.fontMainMedium);
		var _message	= _arrMessage[0];
		var _messageScale	= _arrMessage[1];
		draw_text_transformed(messageX,messageY,_message,_messageScale,_messageScale,0);
		
		preSetForDrawText(fa_center,fa_middle,c_white,1,global.fontMainBold);
		draw_text_transformed(cancleX,cancleY,cancle,mac_default_font_scale-0.2,mac_default_font_scale-0.2,0);
		draw_text_transformed(confirmX,confirmY,confirm,mac_default_font_scale-0.2,mac_default_font_scale-0.2,0);
		surface_reset_target();
	}
	draw_set_color(c_black);
	draw_set_alpha(0.7);
	draw_rectangle(0,0,display_get_gui_width(),display_get_gui_height(),false);
	draw_set_alpha(1);
		
	draw_surface_ext(surface,x,y,1,1,0,c_white,1);
};

touchEvent	= function(){
	if(method_function_input_mouse_pressed_gui_single(id,myWindowLayer,noone) || (forceInput && myWindowLayer == global.windowLayer))
	{
		if(forceInput)
		{
			forceInput	= false;
			instance_destroy();
			var _callback	= choose(callbackCancle,callbackConfirm);
			if(!is_undefined(_callback))
			{
				_callback();
			}
		}
		
		var _guiMouseX	= device_mouse_x_to_gui(0);
		var _guiMouseY	= device_mouse_y_to_gui(0);
		cancleCheck(_guiMouseX,_guiMouseY);
		confirmCheck(_guiMouseX,_guiMouseY);
	}
};

cancleCheck	= function(_guiMouseX,_guiMouseY){
	static _left	= bbox_left;
	static _top		= bbox_top+160;
	static _right	= bbox_left+sprite_width/2-1;
	static _bottom	= bbox_top+200;
	if(point_in_rectangle(_guiMouseX,_guiMouseY,_left,_top,_right,_bottom))
	{
		instance_destroy();
		if(!is_undefined(callbackCancle))
		{
			callbackCancle();
		}
	}
};

confirmCheck	= function(_guiMouseX,_guiMouseY){
	static _left	= bbox_left+sprite_width/2;
	static _top		= bbox_top+160;
	static _right	= bbox_right;
	static _bottom	= bbox_top+200;
	if(point_in_rectangle(_guiMouseX,_guiMouseY,_left,_top,_right,_bottom))
	{
		instance_destroy();
		if(!is_undefined(callbackConfirm))
		{
			callbackConfirm();
		}
	}
};

cleanUp	= function(){
	if(blurCreate)
	{
		system_shader_create_blur(false,1);
	}
	if(surface_exists(surface))
	{
		surface_free(surface);
	}
	global.windowLayer	= beforeWindow;
};