enum enumParticleBurst
{
	hitEffect,
	criticalEffect
}

global.part_system = part_system_create_layer("particle_hit_effect",false);

switch(room)
{
	case ROOM_STAGE : {
		
		arrayPartTypeIndex	= [];
		
		initFunction	= function(){
			array_insert(arrayPartTypeIndex,0,system_particle_hit_effect());
			array_insert(arrayPartTypeIndex,0,system_particle_critical_effect());
		};
		
		makerFunction	= function(_type,_x,_y){
			switch(_type)
			{
				case enumParticleBurst.hitEffect:{
					var _emitter = part_emitter_create(global.part_system);
					part_emitter_region(global.part_system, _emitter, _x, _x,_y,_y, ps_shape_rectangle, ps_distr_gaussian);
					part_emitter_burst(global.part_system, _emitter, global.part_type_hitEffect,20);
					break;
				}
				case enumParticleBurst.criticalEffect:{
					var _emitter = part_emitter_create(global.part_system);
					part_emitter_region(global.part_system, _emitter, _x,_x,_y,_y, ps_shape_ellipse, ps_distr_gaussian);
					part_emitter_burst(global.part_system, _emitter, global.part_type_criticalEffect,1);
					break;
				}
			}
		};
		
		clearFunction	= function(){
			for(var _i=0; _i<array_length(arrayPartTypeIndex); ++_i)
			{
				var _typeIndex	= arrayPartTypeIndex[_i];
				if(part_type_exists(_typeIndex))
				{
					part_type_destroy(_typeIndex);
				}
			}
			part_system_destroy(global.part_system);
		};
		break;
	}
}

initFunction();
