myWindowLayer	= enumWindowLayer.idle;

animationChange	= function(){
	var _state	= global.gridCharInventory[# enumGridCharacterInventory.state,global.inventorySelectedIndex];
	if(_state == "idle")
	{
		skeleton_animation_set("DEFAULT_IDLE");
	}
	else if(_state == "dead")
	{
		skeleton_attachment_set("WEAPON",-1);
		skeleton_animation_set("TIRED");
	}
};