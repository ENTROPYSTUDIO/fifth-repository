myWindowLayer	= enumWindowLayer.idle;
forceInput	= false;
touchEvent	= function() {
	if(global.windowLayer == myWindowLayer)
	{
		if(instance_exists(obj_rm_character_inventory_manager) && global.inventorySelectedIndex != itemIndex)
		{
			method_show_debug(["touch item","start"]);
			global.inventorySelectedIndex	= itemIndex;
			with(obj_rm_character_inventory_info)
			{
				instance_destroy();
			}
			with(instance_create_layer(0,0,"item_infomation",obj_rm_character_inventory_info))
			{
				event_user(0);
			}
			with(obj_rm_character_inventory_manager)
			{
				surfaceUpdate	= true;
			}
			method_show_debug(["touch item","end"]);
		}
	}
}

