forceInput	= false;
surface	= noone;
surfaceUpdate	= false;
myWindowLayer	= enumWindowLayer.idle;

var _roomName	= method_struct_get_deep_value(global.structGameText,["roomName",room_get_name(room),global.languageCode]);
getRoomName	= !is_undefined(_roomName) ? _roomName : "undefined";
switch(room)
{
	case ROOM_CHARACTER_INVENTORY:
	{
		x	= 32;
		y	= 6;
		break;
	}
}

touchEvent	= function(){
	if(method_function_input_mouse_pressed_gui_single(id,myWindowLayer,noone) || (myWindowLayer == global.windowLayer && forceInput))
	{
		forceInput	= false;
		switch(room)
		{
			case ROOM_CHARACTER_INVENTORY:{
				global.beforeRoom	= global.beforeRoom == ROOM_GAME_START ? ROOM_STAGE : global.beforeRoom;
				method_function_room_move(global.beforeRoom,false);
				break;
			}
		}
	}
};

drawRoomName	= function(){
	static xx	= sprite_width+15;
	static yy	= sprite_height/2;
	
	preSetForDrawText(fa_left,fa_middle,c_white,1,global.fontMainBold);
	gpu_set_blendenable(false);
	draw_text_transformed(xx,yy,getRoomName,mac_default_font_scale,mac_default_font_scale,0);
	gpu_set_blendenable(true);
};

drawMain	= function(){
	if(!surface_exists(surface))
	{
		preSetForDrawText(fa_left,fa_middle,c_white,1,global.fontMainBold);
		surface	= surface_create(round(sprite_width+25+string_width(getRoomName)),sprite_height);
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		surfaceUpdate	= false;
		surface_set_target(surface);
		draw_clear_alpha(c_black,0);
		draw_sprite_ext(sprite_index,0,0,0,1,1,0,c_white,1);
		drawRoomName();
		surface_reset_target();
	}
	draw_set_alpha(1);
	draw_surface(surface,x,y);
};