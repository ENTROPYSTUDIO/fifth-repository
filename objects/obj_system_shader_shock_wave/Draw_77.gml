//Do horizontal blur first
if(shaderCompile)
{
	shader_set(shd_shockwave);
	shockTime+=0.015;
	shader_set_uniform_f(shockUniTime, shockTime);
	shader_set_uniform_f(shockUniMousePos,display_get_gui_width()/2,display_get_gui_height()/2);
	shader_set_uniform_f(shockUniResolution,display_get_gui_width(),display_get_gui_height());
	shader_set_uniform_f(shockUniAmplitude, shockAmplitude);
	shader_set_uniform_f(shockUniRefraction, shockRefraction);
	shader_set_uniform_f(shockUniWidth, shockWidth);
	draw_surface_ext(application_surface, 0, 0,window_get_width()/ surface_get_width(application_surface), window_get_height()/ surface_get_height(application_surface),0,c_white,1);
	shader_reset();
}
