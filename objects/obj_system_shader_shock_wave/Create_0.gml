with(obj_system_shader_shock_wave)
{
	if(id != other.id)
	{
		instance_destroy();
	}
}
shockUniTime = shader_get_uniform(shd_shockwave,"time");
shockTime = 0;
shockUniMousePos = shader_get_uniform(shd_shockwave,"mouse_pos");
shockUniResolution = shader_get_uniform(shd_shockwave,"resolution");
shockUniAmplitude = shader_get_uniform(shd_shockwave,"shock_amplitude");
shockAmplitude = 15;
shockUniRefraction = shader_get_uniform(shd_shockwave,"shock_refraction");
shockRefraction = 0.8;
shockUniWidth = shader_get_uniform(shd_shockwave,"shock_width");
shockWidth = 0.1;
shockSpeed	= 0.008;

shaderCompile	= false;
if(shader_is_compiled(shd_shockwave))
{
	shaderCompile	= true;
	application_surface_draw_enable(false);
}
else
{
	instance_destroy();
}
alarm[0]	= 200;



