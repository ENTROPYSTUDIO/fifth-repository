#region apply speed
if(!structState.ready)
{
	structState.vsp += structState.grv;
	var _hsp	= structState.hsp;
	var _vsp	= structState.vsp;
	
	var _nextPosition	= x+_hsp;
	var _half	= abs(sprite_width/2);
	if(_nextPosition <= _half || _nextPosition >= room_width-_half)
	{
		structState.hsp	= 0;
	}
	x	= clamp(_nextPosition,_half,room_width-_half);
	
	var _nextPosition	= y+_vsp;
	if(_nextPosition >= room_height || _nextPosition> structState.finalY)
	{
		structState.vsp	= 0;
		structState.ready	= true;
	}
	
	y	= clamp(_nextPosition,global.roomTop,room_height);
}

#endregion
