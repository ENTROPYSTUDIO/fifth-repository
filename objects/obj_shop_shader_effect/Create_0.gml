var _id	= self.id;
with(obj_shop_shader_effect)
{
	if(self.id != _id)
	{
		instance_destroy();
	}
}
#region shader
shaderCompile	= shader_is_compiled(shd_shockwave);
shockEnabled = false;
//rippleEnabled	= true;
if(shaderCompile)
{
	#region set shockwave
	shockUniTime = shader_get_uniform(shd_shockwave,"time");
	shockTime = 0;
	shockUniMousePos = shader_get_uniform(shd_shockwave,"mouse_pos");
	shockUniResolution = shader_get_uniform(shd_shockwave,"resolution");
	shockUniAmplitude = shader_get_uniform(shd_shockwave,"shock_amplitude");
	shockAmplitude = 15;
	shockUniRefraction = shader_get_uniform(shd_shockwave,"shock_refraction");
	shockRefraction = 0.8;
	shockUniWidth = shader_get_uniform(shd_shockwave,"shock_width");
	shockWidth = 0.1;
	shockSpeed	= 0.008;
	#endregion
}
else
{
	instance_destroy();
}
#endregion
event_user(0);




