if(shaderCompile && application_surface_is_enabled())
{
	shader_set(shd_shockwave);
	shockTime+=0.015;
	shader_set_uniform_f(shockUniTime, shockTime);
	shader_set_uniform_f(shockUniMousePos,display_get_gui_width()/2,display_get_gui_height()/2);
	shader_set_uniform_f(shockUniResolution,display_get_gui_width(),display_get_gui_height());
	shader_set_uniform_f(shockUniAmplitude, shockAmplitude);
	shader_set_uniform_f(shockUniRefraction, shockRefraction);
	shader_set_uniform_f(shockUniWidth, shockWidth);
			
	draw_surface(application_surface,0,0);
	shader_reset();
}
