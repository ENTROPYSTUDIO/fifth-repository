{
  "spriteId": {
    "name": "spr_misc_button_default",
    "path": "sprites/spr_misc_button_default/spr_misc_button_default.yy",
  },
  "solid": false,
  "visible": true,
  "spriteMaskId": null,
  "persistent": false,
  "parentObjectId": {
    "name": "obj_rm_character_parent_info_bt",
    "path": "objects/obj_rm_character_parent_info_bt/obj_rm_character_parent_info_bt.yy",
  },
  "physicsObject": false,
  "physicsSensor": false,
  "physicsShape": 1,
  "physicsGroup": 1,
  "physicsDensity": 0.5,
  "physicsRestitution": 0.1,
  "physicsLinearDamping": 0.1,
  "physicsAngularDamping": 0.1,
  "physicsFriction": 0.2,
  "physicsStartAwake": true,
  "physicsKinematic": false,
  "physicsShapePoints": [],
  "eventList": [
    {"isDnD":false,"eventNum":0,"eventType":0,"collisionObjectId":null,"parent":{"name":"obj_rm_character_info_bt_entry","path":"objects/obj_rm_character_info_bt_entry/obj_rm_character_info_bt_entry.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":8,"collisionObjectId":null,"parent":{"name":"obj_rm_character_info_bt_entry","path":"objects/obj_rm_character_info_bt_entry/obj_rm_character_info_bt_entry.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
    {"isDnD":false,"eventNum":0,"eventType":3,"collisionObjectId":null,"parent":{"name":"obj_rm_character_info_bt_entry","path":"objects/obj_rm_character_info_bt_entry/obj_rm_character_info_bt_entry.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMEvent",},
  ],
  "properties": [],
  "overriddenProperties": [],
  "parent": {
    "name": "INFO",
    "path": "folders/Objects/ROOM/INVENTORY/CHARACTER/INFO.yy",
  },
  "resourceVersion": "1.0",
  "name": "obj_rm_character_info_bt_entry",
  "tags": [],
  "resourceType": "GMObject",
}