surface	= noone;
surfaceUpdate	= false;
myWindowLayer	= enumWindowLayer.idle;
forceInput		= false;

touchEvent	= function() {
	if(method_function_input_mouse_pressed_gui_single(id,myWindowLayer,noone) || (myWindowLayer == global.windowLayer && forceInput))
	{
		forceInput	= false;
		if(!instance_exists(obj_rm_character_team_entry_manager))
		{
			instance_create_layer(0,0,"team_entry_menu",obj_rm_character_team_entry_manager);
		}
	}
};

stepEvent	= function(){
	touchEvent();
};

drawMain	= function() {
	static scale	= mac_default_font_scale-0.2;
	static text		= method_struct_get_deep_value(global.structGameText,["common","entryTeam",global.languageCode]);
	if(btColor)
	{
		var _btColor	= enumColor.skyBlue;
		var _fontColor	= c_white;
	}
	else
	{
		var _btColor	= c_white;
		var _fontColor	= enumColor.btBrown;
	}
	
	draw_sprite_ext(sprite_index,0,x,y,1,1,0,_btColor,1);
	preSetForDrawText(fa_center,fa_middle,_fontColor,1,global.fontMainBold);
	draw_text_transformed(x,y,text,scale,scale,0);
};