#region loginType
enum enumLoginType
{
	googlePlayGame,
	facebook,
	guest
}
#endregion

#region LISTENER_ID
lidSignFacebook	= noone;
lidSignAnon		= noone;
lidPlayGame		= noone;
lidCfinitUser		= noone;

registerProvider	= "Firebase";
firstLogin			= false;
global.authSigninType	= noone;
#endregion

loginStart	= function(_user_info) {
	if(_user_info == "NULL")
	{
		Firebase_Auth_signInAnonymously();
	}
	else
	{
		loginTry();
	}
};

loginTry	= function() {
	var _get_user_info	= Firebase_Auth_getUserInfo();
	method_show_debug(["_get_user_info",_get_user_info]);
	if(_get_user_info != "NULL")
	{
		var _map		= json_parse(_get_user_info);
		var _uid		= _map.Uid;
		//delete _map;
	
		global.firebaseUid	= _uid;
	
		//firebase_analytics_event_login(registerProvider);
		
		var _objStruct	= {
			language:os_get_language(),
			region:os_get_region(),
			buildConfig:os_get_config(),
			registerProvider:registerProvider,
			os:os_type
		};
		var _json	= json_stringify(_objStruct);
		startTime	= current_time;
		
		var _functionData	= new firebase_cloud_function_builder("initializeUserData",{
			language:os_get_language(),
			region:os_get_region(),
			buildConfig:os_get_config(),
			registerProvider:registerProvider,
			os:os_type
		});
		
		firebase_waiting_for_cloud_function_result(id,_functionData,"",cloudFcInitDataDone,cloudFcInitDataFail);
	}
};

cloudFcInitDataDone	= function(_value) {
	var _mapRemote	= json_parse(_value);
	if(is_struct(_mapRemote))
	{
		method_show_debug([string(_mapRemote)]);
		var _accountState	= _mapRemote.accountState;
		firstLogin	= _mapRemote.firstLogin;
		if(_accountState == "ok")
		{
			//global.structUserData	= _mapRemote.userInitData;
			loginSuccess();
		}
		else if(_accountState == "expired")
		{
			var _message	= "account expired";
			show_message_async(_message);
		}
	}
	else
	{
		loginFail();
	}
};
cloudFcInitDataFail	= function(_value) {
	global.authSigninType	= noone;
};

loginSuccess	= function() {
	with(obj_room_game_start_manager)
	{
		gameStartProcess("login");
	}
}

loginFail	= function() {
	global.authSigninType	= noone;
};


loginStart(Firebase_Auth_getUserInfo());