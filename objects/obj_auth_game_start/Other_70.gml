// All Async Events for Authentication
var _type,_event,_value,_exists,_listener_id;
if(!ds_exists(async_load,ds_type_map)) { exit; }
_type		= async_load[? "type"];
if(is_undefined(_type)) { exit; }

if(_type == Firebase_asyncEvent)
{
	_event	= async_load[? "event"];
	_value	= async_load[? "value"];
	_exists	= async_load[? "exists"];
	_listener_id		= async_load[? "Id"];
	switch(_event)
	{
		case "cloudFunctions":
		{
			break;
		}
		
		case "signInFacebook":
		{
			#region SIGN_IN_FACEBOOK
		    if(_value)
			{
				registerProvider	= "Facebook";
		        Firebase_UserMessage("signInFacebook Success");
				loginTry();
				//with(OBJ_AUTH_FACEBOOK)
				//{
				//	method_show_debug(["FIREBASE signInFacebook");
				//	//var _m = ds_map_create();
				//	//_m[? "id"]		= "/me/";
				//	//_m[? "name"]	= "/me/";
				//	//_m[? "picture"] = "/me/";
				//	//fb_graph_request("me?fields=picture.height(150).width(150),name", "GET", _m);
				//	//ds_map_destroy(_m);
				//	//var _m = ds_map_create();
				//	//_m[? "fields"] = "id,name,picture";
				//	//fb_graph_request("me", "GET", _m);
				//	//ds_map_destroy(_m);
				//	//fb_graph_request("me?fields=picture.height(150).width(150),name", "GET", map_graph_request);
				//}
		    }
		    else
			{
				loginFail();
		        Firebase_UserMessage("signInFacebook Failed");
			}
			
			break;
			#endregion
		}
		
		case "signInGooglePlay":
		{
			/*ds_map_destroy(async_load)*/
			#region SIGN_GOOGLE_PLAY
		    if(_listener_id == lidPlayGame && _value)
			{
				registerProvider	= "GooglePlay";
		        Firebase_UserMessage("signInGooglePlay Success");
				loginTry();
		        //event_perform(ev_alarm,0)//nicknameDialog=get_string_async("You Nickname","mNickname")
		    }
		    else
			{
				loginFail();
		        Firebase_UserMessage("signInGooglePlay Failed");
			}
			
			break;
			#endregion
		}
		case "GooglePlayGetAuthCode":
		{
			/*ds_map_destroy(async_load)*/
			#region GOOGLE_GET_AUTH_CODE
			if(Firebase_Auth_getUserInfo() == "NULL")
			{
				if(_value != "Fail")
				{
					lidPlayGame	= Firebase_Auth_signInGooglePlay(_value);
				}
				else
				{
					Firebase_UserMessage("GooglePlayGetAuthCode Failed");
					loginFail();
				}
			}
			
			break;
			#endregion
		}
    
		case Auth_signInAnonymously:
		{
			#region SING_IN_ANON
		    if(_value)
			{
				registerProvider	= "Firebase";
		        Firebase_UserMessage("signInAnonymously SUCCESS");
		        loginTry();
		    }
		    else
			{
		        Firebase_UserMessage("signInAnonymously FAILED");
				loginFail();
			}
			/*ds_map_destroy(async_load)*/
			break;
			#endregion
		}
		case Auth_signInWithEmailAndPassword:
		{
			#region SIGN_EMAIL_AND_PASSWORD
		    if(_value)
			{
		        Firebase_UserMessage("signInWithEmailAndPassword SUCCESS");
				loginTry();
			}
		    else
			{
				loginFail();
		        Firebase_UserMessage("signInWithEmailAndPassword FAILED");
			}
			/*ds_map_destroy(async_load)*/
			break;
			#endregion
		}
    
		case Auth_createUserWithEmailAndPassword:
		{
			/*ds_map_destroy(async_load)*/
			#region Auth_createUserWithEmailAndPassword
		    if(_value)
			{
		        Firebase_UserMessage("createUserWithEmailAndPassword SUCCESS");
		        loginTry();
		    }
		    else
			{
				loginFail();
		        Firebase_UserMessage("createUserWithEmailAndPassword FAILED");
			}
			
			break;
			#endregion
		}
		case "SendSignInLink":
		{
			#region SendSignInLink
		    if(_value)
			{				
				with(OBJ_AUTH_EMAIL_GET_EMAIL)
				{
					instance_destroy();
				}
				if(!instance_exists(OBJ_AUTH_EMAIL_GET_VERIFY_CLOSE))
				{
					var _inst		= instance_create_depth(x,y,-101,OBJ_AUTH_EMAIL_GET_VERIFY_CLOSE);
				}
				
		        Firebase_UserMessage("SendSignInLink Success");
		    }
		    else
			{
				OBJ_DYNAMIC_LINK_CONTROL.deep_link_trace	= false;
				var _message	= "인증 메일 발송에 실패하였습니다.";//hard_coding_text
				//method_function_popup_check_message(_message,0.7,id,global.windowLayer);
				loginFail();
		        Firebase_UserMessage("SendSignInLink Failed");
			}
			/*ds_map_destroy(async_load)*/
			break;
			#endregion
		}
		
		case "FirebaseVerifySignInLink":
		{
			/*ds_map_destroy(async_load)*/
			#region FirebaseVerifySignInLink
		    if(_value)
		    {
				with(OBJ_AUTH_EMAIL_GET_VERIFY_CLOSE)
				{
					instance_destroy();
				}
				loginTry();
		        Firebase_UserMessage("baseVerifySignInLink Success");
		    }
		    else
			{
				with(OBJ_AUTH_EMAIL_GET_VERIFY_CLOSE)
				{
					instance_destroy();
				}
				loginFail();
		        Firebase_UserMessage("baseVerifySignInLink Failed");
			}
			
			break;
			#endregion
		}
		case "signAnonToFacebook":
		{
			/*ds_map_destroy(async_load)*/
		    if(_value)
		    {
		        Firebase_UserMessage("signAnonToFacebook Success");
				loginTry();
		    }
		    else
			{
				loginFail();
		        Firebase_UserMessage("signAnonToFacebook Failed");
			}
			
			break;
		}
		
		case "signAnonToPlayGame":
		{
			/*ds_map_destroy(async_load)*/
		    if(_value)
		    {
		        Firebase_UserMessage("signAnonToPlayGame Success");
				loginTry();
		    }
		    else
			{
				loginFail();
		        Firebase_UserMessage("signAnonToPlayGame Failed");
			}
			
			break;
		}

		case "signAnonToEmail":
		{
		    if(_value)
		    {
				loginTry();
		        Firebase_UserMessage("signAnonToEmail Success");
		    }
		    else
			{
				loginFail();
		        Firebase_UserMessage("signAnonToEmail Failed");
			}
			/*ds_map_destroy(async_load)*/
			break;
		}
		
		case Auth_sendEmailVerification:
		{
			#region Auth_sendEmailVerification
		    if(_value)
			{
		        Firebase_UserMessage("sendEmailVerification SUCCESS");
			}
		    else
			{
		        Firebase_UserMessage("sendEmailVerification FAILED");
			}
			
			/*ds_map_destroy(async_load)*/
			break;
			#endregion
		}
	}
}
else if(_type == "facebook")
{
	_event	= async_load[? "event"];
	//_ref	= async_load[? "ref"];
	_value	= async_load[? "value"];
	_exists	= async_load[? "exists"];
	_listener_id		= async_load[? "Id"];
	switch(_event)
	{
		case "facebook_login_result":
		{
			if(_value == "success")
			{
				var _fb_token	= fb_accesstoken();
				Firebase_Auth_signInFacebook(_fb_token);
			}
			else
			{
				loginFail();
				Firebase_UserMessage("facebook login Failed");
			}
			/*ds_map_destroy(async_load)*/
			break;
		}
		case "fb_graph_request":
		{
			//if(_value == "success")
			//{
			//}
			/*ds_map_destroy(async_load)*/
			break;
		}
	}
}