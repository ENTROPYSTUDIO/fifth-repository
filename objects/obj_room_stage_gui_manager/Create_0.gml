characterSlotCreate	= function(_type) {
	var _names	= variable_struct_get_names(global.structUserData.equip.character);
	static slot	= new constructorCharacterSlotMeta();
	switch(_type)
	{
		case "init" :
		{
			var _startX	= 79;
			var _startY	= 45;
			var _interX	= 100;
			var _size	= variable_struct_names_count(slot);
			for(var _i=0; _i<_size; ++_i)
			{
				var _key	= mac_equip_character_key_prefix+string(_i);
				var _characterUid	= global.structUserData.equip.character[$ _key];
				var _inst	= instance_create_layer(_startX+(_interX*_i),_startY,"character_slot",obj_room_stage_character_slot)

				slot[$ _key][$ "uid"]	= _characterUid;
				slot[$ _key][$ "inst"]	= _inst;
				with(_inst)
				{
					slotName	= _key;
					characterUid	= _characterUid;
					event_user(0);
				}
			}
			
			break;
		}
		case "update" :
		{
			var _size	= variable_struct_names_count(slot);
			for(var _i=0; _i<_size; ++_i)
			{
				var _key	= mac_equip_character_key_prefix+string(_i);
				var _characterUid	= global.structUserData.equip.character[$ _key];
				if(slot[$ _key].uid != _characterUid)
				{
					instance_destroy(slot[$ _key].inst);
					var _inst	= instance_create_layer(_startX+(_interX*_i),_startY,"character_slot",obj_room_stage_character_slot)
					slot[$ _key].inst	= _inst;
					with(_inst)
					{
						slotName	= _key;
						characterUid	= _characterUid;
						event_user(0);
					}
				}
			}
			break;
		}
	}
};

instance_create_layer(0,0,"bottom_gui",obj_room_stage_bottom_gui_manager);
instance_create_layer(0,0,"inst_resource",obj_utility_resource_display_control);

characterSlotCreate("init");