enum enumGridAbilityLevelUp {
	instance,
	size
}

#region init
inventoryLeft	= x;
inventoryTop		= y;
inventoryRight	= x+932;
inventoryBottom	= y+351;
surfaceWidth	= 932;
surfaceHeight	= 351;
#endregion

#region constructInventoryGrid
constructInventoryGrid	= function() constructor {
	gridInventory	= noone; 
	arrayMap	= ["attack","healthPointMax","defence","drainHpChance","drainHpRatio","critical","criticalPower","evasion"];

	static gridCharacter	= function() {
		clearInventory(gridInventory);
		var _size	= array_length(arrayMap);
		gridInventory	= ds_grid_create(enumGridAbilityLevelUp.size,_size);
		for(var _i=0; _i<_size; ++_i)
		{
			var _coordi	= coordiSet(_i);
			var _inst	= instance_create_layer(_coordi[0],_coordi[1],"slot_menu_button",obj_menu_character_ability_button);
			var _array	= arrayMap;
			with(_inst)
			{
				statName	= _array[_i];
				itemIndex	= _i;
				event_user(0);
				if(_i == 0)
				{
					touchEvent();
				}
			}
			gridInventory[# enumGridAbilityLevelUp.instance,_i]	= _inst;
		}
	};
	
	static coordiSet	= function(_index) {
		static startX	= obj_menu_character_main_control.x+315;
		static startY	= obj_menu_character_main_control.y+32;
		static interX	= 289;
		static interY	= 161;
		static maxLow	= 2;
		
		var _row	= _index mod maxLow;
		var _column	= _index div maxLow;
		return [startX+(interX*_column),startY+(interY*_row)];
	};
	
	static clearInventory	= function(_gridInventory) {
		if(_gridInventory != noone) 
		{ 
			ds_grid_destroy(_gridInventory);
			with(obj_menu_character_ability_button)
			{
				instance_destroy();
			}
		}
	}
};
#endregion

#region constructDraw
surfaceInventory	= noone;
surfaceUpdate	= false;
drawMain	= function() {
	if(!surface_exists(surfaceInventory))
	{
		surfaceInventory	= surface_create(surfaceWidth,surfaceHeight);
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		surfaceUpdate	= false;
		//var _listCollision	= ds_list_create();
		//collision_rectangle_list(inventoryLeft,inventoryTop,inventoryRight,inventoryBottom,obj_menu_character_ability_button,false,true,_listCollision,false);
			
		surface_set_target(surfaceInventory);
		draw_clear_alpha(c_white,0);
		var _grid	= structInventory.gridInventory;
		var _size	= ds_grid_height(_grid);
		
		for(var _i=0; _i<_size; ++_i)
		{
			var _inst	= _grid[# enumGridAbilityLevelUp.instance,_i];
			//if(ds_list_find_index(_listCollision,_inst) != -1)
			//{
				with(_inst)
				{
					structDraw.drawMain(_inst.id);
				}
			//}
		}
		
		surface_reset_target();
		//ds_list_destroy(_listCollision);
	}
	draw_surface(surfaceInventory,x,y);
}
#endregion
structInventory	= new constructInventoryGrid();
structDrag	= new constructListViewDrag(enumListDragOrientation.typeHorizon,64,0.1,0.06);
structInventory.gridCharacter();
structGradeColor	= new constructGradeColor();