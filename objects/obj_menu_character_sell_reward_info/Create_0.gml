targetGrade	= global.gridCharInventory[# enumGridCharacterInventory.grade,global.inventorySelectedIndex];

rewardAlignment	= function(_targetGrade) constructor{
	variable_struct_set(self,"currency",[]);
	variable_struct_set(self,"currencyScale",[]);
	variable_struct_set(self,"amount",[]);
	var _meta	= global.structGameMeta.sellReward[$ mac_equip_character_key_prefix+_targetGrade];
	var _names	= variable_struct_get_names(_meta);
	var _size	= array_length(_names);
	for(var _i=0; _i<_size; ++_i)
	{
		var _key	= _names[_i];
		var _iconKey	= _key	= _key != "carrot" ? "carrot" : _key;//debug
		var _icon	= asset_get_index("spr_icon_currency_"+_iconKey);
		var _scale	= 48/sprite_get_height(_icon);
		array_push(self.currency,_icon);
		array_push(self.currencyScale,_scale);
		array_push(self.amount,_meta[$ _key]);
	}
};

structReward	= new rewardAlignment(targetGrade);

drawMain	= function(){
	static infoX	= x;
	static infoY	= y;
	static info		= method_struct_get_deep_value(global.structGameText,["common","reward",global.languageCode]);
	
	preSetForDrawText(fa_right,fa_top,c_lime,1,global.fontMainBold);
	draw_text_transformed(infoX,infoY,info,mac_default_font_scale,mac_default_font_scale,0);
	
	
	static startX	= x;
	static startY	= y+100;
	static interY	= 80;
	static iconScale	= 48;
	static textInterX	= 80;
	
	var _arrayCurrency	= structReward.currency;
	var _arrayCurrencyScale		= structReward.currencyScale;
	var _arrayAmount		= structReward.amount;
	var _size	= array_length(_arrayCurrency);
	preSetForDrawText(fa_right,fa_middle,c_white,1,global.fontMainBold);
	for(var _i=0; _i<_size; ++_i)
	{
		var _currency	= _arrayCurrency[_i];
		var _scale	= _arrayCurrencyScale[_i];
		var _amount	= string_number_resource_abbrev(_arrayAmount[_i],3);
		
		draw_sprite_ext(_currency,0,startX,startY+(_i*interY),_scale,_scale,0,c_white,1);
		draw_text_transformed(startX-textInterX,startY+(_i*interY),_amount,mac_default_font_scale,mac_default_font_scale,0);
	}
};

