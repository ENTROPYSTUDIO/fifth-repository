surface	= noone;
surfaceUpdate	= false;
myWindowLayer	= enumWindowLayer.idle;
forceInput		= false;

touchEvent	= function() {
	if(method_function_input_mouse_pressed_gui_single(id,myWindowLayer,noone) || (myWindowLayer == global.windowLayer && forceInput))
	{
		forceInput	= false;
		var _callback	= levelUpCallback;
		with(obj_menu_character_main_control)
		{
			instance_destroy();
		}
		with(instance_create_layer(0,0,"slot_menu",obj_menu_character_main_control))
		{
			callInstance	= other.id;
			characterUid		= global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
			slotName	= "";
			callbackFunc	= _callback;
			event_user(0);
		}
	}
};

stepEvent	= function(){
	touchEvent();
};

levelUpCallback	= function() {
	with(obj_rm_character_inventory_manager)
	{
		structInventory.inventoryUpdateIndividual(global.inventorySelectedIndex);
	}
	with(obj_rm_character_inventory_info)
	{
		surfaceUpdate	= true;
	}
};

drawMain	= function() {
	static scale	= mac_default_font_scale-0.2;
	static text		= method_struct_get_deep_value(global.structGameText,["common","levelUp",global.languageCode]);

	if(btColor)
	{
		var _btColor	= enumColor.skyBlue;
		var _fontColor	= c_white;
	}
	else
	{
		var _btColor	= c_white;
		var _fontColor	= enumColor.btBrown;
	}

	draw_sprite_ext(sprite_index,0,x,y,1,1,0,_btColor,1);
	preSetForDrawText(fa_center,fa_middle,_fontColor,1,global.fontMainBold);
	draw_text_transformed(x,y,text,scale,scale,0);
};