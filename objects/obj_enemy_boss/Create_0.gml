// Inherit the parent event
event_inherited();

structState	= new constructCharacterState(id,"human","enemy",2.5);
structDraw	= new constructCharacterDraw(0,spr_character_enemy_aura);

roomInPointX	= random_range(200,room_width-200);
roomInPointY	= random_range(global.roomTop+200,room_height-100);

#region binding function
attackFunc	= game_play_default_attack;
#endregion


