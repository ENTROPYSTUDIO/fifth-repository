destroy	= false;
myWindowLayer	= enumWindowLayer.idle;
forceInput	= false;
var _text	= method_struct_get_deep_value(global.structGameText,["common","legendManage",global.languageCode]);
drawText	= new drawTextConstructor(_text,x,y,fa_center,fa_top,global.fontMainBold,mac_default_font_scale-0.2,c_white,1);
drawSprite	= new drawSpriteConstructor(sprite_index,0,x,y,1,0,c_white,1);

structHideAnimation	= new constructBottomGuiHideAnimation();

drawMain	= function(){
	static refY	= obj_room_stage_bottom_gui_manager.y;
	drawSprite.draw(x,y-refY);
	gpu_set_blendenable(false);
	drawText.draw(x,bbox_bottom-refY+5);
	gpu_set_blendenable(true);
};

constructClick	= function() constructor{
	drawAlpha	= 0;
	drawScale	= 0;
	drawX		= other.x;
	drawY		= other.y;
	sprite		= other.sprite_index;
	
	pickUpX	= other.x;
	pickUpY	= other.y-10;
	pickUpSprite		= noone;
	static drawClick	= function(){
		if(drawAlpha > 0)
		{
			draw_sprite_ext(sprite,0,drawX,drawY,drawScale,drawScale,0,c_white,drawAlpha);
			if(pickUpSprite != noone)
			{
				draw_sprite_ext(pickUpSprite,0,pickUpX,pickUpY,drawScale-0.2,drawScale-0.2,0,c_white,drawAlpha);
				pickUpY	-= 1;
			}
			drawAlpha	= lerp(drawAlpha,-1,0.03);
			drawScale	= lerp(drawScale,2,0.02);
		}
	};
	static click	= function(){
		resetDrawValue();
		pickUp();
	};
	static pickUp	= function(){
		var _pickUpSprite	= noone;
		var _inst	= instance_find(obj_parent_resource,0);
		if(_inst != noone)
		{
			with(_inst)
			{
				_pickUpSprite	= sprite_index;
				event_user(0);
			}
		}
		pickUpSprite	= _pickUpSprite;
	};
	
	static resetDrawValue	= function(){
		drawAlpha	= 1;
		drawScale	= 1;
		pickUpX	= other.x;
		pickUpY	= other.y-10;
	};
}

structTouchEvent	= new constructClick();

