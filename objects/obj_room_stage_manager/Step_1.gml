if(keyboard_check_pressed(vk_space))
{
	create	= !create;
	if(!create)
	{
		with(obj_system_particle_main_maneger)
		{
			instance_destroy();
		}
	}
	else
	{
		instance_create_depth(0,0,0,obj_system_particle_main_maneger);
	}
}