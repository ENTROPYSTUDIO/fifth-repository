// Inherit the parent event
event_inherited();
create	= true;
startTime	= current_time+(60*5*1000);
alarm[0]	= 1;

global.roomTop	= 395;
global.roomCenterY	= (room_height/2)+(global.roomTop/2);
global.nowStageKey	= global.structUserData.progress.stage.nowStage;

#region characterCreate
var _startX	= 79;
var _startY	= 45;
var _interX	= 100;
var _names	= variable_struct_get_names(global.structUserData.equip.character);
var _size	= array_length(_names);
for(var _i=0; _i<_size; ++_i)
{
	var _key	= mac_equip_character_key_prefix+string(_i);
	var _uid	= global.structUserData.equip.character[$ _key];
	var _slotInst	= instance_create_layer(_startX+(_interX*_i),_startY,"character_slot",obj_room_stage_character_slot)
	with(_slotInst)
	{
		create(_uid,_key);
	}
}
#endregion

#region petCreate
var _names	= variable_struct_get_names(global.structUserData.equip.pet);
var _size	= array_length(_names);
for(var _i=0; _i<_size; ++_i)
{
	var _key	= mac_equip_pet_key_prefix+string(_i);
	var _uid	= global.structUserData.equip.pet[$ _key];
	var _slotInst	= instance_create_layer(random_range(100,room_width-100),random_range(global.roomTop+100,room_height-100),"inst_pet",obj_pet)
	with(_slotInst)
	{
		create(_uid,_key);
	}
}
#endregion

#region instance_create
instance_create_layer(0,0,"manager",obj_room_stage_enemy_manager);
instance_create_layer(0,0,"bottom_gui",obj_room_stage_bottom_gui_manager);
instance_create_layer(0,0,"inst_resource",obj_utility_resource_display_control);
instance_create_depth(0,0,0,obj_system_particle_main_maneger);
#endregion



