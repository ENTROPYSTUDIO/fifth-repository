deadDelay	= current_time+1000;

system = part_system_create_layer("particle_hit_effect",false);
type = part_type_create();
part_type_shape(type, pt_shape_line);
part_type_size(type, 0.15, 0.25, 0, 0);
part_type_scale(type, 1, 2);
part_type_color1(type, 16777215);
part_type_alpha3(type, 0.21, 1, 0.26);
part_type_speed(type, 3, 7, 0, 0.1);
part_type_direction(type, 0, 360, 0, 0);
part_type_gravity(type, 0, 0);
part_type_orientation(type, 0, 360, -5, 0, 0);
part_type_blend(type, 0);
part_type_life(type, 5, 25);
emitter = part_emitter_create(system);
part_emitter_region(system, emitter, x - 1, x + 1, y - 1, y + 1, ps_shape_rectangle, ps_distr_gaussian);
part_emitter_burst(system, emitter, type, 10);