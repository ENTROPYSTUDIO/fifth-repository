myWindowLayer	= enumWindowLayer.teamEntry;
forceInput	= false;

drawMain	= function(){
	static btColor	= c_white;
	static btName	= method_struct_get_deep_value(global.structGameText,["common","close",global.languageCode]);
	static nameScale	= mac_default_font_scale-0.2;
	draw_sprite_ext(sprite_index,0,x,y,1,1,0,btColor,1);
	preSetForDrawText(fa_center,fa_middle,enumColor.skyBlue,1,global.fontMainBold);
	draw_text_transformed(x,y,btName,nameScale,nameScale,0);
};

touchEvent	= function(){
	if(method_function_input_mouse_pressed_gui_single(id,myWindowLayer,noone) || (myWindowLayer == global.windowLayer && forceInput))
	{
		forceInput	= false;
		with(obj_rm_character_team_entry_parent)
		{
			instance_destroy();
		}
	}
};

stepMain	= function(){
	touchEvent();
};