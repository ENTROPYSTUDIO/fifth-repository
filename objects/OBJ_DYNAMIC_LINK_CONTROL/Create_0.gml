///////Create a Short Link
lidCfDeeplinkProcessing	= noone;
lidDynamicLink	= noone;

if(global.debugMode)
{
	firebase_dynamic_links_debug_mode(true); //Enable debug mode to see debug messages.
}

//////Let's begin!
lidDynamicLink	= firebase_dynamic_links_init();

////Let's generate a basic shortLink. Very easy!
//var goToLink = "https://forum.yoyogames.com";
//var myBaseUrl = "https://gmlinktest12.page.link";
//var iosPackage = "com.gmdevblog.firebaseanalyticstest"; //or whatever your iOS package name is
//var androidPackage = "com.gmdevblog.firebaseanalyticstest"; //or com.gmdevblog.firebaseanalyticstest;
//firebase_dynamic_links_generate_basic(goToLink,myBaseUrl,androidPackage,iosPackage); //Short URL will return in Social Async Event