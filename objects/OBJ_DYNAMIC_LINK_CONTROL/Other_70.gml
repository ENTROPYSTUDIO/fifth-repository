// All Async Events for Authentication
var _type,_event,_value,_exists,_listener_id;
if(!ds_exists(async_load,ds_type_map)) { exit; }
_type		= async_load[? "type"];
if(is_undefined(_type)) { exit; }

if(_type == Firebase_asyncEvent)
{
	_event	= async_load[? "event"];
	//_ref	= async_load[? "ref"];
	_value	= async_load[? "value"];
	_exists	= async_load[? "exists"];
	_listener_id		= async_load[? "Id"];
	switch(_event)
	{
		case "cloudFunctions":
		{
			#region remove success object and transaction count
			if(_listener_id == lidCfDeeplinkProcessing)
			{
				if(_exists)
				{

				}
				/*ds_map_destroy(async_load)*/
			}
			#endregion
			break;
		}
		
		case "dynamic_links_deeplink":
		{
			if(_listener_id == lidDynamicLink)
			{
				var _status = async_load[? "status"];
				var _data = async_load[? "data"];
				/*ds_map_destroy(async_load)*/
				
				if (_status == 1)
				{
					var _map	= {
						deepLink:_data
					};
	
					var _json	= json_stringify(_map);
					//delete _map;
					
					lidCfDeeplinkProcessing	= method_firebase_cloud_functions_call(_json,"clientRequestDeepLinkProcessing",true);
					method_show_debug(["dynamic_links_deeplink",_data]);
				}
			}
			break;
		}
	}
}