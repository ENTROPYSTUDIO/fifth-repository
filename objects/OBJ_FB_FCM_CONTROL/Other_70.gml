// All Async Events for Authentication
var _type,_event,_value,_exists,_listener_id;
if(!ds_exists(async_load,ds_type_map)) { exit; }
_type		= async_load[? "type"];
if(is_undefined(_type)) { exit; }

if(_type == Firebase_asyncEvent)
{
	_event	= async_load[? "event"];
	//_ref	= async_load[? "ref"];
	_value	= async_load[? "value"];
	_exists	= async_load[? "exists"];
	_listener_id		= async_load[? "Id"];
	switch(_event)
	{
		case FCM_getToken:
		case FCM_onNewToken:
		{
			var _get_user_info	= Firebase_Auth_getUserInfo();
			if(_get_user_info != "NULL" && _exists)
			{
			    var _map = json_parse(_get_user_info);
			    var _uid = _map.Uid;
			
			    if(!is_undefined(_uid))
			    {
					var _os = "Browser";
					if(os_browser == browser_not_a_browser)
					{
						switch(os_type)
						{
							case os_android:
							{
								_os = "Android";
								break;
							}
							case os_ios:
							{
								_os = "iOS";
								break;
							}
							default:
							{
								_os = "No Browser,iOS or Android";//This dont happen
								break;
							}
						}
					}
			
			        var _ref = method_firebase_ref(["main","users",_uid,"FCM"]);
					method_show_debug(["FCM_getToken",_value]);
					var _map	= {
						token:_value,
						os:_os
					};
					var _json	= json_stringify(_map);
			        Firebase_DataBase_setNode(_ref,_json,false,false,true);
			    }
			}
			break;
		}
    
		case FCM_onMessageReceived:
		{
			method_show_debug(["FCM_onMessageReceived"]);
			if(os_browser == browser_not_a_browser)
			{
				
			}
			
	        break;
		}
            
		case FCM_notification:
		{
			method_show_debug(["FCM_notification"]);
			method_show_debug(["_value",_value]);
		    var _map = json_parse(_value);
			
			if(is_struct(_map))
			{	
				method_show_debug(["fcm data",_map]);
				
				asdf	= 1;
			}
			break;
		}
		case FCM_subscribeToTopic:
		{
			method_show_debug(["FCM_subscribeToTopic"]);
			break;
		}
	}
}


