if(method_function_input_mouse_pressed_gui_single(self.id,myWindowLayer,noone) || (forceInput && myWindowLayer == global.windowLayer))
{
	forceInput	= false;
	with(obj_menu_character_purchase_amount_ability)
	{
		myColor	= id != other.id ? c_gray : c_white;
	}
	with(obj_menu_character_main_control)
	{
		structPurchaseAmount.nowAmount	= other.myAmount;
		surfaceUpdate	= true;
	}
	with(obj_menu_character_ability_button)
	{
		structDraw.updateStruct(characterUid,statName);
		surfaceUpdate	= true;
	}
	with(obj_menu_character_ability_control)
	{
		surfaceUpdate	= true;
	}
}