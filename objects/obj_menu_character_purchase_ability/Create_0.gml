forceInput	= false;
myWindowLayer	= enumWindowLayer.slotMenu;
var _text	= method_struct_get_deep_value(global.structGameText,["common","levelUp",global.languageCode]);
structTextDraw	= new drawTextConstructor(_text,x,y,fa_center,fa_middle,global.fontMainBold,mac_default_font_scale-0.2,c_white,1);
structSpriteDraw	= new drawSpriteConstructor(sprite_index,0,x,y,1,0,enumColor.skyBlue,1);

purchaseAbility		= function() {
	if(!instance_exists(obj_menu_character_main_control)) { return 0; }
	var _callbackDone	= callbackDone;
	var _callbackFail	= callbackFail;
	with(global.selectedUpgradeAbility)
	{
		var _purchaseAmount	= obj_menu_character_main_control.structPurchaseAmount.nowAmount;
		var _maxCheck	= character_upgrade_level(characterUid,statName,"check",_purchaseAmount);
		var _nowResource	= user_resource_get_real("carrot");
		var _price	= character_get_stat_upgrade_price(characterUid,statName,"default",_purchaseAmount);
		if(_nowResource >= _price)
		{
			var _finalResource	= string(_nowResource-_price);
			var _nowLevel	= method_struct_get_deep_value(global.structUserData,["inventory","character",characterUid,statName]);
			
			global.structUserData.resource.carrot	= _finalResource;
			method_struct_increment_deep_value(global.structUserData,["inventory","character",characterUid,statName],_purchaseAmount);
			
			firebase_userdata_pipeline_add("resource/carrot",_finalResource);
			firebase_userdata_pipeline_add("inventory/character/"+characterUid+"/"+statName,_nowLevel);
			
			_callbackDone();
		}
		else
		{
			_callbackFail();
		}
	}
};

callbackDone	= function(){

	if(!instance_exists(obj_menu_character_main_control)) { return 0; }
	with(global.selectedUpgradeAbility)
	{
		character_status_upgrade_update(characterUid,statName);
		
		with(instance_create_depth(x,y,depth-10,obj_menu_character_upgrade_effect))
		{
			motherInst	= other.id;
			drawType	= 1;
		}
			
		with(obj_menu_character_main_control)
		{
			structShockWave.shaderReset();
		}
		if(!ex_camera_is_shaking(MAC_CAMERA_NAME_MAIN) && global.structUserData.option.screenShake)
		{
			//ex_camera_shake(MAC_CAMERA_NAME_MAIN, 4, 4, 4, 2,undefined,undefined,undefined);
		}
		with(obj_menu_character_ability_control)
		{
			surfaceUpdate	= true;
		}
	}
};

callbackFail	= function(){
	var _message	= method_struct_get_deep_value(global.structGameText,["error","notEnoughResource",global.languageCode]);
	popup_message(_message,120);
};