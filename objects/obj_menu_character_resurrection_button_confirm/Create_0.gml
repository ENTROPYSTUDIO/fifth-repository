myWindowLayer	= enumWindowLayer.characterResurrection;
forceInput	= false;

drawMain	= function(){
	static btColor	= enumColor.skyBlue;
	static btName	= method_struct_get_deep_value(global.structGameText,["common","resurrection",global.languageCode]);
	static nameScale	= mac_default_font_scale-0.2;
	draw_sprite_ext(sprite_index,0,x,y,1,1,0,btColor,1);
	preSetForDrawText(fa_center,fa_middle,c_white,1,global.fontMainBold);
	draw_text_transformed(x,y,btName,nameScale,nameScale,0);
};

touchEvent	= function(){
	if(method_function_input_mouse_pressed_gui_single(id,myWindowLayer,noone) || (myWindowLayer == global.windowLayer && forceInput))
	{
		forceInput	= false;
		if(instance_exists(obj_menu_character_resurrection_manager))
		{
			var _key	= obj_menu_character_resurrection_manager.optionAllLegend ? "" : global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
			if(obj_menu_character_resurrection_manager.optionAllLegend)
			{
				callbackAllResurrectionDone();
			}
			else
			{
				var _key	= global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
				var _price	= getResurrectionPrice(_key);
				var _currency	= global.structGameMeta.gameSetting.value.resurrectionCurrency;
				var _nowResource	= user_resource_get_real(_currency);
				if(_nowResource >= _price)
				{
					callbackIndividualResurrectionDone();
				}
				else
				{
					var _message	= method_struct_get_deep_value(global.structGameText,["error","notEnoughResource",global.languageCode]);
					popup_message(_message,120);
				}
			}
		}
	}
};

callbackAllResurrectionDone	= function(){
	var _price	= getResurrectionPrice("");
	var _currency	= global.structGameMeta.gameSetting.value.resurrectionCurrency;
	var _nowResource	= user_resource_get_real(_currency);
	if(_nowResource >= _price)
	{
		var _finalResource	= string(_nowResource-_price);
		global.structUserData.resource[$ _currency]	= _finalResource;
		firebase_userdata_pipeline_add("resource/"+_currency,_finalResource);
		
		var _struct	= global.structUserData.inventory.character;
		var _size	= ds_grid_height(global.gridCharInventory);
		for(var _i=0; _i<_size; ++_i)
		{
			var _key	= global.gridCharInventory[# enumGridCharacterInventory.uid,_i];
			var _state	= _struct[$ _key].state;
			if(_state == "dead")
			{
				firebase_userdata_pipeline_add("inventory/character/"+_key+"/healthWeight",1);
				firebase_userdata_pipeline_add("inventory/character/"+_key+"/state","idle");
				firebase_userdata_pipeline_add("inventory/character/"+_key+"/resurrectionTime",0);
			
				_struct[$ _key].healthWeight	= 1;
				_struct[$ _key].state	= "idle";
				_struct[$ _key].resurrectionTime	= 0;
				with(obj_rm_character_inventory_manager)
				{
					structInventory.inventoryUpdateIndividual(_i);
					surfaceUpdate	= true;
				}
			}
		}
		firebase_userdata_pipeline_upload();
		
		var _message	= method_struct_get_deep_value(global.structGameText,["common","legendResurrectionAll",global.languageCode]);
		popup_message(_message,120);
	
		with(obj_menu_character_resurrection_parent)
		{
			instance_destroy();
		}
	}
	else
	{
		var _message	= method_struct_get_deep_value(global.structGameText,["error","notEnoughResource",global.languageCode]);
		popup_message(_message,120);
	}
};
callbackIndividualResurrectionDone	= function(){
	var _key	= global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
	var _price	= getResurrectionPrice(_key);
	var _currency	= global.structGameMeta.gameSetting.value.resurrectionCurrency;
	var _nowResource	= user_resource_get_real(_currency);
	if(_nowResource >= _price)
	{
		var _finalResource	= string(_nowResource-_price);
		firebase_userdata_pipeline_add("resource/"+_currency,_finalResource);
		
		var _struct	= global.structUserData.inventory.character;
		var _key	= global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
		var _state	= _struct[$ _key].state;
		if(_state == "dead")
		{
			firebase_userdata_pipeline_add("inventory/character/"+_key+"/healthWeight",1);
			firebase_userdata_pipeline_add("inventory/character/"+_key+"/state","idle");
			firebase_userdata_pipeline_add("inventory/character/"+_key+"/resurrectionTime",0);
			_struct[$ _key].healthWeight	= 1;
			_struct[$ _key].state	= "idle";
			_struct[$ _key].resurrectionTime	= 0;
			with(obj_rm_character_inventory_manager)
			{
				structInventory.inventoryUpdateIndividual(global.inventorySelectedIndex);
				surfaceUpdate	= true;
			}
		}
	
		var _message	= method_struct_get_deep_value(global.structGameText,["common","legendResurrection",global.languageCode]);
		popup_message(_message,120);
		with(obj_menu_character_resurrection_parent)
		{
			instance_destroy();
		}
	}
	else
	{
		var _message	= method_struct_get_deep_value(global.structGameText,["error","notEnoughResource",global.languageCode]);
		popup_message(_message,120);
	}
};

stepMain	= function(){
	touchEvent();
};
