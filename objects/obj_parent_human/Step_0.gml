#region init
var _target	= structState.target;
var _targetExists	= false;
#endregion

if(!structState.moveDisable())
{
	#region target reset
	if(_target != noone)
	{
		_targetExists	= true;
		if(!instance_exists(_target) || structState.target.structState.die)
		{
			_target	= noone;
			_targetExists	= false;
			structState.target	= noone;
		}
	}
	#endregion

	#region set speed
	if(_targetExists)
	{
		if(!place_meeting(xprevious,yprevious,_target))
		{
			var _xSign	= sign(x-_target.x);
			var _targetX	= _xSign < 0 ? _target.bbox_left : _target.bbox_right;
			var _dir = point_direction(xprevious,yprevious,_targetX,_target.y);
			structState.hsp = lengthdir_x(structAbility.moveSpeed, _dir);
			structState.vsp = lengthdir_y(structAbility.moveSpeed, _dir);
					
			//var _alpha = _target.structAbility.moveSpeed / structAbility.moveSpeed;
			//var _phi = degtorad(_target.direction - _dir);
			//var _beta = _alpha * sin(_phi);
			//if(abs(_beta) < 1) 
			//{
			//	_dir += radtodeg(arcsin(_beta));
			//	structState.hsp = lengthdir_x(structAbility.moveSpeed, _dir);
			//	structState.vsp = lengthdir_y(structAbility.moveSpeed, _dir);
			//	//motion_set(_dir,structAbility.moveSpeed);
			//}

		}
	}
	#endregion
}

#region attack
var _myId	= id;
if(_target != noone)
{
	if(place_meeting(xprevious,yprevious,_target))
	{
		if(structState.animationNow != "defaultAttack" && current_time > structState.attackTime)
		{
			var _xSign	= _target.x > x ? 1 : -1;
			if(_xSign != 0)
			{
				image_xscale	= structState.imageScale*_xSign;
			}
			
			structState.attackTime	= current_time+structAbility.attackDelay;
			var _animation	= "defaultAttack";
			structState.animationSetPriority(_animation,structState.animationSet,global.structHumanAnimation);
		}
		
		structState.hsp	= 0;
		structState.vsp	= 0;
		
		if(structState.hsp == 0 && structState.vsp == 0 && !structState.die)
		{
			var _gapY	= abs(x-_target.y);
			if(_gapY > 20)
			{
				y	= lerp(y,_target.y,0.05);
			    //var _dir = point_direction(x,y,_target.x,_target.y);
				//structState.vsp = lengthdir_y(structAbility.moveSpeed, _dir);
			}
		
			var _gapX	= x-_target.x;
			if(abs(_gapX) < 10)
			{
				var _sign	= sign(_gapX);
				if(_sign < 0)
				{
				    //var _dir = point_direction(x,y,_target.bbox_left,_target.y);
					//structState.hsp = lengthdir_x(structAbility.moveSpeed, _dir);
					x	= lerp(x,_target.bbox_left,0.05);
				}
				else
				{
				    //var _dir = point_direction(x,y,_target.bbox_right,_target.y);
					//structState.hsp = lengthdir_x(structAbility.moveSpeed, _dir);
					x	= lerp(x,_target.bbox_right,0.05);
				}
			}
		}
	}
}
#endregion



