#region force animation
if(structState.forceAnimation != "none")
{
	structState.animationSet	= structState.forceAnimation;
	structState.forceAnimation	= "none";
}
#endregion

#region animation
var _setAnimation	= structState.animationSet;
var _nowAnimation	= structState.animationNow;
if(_setAnimation != _nowAnimation)
{
	var _structSetAnimation	= variable_struct_get(global.structHumanAnimation,_setAnimation);
	var _setPriority	= _structSetAnimation.priority;
	var _nowPriority	= variable_struct_get(global.structHumanAnimation,_nowAnimation).priority;
	if(_setPriority >= _nowPriority)
	{
		//var _arrayAnimation	= ;
		//var _ranIndex	= irandom(array_length(_arrayAnimation)-1);
		//var _animation	= _arrayAnimation[_ranIndex];
		skeleton_animation_set(_structSetAnimation.animation());
		structState.animationNow	= structState.animationSet;
		image_speed	= method_skeleton_animation_speed(_structSetAnimation.imageSpeed);
		structState.standing	= _structSetAnimation.standing;
		structState.orientation	= _structSetAnimation.orientation;
	}
}
structState.animationSet	= "none";
#endregion


