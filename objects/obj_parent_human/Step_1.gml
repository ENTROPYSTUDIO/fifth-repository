#region depth and orientation
depth	= round(-y);
#endregion

structDraw.auraScale();
structState.healthRatioUpdate(structAbility);
structState.actionDisable();

if(structState.attack)
{
	structState.attack	= false;
	var _multipleTarget	= structAbility.multipleTarget;
	var _target	= structState.target;
	var _targetExists	= attackFunc(_target);
	if(_targetExists && --_multipleTarget > 0)
	{
		var _targetObject	= object_get_parent(_target.object_index);
		var _list	= ds_list_create();
		var _number	= instance_place_list(x,y,_targetObject,_list,false);
		if(_number > 0)
		{
			var _attacker	= id;
			for(var _i=0; _i<_number; ++_i)
			{
				var _target	= _list[| _i];
				with(_target)
				{
					if(!structState.die && structState.roomIn)
					{
						other.attackFunc(id);
						--_multipleTarget;
					}
				}
				if(_multipleTarget < 1)
				{
					break;
				}
			}
		}
		ds_list_destroy(_list);
	}
}






