myWindowLayer	= enumWindowLayer.idle;
slotName	= "";
structStatus	= noone;
structAbility	= noone;
myCharacterInst		= noone;
surfaceUpdate	= false;
destroy	= false;
forceInput	= false;
existsCharacter	= false;

create	= function(_uid,_slotName){
	switch(_uid)
	{
		case "lock" :
		{
			instance_destroy();
			break;
		}
		case "unlocking" :
		{
			instance_destroy();
			break;
		}
		case "empty" :
		{
			instance_destroy();
			break;
		}
		default :
		{
			slotName		= _slotName;
			characterUid	= _uid;
			existsCharacter	= true;
			structStatus	= global.structUserData.inventory.character[$ characterUid];
			structAbility	= new constructPlayerAbility(characterUid);
		
			var _grade	= character_get_static_ability(characterUid,"grade","string");
			var _index	= character_get_static_ability(characterUid,"index","string");
			var _spineSprite	= asset_get_index("SPR_CHARACTER_GRADE_"+_grade+"_"+_index);
			var _starIcon	= asset_get_index("spr_character_star_grade_"+_grade);
			var _heroKey	= structStatus.key;
			var _heroName	= method_struct_get_deep_value(global.structGameText,["heroName",_heroKey,global.languageCode]);
		
			#region avatar set
			var _frameSub	= real(_grade);
			var _avatarSub	= real(_index);
			var _sprAvatar	= asset_get_index("SPR_CHARACTER_HEAD_GRADE_"+_grade);

			structDraw	= new constructStageDrawAvatar(_frameSub,_sprAvatar,_avatarSub,0.5,10,_starIcon,c_white,c_white,1);
			#endregion
		
			#region player create
			{
				with(instance_create_depth(room_width/2,global.roomCenterY,0,obj_player))
				{
					structAbility	= other.structAbility;
					sprite_index	= _spineSprite;
					motherInst	= other.id;
					other.myCharacterInst	= id;
				
					structDraw.starIcon	= _starIcon;
					structDraw.heroName	= _heroName;
					structDraw.avatarSprite	= _sprAvatar;
					structDraw.avatarSub	= _avatarSub;
				}
			}
			#endregion
		
			#region player create
		
			#endregion
			break;
		}
	}
};