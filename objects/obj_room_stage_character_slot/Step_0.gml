if(method_function_input_mouse_pressed_gui_single(id,myWindowLayer,noone) || (forceInput && myWindowLayer == global.windowLayer))
{
	forceInput	= false;
	var _create	= false;
	if(instance_exists(obj_menu_character_main_control))
	{
		_create	= obj_menu_character_main_control.callInstance != id;
		if(_create)
		{
			with(obj_menu_character_main_control)
			{
				event_user(1);
			}
		}
	}
	else
	{
		_create	= true;
	}
	if(_create)
	{
		with(instance_create_layer(0,0,"slot_menu",obj_menu_character_main_control))
		{
			callInstance	= other.id;
			characterUid		= other.characterUid;
			event_user(0);
		}

		with(obj_room_stage_character_slot)
		{
			if(other.id != id)
			{
				structDraw.surfaceColor	= c_gray;
			}
			else
			{
				structDraw.surfaceColor	= c_white;
			}
			surfaceUpdate	= true;
		}
	}
}
y	= lerpGoalY(y,structDraw.lerpGoalY,0.05);
