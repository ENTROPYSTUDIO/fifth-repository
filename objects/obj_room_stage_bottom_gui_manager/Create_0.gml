x	= 0;
y	= 570;
surface	= noone;
surfaceUpdate	= false;

drawMain	= function(){
	if(!surface_exists(surface))
	{
		surface	= surface_create(display_get_gui_width(),display_get_gui_height()-y);
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		surfaceUpdate	= false;
		surface_set_target(surface);
		draw_clear_alpha(c_black,0);
		
		with(obj_room_stage_bottom_parent)
		{
			drawMain();
		}
		
		surface_reset_target();
	}
	//gpu_set_blendenable(false);
	draw_set_alpha(1);
	draw_surface(surface,0,y);
	//gpu_set_blendenable(true);
};

cleanUp	= function(){
	if(surface_exists(surface))
	{
		surface_free(surface);
	}
};
instance_create_layer(100,640,"bottom_gui",obj_room_stage_bottom_gui_character);
instance_create_layer(640,633,"bottom_clicker",obj_room_stage_bottom_gui_clicker);
