myWindowLayer	= enumWindowLayer.idle;
forceInput	= false;
surface	= noone;
surfaceUpdate	= false;

touchEffect	= 0;

drawFilterName	= function() {
	preSetForDrawText(fa_left,fa_middle,enumColor.navy,1,global.fontMainMedium);
	var _scale	= mac_default_font_scale-0.3;
	var _key	= "grade";
	switch(global.inventoryNowFilter)
	{
		case enumGridCharacterInventory.equip : { var _key	= "equip"; break; }
		case enumGridCharacterInventory.grade : { var _key	= "grade"; break; }
		case enumGridCharacterInventory.attack : { var _key	= "attack"; break; }
		case enumGridCharacterInventory.defence : { var _key	= "defence"; break; }
		case enumGridCharacterInventory.critical : { var _key	= "critical"; break; }
		case enumGridCharacterInventory.criticalPower : { var _key	= "criticalPower"; break; }
		case enumGridCharacterInventory.evasion : { var _key	= "evasion"; break; }
		case enumGridCharacterInventory.healthPointMax : { var _key	= "healthPointMax"; break; }
		case enumGridCharacterInventory.drainHpChance : { var _key	= "drainHpChance"; break; }
		case enumGridCharacterInventory.drainHpRatio : { var _key	= "drainHpRatio"; break; }
		case enumGridCharacterInventory.healthWeight : { var _key	= "healthWeight"; break; }
		case enumGridCharacterInventory.state : { var _key	= "state"; break; }
	}
	var _text	= method_struct_get_deep_value(global.structGameText,["characterAbility",_key,global.languageCode]);
	draw_text_transformed(10,surface_get_height(surface)/2,_text,_scale,_scale,0);
};

drawMain	= function() {
	if(!surface_exists(surface))
	{
		surface	= surface_create(sprite_width,sprite_height);
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		surfaceUpdate	= false;
		surface_set_target(surface);
		draw_clear_alpha(c_black,0);
		draw_set_alpha(1);
		draw_sprite(sprite_index,0,0,0);
		drawFilterName();
		surface_reset_target();
	}
	draw_set_alpha(1);
	if(touchEffect > 0)
	{
		gpu_set_blendmode(bm_add);
	}
	draw_surface(surface,x,y);
	gpu_set_blendmode(bm_normal);
	if(touchEffect > 0)
	{
		touchEffect--;
	}
};

touchEvent	= function() {
	if((method_function_input_mouse_pressed_gui_single(id,myWindowLayer,noone)) || (global.windowLayer == myWindowLayer && forceInput))
	{
		forceInput	= false;
		global.inventoryNowFilter++;
		if(global.inventoryNowFilter > enumGridCharacterInventory.state)
		{
			global.inventoryNowFilter	= enumGridCharacterInventory.equip;
		}
		var _filter	= global.inventoryNowFilter;
		with(obj_rm_character_inventory_manager)
		{
			structInventory.inventoryFiltering(_filter);
		}
		surfaceUpdate	= true;
		touchEffect	= 5;
	}
}