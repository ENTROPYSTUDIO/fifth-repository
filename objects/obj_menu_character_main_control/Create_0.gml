global.selectedUpgradeAbility	= noone;
with(obj_room_stage_bottom_parent)
{
	destroy	= true;
}
with(obj_room_stage_character_slot)
{
	structDraw.surfaceScaleChange(false);
}
global.windowLayer	= enumWindowLayer.slotMenu;
x	= 39;
y	= 100;

destroy			= false;
structStatus	= noone;
surfaceMain		= noone;
spineDummy		= noone;
upgradeControl	= noone;
surfaceUpdate	= false;
callbackFunc	= undefined;
system_shader_create_blur(true,1);
