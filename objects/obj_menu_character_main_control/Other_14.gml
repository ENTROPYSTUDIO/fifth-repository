#region leftLockDraw
leftLockDraw	= function() {
	static lockX	= x+134;
	static lockY	= y+240;
	static lockScale	= 1;
		
	draw_sprite_ext(spr_icon_unlock,0,lockX,lockY,lockScale,lockScale,0,c_white,1);
		
	static textX	= x+134;
	static textY	= y+315;
	static text		= method_struct_get_deep_value(global.structGameText,["common","open",global.languageCode]);
	static textScale	= mac_default_font_scale;
	
	draw_set_color(c_white);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	draw_set_font(global.fontMainBold);
	draw_text_transformed(textX,textY,text,textScale,textScale,0);
};
#endregion

#region btDraw
btDraw	= function() {
	with(obj_menu_character_button_parent)
	{
		structSpriteDraw.draw();
		structTextDraw.draw();
	}
};
#endregion

#region releaseInfoDraw
releaseInfoDraw	= function() {
	static iconX	= x+735;
	static iconY	= y+140;
	static iconScale	= 1;
	draw_sprite_ext(spr_icon_character_dummy,0,iconX,iconY,iconScale,iconScale,0,c_white,1);
		
	var _infoX	= x+735;
	var _infoY	= y+330;
	var _arrayText	= method_function_string_display_set(method_struct_get_deep_value(global.structGameText,["common","characterEmptySlotInfo",global.languageCode]),800,100,mac_default_font_scale-0.2,global.fontMainMedium);
	var _info	= _arrayText[0];
	var _infoScale	= _arrayText[1];
	draw_set_color($c9c9c9);
	draw_set_halign(fa_center);
	draw_set_valign(fa_bottom);
	draw_set_font(global.fontMainMedium);
	draw_text_transformed(_infoX,_infoY,_info,_infoScale,_infoScale,0);
};
#endregion

#region drawMain
drawMain = function() {
	if(!surface_exists(surfaceMain))
	{
		surfaceMain	= surface_create(display_get_gui_width(),display_get_gui_height());
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		surfaceUpdate	= false;
		surface_set_target(surfaceMain);
		draw_clear_alpha(c_black,0.5);
		draw_set_alpha(1);
		draw_sprite(other.sprite_index,0,x,y);
		shader_premultiply(true,undefined,undefined);
		leftLockDraw();
		releaseInfoDraw();
		btDraw();
		shader_premultiply(false,undefined,undefined);
		surface_reset_target();
		draw_set_color(c_white);
	}
	draw_set_alpha(1);
	draw_surface(surfaceMain,0,0);
};
#endregion

#region stepMain
stepMain	= undefined;
#endregion

#region clean up function
cleanUp	= function() {
	global.windowLayer	= enumWindowLayer.idle;
	with(obj_room_stage_bottom_parent)
	{
		destroy	= false;
	}
	with(obj_room_stage_character_slot)
	{
		structDraw.lerpGoalY	= ystart;
	}
	with(obj_menu_character_button_slot)
	{
		instance_destroy();
	}
	instance_destroy();
}
#endregion

#region instance
instance_create_layer(x+644,y+424,"slot_menu_button",obj_menu_character_button_done);
instance_create_layer(x+824,y+424,"slot_menu_button",obj_menu_character_emtpy_slot_goto_team_menu);
#endregion