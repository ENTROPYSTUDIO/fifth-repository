system_shader_create_blur(false,undefined);
if(surface_exists(surfaceMain))
{
	surface_free(surfaceMain);
}
with(obj_room_stage_character_slot)
{
	structDraw.surfaceColor	= c_white;
	structDraw.surfaceScaleChange(true);
}
with(obj_menu_character_parent)
{
	instance_destroy();
}
if(!is_undefined(callbackFunc))
{
	callbackFunc();
}