#region init
instanceUnlockGetPrice	= function() {
	var _goalTime	= method_struct_get_deep_value(global.structUserData,["timestamp","equip","character",slotName]);
	if(!is_undefined(_goalTime))
	{
		var _nowTime	= global.serverTimestamp;
		var _finalTime	= (_goalTime-_nowTime)/1000/60;
		var _priceBase	= method_struct_get_deep_value(global.structGameMeta,["unlock",slotName,"unlockInstantPrice"]);
		var _price		= max(ceil(_priceBase*_finalTime),0);
	
		return _price;
	}
	else
	{
		return 0;
	}
}
#endregion

#region leftLockDraw
leftLockDraw	= function() {
	static lockX	= x+134;
	static lockY	= y+240;
	static lockScale	= 1;
	static loadingRot	= 0;
	static rotSwitch = 1;
	if(loadingRot > 359 || loadingRot < 0) { rotSwitch *= -1; }
	loadingRot-=5;
	draw_sprite_ext(spr_icon_loading_circle,0,lockX,lockY,lockScale,lockScale,loadingRot,c_white,1);
		
	static textX	= x+134;
	static textY	= y+315;
	static arrayText	= method_function_string_display_set(method_struct_get_deep_value(global.structGameText,["common","unlocking",global.languageCode]),240,200,mac_default_font_scale,global.fontMainBold);
	static text		= arrayText[0];
	static textScale	= arrayText[1];
	draw_set_color(c_white);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	draw_set_font(global.fontMainBold);
	draw_text_transformed(textX,textY,text,textScale,textScale,0);
};
#endregion

#region bottomPriceDraw
bottomPriceDraw	= function() {		
	var _price	= instanceUnlockGetPrice();
	_price	= string_number_format_thousand(_price,0,".",",");
	static priceX		= x+750;
	static priceY		= y+380;
	static priceScale	= mac_default_font_scale;
	draw_set_color(c_lime);
	draw_set_halign(fa_left);
	draw_set_valign(fa_middle);
	draw_set_font(global.fontMainBold);
	draw_text_transformed(priceX,priceY,_price,priceScale,priceScale,0);
	
	var _currencyX	= x+770+(string_width(string(_price))*mac_default_font_scale);
	static currencyY	= y+380;
	static currencyScale	= 1;
	
	var _currency	= method_struct_get_deep_value(global.structGameMeta,["unlock",slotName,"unlockInstantCurrency"]);
	var _sprite	= asset_get_index("spr_icon_currency_"+_currency);
	draw_sprite_ext(_sprite,0,_currencyX,currencyY,currencyScale,currencyScale,0,c_white,1);
		
	var _priceNameX	= x+720;
	static priceNameY	= y+380;
	static priceName		= method_struct_get_deep_value(global.structGameText,["common","instantUnlock",global.languageCode]);
	static priceNameScale	= mac_default_font_scale-0.2;
	draw_set_color(enumColor.goldBrown);
	draw_set_halign(fa_right);
	draw_set_valign(fa_middle);
	draw_set_font(global.fontMainMedium);
	draw_text_transformed(_priceNameX,priceNameY,priceName,priceNameScale,priceNameScale,0);
	
	static unlockX	= x+735;
	static unlockY	= y+330;
	static arrayText	= method_function_string_display_set(method_struct_get_deep_value(global.structGameText,["common","instantUnlockInfo",global.languageCode]),800,100,mac_default_font_scale-0.2,global.fontMainMedium);
	static unlock	= arrayText[0];
	static unlockScale	= arrayText[1];
	draw_set_color($c9c9c9);
	draw_set_halign(fa_center);
	draw_set_valign(fa_bottom);
	draw_set_font(global.fontMainMedium);
	draw_text_transformed(unlockX,unlockY,unlock,unlockScale,unlockScale,0);
};
#endregion

#region btDraw
btDraw	= function() {
	with(obj_menu_character_button_parent)
	{
		draw_set_alpha(1);
		structSpriteDraw.draw();
		structTextDraw.draw();
	}
};
#endregion

#region releaseInfoDraw
releaseTime		= "";
releaseInfoDraw	= function() {
	static timerIconX	= x+735;
	static timerIconY	= y+100;
	static timerIconScale	= 1;
	draw_sprite_ext(spr_icon_timer,0,timerIconX,timerIconY,timerIconScale,timerIconScale,0,c_white,1);
		
	static releaseInfoX	= x+735;
	static releaseInfoY	= y+160;
	static releaseInfo		= method_struct_get_deep_value(global.structGameText,["common","unlockRemain",global.languageCode]);
	static releaseInfoScale	= mac_default_font_scale;
	draw_set_color(c_white);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	draw_set_font(global.fontMainBold);
	draw_text_transformed(releaseInfoX,releaseInfoY,releaseInfo,releaseInfoScale,releaseInfoScale,0);
		
	static releaseTimeX	= x+735;
	static releaseTimeY	= y+195;
	
	static releaseTimeScale	= mac_default_font_scale;
	draw_set_color(c_orange);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	draw_set_font(global.fontMainBold);
	draw_text_transformed(releaseTimeX,releaseTimeY,releaseTime,releaseTimeScale,releaseTimeScale,0);
};
#endregion

#region drawMain
drawMain = function() {
	if(!surface_exists(surfaceMain))
	{
		surfaceMain	= surface_create(display_get_gui_width(),display_get_gui_height());
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		surfaceUpdate	= false;
		surface_set_target(surfaceMain);
		draw_clear_alpha(c_black,0.5);
		draw_set_alpha(1);
		draw_sprite(other.sprite_index,0,x,y);
		shader_premultiply(true,undefined,undefined);
		leftLockDraw();
		bottomPriceDraw();
		btDraw();
		releaseInfoDraw();
		shader_premultiply(false,undefined,undefined);
		surface_reset_target();
		draw_set_color(c_white);
	}
	draw_set_alpha(1);
	draw_surface(surfaceMain,0,0);
};
#endregion

#region stepMain
stepMain	= function() {
	static timeToEmptyDelay	= 2000;
	static timeToEmptyTime	= 0;
	var _goalTime	= method_struct_get_deep_value(global.structUserData,["timestamp","equip","character",slotName]);
	if(!is_undefined(_goalTime))
	{
		releaseTime	= method_function_time_draw_timestamp_to_hms(_goalTime-global.serverTimestamp);
		surfaceUpdate	= true;
	
		if(global.serverTimestamp > _goalTime+timeToEmptyDelay)
		{
			if(current_time > timeToEmptyTime)
			{
				timeToEmptyTime	= current_time+timeToEmptyDelay;
				with(obj_menu_character_purchase_instant_unlock)
				{
					var _functionData	= new firebase_cloud_function_builder("unlockCharacterSlot",{
						slotKey	: slotName,
						slotType : "timeToEmpty"
					});
					
					firebase_waiting_for_cloud_function_result(id,_functionData,"",purchaseDone,purchaseFail);
				}
				if(os_get_config() == "pc")
				{
					with(obj_menu_character_purchase_instant_unlock)
					{
						purchase();
					}
				}
			}
		}
	}
	var _characterUid	= global.structUserData.equip.character[$ slotName];
	if(characterUid != _characterUid)
	{
		with(callInstance)
		{
			slotManageCreate();
		}
	}
}
#endregion

#region instance
instance_create_layer(x+644,y+444,"slot_menu_button",obj_menu_character_button_done);
instance_create_layer(x+824,y+444,"slot_menu_button",obj_menu_character_purchase_instant_unlock);
#endregion

#region clean up function
cleanUp	= function() {
	global.windowLayer	= enumWindowLayer.idle;
	with(obj_room_stage_bottom_parent)
	{
		destroy	= false;
	}
	with(obj_room_stage_character_slot)
	{
		structDraw.lerpGoalY	= ystart;
	}
	with(obj_menu_character_button_slot)
	{
		instance_destroy();
	}
	instance_destroy();
}
#endregion