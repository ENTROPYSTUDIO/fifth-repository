#region draw struct
structDraw	= {
	frameX	: x,
	frameY	: y,
	
	spineX	: x+133,
	spineY	: y+135,
	
	starX : x+133,
	starY : y+225,
	starIcon : noone,
	
	nameX : x+133,
	nameY : y+32,
	nameH : fa_center,
	nameV : fa_middle,
	nameText : "abc",
	nameFont : global.fontMainBold,
	nameColor : c_white,
	nameScale	: mac_default_font_scale,
	abilityInfoX : x+133,
	abilityInfoY : y+290,
	abilityInfoH : fa_center,
	abilityInfoV : fa_middle,
	abilityInfoText : method_struct_get_deep_value(global.structGameText,["characterAbility","defaultAbility","ko"]),
	abilityInfoFont : global.fontMainBold,
	abilityInfoColor : $c9c9c9,
	abilityInfoScale : mac_default_font_scale,
	
	abilityNameX : x+20,
	abilityNameY : y+320,
	abilityNameInterY : 30,
	abilityNameH : fa_left,
	abilityNameV : fa_top,
	abilityNameFont : global.fontMainBold,
	abilityNameColor : enumColor.goldBrown,
	abilityNameScale : mac_default_font_scale-0.1,
	
	abilityValueX : x+246,
	abilityValueY : y+320,
	abilityValueInterY : 30,
	abilityValueH : fa_right,
	abilityValueV : fa_top,
	abilityValueFont : global.fontMainBold,
	abilityValueColor : make_color_rgb(0,230,0),
	abilityValueScale : mac_default_font_scale-0.1,
	
	statNameX : x+316,
	statNameY : y+370,
	statNameInterX : 260,
	statNameInterY : 23,
	statNameH : fa_left,
	statNameV : fa_top,
	statNameFont : global.fontMainMedium,
	statNameColor : enumColor.goldBrown,
	statNameScale : mac_default_font_scale-0.2,
	
	statValueX : x+490,
	statValueY : y+370,
	statValueInterX : 260,
	statValueInterY : 23,
	statValueH : fa_right,
	statValueV : fa_top,
	statValueFont : global.fontMainMedium,
	statValueColor : $c9c9c9,
	statValueScale : mac_default_font_scale-0.2,
	
	pAmountX : 0,
	pAmountY : 0,
	pAmountText	: method_struct_get_deep_value(global.structGameText,["common","purchaseAmount","ko"]),
	pAmountFont : global.fontMainMedium,
	pAmountColor : $c9c9c9,
	pAmountH : fa_right,
	pAmountV : fa_center,
	pAmountScale : mac_default_font_scale-0.2,
};
#endregion

#region struct amount
structPurchaseAmount	= {
	nowAmount	: 1,
	arrayPurchaseAmount : [99,50,10,1],
	startX : 1156,
	startY : 385,
	interX : 50
};
#endregion

#region struct character info
structStatus	= method_struct_get_deep_value(global.structUserData,["inventory","character",characterUid]);
var _grade	= character_get_static_ability(characterUid,"grade","string");
var _index	= character_get_static_ability(characterUid,"index","string");
var _heroKey	= structStatus.key;
structDraw.nameText	= method_struct_get_deep_value(global.structGameText,["heroName",_heroKey,global.languageCode]);

#region status draw set
structDraw.statProperty = ["attack","healthPointMax","defence","drainHpChance","drainHpRatio","critical","criticalPower","evasion"];
structDraw.statName	= {};
structDraw.statValue	= {};
for(var _i=0; _i<array_length(structDraw.statProperty); ++_i)
{
	var _property	= structDraw.statProperty[_i];
	variable_struct_set(structDraw.statName,_property,method_struct_get_deep_value(global.structGameText,["characterAbility",_property,global.languageCode]));
	variable_struct_set(structDraw.statValue,_property,character_get_mutable_ability(characterUid,_property,"display"));
}
		
structDraw.abilityProperty = ["attackSpeed","moveSpeed","multipleTarget","aggroRange","luck"];
structDraw.abilityName	= {};
structDraw.abbiltyValue	= {};		
for(var _i=0; _i<array_length(structDraw.abilityProperty); ++_i)
{
	var _property	= structDraw.abilityProperty[_i];
	variable_struct_set(structDraw.abilityName,_property,method_struct_get_deep_value(global.structGameText,["characterAbility",_property,global.languageCode]));
	variable_struct_set(structDraw.abbiltyValue,_property,character_get_static_ability(characterUid,_property,"display"));
}
structDraw.starIcon	= asset_get_index("spr_character_star_grade_"+_grade);
#endregion
		
#region spine dummy
var _spineSprite	= asset_get_index("SPR_CHARACTER_GRADE_"+_grade+"_"+_index);
with(instance_create_layer(structDraw.spineX,structDraw.spineY,"slot_menu_dummy",obj_menu_character_spine_dummy))
{
	sprite_index	= _spineSprite;
	var _scale	= 150/sprite_height;
	image_xscale	= _scale;
	image_yscale	= _scale;
	y	+= sprite_height/2;
	skeleton_animation_set("DEFAULT_IDLE");
}
#endregion
		
#endregion

#region instance
instance_create_layer(x+910,y+444,"slot_menu_button",obj_menu_character_button_done);
instance_create_layer(x+1094,y+444,"slot_menu_button",obj_menu_character_purchase_ability);
instance_create_layer(x+268,y+1,"slot_menu_button",obj_menu_character_ability_control);

var _length	= array_length(structPurchaseAmount.arrayPurchaseAmount);
for(var _i=0; _i<_length; ++_i)
{
	var _x	= structPurchaseAmount.startX-(structPurchaseAmount.interX*_i);
	var _y	= structPurchaseAmount.startY;
	with(instance_create_layer(x+_x,y+_y,"slot_menu_button",obj_menu_character_purchase_amount_ability))
	{
		myAmount	= other.structPurchaseAmount.arrayPurchaseAmount[_i];
		structDraw.text	= myAmount;
		if(myAmount != other.structPurchaseAmount.nowAmount)
		{
			myColor	= c_gray;
		}
		
		if(_i == _length-1)
		{
			other.structDraw.pAmountX	= bbox_left-10;
			other.structDraw.pAmountY	= y;
		}
	}
}
#endregion

#region shader
structShockWave	= {
	shaderCompile	: shader_is_compiled(shd_shockwave),
	shockEnabled : false,
	shockUniTime : shader_get_uniform(shd_shockwave,"time"),
	shockTime : 0,
	shockUniMousePos : shader_get_uniform(shd_shockwave,"mouse_pos"),
	shockUniResolution : shader_get_uniform(shd_shockwave,"resolution"),
	shockUniAmplitude : shader_get_uniform(shd_shockwave,"shock_amplitude"),
	shockAmplitude : 10,
	shockUniRefraction : shader_get_uniform(shd_shockwave,"shock_refraction"),
	shockRefraction : 0.8,
	shockUniWidth : shader_get_uniform(shd_shockwave,"shock_width"),
	shockWidth : 0.1,
	shockSpeed	: 0.008,
	delay : 0,
	surf	: noone,
	
	shaderReset : function() {
		delay	= 120;
		shockTime	= 0;
	},
	shaderStart	: function() {
		if(delay > 0 && shaderCompile)
		{
			shader_set(shd_shockwave);
			shockTime+=0.025;
			shader_set_uniform_f(shockUniTime, shockTime);
			shader_set_uniform_f(shockUniMousePos,display_get_gui_width()/2,display_get_gui_height()/2);
			shader_set_uniform_f(shockUniResolution,display_get_gui_width(),display_get_gui_height());
			shader_set_uniform_f(shockUniAmplitude, shockAmplitude);
			shader_set_uniform_f(shockUniRefraction, shockRefraction);
			shader_set_uniform_f(shockUniWidth, shockWidth);
		}
	},
	shaderEnd : function() {
		if(delay > 0 && shaderCompile)
		{
			shader_reset();
			delay--;
		}
	}
};
#endregion

#region stepMain
stepMain	= undefined;
#endregion

#region clean up function
cleanUp	= function() {
	global.windowLayer	= enumWindowLayer.idle;
	with(obj_room_stage_bottom_parent)
	{
		destroy	= false;
	}
	with(obj_room_stage_character_slot)
	{
		structDraw.lerpGoalY	= ystart;
	}
	with(obj_menu_character_button_slot)
	{
		instance_destroy();
	}
	
	instance_destroy();
}
#endregion

#region draw main
drawMain	= function() {
	if(!surface_exists(surfaceMain))
	{
		surfaceMain	= surface_create(display_get_gui_width(),display_get_gui_height());
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		surfaceUpdate	= false;
		surface_set_target(surfaceMain);
		draw_clear_alpha(c_black,0.5);
		draw_set_alpha(1);
		
		//shader_premultiply(true,undefined,undefined);
		draw_sprite(sprite_index,0,x,y);
				
		#region star
		draw_sprite_ext(structDraw.starIcon,0,structDraw.starX,structDraw.starY,0.8,0.8,0,c_white,1);
		#endregion
		
		gpu_set_colorwriteenable(true,true,true,false);
		
		#region name
		draw_set_color(structDraw.nameColor);
		draw_set_halign(structDraw.nameH);
		draw_set_valign(structDraw.nameV);
		draw_set_font(structDraw.nameFont);
		draw_text_transformed(structDraw.nameX,structDraw.nameY,structDraw.nameText,structDraw.nameScale,structDraw.nameScale,0);
		#endregion
		
		#region ability info
		draw_set_color(structDraw.abilityInfoColor);
		draw_set_halign(structDraw.abilityInfoH);
		draw_set_valign(structDraw.abilityInfoV);
		draw_set_font(structDraw.abilityInfoFont);
		draw_text_transformed(structDraw.abilityInfoX,structDraw.abilityInfoY,structDraw.abilityInfoText,structDraw.abilityInfoScale,structDraw.abilityInfoScale,0);
		#endregion
		
		#region ability
		var _names	= structDraw.abilityProperty;
		var _size	= array_length(_names);
		for(var _i=0; _i<_size; ++_i)
		{
			var _x	= structDraw.abilityNameX;
			var _y	= structDraw.abilityNameY+(_i*structDraw.abilityNameInterY);
			draw_set_halign(structDraw.abilityNameH);
			draw_set_valign(structDraw.abilityNameV);
			draw_set_font(structDraw.abilityNameFont);
			draw_set_color(structDraw.abilityNameColor);
			draw_text_transformed(_x,_y,structDraw.abilityName[$ _names[_i]],structDraw.abilityNameScale,structDraw.abilityNameScale,0);
		
			var _x	= structDraw.abilityValueX;
			var _y	= structDraw.abilityValueY+(_i*structDraw.abilityValueInterY);
			draw_set_halign(structDraw.abilityValueH);
			draw_set_valign(structDraw.abilityValueV);
			draw_set_font(structDraw.abilityValueFont);
			draw_set_color(structDraw.abilityValueColor);
			draw_text_transformed(_x,_y,structDraw.abbiltyValue[$ _names[_i]],structDraw.abilityValueScale,structDraw.abilityValueScale,0);
		}
		#endregion
		
		#region stat
		var _names	= structDraw.statProperty;
		var _size	= array_length(_names);
		var _indexY	= 0;
		var _indexX	= 0;
		for(var _i=0; _i<_size; ++_i)
		{
			if(_indexY == 4)
			{
				_indexY	= 0;
				_indexX++;
			}
			var _x	= structDraw.statNameX+(_indexX*structDraw.statNameInterX);
			var _y	= structDraw.statNameY+(_indexY*structDraw.statNameInterY);
			draw_set_halign(structDraw.statNameH);
			draw_set_valign(structDraw.statNameV);
			draw_set_font(structDraw.statNameFont);
			draw_set_color(structDraw.statNameColor);
			draw_text_transformed(_x,_y,structDraw.statName[$ _names[_i]],structDraw.statNameScale,structDraw.statNameScale,0);
		
			var _x	= structDraw.statValueX+(_indexX*structDraw.statNameInterX);
			var _y	= structDraw.statValueY+(_indexY*structDraw.statValueInterY);
			draw_set_halign(structDraw.statValueH);
			draw_set_valign(structDraw.statValueV);
			draw_set_font(structDraw.statValueFont);
			draw_set_color(structDraw.statValueColor);
			draw_text_transformed(_x,_y,structDraw.statValue[$ _names[_i]],structDraw.statValueScale,structDraw.statValueScale,0);
			_indexY++;
		}
		#endregion
		
		#region purchase amount info
		draw_set_color(structDraw.pAmountColor);
		draw_set_halign(structDraw.pAmountH);
		draw_set_valign(structDraw.pAmountV);
		draw_set_font(structDraw.pAmountFont);
		draw_text_transformed(structDraw.pAmountX,structDraw.pAmountY,structDraw.pAmountText,structDraw.pAmountScale,structDraw.pAmountScale,0);
		
		with(obj_menu_character_purchase_amount_ability)
		{
			draw_sprite_ext(sprite_index,0,x,y,1,1,0,myColor,1);
			draw_set_font(structDraw.textFont);
			draw_set_color(enumColor.btBrown);
			draw_set_halign(structDraw.textH);
			draw_set_valign(structDraw.textV);
			draw_text_transformed(structDraw.textX,structDraw.textY,structDraw.text,structDraw.textScale,structDraw.textScale,0);
		}
		#endregion
		
		gpu_set_colorwriteenable(true,true,true,true);
		
		#region button
		with(obj_menu_character_button_parent)
		{
			draw_set_alpha(1);
			structSpriteDraw.draw();
			structTextDraw.draw();
		}
		#endregion
		
		//shader_premultiply(false,undefined,undefined);
		surface_reset_target();
		draw_set_color(c_white);
	}
	
	structShockWave.shaderStart();
	draw_set_alpha(1);
	draw_surface(surfaceMain,0,0);
	structShockWave.shaderEnd();
}
#endregion