#region leftLockDraw
leftLockDraw	= function() {
	static lockX	= x+134;
	static lockY	= y+240;
	static lockScale	= 1;
		
	draw_sprite_ext(spr_icon_lock,0,lockX,lockY,lockScale,lockScale,0,c_white,1);
		
	static textX	= x+134;
	static textY	= y+315;
	static text		= method_struct_get_deep_value(global.structGameText,["common","locked",global.languageCode]);
	static textScale	= mac_default_font_scale;
	
	draw_set_color(c_white);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	draw_set_font(global.fontMainBold);
	draw_text_transformed(textX,textY,text,textScale,textScale,0);
};
#endregion

#region bottomPriceDraw
bottomPriceDraw	= function() {		
	var _price		= method_struct_get_deep_value(global.structGameMeta,["unlock",slotName,"unlockStartPrice"]);
	static priceX		= x+750;
	static priceY		= y+380;
	static priceScale	= mac_default_font_scale;
	draw_set_color(c_lime);
	draw_set_halign(fa_left);
	draw_set_valign(fa_middle);
	draw_set_font(global.fontMainBold);
	
	var _price	= string_number_abbrev(_price,3);
	draw_text_transformed(priceX,priceY,_price,priceScale,priceScale,0);
	
	var _currencyX	=	x+770+(string_width(_price)*mac_default_font_scale);
	static currencyY	= y+380;
	static currencyScale	= 1;
	static currency		= method_struct_get_deep_value(global.structGameMeta,["unlock",slotName,"unlockStartCurrency"]);
	static spriteCurrency	= asset_get_index("spr_icon_currency_"+currency);
	draw_sprite_ext(spriteCurrency,0,_currencyX,currencyY,currencyScale,currencyScale,0,c_white,1);
		
	static priceNameX	= x+720;
	static priceNameY	= y+380;
	static priceName		= method_struct_get_deep_value(global.structGameText,["common","unlockStart",global.languageCode]);
	static priceNameScale	= mac_default_font_scale-0.2;
	draw_set_color(enumColor.goldBrown);
	draw_set_halign(fa_right);
	draw_set_valign(fa_middle);
	draw_set_font(global.fontMainMedium);
	draw_text_transformed(priceNameX,priceNameY,priceName,priceNameScale,priceNameScale,0);
	
	static unlockX	= x+735;
	static unlockY	= y+330;
	var _arrayText	= method_function_string_display_set(method_struct_get_deep_value(global.structGameText,["common","unlockInfo",global.languageCode]),800,100,mac_default_font_scale-0.2,global.fontMainMedium);
	var _unlock	= _arrayText[0];
	var _unlockScale	= _arrayText[1];
	draw_set_color($c9c9c9);
	draw_set_halign(fa_center);
	draw_set_valign(fa_bottom);
	draw_set_font(global.fontMainMedium);
	draw_text_transformed(unlockX,unlockY,_unlock,_unlockScale,_unlockScale,0);
};
#endregion

#region btDraw
btDraw	= function() {
	with(obj_menu_character_button_parent)
	{
		
		structSpriteDraw.draw();
		structTextDraw.draw();
	}
};
#endregion

#region releaseInfoDraw
releaseInfoDraw	= function() {
	static timerIconX	= x+735;
	static timerIconY	= y+100;
	static timerIconScale	= 1;
	draw_sprite_ext(spr_icon_timer,0,timerIconX,timerIconY,timerIconScale,timerIconScale,0,c_white,1);
		
	static releaseInfoX	= x+735;
	static releaseInfoY	= y+160;
	static releaseInfo		= method_struct_get_deep_value(global.structGameText,["common","unlockRemain",global.languageCode]);
	static releaseInfoScale	= mac_default_font_scale;
	draw_set_color(c_white);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	draw_set_font(global.fontMainBold);
	draw_text_transformed(releaseInfoX,releaseInfoY,releaseInfo,releaseInfoScale,releaseInfoScale,0);
		
	static releaseTimeX	= x+735;
	static releaseTimeY	= y+195;
	var _releaseTime		= method_function_time_draw_timestamp_to_hms(method_struct_get_deep_value(global.structGameMeta,["unlock",slotName,"unlockTimestamp"]));
	static releaseTimeScale	= mac_default_font_scale;
	draw_set_color($c9c9c9);
	draw_set_halign(fa_center);
	draw_set_valign(fa_top);
	draw_set_font(global.fontMainBold);
	draw_text_transformed(releaseTimeX,releaseTimeY,_releaseTime,releaseTimeScale,releaseTimeScale,0);
};
#endregion

#region drawMain
drawMain = function() {
	if(!surface_exists(surfaceMain))
	{
		surfaceMain	= surface_create(display_get_gui_width(),display_get_gui_height());
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		surfaceUpdate	= false;
		surface_set_target(surfaceMain);
		draw_clear_alpha(c_black,0.5);
		draw_set_alpha(1);
		draw_sprite(other.sprite_index,0,x,y);
		shader_premultiply(true,undefined,undefined);
		leftLockDraw();
		bottomPriceDraw();
		releaseInfoDraw();
		btDraw();
		shader_premultiply(false,undefined,undefined);
		surface_reset_target();
		draw_set_color(c_white);
	}
	draw_set_alpha(1);
	draw_surface(surfaceMain,0,0);
};
#endregion

#region stepMain
stepMain	= function(){
	var _characterUid	= global.structUserData.equip.character[$ slotName];
	if(characterUid != _characterUid)
	{
		with(callInstance)
		{
			slotManageCreate();
		}
	}
};
#endregion

#region clean up function
cleanUp	= function() {
	global.windowLayer	= enumWindowLayer.idle;
	with(obj_room_stage_bottom_parent)
	{
		destroy	= false;
	}
	with(obj_room_stage_character_slot)
	{
		structDraw.lerpGoalY	= ystart;
	}
	with(obj_menu_character_button_slot)
	{
		instance_destroy();
	}
	instance_destroy();
}
#endregion

#region instance
instance_create_layer(x+644,y+444,"slot_menu_button",obj_menu_character_button_done);
instance_create_layer(x+824,y+444,"slot_menu_button",obj_menu_character_purchase_start_unlock);
#endregion