drawParticle	= function() {
	if(myWindowLayer == global.windowLayer)
	{
		part_system_drawit(particleSystem);
	}
};

drawStar	= function() {
	static starX	= 640;
	static starY	= 535;
	static starScale	= 1;
	var _starIcon	= global.gridCharInventory[# enumGridCharacterInventory.starIcon,global.inventorySelectedIndex];
	draw_sprite_ext(_starIcon,0,starX,starY,starScale,starScale,0,c_white,1);
};

drawName	= function() {
	static xx	= 640;
	static yy	= 93;
	var _name	= character_get_name(global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex]);
	
	preSetForDrawText(fa_center,fa_top,c_white,1,global.fontMainBold);
	//gpu_set_blendenable(false);
	draw_text_transformed(xx,yy,_name,mac_default_font_scale,mac_default_font_scale,0);
	//gpu_set_blendenable(true);
}
drawStaticAbility	= function() {
	//static infoX	= 1090;
	//static infoY	= 64;
	//var _arrayInfo		= method_function_string_display_set(method_struct_get_deep_value(global.structGameText,["characterAbility","defaultAbility","ko"]),
	//300,60,mac_default_font_scale,global.fontMainBold);
	
	//var _info	= _arrayInfo[0];
	//var _infoScale	= _arrayInfo[1];
	//preSetForDrawText(fa_center,fa_top,c_white,1,global.fontMainBold);

	//draw_text_transformed(infoX,infoY,_info,_infoScale,_infoScale,0);
	
	static startX	= 900;
	static startY	= 93;
	static interX	= 348;
	static interY	= 30;
	static scale	= mac_default_font_scale;
	static maxColumn	= 1;
	static arraySize	= array_length(global.arrayStaticAbility);
	structStaticAbility	= new ability_get_name_and_value(global.arrayStaticAbility,"static",global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex]);
	//gpu_set_blendenable(false);
	preSetForDrawText(fa_center,fa_top,c_white,1,global.fontMainBold);
	for(var _i=0; _i<arraySize; ++_i)
	{
		var _ability	= global.arrayStaticAbility[_i];
		var _column	= _i mod maxColumn;
		var _row	= _i div maxColumn;
		var _key	= "";
		var _color	= c_white;
		
		for(var _j=0; _j<2; ++_j)
		{
			if(_j == 0) 
			{ _key	= "name"; draw_set_halign(fa_left); draw_set_color(c_white); } 
			else 
			{ _key = "value"; draw_set_halign(fa_right); draw_set_color(c_lime); }
			
			var _x	= startX+(_j*interX);
			var _y	= startY+(_row*interY);
			draw_text_transformed(_x,_y,structStaticAbility[$ _ability][$ _key],scale,scale,0);
		}
	}
	//gpu_set_blendenable(true);

};

drawMutableAbility	= function() {
	static startX	= 900;
	static startY	= 300;
	static interX	= 348;
	static interY	= 55;
	static textInter	= 60;
	static iconInter	= 24;
	static iconScale	= 48/sprite_get_height(spr_character_ability_attack);
	static scale	= mac_default_font_scale-0.2;
	static maxColumn	= 1;
	static arraySize	= array_length(global.arrayMutableAbility);
	structMutableAbility	= new ability_get_name_and_value(global.arrayMutableAbility,"mutable",global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex]);
	
	preSetForDrawText(fa_left,fa_middle,c_white,1,global.fontMainMedium);
	for(var _i=0; _i<arraySize; ++_i)
	{
		var _ability	= global.arrayMutableAbility[_i];
		var _column	= _i mod maxColumn;
		var _row	= _i div maxColumn;
		var _key	= "";
		var _textInter	= 0;
		var _level	= "0";
		var _drawText	= "";
		for(var _j=0; _j<2; ++_j)
		{
			var _x	= startX+(_j*interX);
			var _y	= startY+(_row*interY);
			if(_j == 0) 
			{
				draw_set_halign(fa_left);
				_key	= "name"; 
				_textInter	= textInter;
				_level	= string(method_struct_get_deep_value(global.structUserData,["inventory","character",global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex],_ability])+1);
				_level	= string_number_abbrev(_level,3);
				_drawText	= structMutableAbility[$ _ability][$ _key]+"  Lv."+_level;
				
				draw_sprite_ext(asset_get_index("spr_character_ability_"+_ability),0,_x+iconInter,_y,iconScale,iconScale,0,c_white,1);
			} 
			else 
			{ 
				_key = "value"; draw_set_halign(fa_right); 
				_textInter	= 0;
				_drawText	= structMutableAbility[$ _ability][$ _key];
			}
			//gpu_set_blendenable(false);
			draw_text_transformed(_x+_textInter,_y,_drawText,scale,scale,0);
			//gpu_set_blendenable(true);
		}
	}
};


drawMain	= function() {
	if(!surface_exists(surface))
	{
		surface	= surface_create(room_width,room_height);
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		surfaceUpdate	= false;
		surface_set_target(surface);
		draw_clear_alpha(c_white,0);
		shader_premultiply(true,undefined,undefined);
		if(myWindowLayer == global.windowLayer)
		{
			drawStar();
			drawName();
			with(obj_rm_character_parent_info_bt)
			{
				drawMain();
			}
		}

		drawStaticAbility();
		drawMutableAbility();
		shader_premultiply(false,undefined,undefined);
		surface_reset_target();
	}
	draw_surface_ext(surface,0,0,1,1,0,c_white,1);
};


#region particle
particleSystem	= system_particle_character_bottom_aura(true);
part_system_automatic_draw(particleSystem,false);
#endregion

#region instance
with(instance_create_layer(x,y,"slot_menu_dummy",obj_rm_character_inventory_dummy))
{
	sprite_index	= global.gridCharInventory[# enumGridCharacterInventory.spineSprite,global.inventorySelectedIndex];
	image_xscale	= 330/sprite_height;
	image_yscale	= 330/sprite_height;
	animationChange();
}


infoUpdate	= function(){
	static structButton	= {
		a	: {
			xx	: 531,
			yy	: 603,
		},
		b	: {
			xx	: 749,
			yy	: 603,
		},
		c	: {
			xx	: 531,
			yy	: 668,
		},
		d	: {
			xx	: 749,
			yy	: 668,
		},
		e	: {
			xx	: 640,
			yy	: 603,
		}
	};
	
	with(obj_rm_character_parent_info_bt)
	{
		instance_destroy();
	}
	with(obj_rm_character_info_resurrection_time)
	{
		instance_destroy();
	}
	with(obj_rm_character_inventory_dummy)
	{
		animationChange();
	}
	
	var _equip	= ds_grid_get(global.gridCharInventory,enumGridCharacterInventory.equip,global.inventorySelectedIndex);
	var _state	= ds_grid_get(global.gridCharInventory,enumGridCharacterInventory.state,global.inventorySelectedIndex);
	var _healthWeight	= ds_grid_get(global.gridCharInventory,enumGridCharacterInventory.healthWeight,global.inventorySelectedIndex);
	if(_equip == "none")
	{
		if(_state != "dead")
		{
			structButton.arrayObj	= [obj_rm_character_info_bt_entry,obj_rm_character_info_bt_levelup,obj_rm_character_info_bt_sell];
			structButton.arrayCoordi	= ["a","b","c"];
			instance_create_layer(575,130,"infomation_button",obj_rm_character_info_health_bar);
			if(_healthWeight < 1)
			{
				array_push(structButton.arrayObj,obj_rm_character_info_bt_recovery);
				array_push(structButton.arrayCoordi,"d");
			}
		}
		else
		{
			structButton.arrayObj	= [obj_rm_character_info_bt_entry,obj_rm_character_info_bt_levelup,obj_rm_character_info_bt_sell,obj_rm_character_info_bt_resurrection];
			structButton.arrayCoordi	= ["a","b","c","d"];
			instance_create_layer(575,130,"infomation_button",obj_rm_character_info_resurrection_time);
		}
	}
	else
	{
		if(_state != "dead")
		{
			structButton.arrayObj	= [obj_rm_character_info_bt_levelup];
			structButton.arrayCoordi	= ["e"];
			instance_create_layer(575,130,"infomation_button",obj_rm_character_info_health_bar);
			if(_healthWeight < 1)
			{
				array_push(structButton.arrayObj,obj_rm_character_info_bt_recovery);
				array_push(structButton.arrayCoordi,"b");
				structButton.arrayCoordi[0]	= "a";
			}
		}
		else
		{
			structButton.arrayObj	= [obj_rm_character_info_bt_levelup,obj_rm_character_info_bt_resurrection];
			structButton.arrayCoordi	= ["a","b"];
			instance_create_layer(575,130,"infomation_button",obj_rm_character_info_resurrection_time);
		}
	}
	var _btColor	= true;
	for(var _i=0; _i<array_length(structButton.arrayObj); ++_i)
	{
		_btColor	= !_btColor;
		var _obj	= structButton.arrayObj[_i];
		var _coordi	= structButton.arrayCoordi[_i];
		var _xx	= structButton[$ _coordi].xx;
		var _yy	= structButton[$ _coordi].yy;
		with(instance_create_layer(_xx,_yy,"infomation_button",_obj))
		{
			btColor	= _btColor;
		}
	}
};

infoUpdate();
#endregion













