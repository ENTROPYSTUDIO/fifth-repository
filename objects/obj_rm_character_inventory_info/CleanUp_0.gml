system_particle_character_bottom_aura(false);
with(obj_rm_character_inventory_dummy)
{
	instance_destroy();
}
with(obj_rm_character_info_parent)
{
	instance_destroy();
}

if(surface_exists(surface))
{
	surface_free(surface);
	surface	= noone;
}