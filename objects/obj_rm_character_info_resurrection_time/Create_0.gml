x	= 640;
y	= 170;

asyncProcess	= false;
releaseTime	= "";
drawMain	= function(){
	static text1Scale	= mac_default_font_scale;
	static text2Scale	= mac_default_font_scale-0.2;
	var _text1	= string(method_struct_get_deep_value(global.structGameText,["common","untilResurrection",global.languageCode]));
	var _text2	= releaseTime;
	preSetForDrawText(fa_center,fa_bottom,c_white,1,global.fontMainMedium);
	draw_text_transformed(x,y-20,_text1,text1Scale,text1Scale,0);
	preSetForDrawText(fa_center,fa_bottom,c_white,1,global.fontMainMedium);
	draw_text_transformed(x,y,_text2,text2Scale,text2Scale,0);
};

stepMain	= function() {
	static timeToEmptyDelay	= 1000;
	static timeToEmptyTime	= 0;
	var _myKey	= global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
	var _goalTime	= method_struct_get_deep_value(global.structUserData,["inventory","character",_myKey,"resurrectionTime"]);
	if(!is_undefined(_goalTime))
	{
		releaseTime	= method_function_time_draw_timestamp_to_hms(_goalTime-global.serverTimestamp);
		surfaceUpdate	= true;
	
		if(global.serverTimestamp > _goalTime+timeToEmptyDelay && !asyncProcess)
		{
			if(current_time > timeToEmptyTime)
			{
				asyncProcess	= true;
				timeToEmptyTime	= current_time+timeToEmptyDelay;
				var _multiUpdate	= new firebaseRtdbUpdateMultiPathConstructor();
				
				_multiUpdate.add("inventory/character/"+_myKey+"/resurrectionTime",0);
				_multiUpdate.add("inventory/character/"+_myKey+"/state","idle");
				_multiUpdate.add("inventory/character/"+_myKey+"/healthWeight",1);
				//var _arrPath	= [
				//	"inventory/character/"+_myKey+"/resurrectionTime",
				//	"inventory/character/"+_myKey+"/state",
				//	"inventory/character/"+_myKey+"/healthWeight",
				//];
				//var _arrValue	= [
				//	0,
				//	"idle",
				//	1
				//];
				//var _updateStruct	= new firebaseRtdbUpdateMultiPathConstructor(_arrPath,_arrValue);
				if(_multiUpdate.existsCheck())
				{
					var _json	= _multiUpdate.getJson();
					firebase_rtdb_node_builder(mac_firebase_ref_my_uid,_json,false,true,true,callbackDone,callbackFail);
					delete(_multiUpdate);
				}
				
				if(os_get_config() == "pc")
				{
					global.structUserData.inventory.character[$ _myKey].resurrectionTime	= 0;
					global.structUserData.inventory.character[$ _myKey].state	= "idle";
					global.structUserData.inventory.character[$ _myKey].healthWeight	= 1;
					callbackDone();
				}
			}
		}
	}
};

callbackDone	= function(){
	asyncProcess	= true;
	with(obj_rm_character_inventory_manager)
	{
		structInventory.inventoryUpdateIndividual(global.inventorySelectedIndex);
		surfaceUpdate	= true;
	}
	with(obj_rm_character_inventory_dummy)
	{
		animationChange();
	}
	with(obj_menu_character_resurrection_parent)
	{
		instance_destroy();
	}
};
callbackFail	= function(){
	asyncProcess	= false;
};
