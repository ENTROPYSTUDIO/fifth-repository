var _count	= instance_number(obj_parent_player);
if(_count > 0)
{
	var _x	= 0;
	var _y	= 0;
	with(obj_parent_player)
	{
		_x	+= x;
		_y	+= y;
	}
	ex_camera_scroll_to_point(MAC_CAMERA_NAME_MAIN,_x/_count,_y/_count-80,6,method_ease_inout_cubic,true,undefined,undefined);
}