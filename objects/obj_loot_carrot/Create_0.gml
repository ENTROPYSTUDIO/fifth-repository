// Inherit the parent event
event_inherited();
structState	= new constructLootState();
structDraw		= new constructLootDraw();
image_xscale	= structState.imageScale;
image_yscale	= structState.imageScale;
structState.finalY	= random_range(y,y+120);
deadTime	= current_time+global.structGameMeta.gameSetting.value.lootDeadTime;
structState.category	= "carrot";
