#region depth and orientation
depth	= round(-y);
#endregion

structDraw.auraScale();

#region aggro
if(current_time > structState.lootTime && structState.target == noone)
{
	var _myId	= id;
	structState.lootTime	= current_time+structAbility.lootDelay;
	var _range	= structAbility.range;
	var _near	= instance_nearest(x,y,obj_parent_loot);
	var _success	= false;
	repeat(instance_number(obj_parent_pet))
	{
		if(_near != noone)
		{
			var _nearstructState	= _near.structState;
			if(_nearstructState.target == noone && !_nearstructState.die && distance_to_object(_near) < _range && _nearstructState.ready)
			{
				_nearstructState.target	= id;
				structState.target	= _near;
				_success	= true;
				break;
			}
			else
			{
				with(_near)
				{
					_near	= instance_nearest(x,y,obj_parent_loot);
				}
			}
		}
		else
		{
			break;
		}
	}
}
#endregion
