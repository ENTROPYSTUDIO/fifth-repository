var _eventName	= event_data[? "name"];
if(is_undefined(_eventName)) { exit; }
//ex_camera_shake(MAC_CAMERA_NAME_MAIN, 8, 8, 8, 8,method_ease_out_sine,undefined,undefined);
switch(structState.animationNow)
{
	#region default attack
	case "defaultAttack":
	{
		if(_eventName == "action")
		{
			lootAction();
		}
		else if(_eventName == "end")
		{
			structState.animationNow	= "none";
		}
		break;
	}
	#endregion
	
	#region small hit
	case "hit":
	{
		if(_eventName == "end")
		{
			structState.animationNow	= "none";
		}
		break;
	}
	#endregion
	
	#region small hit
	case "bigHit":
	{
		if(_eventName == "end")
		{
			structState.forceAnimation	= "revive";
		}
		break;
	}
	#endregion
	
	#region small hit
	case "revive":
	{
		if(_eventName == "end")
		{
			structState.animationNow	= "none";
		}
		break;
	}
	#endregion
	
	#region die
	case "die":
	{
		if(_eventName == "end")
		{
			structState.forceAnimation	= "dieIdle";
		}
		break;
	}
	#endregion
}
