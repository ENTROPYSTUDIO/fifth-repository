event_inherited();

#region init
var _target	= structState.target;
var _targetExists	= false;
#endregion


#region target reset
if(_target != noone)
{
	_targetExists	= true;
		
	if(!instance_exists(_target) || structState.target.structState.die)
	{
		_target	= noone;
		_targetExists	= false;
		structState.target	= noone;
	}
}
#endregion

#region set speed
if(_targetExists)
{
	if(!place_meeting(xprevious,yprevious,_target))
	{
		var _xSign	= sign(x-_target.y);
		if(_xSign != 0)
		{
			var _targetX	= _xSign < 0 ? _target.bbox_left : _target.bbox_right;
			var _dir = point_direction(x,y,_targetX,_target.y);
			structState.hsp = lengthdir_x(structAbility.moveSpeed, _dir);
			structState.vsp = lengthdir_y(structAbility.moveSpeed, _dir);
		}
	}
	else
	{
		structState.hsp	= 0;
		structState.vsp	= 0;
	}
}
else
{
	var _target	= instance_nearest(x,y,obj_parent_player);
	if(_target != noone && !place_meeting(xprevious,yprevious,_target))
	{
		var _move	= _target.structState.hsp+_target.structState.vsp;
		if(!_move)
		{
			var _xSign	= sign(x-_target.y);
			if(_xSign != 0)
			{
				var _targetX	= _xSign < 0 ? _target.bbox_left : _target.bbox_right;
				var _dir = point_direction(x,y,_targetX,_target.y);
				structState.hsp = lengthdir_x(structAbility.moveSpeed, _dir);
				structState.vsp = lengthdir_y(structAbility.moveSpeed, _dir);
			}
		}
	}
	else
	{
		structState.hsp	= 0;
		structState.vsp	= 0;
	}
}
#endregion

#region apply speed
var _hspAmount	= sign(lerpHsp-structState.hsp) >= 0 ? 0.1 : 0.1;
var _vspAmount	= sign(lerpVsp-structState.vsp) >= 0 ? 0.1 : 0.1;
lerpHsp	= lerp(lerpHsp,structState.hsp,_hspAmount);
lerpVsp	= lerp(lerpVsp,structState.vsp,_vspAmount);
var _hsp	= lerpHsp;
var _vsp	= lerpVsp;

var _hsp	= structState.hsp;
var _vsp	= structState.vsp;
var _nextPositionX	= x+_hsp;
var _nextPositionY	= y+_vsp;
if(_hsp != 0 || _vsp != 0)
{
	if(structState.orientation && _hsp != 0)
	{
		image_xscale	= structState.imageScale*sign(_hsp);
	}
	structState.animationSet	= "walk";
	x	= _nextPositionX;
	y	= _nextPositionY;
}
else
{
	structState.hsp	= 0;
	structState.vsp	= 0;
	structState.animationSet	= "idle";
}
#endregion

#region looting
var _myId	= id;
var _target	= structState.target;
if(_target != noone && instance_exists(_target))
{
	if(_target.structState.objectType == "loot")
	if(place_meeting(xprevious,yprevious,_target))
	{
		var _xSign	= _target.x > x ? 1 : -1;
		if(_xSign != 0)
		{
			image_xscale	= structState.imageScale*_xSign;
		}
		var _animation	= "defaultAttack";
		structState.animationSet	= _animation;
	}
}
#endregion