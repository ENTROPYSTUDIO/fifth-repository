event_inherited();

#region aggro
if(current_time > structState.aggroTime)
{
	var _myId	= id;
	structState.aggroTime	= current_time+structAbility.aggroSpeed;
	var _range	= structAbility.aggroRange/2;
	var _aggroPower	= structAbility.aggroPower;
	var _targetMax	= structAbility.multipleTarget;
	//var _list	= ds_list_create();
	//var _number	= collision_rectangle_list(x-_range,y-_range,x+_range,y+_range,obj_parent_enemy,false,true,_list,true);
	//var _index	= 0;
	//if(_number > 0)
	//{
	//	var _size	= ds_list_size(_list);
	//	for(var _i=0; _i<_size; ++_i)
	//	{
	//		var _inst	= _list[| _i];
	//		if(!_inst.structState.die && _inst.structState.roomIn)
	//		{
	//			method_aggro_calculate(_inst,_myId,_aggroPower);
	//			_index++;
	//		}
	//		if(_index >= _targetMax)
	//		{
	//			break;
	//		}
	//	}
	//}
	//ds_list_destroy(_list);
	
	var _left	= x-_range;
	var _top	= y-_range;
	var _right	= x+_range;
	var _bottom	= y+_range;
	var _index	= 0;
	with(obj_parent_enemy)
	{
		var _in	= !(bbox_left < _left || bbox_top < _top || bbox_right > _right || bbox_bottom > _bottom);
		if(_in)
		{
			if(!structState.die && structState.roomIn)
			{
				method_aggro_calculate(id,_myId,_aggroPower);
				_index++;
			}
			if(_index >= _targetMax)
			{
				break;
			}
		}
	}
}
#endregion

#region target set
if(structState.target == noone)
{
	var _nearState	= false;
	var _target	= instance_nearest(x,y,obj_parent_enemy);
	if(_target != noone)
	{
		if(!_target.structState.die && _target.structState.roomIn)
		{
			structState.target	= _target;
			_nearState	= true;
		}
	}
	
	if(!_nearState)
	{
		var _nearInstance	= noone;
		var _myId	= id;
		var _minDistance	= noone;
		with(obj_parent_enemy)
		{
			if(structState.die || !structState.roomIn)
			{
				continue;
			}
			var _distance	= distance_to_object(_myId);
			if(_distance < _minDistance || _minDistance == noone)
			{
				_minDistance	= _distance;
				_nearInstance	= id;
			}
		}
		structState.target	= _nearInstance;
	}
}
#endregion