debugInputDirectObject	= function() {
	static delay	= 100;
	static time	= current_time;
	
	if(current_time > time)
	{
		time	= current_time+delay;
		var _ranObj	= instance_find(all,irandom(instance_count-1));
		if(!variable_instance_exists(_ranObj,"forceInput")) { time =0; return true; }
		with(_ranObj)
		{
			forceInput	= true;
			effect_create_above(ef_firework, x, y, 0, c_white);
			//method_show_debug(["object",object_get_name(object_index)]);
		}
	}
}