myWindowLayer	= enumWindowLayer.characterRecovery;
forceInput	= false;

drawMain	= function(){
	static btColor	= enumColor.skyBlue;
	static btName	= method_struct_get_deep_value(global.structGameText,["common","recovery",global.languageCode]);
	static nameScale	= mac_default_font_scale-0.2;
	draw_sprite_ext(sprite_index,0,x,y,1,1,0,btColor,1);
	preSetForDrawText(fa_center,fa_middle,c_white,1,global.fontMainBold);
	draw_text_transformed(x,y,btName,nameScale,nameScale,0);
};

touchEvent	= function(){
	if(method_function_input_mouse_pressed_gui_single(id,myWindowLayer,noone) || (myWindowLayer == global.windowLayer && forceInput))
	{
		forceInput	= false;
		if(instance_exists(obj_menu_character_recovery_manager))
		{
			var _key	= obj_menu_character_recovery_manager.optionAllLegend ? "" : global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
			if(obj_menu_character_recovery_manager.optionAllLegend)
			{
				var _price			= getCharacterRecoveryPrice("");
				var _nowResource	= user_resource_get_real("carrot");
				if(_nowResource >= _price)
				{
					callbackAllRecoveryDone();
				}
				else
				{
					var _message	= method_struct_get_deep_value(global.structGameText,["error","notEnoughResource",global.languageCode]);
					popup_message(_message,120);
				}
			}
			else
			{
				var _key			= global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
				var _price			= getCharacterRecoveryPrice(_key);
				var _nowResource	= user_resource_get_real("carrot");
				if(_nowResource >= _price)
				{
					callbackIndividualRecoveryDone();
				}
				else
				{
					var _message	= method_struct_get_deep_value(global.structGameText,["error","notEnoughResource",global.languageCode]);
					popup_message(_message,120);
				}
			}
		}
	}
};

callbackAllRecoveryDone	= function(){
	var _price			= getCharacterRecoveryPrice("");
	var _nowResource	= user_resource_get_real("carrot");
	var _finalResource	= string(_nowResource-_price);
	
	global.structUserData.resource.carrot	= _finalResource;
	firebase_userdata_pipeline_add("resource/carrot",_finalResource);
	
	var _struct	= global.structUserData.inventory.character;
	var _size	= ds_grid_height(global.gridCharInventory);
	for(var _i=0; _i<_size; ++_i)
	{
		var _key	= global.gridCharInventory[# enumGridCharacterInventory.uid,_i];
		var _healthWeight	= _struct[$ _key].healthWeight;
		if(_healthWeight < 1)
		{
			firebase_userdata_pipeline_add("inventory/character/"+_key+"/healthWeight",1);
			_struct[$ _key].healthWeight	= 1;
			with(obj_rm_character_inventory_manager)
			{
				structInventory.inventoryUpdateIndividual(_i);
			}
		}
	}
	firebase_userdata_pipeline_upload();
	
	var _message	= method_struct_get_deep_value(global.structGameText,["common","legendRecoveryAll",global.languageCode]);
	popup_message(_message,120);
	
	with(obj_menu_character_recovery_parent)
	{
		instance_destroy();
	}
};
callbackIndividualRecoveryDone	= function(){
	var _key			= global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
	var _price			= getCharacterRecoveryPrice(_key);
	var _nowResource	= user_resource_get_real("carrot");
	var _finalResource	= string(_nowResource-_price);
	
	global.structUserData.resource.carrot	= _finalResource;
	firebase_userdata_pipeline_add("resource/carrot",_finalResource);
	
	var _struct	= global.structUserData.inventory.character;
	var _healthWeight	= _struct[$ _key].healthWeight;
	if(_healthWeight < 1)
	{
		firebase_userdata_pipeline_add("inventory/character/"+_key+"/healthWeight",1);
		_struct[$ _key].healthWeight	= 1;
		
		with(obj_rm_character_inventory_manager)
		{
			structInventory.inventoryUpdateIndividual(global.inventorySelectedIndex);
		}
	}
	
	var _message	= method_struct_get_deep_value(global.structGameText,["common","legendRecovery",global.languageCode]);
	popup_message(_message,120);
	
	with(obj_menu_character_recovery_parent)
	{
		instance_destroy();
	}
};

stepMain	= function(){
	touchEvent();
};
