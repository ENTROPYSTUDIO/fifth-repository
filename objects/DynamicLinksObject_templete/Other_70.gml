///This will be called once our configs are loaded.

var category = async_load[? "category"];

switch(category)
{
	case "dynamic_links_deeplink": //Let's see if this app is opened from an URL
	var status = async_load[? "status"];
	var data = async_load[? "data"];
	if (status==1)  //Yes this app is opened from a deep link.
	{
		show_message_async("This game is opened from the URL : "+string(data));
	}
	break;
	
	case "dynamic_link_basic": //Lets see if our short link is created?
	var status = async_load[? "status"];
	var data = async_load[? "data"];
	
	if (status==1)
	{
		show_message_async("Basic Short link created as : "+string(data));
	}
	break;
	
}