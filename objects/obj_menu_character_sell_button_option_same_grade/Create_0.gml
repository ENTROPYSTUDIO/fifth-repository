myWindowLayer	= enumWindowLayer.characterSell;
forceInput	= false;
buttonState	= false;

drawMain	= function(){
	btColor	= !buttonState ? c_white : c_lime;
	static btName	= method_struct_get_deep_value(global.structGameText,["common","sellSameGradeInfo",global.languageCode]);
	static nameScale	= mac_default_font_scale-0.2;
	static nameX	= x-(sprite_width/2+10);
	var _scale	= 32/sprite_height;
	draw_sprite_ext(sprite_index,0,x,y,_scale,_scale,0,btColor,1);
	
	preSetForDrawText(fa_right,fa_middle,c_white,1,global.fontMainMedium);
	draw_text_transformed(nameX,y,btName,nameScale,nameScale,0);
};

touchEvent	= function(){
	if(method_function_input_mouse_pressed_gui_single(id,myWindowLayer,noone) || (myWindowLayer == global.windowLayer && forceInput))
	{
		forceInput	= false;
		with(obj_menu_character_sell_manager)
		{
			optionSameGradeSell	= !optionSameGradeSell;
			other.buttonState	= optionSameGradeSell;
			surfaceUpdate	= true;
		}
	}
};

stepMain	= function(){
	touchEvent();
};