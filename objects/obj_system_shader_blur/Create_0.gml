with(obj_system_shader_blur)
{
	if(id != other.id)
	{
		instance_destroy();
	}
}
uni_resolution_hoz = shader_get_uniform(shd_gaussian_horizontal,"resolution");
uni_resolution_vert = shader_get_uniform(shd_gaussian_vertical,"resolution");
var_resolution_x = surface_get_width(application_surface);
var_resolution_y = surface_get_height(application_surface);

uni_blur_amount_hoz = shader_get_uniform(shd_gaussian_vertical,"blur_amount");
uni_blur_amount_vert = shader_get_uniform(shd_gaussian_horizontal,"blur_amount");
var_blur_amount = 0.5;

shaderBlur	= false;
surfaceBlur	= noone;
if(shader_is_compiled(shd_gaussian_horizontal))
{
	shaderBlur	= true;
	surfaceBlur	= noone;
	application_surface_draw_enable(false);
}
else
{
	instance_destroy();
}
