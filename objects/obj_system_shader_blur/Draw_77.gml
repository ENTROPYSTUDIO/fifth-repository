//Do horizontal blur first
if(shaderBlur)
{
	if(!surface_exists(surfaceBlur))
	{
		surfaceBlur	= surface_create(surface_get_width(application_surface),surface_get_height(application_surface));
	}
	surface_set_target(surfaceBlur);
	draw_clear_alpha(c_white,1);
	shader_set(shd_gaussian_horizontal);
	shader_set_uniform_f(uni_resolution_hoz, var_resolution_x, var_resolution_y);
	shader_set_uniform_f(uni_blur_amount_hoz, var_blur_amount);
	draw_surface_ext(application_surface, 0, 0, 1, 1, 0, c_white, 1);
	shader_reset();
	surface_reset_target();

	shader_set(shd_gaussian_vertical);
	shader_set_uniform_f(uni_resolution_vert, var_resolution_x, var_resolution_y);
	shader_set_uniform_f(uni_blur_amount_vert, var_blur_amount);
	gpu_set_blendenable(false);
	draw_surface_ext(surfaceBlur,0,0,window_get_width()/surface_get_width(surfaceBlur),window_get_height()/surface_get_height(surfaceBlur),0,c_white,1);
	gpu_set_blendenable(true);
	shader_reset();
	
}
