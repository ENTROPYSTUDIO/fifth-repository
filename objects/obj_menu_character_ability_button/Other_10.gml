if(!instance_exists(obj_menu_character_main_control)) { instance_destroy(); exit; }
characterUid	= obj_menu_character_main_control.characterUid;
var _level	= method_struct_get_deep_value(global.structUserData,["inventory","character",characterUid,statName]);

constructStructDraw	= function(characterUid,_statName) constructor {
	motherInst	= other.id;
	iconX = 48;
	iconY = 48;
	sprIcon	= asset_get_index("spr_character_ability_"+_statName);
	spriteIndex	= other.sprite_index;
	spriteWidth	= other.sprite_width;
	spriteHeight	= other.sprite_height;
	statNameX = 248;
	statNameY = 10;
	statNameH = fa_right;
	statNameV = fa_top;
	statNameFont = global.fontMainBold;
	statNameColor = c_white;
	statNameScale = mac_default_font_scale-0.1;
	statName = method_struct_get_deep_value(global.structGameText,["characterAbility",_statName,global.languageCode]);
	
	levelX = 248;
	levelY = 36;
	levelH = fa_right;
	levelV = fa_top;
	levelFont = global.fontMainMedium;
	levelColor = c_white;
	levelScale = mac_default_font_scale-0.2;
	level = character_upgrade_level(characterUid,_statName,"display",obj_menu_character_main_control.structPurchaseAmount.nowAmount);
	
	increaseX = 248;
	increaseY = 66;
	increaseH = fa_right;
	increaseV = fa_top;
	increaseFont = global.fontMainBold;
	increaseColor = make_color_rgb(0,255,0);
	increaseScale = mac_default_font_scale-0.1;
	increase = "+"+string(character_get_stat_upgrade_amount(characterUid,_statName,"display",obj_menu_character_main_control.structPurchaseAmount.nowAmount));
	
	priceX = 15;
	priceY = 112;
	price = character_get_stat_upgrade_price(characterUid,_statName,"display",obj_menu_character_main_control.structPurchaseAmount.nowAmount);
	priceReal = character_get_stat_upgrade_price(characterUid,_statName,"default",obj_menu_character_main_control.structPurchaseAmount.nowAmount);
	priecrFont = font_character_update_price;
	priceH = fa_left;
	priceV = fa_middle;
	priceColor = c_white;
	
	currencyX = 232;
	currencyY = 110;
	currencyIcon = spr_icon_currency_carrot;
	
	updateStruct = function(_characterUid,_statName) {
		var _level	= method_struct_get_deep_value(global.structUserData,["inventory","character",_characterUid,_statName]);
		level	= character_upgrade_level(_characterUid,_statName,"display",obj_menu_character_main_control.structPurchaseAmount.nowAmount);
		increase	= "+"+string(character_get_stat_upgrade_amount(_characterUid,_statName,"display",obj_menu_character_main_control.structPurchaseAmount.nowAmount));
		price	= character_get_stat_upgrade_price(_characterUid,_statName,"display",obj_menu_character_main_control.structPurchaseAmount.nowAmount);
		priceReal	= character_get_stat_upgrade_price(_characterUid,_statName,"default",obj_menu_character_main_control.structPurchaseAmount.nowAmount);
	};
	purchaseAvailable = function() {
		if(priceReal <= real(global.structUserData.resource.carrot))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	drawMain	= function(_id) {
		if(!surface_exists(_id.surface))
		{
			_id.surface	= surface_create(_id.sprite_width,_id.sprite_height);
			_id.surfaceUpdate	= true;
		}

		if(_id.surfaceUpdate)
		{
			_id.surfaceUpdate	= false;
			surface_set_target(_id.surface);
			draw_set_alpha(1);
			draw_clear_alpha(c_black,0);
			draw_sprite(_id.sprite_index,0,0,0);
			
			shader_premultiply(true,undefined,undefined);
			draw_set_halign(statNameH);
			draw_set_valign(statNameV);
			draw_set_color(statNameColor);
			draw_set_font(statNameFont);
			draw_text_transformed(statNameX,statNameY,statName,statNameScale,statNameScale,0);

			draw_set_color(levelColor);
			draw_set_font(levelFont);
			draw_text_transformed(levelX,levelY,level,levelScale,levelScale,0);

			draw_set_color(increaseColor);
			draw_set_font(increaseFont);
			draw_text_transformed(increaseX,increaseY,increase,increaseScale,increaseScale,0);
	
			draw_set_halign(priceH);
			draw_set_valign(priceV);
			draw_set_color(priceColor);
			draw_set_font(priecrFont);
			draw_text(priceX,priceY,price);

			draw_sprite(currencyIcon,0,currencyX,currencyY);
			draw_sprite(sprIcon,0,iconX,iconY);
			
			draw_set_halign(fa_center);
			shader_premultiply(false,undefined,undefined);
			
			surface_reset_target();
		}
		draw_surface_ext(_id.surface,_id.x-other.x,_id.y-other.y,1,1,0,_id.surfaceColor,1);
	}
};

structDraw	= new constructStructDraw(characterUid,statName);