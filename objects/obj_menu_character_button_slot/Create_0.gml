myWindowLayer	= enumWindowLayer.idle;
slotName	= "";
structStatus	= noone;
structAbility	= noone;
myCharacterInst		= noone;
surfaceUpdate	= false;
destroy	= false;
forceInput	= false;

#region slotManageCreate
slotManageCreate	= function(){
	var _callInstance	= id;
	var _create	= true;

	with(obj_menu_character_main_control)
	{
		instance_destroy();
	}
	var _characterUid	= global.structUserData.equip.character[$ slotName];
	var _slotName	= slotName;
	with(instance_create_layer(0,0,"slot_menu",obj_menu_character_main_control))
	{
		callInstance	= _callInstance;
		characterUid		= _characterUid;
		slotName	= _slotName;
		event_user(0);
	}

	with(obj_menu_character_button_slot)
	{
		if(other.id != id)
		{
			structDraw.surfaceColor	= c_gray;
		}
		else
		{
			structDraw.surfaceColor	= c_white;
		}
	}
};
#endregion



