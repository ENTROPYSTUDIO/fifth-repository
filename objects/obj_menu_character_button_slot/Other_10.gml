//constructStageDrawAvatar
/*
function constructStageDrawAvatar(_frameSub,_sprAvatar,_avatarSub,_avatarX,_avatarY) constructor {
	surfaceMask	= noone;
	surfaceCenterX	= 128;
	surfaceCenterY	= 128;
	frameSub	= _frameSub;
	sprAvatar	= _sprAvatar;
	avatarSub	= _avatarSub;
	avatarX		= _avatarX;
	avatarY		= _avatarY;
}
*/
var _characterUid	= global.structUserData.equip.character[$ slotName];
switch(_characterUid)
{
	case "lock" :
	{
		structDraw	= new constructStageDrawAvatar(0,spr_icon_lock,0,0.8,0,noone,enumColor.navy,c_ltgray,1);
		break;
	}
	case "empty" :
	{
		structDraw	= new constructStageDrawAvatar(0,spr_icon_unlock,0,0.8,0,noone,enumColor.navy,c_white,1);
		break;
	}
	case "unlocking" :
	{
		structDraw	= new constructStageDrawAvatar(0,spr_icon_lock,0,0.8,0,noone,enumColor.navy,c_ltgray,1);
		break;
	}
	default :
	{
		structStatus	= global.structUserData.inventory.character[$ _characterUid];
		structAbility	= new constructPlayerAbility(_characterUid);
		
		var _grade	= character_get_static_ability(_characterUid,"grade","string");
		var _index	= character_get_static_ability(_characterUid,"index","string");
		var _starIcon	= asset_get_index("spr_character_star_grade_"+_grade);
		var _frameSub	= real(_grade);
		var _avatarSub	= real(_index);
		var _sprAvatar	= asset_get_index("SPR_CHARACTER_HEAD_GRADE_"+_grade);

		structDraw	= new constructStageDrawAvatar(_frameSub,_sprAvatar,_avatarSub,0.7,0,_starIcon,c_white,c_white,1);
		break;
	}
}

y	+= 300;