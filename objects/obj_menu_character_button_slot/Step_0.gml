var _onClickMyLayer	= method_function_input_mouse_pressed_gui_single(id,myWindowLayer,noone);
var _onClickMyGuiLayer	= method_function_input_mouse_pressed_gui_single(id,enumWindowLayer.slotMenu,noone);
var _validClick	= _onClickMyLayer || _onClickMyGuiLayer;
if((_validClick) || (forceInput && (global.windowLayer == enumWindowLayer.idle || global.windowLayer == enumWindowLayer.slotMenu)))
{
	forceInput	= false;
	slotManageCreate();
}

var _characterUid	= global.structUserData.equip.character[$ slotName];
if(characterUid != _characterUid)
{
	characterUid	= _characterUid;
	forceInput	= true;
}

y	= lerpGoalY(y,ystart,0.1);
