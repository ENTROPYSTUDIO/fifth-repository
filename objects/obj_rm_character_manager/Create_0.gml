event_inherited();
surfaceBackgrond	= noone;
surfaceUpdate	= false;
drawBackground	= function() {
	//var _center	= $ffc600;//blue
	//var _center	= enumColor.navy;//gold
	//var _center	= $a54164;//purple
	var _center	= enumColor.goldBrown;//goldBrown
	//var _center	= enumColor.skyBlue;//skyBlue
	static centerColor	= _center;
	static edgeColor	= c_black;//$45082a;//make_color_rgb(0,114,255);

	if(!surface_exists(surfaceBackgrond))
	{
		surfaceBackgrond	= surface_create(room_width,room_height);
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		surfaceUpdate	= false;
		surface_set_target(surfaceBackgrond);
		draw_clear_alpha(c_black,1);
		
		draw_set_alpha(1);
		draw_circle_color(room_width/2,room_height/2,room_width/1.5,centerColor,edgeColor,false);
		
		//draw_set_alpha(0.2);
		//draw_set_color(c_black);
		//draw_rectangle_color(868,0,room_width,room_height,c_black,c_black,c_black,c_black,false);
		
		draw_sprite_ext(spr_listview_mask,0,0,0,room_width/sprite_get_width(spr_listview_mask),60/sprite_get_height(spr_listview_mask),0,c_white,0.3);
		
		draw_set_alpha(1);
		surface_reset_target();
	}
	draw_surface_ext(surfaceBackgrond,0,0,1,1,0,c_white,1);
};

#region instance
instance_create_layer(17,70,"inventory_manager",obj_rm_character_inventory_manager);
instance_create_layer(33,600,"inventory_manager",obj_rm_character_inventory_filter_button);
instance_create_layer(0,0,"resource",obj_utility_resource_display_control);
instance_create_layer(0,0,"resource",obj_utility_room_back_button);
instance_create_layer(0,0,"particleBackground",obj_system_particle_cloud_square);
#endregion

