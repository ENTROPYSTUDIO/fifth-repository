// Inherit the parent event
event_inherited();
#region prowl
if(structState.roomIn)
{
	if(structState.target == noone)
	{
		//var _target	= noone;
		//if(place_meeting(xprevious,yprevious,obj_parent_player))
		//{
		//	var _inst	= instance_place(xprevious,yprevious,obj_parent_player);
		//	if(_inst != noone)
		//	if(!_inst.structState.die)
		//	{
		//		structState.target	= _inst;
		//		_target	= _inst;
		//	}
		//}
		if(current_time > structState.prowlTime)
		{
			structState.prowlTime	= current_time+structState.prowlDelay;
			if(random(100) < 90)
			{
				var _roomInPointX	= random_range(0,room_width);
				var _roomInPointY	= random_range(global.roomTop,room_height);
				var _dir = point_direction(x,y,_roomInPointX,_roomInPointY);
				structState.hsp = lengthdir_x(irandom_range(1,structState.prowlSpeed), _dir);
				structState.vsp = lengthdir_y(irandom_range(1,structState.prowlSpeed), _dir);
			}
			else
			{
				structState.hsp	= 0;
				structState.vsp	= 0;
			}
		}
	}
}
else
{
	if(distance_to_point(roomInPointX,roomInPointY) < 100)
	{
		structState.roomIn	= true;
	}
	else
	{
		var _dir = point_direction(x,y,roomInPointX,roomInPointY);
		//motion_set(_dir,structAbility.moveSpeed*3);
		structState.hsp = lengthdir_x(structAbility.moveSpeed*3, _dir);
		structState.vsp = lengthdir_y(structAbility.moveSpeed*3, _dir);
	}
}
#endregion

if(structState.die)
{
	image_alpha -= 0.005;
}
if(image_alpha < 0)
{
	instance_destroy();
}