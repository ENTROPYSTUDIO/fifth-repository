// Inherit the parent event
event_inherited();

if(structState.roomIn)
{
	structState.applySpeed(true);

}
else
{
	structState.applySpeed(false);
}
