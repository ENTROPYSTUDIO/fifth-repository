#macro ex_camera_arguments_undefined ""
#macro ex_camera_draw_target_event 0
#macro ex_camera_draw_target_gui 1

#macro mac_list_item_touch_delay 1000
#macro mac_list_item_touch_distance 12

#macro mac_hit_duration	5
#macro mac_run_or_walk 7
#macro mac_animation_tired 0.3

#macro Scroll_K_transfer 4

#macro mac_default_font_scale 1

//userdata
#macro mac_equip_character_key_prefix "character_"
#macro mac_equip_pet_key_prefix "pet_"
#macro mac_equip_unique_key_prefix "unique_"


//firebase
#macro mac_firebase_ref_my_uid firebase_ref(["main","users",global.firebaseUid])

//listview
#macro mac_listview_drag_distance_min 8
#macro mac_listview_drag_time_min 3
#macro mac_listview_move_inter 0.15
#macro mac_listview_move_speed 0.03