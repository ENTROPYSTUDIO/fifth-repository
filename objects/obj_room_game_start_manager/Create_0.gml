//set_resolution_device(MAC_GAME_BASE_WIDTH,false,false,false,1);
event_user(2);
methodGameSetting();
method_setting_os_device();
instance_create_layer(0,0,"manager",obj_firebase_rtdb_listener_child_callback);
instance_create_layer(0,0,"manager",obj_firebase_rtdb_node_callback);
instance_create_layer(0,0,"manager",obj_system_camera);

if(global.debugMode)
{
	//instance_create_depth(0,0,0,obj_debug_input_test);
}

debugLoad	= function() {
	global.languageCode	= os_get_language();
	global.serverTimestamp	= method_function_time_datetime_to_timestamp(date_current_datetime());
	
	var _fileName	= "gameText.json";
	var _jsonString	= "";
	if(file_exists(_fileName))
	{
		var _open_file	= file_text_open_read(_fileName);
		while(!file_text_eof(_open_file))
		{
			_jsonString	+= file_text_readln(_open_file);
		}
		file_text_close(_open_file);
	
		global.structGameText = json_parse(_jsonString);
	}
	
	var _start	= get_timer();
	var _fileName	= "gameMeta.json";
	var _jsonString	= "";
	if(file_exists(_fileName))
	{
		var _open_file	= file_text_open_read(_fileName);
		while(!file_text_eof(_open_file))
		{
			_jsonString	+= file_text_readln(_open_file);
		}
		file_text_close(_open_file);
	
		global.structGameMeta = json_parse(_jsonString);
	}
	show_debug_message(get_timer()-_start);
	
};
debugUserData	= function() {
	global.firebaseUid	= "";
	instance_create_layer(0,0,"manager",obj_system_control);
	global.structUserData	= {
		resource : {
			carrot : "0",
			gold : "0",
			gem : "0"
		},
		inventory : {
			character : {
				a : {
					key : "HERO_7_6",
					attack	: 0,
					defence	: 0,
					critical	: 0,
					criticalPower : 0,
					evasion	: 0,
					healthPointMax	: 0,
					drainHpChance : 0,
					drainHpRatio : 0,
					
					healthWeight : 1,
					state : "idle",
					equip : "character_0"
				}
			},
			pet : {
				a : {
					grade : "8",
					sprite : "SPR_PET_"+string(irandom(14)),
					equip : "pet_0",
				},
				b : {
					grade : "8",
					sprite : "SPR_PET_"+string(irandom(14)),
					equip : "pet_1",
				},
				c : {
					grade : "8",
					sprite : "SPR_PET_"+string(irandom(14)),
					equip : "pet_2",
				},
			},
			unique : {
				uid : {
					equip : "a"
				}
			}
		},
		equip : {
			character : {
				character_0 : "a",
				character_1 : "lock",
				character_2 : "lock",
				character_3 : "lock",
				character_4 : "lock",
				character_5 : "lock"
			},
			unique : {
				unique_0: "lock",
				unique_1 : "lock",
				unique_2 : "lock",
				unique_3 : "lock",
				unique_4 : "lock",
				unique_5 : "lock"
			},
			pet : {
				pet_0 : "a",
				pet_1 : "b",
				pet_2 : "c",
				pet_3 : "lock",
				pet_4 : "lock",
				pet_5 : "lock"
			}
		},
		statistics : {
			adView : 0,
			playTime : 0,
			iapPurchase : 0,
			iapTotal : 0
		},
		option : {
			screenShake : 1,
			bgm : 1,
			sfx : 1
		},
		timestamp : {
			equip : {
				character : {
					a : 0,
					b : 0,
					c : 0,
					d : 0,
					e : 0,
					f : 0
				}
			}
		},
		profile: {
			nickname : "anon",
			linkProvider : "firebase",
			registerProvider : "firebase",
			buildConfig : "Default",
			region : "KR",
			language: "ko"
	    },
		progress:{
			stage :{
				nowStage : "stage_1",
				stage_0 : true
			}
		}
	};
}

if(os_get_config() == "pc")
{
	debugLoad();
	debugUserData();
	method_function_room_move(ROOM_CHARACTER_INVENTORY,false);
}
else
{
	gameStartProcess	= function(_completeProcess) {
		switch(_completeProcess)
		{
			case "start" : 
			{
				instance_create_depth(0,0,0,obj_game_data_fetch);
				break;
			}
			case "fetch" :
			{
				debugLoad();
				instance_create_depth(0,0,0,obj_auth_game_start);
				break;
			}
			case "login" :
			{
				instance_create_layer(0,0,"manager",obj_system_control);
				break;
			}
			case "loadUserData" :
			{
				method_function_room_move(ROOM_CHARACTER_INVENTORY,false);
			}
		}
	}
	
	gameStartProcess("start");
}


