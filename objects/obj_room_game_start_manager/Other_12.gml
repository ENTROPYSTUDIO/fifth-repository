global.userCharacter	= noone;
global.serverTimestamp	= 0;
global.roomTop	= 0;
global.structUserData	= {};
global.windowLayer	= enumWindowLayer.idle;
global.fontMainMedium	= font_language_medium_ko;
global.fontMainBold	= font_language_bold_ko;
global.structHumanAnimation	= {
	none : { 
		priority : 0,
		imageSpeed	: "default",
		standing : false,
		orientation : false,
		animation : ""
	},
	defaultIdle : { 
		priority : 0,
		imageSpeed	: "default",
		standing : false,
		orientation : false,
		animation : "DEFAULT_IDLE"
	},
	walk : { 
		priority : 100,
		imageSpeed	: "default",
		standing : false,
		orientation : true,
		animation : function() {
			return choose("WALK_1","WALK_2");
		}
	},
	run : { 
		priority : 100,
		imageSpeed	: "default",
		standing : false,
		orientation : true,
		animation : function() {
			return choose("RUN_1","RUN_2");
		}
	},
	idle : { 
		priority : 100,
		imageSpeed	: "default",
		standing : false,
		orientation : true,
		animation : function() {
			return "IDLE_1";
		}
	},
	tired : { 
		priority : 100,
		imageSpeed	: "default",
		standing : false,
		orientation : true,
		animation : function() {
			return "TIRED";
		}
	},
	hit : { 
		priority : 200,
		imageSpeed	: "attackSpeed",
		standing : false,
		orientation : false,
		animation : function() {
			return choose("HIT_1","HIT_1");
		}
	},
	defaultAttack : { 
		priority : 300,
		imageSpeed	: "attackSpeed",
		standing : true,
		orientation : false,
		animation : function() {
			return choose("ATTACK_1","ATTACK_2","ATTACK_3","ATTACK_4","ATTACK_5");
		}
	},
	die : { 
		priority : 600,
		imageSpeed	: "default",
		standing : true,
		orientation : false,
		animation : function() {
			return "DIE";
		}
	},
	dieIdle : { 
		priority : 1000,
		imageSpeed	: "default",
		standing : true,
		orientation : false,
		animation : function() {
			return "DIE_IDLE";
		}
	},
	bigHit : { 
		priority : 400,
		imageSpeed	: "default",
		standing : true,
		orientation : false,
		animation : function() {
			return "BIG_HIT";
		}
	},
	revive : { 
		priority : 500,
		imageSpeed	: "default",
		standing : true,
		orientation : false,
		animation : function() {
			return "REVIVE";
		}
	}
}
global.structPetAnimation	= {
	none : { 
		priority : 0,
		imageSpeed	: "default",
		standing : false,
		orientation : false,
		animation : ""
	},
	walk : { 
		priority : 100,
		imageSpeed	: "default",
		standing : false,
		orientation : true,
		animation : function() {
			return "WALK_1";
		}
	},
	idle : { 
		priority : 100,
		imageSpeed	: "default",
		standing : false,
		orientation : true,
		animation : function() {
			return "IDLE_1";
		}
	},
	hit : { 
		priority : 200,
		imageSpeed	: "default",
		standing : false,
		orientation : false,
		animation : function() {
			return "HIT_1";
		}
	},
	defaultAttack : { 
		priority : 300,
		imageSpeed	: "default",
		standing : true,
		orientation : false,
		animation : function() {
			return "ATTACK_1";
		}
	},
	die : { 
		priority : 600,
		imageSpeed	: "default",
		standing : true,
		orientation : false,
		animation : function() {
			return "DIE_1";
		}
	},
	dieIdle : { 
		priority : 1000,
		imageSpeed	: "default",
		standing : true,
		orientation : false,
		animation : function() {
			return "DIE_IDLE";
		}
	},
	revive : { 
		priority : 500,
		imageSpeed	: "default",
		standing : true,
		orientation : false,
		animation : function() {
			return "REVIVE";
		}
	}
};

global.arrayStaticAbility	= ["attackSpeed","moveSpeed","multipleTarget","aggroRange","luck"];
global.arrayMutableAbility	= ["attack","healthPointMax","defence","drainHpChance","drainHpRatio","critical","criticalPower","evasion"];

var _spriteGrade	= 0;
var _spriteIndex	= 0;
var _arrayIndex		= 0;
while(true)
{
	var _spriteName	= "SPR_CHARACTER_GRADE_"+string(_spriteGrade)+"_"+string(_spriteIndex);
	var _assetIndex	= asset_get_index(_spriteName);
	if(_assetIndex > -1)
	{
		global.characterSpriteTotal[_arrayIndex]	= _assetIndex;
		_spriteIndex++;
		_arrayIndex++;
	}
	else if(_spriteIndex == 0)
	{
		break;
	}
	else
	{
		_spriteGrade++;
		_spriteIndex	= 0;
	}
}

show_debug_message(global.characterSpriteTotal);










