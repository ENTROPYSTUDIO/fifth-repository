enum enumFbReturnDataType
{
	long,
	real,
	string,
	json
}

enum enumObjectType
{
	human,
	pet,
	loot
}

enum enumWindowLayer
{
	idle,
	guiMenu,
	popupQuestion,
	teamEntry,
	characterSell,
	characterRecovery,
	characterResurrection,
	slotMenu,
	stagePause,
	cloudFunction,
	callbackWaiting
}

enum enumColor
{
	goldBrown	= $a4bed1,
	btBrown	= $3b4657,
	navy	= $5b4840,
	skyBlue = $ff7200
}

enum enumListDragOrientation {
	vertical	= 1,
	inverseVertical	= -1,
	horizon	= 1,
	inverseHorizon	= -1,
	typeVertical	= 0,
	typeHorizon		= 1
}