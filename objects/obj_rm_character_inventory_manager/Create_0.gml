enum enumGridCharacterInventory {
	instance,
	equip,
	grade,
	attack,
	defence,
	critical,
	criticalPower,
	evasion,
	healthPointMax,
	drainHpChance,
	drainHpRatio,
	healthWeight,
	state,
	avatar,
	avatarSub,
	avatarScale,
	starIcon,
	starIconScale,
	spineSprite,
	uid,
	legendKey,
	size
};

if(true)//os_get_config() == "pc")
{
	var _multiUpdate	= new firebaseRtdbUpdateMultiPathConstructor();
	
	var _character	= global.structUserData.inventory.character;
	var _names	= variable_struct_get_names(_character);
	if(array_length(_names) > 0 && array_length(_names) < 30)
	{
		var _struct	= global.structUserData.inventory.character[$ _names[0]];
		var _arrPath	= [];
		var _arrValue	= [];
		repeat(3)
		{
			var _ranGrade	= string(irandom(9));
			var _ranIndex	= string(irandom(6));
			var _newKey	= "GRADE_"+_ranGrade+"_"+string(get_timer()+irandom(99999));

			var _names	= variable_struct_get_names(_struct);
			variable_struct_set(global.structUserData.inventory.character,_newKey,{});
			for(var _i=0; _i<array_length(_names); _i++;)
			{
				var _key	= _names[_i];
				variable_struct_set(global.structUserData.inventory.character[$ _newKey],_key,_struct[$ _key]);
			}
			var _state	= choose("idle","dead");
			global.structUserData.inventory.character[$ _newKey][$ "key"]	= "HERO_"+_ranGrade+"_"+_ranIndex;
			global.structUserData.inventory.character[$ _newKey][$ "state"]	= _state;
			global.structUserData.inventory.character[$ _newKey][$ "equip"]	= "none";
			
			global.structUserData.inventory.character[$ _newKey][$ "healthWeight"]	= random(1);
			if(_state == "dead")
			{
				global.structUserData.inventory.character[$ _newKey][$ "resurrectionTime"]	
				= global.serverTimestamp+method_struct_get_deep_value(global.structGameMeta,["heroSetting",mac_equip_character_key_prefix+_ranGrade,"resurrectionTime"]);
				
				global.structUserData.inventory.character[$ _newKey][$ "healthWeight"]	
				= 0;
			}
			
			_multiUpdate.add("/inventory/character/"+_newKey,global.structUserData.inventory.character[$ _newKey]);
			//array_push(_arrPath,"/inventory/character/"+_newKey);
			//array_push(_arrValue,global.structUserData.inventory.character[$ _newKey]);
		}
		if(_multiUpdate.existsCheck())
		{
			var _json	= _multiUpdate.getJson();
			firebase_rtdb_node_builder(mac_firebase_ref_my_uid,_json,false,false,true,undefined,undefined);
			delete(_multiUpdate);
		}
	}
}

global.gridCharInventory	= noone;
global.inventorySelectedIndex	= 0;
global.inventoryNowFilter	= enumGridCharacterInventory.equip;

myWindowLayer	= enumWindowLayer.idle;
surfaceWidth	= 380;
surfaceHeight	= 500;

inventoryLeft	= x;
inventoryTop		= y;
inventoryRight	= x+surfaceWidth;
inventoryBottom	= y+surfaceHeight;
inventoryDrag	= false;
#region constructInventoryGrid
constructInventoryGrid	= function() constructor {
	global.gridCharInventory	= noone; 
	objectIndex	= obj_rm_character_inventory_item;

	static inventoryCreate	= function() {
		//clearInventory(global.gridCharInventory);
		
		var _structCharacter	= global.structUserData.inventory.character;
		var _names	= variable_struct_get_names(_structCharacter);
		var _size	= array_length(_names);
		global.gridCharInventory	= ds_grid_create(enumGridCharacterInventory.size,_size);
		for(var _i=0; _i<_size; ++_i)
		{
			var _key	= _names[_i];
			var _data	= _structCharacter[$ _key];
			var _grade	= character_get_static_ability(_key,"grade","string");
			var _index	= character_get_static_ability(_key,"index","string");
			
			global.gridCharInventory[# enumGridCharacterInventory.grade,_i]	= _grade;
			global.gridCharInventory[# enumGridCharacterInventory.attack,_i]	= character_get_mutable_ability(_key,"attack","battle");
			global.gridCharInventory[# enumGridCharacterInventory.defence,_i]	= character_get_mutable_ability(_key,"defence","battle");
			global.gridCharInventory[# enumGridCharacterInventory.critical,_i]	= character_get_mutable_ability(_key,"critical","battle");
			global.gridCharInventory[# enumGridCharacterInventory.criticalPower,_i]	= character_get_mutable_ability(_key,"criticalPower","battle");
			global.gridCharInventory[# enumGridCharacterInventory.evasion,_i]	= character_get_mutable_ability(_key,"evasion","battle");
			global.gridCharInventory[# enumGridCharacterInventory.healthPointMax,_i]	= character_get_mutable_ability(_key,"healthPointMax","battle");
			global.gridCharInventory[# enumGridCharacterInventory.drainHpChance,_i]	= character_get_mutable_ability(_key,"drainHpChance","battle");
			global.gridCharInventory[# enumGridCharacterInventory.drainHpRatio,_i]	= character_get_mutable_ability(_key,"drainHpRatio","battle");
			
			global.gridCharInventory[# enumGridCharacterInventory.healthWeight,_i]	= _data[$ "healthWeight"];
			
			var _inst	= instance_create_layer(0,0,"inventory_item",objectIndex);
			global.gridCharInventory[# enumGridCharacterInventory.instance,_i]	= _inst;
			
			global.gridCharInventory[# enumGridCharacterInventory.avatar,_i]	= asset_get_index("SPR_CHARACTER_HEAD_GRADE_"+_grade);
			global.gridCharInventory[# enumGridCharacterInventory.avatarSub,_i]	= _index;
			global.gridCharInventory[# enumGridCharacterInventory.avatarScale,_i]	= 72/sprite_get_height(global.gridCharInventory[# enumGridCharacterInventory.avatar,_i]);
			global.gridCharInventory[# enumGridCharacterInventory.starIcon,_i]	= asset_get_index("spr_character_star_grade_"+_grade);
			global.gridCharInventory[# enumGridCharacterInventory.starIconScale,_i]	= 19/sprite_get_height(global.gridCharInventory[# enumGridCharacterInventory.starIcon,_i]);
			global.gridCharInventory[# enumGridCharacterInventory.spineSprite,_i]	= asset_get_index("SPR_CHARACTER_GRADE_"+_grade+"_"+_index);
			global.gridCharInventory[# enumGridCharacterInventory.uid,_i]	= _key;
			global.gridCharInventory[# enumGridCharacterInventory.legendKey,_i]	= _data[$ "key"];
			global.gridCharInventory[# enumGridCharacterInventory.equip,_i]	= _data[$ "equip"];
			global.gridCharInventory[# enumGridCharacterInventory.state,_i]	= _data[$ "state"];
		}
	};
	
	static inventoryFiltering	= function(_alignment) {
		var _nowItem	= ds_grid_get(global.gridCharInventory,enumGridCharacterInventory.instance,global.inventorySelectedIndex);
		var _asending	= _alignment != enumGridCharacterInventory.equip ? false : true;
		ds_grid_sort(global.gridCharInventory,_alignment,_asending);
		itemAlignmentPosition();
		
		if(instance_exists(_nowItem))
		{
			global.inventorySelectedIndex	= ds_grid_value_y(global.gridCharInventory,enumGridCharacterInventory.instance,0,enumGridCharacterInventory.instance,
			ds_grid_height(global.gridCharInventory)-1,_nowItem);
		}
		else
		{
			global.inventorySelectedIndex	= 0;
		}
		
		var _lastInst	= ds_grid_get(global.gridCharInventory,enumGridCharacterInventory.instance,ds_grid_height(global.gridCharInventory)-1);
		with(obj_rm_character_inventory_manager)
		{
			inventoryDrag	= _lastInst.bbox_bottom > inventoryBottom;
			surfaceUpdate	= true;
		}
	};
	
	static inventoryUpdateIndividual	= function(_indexY){
		var _structCharacter	= global.structUserData.inventory.character;
		var _key	= global.gridCharInventory[# enumGridCharacterInventory.uid,_indexY];
		if(!is_undefined(_key))
		{
			var _data	= _structCharacter[$ _key];
			var _grade	= character_get_static_ability(_key,"grade","string");
			var _index	= character_get_static_ability(_key,"index","string");
			
			global.gridCharInventory[# enumGridCharacterInventory.grade,_indexY]	= _grade;
			global.gridCharInventory[# enumGridCharacterInventory.attack,_indexY]	= character_get_mutable_ability(_key,"attack","battle");
			global.gridCharInventory[# enumGridCharacterInventory.defence,_indexY]	= character_get_mutable_ability(_key,"defence","battle");
			global.gridCharInventory[# enumGridCharacterInventory.critical,_indexY]	= character_get_mutable_ability(_key,"critical","battle");
			global.gridCharInventory[# enumGridCharacterInventory.criticalPower,_indexY]	= character_get_mutable_ability(_key,"criticalPower","battle");
			global.gridCharInventory[# enumGridCharacterInventory.evasion,_indexY]	= character_get_mutable_ability(_key,"evasion","battle");
			global.gridCharInventory[# enumGridCharacterInventory.healthPointMax,_indexY]	= character_get_mutable_ability(_key,"healthPointMax","battle");
			global.gridCharInventory[# enumGridCharacterInventory.drainHpChance,_indexY]	= character_get_mutable_ability(_key,"drainHpChance","battle");
			global.gridCharInventory[# enumGridCharacterInventory.drainHpRatio,_indexY]	= character_get_mutable_ability(_key,"drainHpRatio","battle");
		
			global.gridCharInventory[# enumGridCharacterInventory.healthWeight,_indexY]	= _data[$ "healthWeight"];
			global.gridCharInventory[# enumGridCharacterInventory.equip,_indexY]	= _data[$ "equip"];
			global.gridCharInventory[# enumGridCharacterInventory.state,_indexY]	= _data[$ "state"];
			
			if(_indexY == global.inventorySelectedIndex)
			{
				with(obj_rm_character_inventory_info) 
				{
					infoUpdate();
					surfaceUpdate	= true;
				}
			}
			other.surfaceUpdate	= true;
		}
	};
	
	static itemAlignmentPosition	= function() {
		var _getFunction	= coordiGet;
		with(objectIndex)
		{
			var _itemIndex	= ds_grid_value_y(global.gridCharInventory,enumGridCharacterInventory.instance,0,enumGridCharacterInventory.instance,ds_grid_height(global.gridCharInventory)-1,id);
			var _coordi	= _getFunction(_itemIndex);
			x	= _coordi[0];
			y	= _coordi[1];
			itemIndex	= _itemIndex;
		}
	};
	
	static coordiGet	= function(_index) {
		static startX	= 72;
		static startY	= 143;
		static interX	= 90;
		static interY	= 120;
		static maxColumn	= 4;
		
		var _column	= _index mod maxColumn;
		var _row	= _index div maxColumn;
		return [startX+(interX*_column),startY+(interY*_row)];
	};
	
	static clearInventory	= function(_gridInventory) {
		if(_gridInventory != noone) 
		{ 
			ds_grid_destroy(_gridInventory);
			with(obj_rm_character_inventory_parent_item)
			{
				instance_destroy();
			}
			global.gridCharInventory	= noone;
		}
	};
	
	static inventoryReset	= function(){
		global.inventorySelectedIndex	= 0;
		clearInventory(global.gridCharInventory);
		inventoryCreate();
		inventoryFiltering(global.inventoryNowFilter);
		var _firstInst	= ds_grid_get(global.gridCharInventory,enumGridCharacterInventory.instance,0);
		with(_firstInst)
		{
			global.inventorySelectedIndex	= noone;
			forceInput	= true;
			touchEvent();
		}
	};
	
	inventoryCreate();
	inventoryFiltering(enumGridCharacterInventory.equip);
};
#endregion

deadChecker	= function(){
	static checkDealy	= 10000;
	static checkTime	= 0;
	static goalCheckMargin	= 1000;
	
	if(current_time < checkTime) { return 0; }
	checkTime	= current_time+checkDealy;
	var _size	= ds_grid_height(global.gridCharInventory);
	var _arrPath	= [];
	var _arrValue	= [];
	var _multiUpdate	= new firebaseRtdbUpdateMultiPathConstructor();
	for(var _i=0; _i<_size; ++_i)
	{
		var _myKey	= global.gridCharInventory[# enumGridCharacterInventory.uid,_i];
		var _goalTime	= method_struct_get_deep_value(global.structUserData,["inventory","character",_myKey,"resurrectionTime"]);
		var _state	= method_struct_get_deep_value(global.structUserData,["inventory","character",_myKey,"state"]);
		if(!is_undefined(_goalTime) && _state == "dead")
		{
			if(global.serverTimestamp > _goalTime+goalCheckMargin)
			{
				_multiUpdate.add("inventory/character/"+_myKey+"/resurrectionTime",0);
				_multiUpdate.add("inventory/character/"+_myKey+"/state","idle");
				_multiUpdate.add("inventory/character/"+_myKey+"/healthWeight",1);
				
				//array_push(_arrPath,
				//"inventory/character/"+_myKey+"/resurrectionTime",
				//"inventory/character/"+_myKey+"/state",
				//"inventory/character/"+_myKey+"/healthWeight");
				
				//array_push(_arrValue,
				//0,
				//"idle",
				//1);
				
				global.structUserData.inventory.character[$ _myKey].resurrectionTime	= 0;
				global.structUserData.inventory.character[$ _myKey].state	= "idle";
				global.structUserData.inventory.character[$ _myKey].healthWeight	= 1;
				
				structInventory.inventoryUpdateIndividual(_i);
			}
		}	
	}
	
	if(_multiUpdate.existsCheck())
	{
		//method_show_debug(["_arrPath",_arrPath]);
		//method_show_debug(["_arrValue",_arrValue]);
		//var _updateStruct	= new firebaseRtdbUpdateMultiPathConstructor(_arrPath,_arrValue);
		var _json	= _multiUpdate.getJson();
		firebase_rtdb_node_builder(mac_firebase_ref_my_uid,_json,false,false,true,undefined,undefined);
		delete(_multiUpdate);
	}
};

#region constructDraw
surfaceInventory	= noone;
surfaceUpdate	= false;
drawMain	= function() {
	if(!surface_exists(surfaceInventory))
	{
		surfaceInventory	= surface_create(surfaceWidth,surfaceHeight);
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		method_show_debug(["surfaceUpdate"]);
		surfaceUpdate	= false;
		surface_set_target(surfaceInventory);
		draw_clear_alpha(c_black,0);
		
		var _inventoryLeft	= inventoryLeft;
		var _inventoryTop	= inventoryTop;
		var _inventoryRight	= inventoryRight;
		var _inventoryBottom	= inventoryBottom;
		with(obj_rm_character_inventory_parent_item)
		{
			visible	= false;
			var _in	= bbox_bottom > _inventoryTop && bbox_top < _inventoryBottom;
			if(_in)
			{
				visible	= true;
				var _avatar	= global.gridCharInventory[# enumGridCharacterInventory.avatar,itemIndex];
				var _avatarSub	= global.gridCharInventory[# enumGridCharacterInventory.avatarSub,itemIndex];
				var _avatarScale	= global.gridCharInventory[# enumGridCharacterInventory.avatarScale,itemIndex];
				var _starIcon	= global.gridCharInventory[# enumGridCharacterInventory.starIcon,itemIndex];
				var _starScale	= global.gridCharInventory[# enumGridCharacterInventory.starIconScale,itemIndex];
				var _equip	= global.gridCharInventory[# enumGridCharacterInventory.equip,itemIndex];
				var _state	= global.gridCharInventory[# enumGridCharacterInventory.state,itemIndex];
				var _grade	= global.gridCharInventory[# enumGridCharacterInventory.grade,itemIndex];
				
				if(global.inventorySelectedIndex == itemIndex)
				{
					var _color	= c_white;
					//if(other.structGradeColor[$ "grade_"+_grade] == $ffffff)
					//{
					//	//_color	= c_lime;
					//}
					shader_set(shd_hit_flash_default);
					draw_sprite_ext(sprite_index,0,x-other.x,y-other.y,1.1,1.1,0,c_white,1);
					shader_reset();
						
					if(_state == "dead") { shader_set(shd_grayscale); }
					draw_sprite_ext(_avatar,_avatarSub,
					x-other.x,
					y-other.y-8,_avatarScale+0.1,_avatarScale+0.1,0,c_white,1);
						
					draw_sprite_ext(_starIcon,0,
					x-other.x,
					y-other.y+43,_starScale,_starScale,0,c_white,1);
					if(_state == "dead") { shader_reset(); }

				}
				else
				{
					draw_sprite_ext(sprite_index,0,x-other.x,y-other.y,1,1,0,other.structGradeColor[$ "grade_"+_grade],1);
					if(_state == "dead") { shader_set(shd_grayscale); }
					
					draw_sprite_ext(_avatar,_avatarSub,
					x-other.x,
					y-other.y-10,_avatarScale,_avatarScale,0,c_white,1);
					
					draw_sprite_ext(_starIcon,0,
					x-other.x,
					y-other.y+39,_starScale,_starScale,0,c_white,1);
					if(_state == "dead") { shader_reset(); }

				}
						
				if(_equip != "none")
				{
					draw_sprite_ext(spr_character_inventory_team_flag,0,bbox_left-other.x+14,bbox_top-other.y+14,0.8,0.8,0,c_white,1);
				}
			}
		}
		
		surface_reset_target();
		
		//surfaceUpdate	= false;
		//var _listCollision	= ds_list_create();
		//collision_rectangle_list(inventoryLeft,inventoryTop,inventoryRight,inventoryBottom,obj_rm_character_inventory_parent_item,false,true,_listCollision,false);
			
		//surface_set_target(surfaceInventory);
		//draw_clear_alpha(c_black,0);
		//var _size	= ds_grid_height(global.gridCharInventory);
		
		//for(var _i=0; _i<_size; ++_i)
		//{
		//	var _inst	= global.gridCharInventory[# enumGridCharacterInventory.instance,_i];
		//	if(ds_list_find_index(_listCollision,_inst) != -1)
		//	{
		//		with(_inst)
		//		{
		//			visible	= true;
		//			var _avatar	= global.gridCharInventory[# enumGridCharacterInventory.avatar,_i];
		//			var _avatarSub	= global.gridCharInventory[# enumGridCharacterInventory.avatarSub,_i];
		//			var _avatarScale	= global.gridCharInventory[# enumGridCharacterInventory.avatarScale,_i];
		//			var _starIcon	= global.gridCharInventory[# enumGridCharacterInventory.starIcon,_i];
		//			var _starScale	= global.gridCharInventory[# enumGridCharacterInventory.starIconScale,_i];
		//			var _equip	= global.gridCharInventory[# enumGridCharacterInventory.equip,_i];
		//			var _grade	= global.gridCharInventory[# enumGridCharacterInventory.grade,_i];
					
		//			if(global.inventorySelectedIndex == _i)
		//			{
		//				var _color	= c_white;
		//				if(other.structGradeColor[$ "grade_"+_grade] == $ffffff)
		//				{
		//					//_color	= c_lime;
		//				}
		//				shader_set(shd_hit_flash_default);
		//				draw_sprite_ext(sprite_index,0,x-other.x,y-other.y,1.1,1.1,0,_color,1);
		//				shader_reset();
						
		//				draw_sprite_ext(_avatar,_avatarSub,
		//				x-other.x,
		//				y-other.y-8,_avatarScale+0.1,_avatarScale+0.1,0,c_white,1);
						
		//				draw_sprite_ext(_starIcon,0,
		//				x-other.x,
		//				y-other.y+43,_starScale,_starScale,0,c_white,1);
						
		//				if(_equip != "none")
		//				{
		//					draw_sprite_ext(spr_character_inventory_team_flag,0,bbox_left-other.x+16,bbox_top-other.y+16,0.8,0.8,0,c_white,1);
		//				}
		//			}
		//			else
		//			{
		//				draw_sprite_ext(sprite_index,0,x-other.x,y-other.y,1,1,0,other.structGradeColor[$ "grade_"+_grade],1);

		//				draw_sprite_ext(_avatar,_avatarSub,
		//				x-other.x,
		//				y-other.y-10,_avatarScale,_avatarScale,0,c_white,1);
					
		//				draw_sprite_ext(_starIcon,0,
		//				x-other.x,
		//				y-other.y+39,_starScale,_starScale,0,c_white,1);
						
		//				if(_equip != "none")
		//				{
		//					draw_sprite_ext(spr_character_inventory_team_flag,0,bbox_left-other.x+16,bbox_top-other.y+16,0.8,0.8,0,c_white,1);
		//				}
		//			}
		//		}
		//	}
		//	else
		//	{
		//		with(_inst)
		//		{
		//			visible	= false;
		//		}
		//	}
		//}
		
		//surface_reset_target();
		//ds_list_destroy(_listCollision);
	}
	
	draw_surface(surfaceInventory,x,y);
}
#endregion

structInventory	= new constructInventoryGrid();
structDrag	= new constructListViewDrag(enumListDragOrientation.typeVertical,64,mac_listview_move_inter,mac_listview_move_speed);
structGradeColor	= new constructGradeColor();

#region initInfo

var _firstInst	= ds_grid_get(global.gridCharInventory,enumGridCharacterInventory.instance,0);
with(_firstInst)
{
	global.inventorySelectedIndex	= noone;
	forceInput	= true;
	touchEvent();
}
#endregion





