normalEnemyCreate	= function(){
	static delay	= global.structGameMeta.stageData.normal.regenDelay;
	static regenNumber	= global.structGameMeta.stageData.normal.regenNumber;
	static regenTotal	= global.structGameMeta.stageData.normal.regenTotal;
	static spriteExhaust	= global.structGameMeta.stageData.normal.spriteExhaust;
	static scaleMin	= global.structGameMeta.stageData.normal.spriteScaleMin;
	static scaleMax	= global.structGameMeta.stageData.normal.spriteScaleMax;
	static spriteLength	= array_length(global.characterSpriteTotal);
	static time		= current_time;
	
	if(current_time > time)
	{
		time	= current_time+delay;
		var _sprMin	= real(string_digits(global.nowStageKey)) mod spriteLength;
		var _sprMax	= clamp(_sprMin+spriteExhaust,0,spriteLength-1);
		repeat(regenNumber)
		{
			var _sprite	= global.characterSpriteTotal[irandom_range(_sprMin,_sprMax)];
			var _scale	= random_range(scaleMin,scaleMax);
			if(random(100) < 50)
			{
				var _inst	= instance_create_depth(random(room_width),room_height+200,0,obj_enemy_normal);
			}
			else
			{
				var _inst	= instance_create_depth(choose(-200,room_width+200),random_range(global.roomTop,room_height),0,obj_enemy_normal);
			}
			with(_inst)
			{
				sprite_index	= _sprite;
				structState	= new constructCharacterState(id,"human","enemy",_scale);
				structAbility	= new constructEnemyAbility(global.nowStageKey,"normal");
			}
			var _chance	= global.structGameMeta.stageData.normal.bossChance;
			bossEnemyCreate(_chance);
			if(instance_number(obj_parent_enemy) > regenTotal) { break; }
		}
	}
};
bossEnemyCreate	= function(_chance){
	static spriteExhaust	= global.structGameMeta.stageData.boss.spriteExhaust;
	static spriteLength	= array_length(global.characterSpriteTotal);
	static scaleMin	= global.structGameMeta.stageData.boss.spriteScaleMin;
	static scaleMax	= global.structGameMeta.stageData.boss.spriteScaleMax;
	if(random(100) < _chance)
	{
		var _sprMin	= real(string_digits(global.nowStageKey)) mod spriteLength;
		var _sprMax	= clamp(_sprMin+spriteExhaust,0,spriteLength-1);
		var _sprite	= global.characterSpriteTotal[irandom_range(_sprMin,_sprMax)];
		var _scale	= random_range(scaleMin,scaleMax);
		if(random(100) < 50)
		{
			var _inst	= instance_create_depth(random(room_width),room_height+200,0,obj_enemy_boss);
		}
		else
		{
			var _inst	= instance_create_depth(choose(-200,room_width+200),random_range(global.roomTop,room_height),0,obj_enemy_boss);
		}
		with(_inst)
		{
			sprite_index	= _sprite;
			structState	= new constructCharacterState(id,"human","enemy",_scale);
			structAbility	= new constructEnemyAbility(global.nowStageKey,"boss");
		}
		_chance	= global.structGameMeta.stageData.boss.bossChance;
		bossEnemyCreate(_chance);
	}
};