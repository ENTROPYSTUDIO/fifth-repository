drawMain	= function(){
	if(!instance_exists(obj_menu_character_recovery_manager)) { return 0; }
	static infoX	= x;
	static infoY	= y;
	static info		= method_struct_get_deep_value(global.structGameText,["common","cost",global.languageCode]);
	
	preSetForDrawText(fa_right,fa_top,c_lime,1,global.fontMainBold);
	draw_text_transformed(infoX,infoY,info,mac_default_font_scale,mac_default_font_scale,0);
	
	static startX	= x;
	static startY	= y+100;
	static interY	= 80;
	static iconScale	= 48;
	static textInterX	= 80;
	
	var _currency	= spr_icon_currency_carrot;
	var _scale	= 48/sprite_get_height(_currency);
	draw_sprite_ext(_currency,0,startX,startY+(0*interY),_scale,_scale,0,c_white,1);

	if(obj_menu_character_recovery_manager.optionAllLegend)
	{
		var _price	= getCharacterRecoveryPrice("");
	}
	else
	{
		var _key	= global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
		var _price	= getCharacterRecoveryPrice(_key);
	}
	_price	= string_number_resource_abbrev(_price,3);
	
	preSetForDrawText(fa_right,fa_middle,c_white,1,global.fontMainBold);
	draw_text_transformed(startX-textInterX,startY+(0*interY),_price,mac_default_font_scale,mac_default_font_scale,0);
};

