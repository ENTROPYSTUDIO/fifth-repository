destroy	= false;
myWindowLayer	= enumWindowLayer.idle;
forceInput	= false;
var _text	= method_struct_get_deep_value(global.structGameText,["common","legendManage",global.languageCode]);
drawText	= new drawTextConstructor(_text,x,y,fa_center,fa_top,global.fontMainBold,mac_default_font_scale-0.2,c_white,1);
drawSprite	= new drawSpriteConstructor(sprite_index,0,x,y,1,0,c_white,1);

structHideAnimation	= new constructBottomGuiHideAnimation();

drawMain	= function(){
	static refY	= obj_room_stage_bottom_gui_manager.y;
	drawSprite.draw(x,y-refY);
	gpu_set_blendenable(false);
	drawText.draw(x,bbox_bottom-refY+5);
	gpu_set_blendenable(true);
};