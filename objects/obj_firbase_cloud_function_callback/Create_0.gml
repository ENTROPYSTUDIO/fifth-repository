alarm[0]	= 120;
beforeLayer	= global.windowLayer;
popupName	= "";
drawEnabled	= false;
lid	= noone;
drawMain	= function() {
	static loadingRot	= 0;
	static rotSwitch = 1;
	if(drawEnabled)
	{	
		draw_set_alpha(0.5);
		draw_set_color(c_black);
		draw_rectangle(0,0,display_get_gui_width(),display_get_gui_height(),false);
		draw_set_alpha(1);
		draw_set_color(c_white);
		draw_rectangle(460,230,820,410,false);
	
		draw_set_halign(fa_center);
		draw_set_valign(fa_top);
		draw_set_font(global.fontMainMedium);
		draw_set_color(enumColor.navy);
		draw_text_transformed(640,250,popupName,mac_default_font_scale,mac_default_font_scale,0);
	
		if(loadingRot > 359 || loadingRot < 0) { rotSwitch *= -1; }
		loadingRot-=4;
		draw_sprite_ext(spr_icon_loading_circle,0,640,375,1,1,loadingRot,c_white,1);
	}
};