// All Async Events for Authentication
var _type,_event,_value,_exists,_listener_id;
if(!ds_exists(async_load,ds_type_map)) { exit; }
_type	= async_load[? "type"];
_event	= async_load[? "event"];
if(is_undefined(_type)) { exit; }

if(_type == Firebase_asyncEvent && _event == "cloudFunctions")
{
	_listener_id		= async_load[? "Id"];
	if(_listener_id == lid)
	{
		instance_destroy();
		_value	= async_load[? "value"];
		_exists	= async_load[? "exists"];
		if(_exists)
		{
			if(!is_undefined(callbackDone) && !is_undefined(_value))
			{
				callbackDone(_value);
			}
		}
		else
		{
			if(!is_undefined(callbackFail) && !is_undefined(_value))
			{
				callbackFail(_value);
			}
		}
		lid	= noone;
	}
}