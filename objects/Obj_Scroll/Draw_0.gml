/// @description Insert description here
// You can write your code in this editor
if(!surface_exists(surf))
{
	surf = surface_create(width,heigth);
}

if(!owner)
{
	//var surf = surface_create(width,heigth);

	surface_set_target(surf);
	draw_clear_alpha(c_black,0);
	draw_set_alpha(1);
	
	var _surfaceLeft	= surfaceLeft;
	var _surfaceTop		= surfaceTop;
	var _surfaceRight	= surfaceRight;
	var _surfaceBottom	= surfaceBottom;
	var _V_H			= V_H;
	
	var _a = 0;
	while(true)
	{
		var _inst = list[| _a];
	
		if(is_undefined(_inst))
		{
			break;
		}
	
		with(_inst)
		{
			if(_V_H == "H")
			{
				if(bbox_left < _surfaceRight && bbox_right > _surfaceLeft)
				{
					draw = true;
					event_perform(ev_draw,0);
				}
				draw = false;
				visible	= false;
			}
			else if(_V_H == "V")
			{
				if(bbox_bottom > _surfaceTop && bbox_top < _surfaceBottom)
				{
					draw = true;
					event_perform(ev_draw,0);
				}
				draw = false;
				visible	= false;
			}
		}
		_a++;
	}
	surface_reset_target();

	draw_surface(surf,x,y);
}
else
{
	if(!draw)
	{
		exit;
	}
	
	method_Scroll_draw_childs(list);
}


