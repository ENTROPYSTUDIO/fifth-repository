asyncProcess	= false;
releaseTime	= "";
drawMain	= function(){
	static text1Scale	= mac_default_font_scale;
	static text2Scale	= mac_default_font_scale-0.2;
	var _text1	= string(method_struct_get_deep_value(global.structGameText,["common","untilResurrection",global.languageCode]));
	var _text2	= releaseTime;
	preSetForDrawText(fa_center,fa_bottom,c_white,1,global.fontMainMedium);
	draw_text_transformed(x,y-25,_text1,text1Scale,text1Scale,0);
	preSetForDrawText(fa_center,fa_bottom,c_white,1,global.fontMainMedium);
	draw_text_transformed(x,y,_text2,text2Scale,text2Scale,0);
};

stepMain	= function() {
	static timeToEmptyDelay	= 1000;
	static timeToEmptyTime	= 0;
	var _myKey	= global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
	var _goalTime	= method_struct_get_deep_value(global.structUserData,["inventory","character",_myKey,"resurrectionTime"]);
	if(!is_undefined(_goalTime))
	{
		releaseTime	= method_function_time_draw_timestamp_to_hms(_goalTime-global.serverTimestamp);
		surfaceUpdate	= true;
	}
};