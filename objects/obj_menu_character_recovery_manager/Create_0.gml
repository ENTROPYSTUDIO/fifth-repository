beforeWindow	= global.windowLayer;
global.windowLayer	= enumWindowLayer.characterRecovery;
surface	= noone;
surfaceUpdate	= false;

optionAllLegend	= false;

init	= function(){
	with(obj_menu_character_recovery_child_parent)
	{
		instance_destroy();
	}
	
	with(obj_rm_character_info_parent)
	{
		visible	= false;
	}
	instance_create_layer(get_x_pos_for_gui_ratio(1198,""),get_y_pos_for_gui_ratio(150),layer,obj_menu_character_recovery_reward_info);
	instance_create_layer(get_x_pos_for_gui_ratio(932,""),get_y_pos_for_gui_ratio(636),"sell_button",obj_menu_character_recovery_button_cancle);
	instance_create_layer(get_x_pos_for_gui_ratio(1136,""),get_y_pos_for_gui_ratio(636),"sell_button",obj_menu_character_recovery_button_confirm);
	instance_create_layer(get_x_pos_for_gui_ratio(1198,""),get_y_pos_for_gui_ratio(568),"sell_button",obj_menu_character_recovery_button_option_all_legend);
	with(instance_create_layer(get_x_pos_for_gui_ratio(250,""),get_y_pos_for_gui_ratio(535),"sell_spine_dummy",obj_menu_character_recovery_spine_dummy))
	{
		sprite_index	= global.gridCharInventory[# enumGridCharacterInventory.spineSprite,global.inventorySelectedIndex];
		image_xscale	= 350/sprite_height;
		image_yscale	= 350/sprite_height;
		skeleton_animation_set("DEFAULT_IDLE");
	}
	instance_create_layer(get_x_pos_for_gui_ratio(250-sprite_get_width(spr_inventory_health_bar_back)/2,""),
	get_y_pos_for_gui_ratio(obj_menu_character_recovery_spine_dummy.bbox_top-50),
	"sell_button",
	obj_menu_character_recovery_health_bar);
	system_shader_create_blur(true,1);
};

drawMain	= function(){
	if(!surface_exists(surface))
	{
		surface	= surface_create(display_get_gui_width(),display_get_gui_height());
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		surfaceUpdate	= false;
		surface_set_target(surface);
		draw_clear_alpha(c_black,0.7);
		
		shader_premultiply(true,undefined,undefined);
		with(obj_menu_character_recovery_reward_info) { drawMain(); }
		with(obj_menu_character_recovery_health_bar) { drawMain(); }
		with(obj_menu_character_recovery_button_confirm) { drawMain(); }
		with(obj_menu_character_recovery_button_cancle) { drawMain(); }
		with(obj_menu_character_recovery_button_option_all_legend) { drawMain(); }
		with(obj_menu_character_recovery_spine_dummy) { drawMain(); }
		shader_premultiply(false,undefined,undefined);

		surface_reset_target();
	}
	
	draw_surface_ext(surface,0,0,1,1,0,c_white,1);
};

cleanUp	= function(){
	system_shader_create_blur(false,1);
	if(surface_exists(surface))
	{
		surface_free(surface);
	}
	with(obj_rm_character_info_parent)
	{
		visible	= true;
	}
	global.windowLayer	= beforeWindow;
};

init();