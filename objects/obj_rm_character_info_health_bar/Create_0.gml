x	= 575;
y	= 130;
	
drawMain	= function(){
	static healthBarWidth	= sprite_get_width(spr_inventory_health_bar_fore);
	static healthBarHeight	= sprite_get_height(spr_inventory_health_bar_fore);
	static textX	= x+10;
	static textY	= y+healthBarHeight/2+1;
	static textScale	= mac_default_font_scale-0.3;
	
	var _ratio	= global.gridCharInventory[# enumGridCharacterInventory.healthWeight,global.inventorySelectedIndex];
	var _barWidth	= healthBarWidth*_ratio;
	
	draw_sprite_ext(sprite_index,0,x,y,1,1,0,c_white,1);
	draw_sprite_part(spr_inventory_health_bar_fore,0,0,0,_barWidth,healthBarHeight,x+1,y+1);
	
	preSetForDrawText(fa_left,fa_middle,c_white,1,global.fontMainBold);
	var _drawText	= string(_ratio*100)+"%";
	draw_text_transformed(textX,textY,_drawText,textScale,textScale,0);
};