myWindowLayer	= enumWindowLayer.characterRecovery;
drawMain	= function(){
	var _starIcon	= global.gridCharInventory[# enumGridCharacterInventory.starIcon,global.inventorySelectedIndex];
	static starIconX	= x;
	static starIconY	= bbox_bottom+40;
	
	var _name		= character_get_name(global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex]);
	static nameX	= x;
	static nameY	= bbox_top-90;
	
	draw_sprite_ext(_starIcon,0,starIconX,starIconY,1,1,0,c_white,1);
	
	preSetForDrawText(fa_center,fa_middle,c_white,1,global.fontMainBold);
	draw_text_transformed(nameX,nameY,_name,mac_default_font_scale,mac_default_font_scale,0);
	
};