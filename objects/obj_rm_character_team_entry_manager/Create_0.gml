global.windowLayer	= enumWindowLayer.teamEntry;
surface	= noone;
surfaceUpdate	= false;
createTeamEntryMenu	= function(){
	with(obj_rm_character_team_entry_slot)
	{
		instance_destroy();
	}
	with(obj_rm_character_team_entry_dummy)
	{
		instance_destroy();
	}
	static startX	= 302;
	static startY	= 524;
	static interX	= 135;
	static insertKey	= global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
	
	#region slot
	var _size	= variable_struct_names_count(global.structUserData.equip.character);
	for(var _i=0; _i<_size; ++_i)
	{
		var _key	= mac_equip_character_key_prefix+string(_i);
		var _characterUid	= global.structUserData.equip.character[$ _key];
		var _inst	= instance_create_layer(startX+(interX*_i),startY,"team_entry_menu_button",obj_rm_character_team_entry_slot)
		with(_inst)
		{
			slotGridIndexY	= ds_grid_value_y(global.gridCharInventory,
			enumGridCharacterInventory.uid,0,enumGridCharacterInventory.uid,
			ds_grid_height(global.gridCharInventory)-1,_characterUid);
			
			slotName	= _key;
			createFunction();
		}
	}
	#endregion
	
	#region dummy
	with(instance_create_layer(640,400,"team_entry_spine_dummy",obj_rm_character_team_entry_dummy))
	{
		sprite_index	= global.gridCharInventory[# enumGridCharacterInventory.spineSprite,global.inventorySelectedIndex];
		image_xscale	= 300/sprite_height;
		image_yscale	= 300/sprite_height;
		skeleton_animation_set("DEFAULT_IDLE");
	}
	#endregion
	
	#region button
	instance_create_layer(640,670,"team_entry_menu_button",obj_rm_character_team_entry_button_close);
	#endregion
	
	#region blur shader
	system_shader_create_blur(true,1);
	#endregion
	
	#region visible back
	with(obj_rm_character_inventory_dummy) { visible	= false; }
	with(obj_rm_character_inventory_info) { surfaceUpdate	= true; }
	#endregion
};


drawMain	= function(){
	if(!surface_exists(surface))
	{
		surface	= surface_create(room_width,room_height);
		surfaceUpdate	= true;
	}
	if(surfaceUpdate)
	{
		surfaceUpdate	= false;
		surface_set_target(surface);
		draw_clear_alpha(c_black,0);
		draw_set_color(c_black);
		draw_set_alpha(0.7);
		draw_rectangle(0,0,room_width,room_height,false);
		draw_set_alpha(1);

		with(obj_rm_character_team_entry_button_close)
		{
			drawMain();
		}
		with(obj_rm_character_team_entry_slot)
		{
			drawMain();
		}
		with(obj_rm_character_team_entry_dummy)
		{
			shader_premultiply(true,bm_one,bm_one);
			drawMain();
			shader_premultiply(false,undefined,undefined);
		}
		
		static infoX	= 640;
		static infoY	= 600;
		static info	= method_struct_get_deep_value(global.structGameText,["common","slotEquipInfo",global.languageCode]);
		static infoScale	= mac_default_font_scale-0.2;
		preSetForDrawText(fa_center,fa_middle,c_white,1,global.fontMainMedium);
		shader_premultiply(true,bm_one,bm_one);
		draw_text_transformed(infoX,infoY,info,infoScale,infoScale,0);
		shader_premultiply(false,undefined,undefined);
		surface_reset_target();
	}
	
	draw_surface_ext(surface,0,0,1,1,0,c_white,1);
};

clearFunction	= function(){
	system_shader_create_blur(false,1);
	if(surface_exists(surface))
	{
		surface_free(surface);
		surface	= noone;
	}

	global.windowLayer	= enumWindowLayer.idle;
	with(obj_rm_character_inventory_dummy) { visible	= true; }
	with(obj_rm_character_inventory_info) { surfaceUpdate	= true; }
};

createTeamEntryMenu();
