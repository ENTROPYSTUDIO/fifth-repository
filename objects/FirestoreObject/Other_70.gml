/// @description Listening to

var category = async_load[? "[category]"];

switch(category)
{
	
	case "firestore_get_document": //When we use firebase_firestore_listen_document
	var status = async_load[? "[status]"];
	
	if (status==1) //Okay
	{
		var document_id, document_data, document_location;
		document_id = async_load[? "[id]"];
		document_data = async_load[? "[data]"];
		document_location = async_load[? "[location]"];
		//text = async_load[? "message"];
		
		//Just for experimenting, let's list all of the keys and assign it to our documentText.
		for (var key = ds_map_find_first(async_load); !is_undefined(key); key = ds_map_find_next(async_load, key)) {
		  var keyVal = async_load[? key];
		  
		  documentText+=string(key)+" : "+string(keyVal)+"\n";
		  
		}
	
	}
	break;
	
	case "firestore_query_document":
	var status = async_load[? "[status]"];
	
	if (status==1) //Okay
	{
		var document_id, document_data, document_location;
		document_id = async_load[? "[id]"];
		document_data = async_load[? "[data]"];
		document_location = async_load[? "[location]"];
		//text = async_load[? "message"];
		
		//Just for experimenting, let's list all of the keys and assign it to our documentText.
		for (var key = ds_map_find_first(async_load); !is_undefined(key); key = ds_map_find_next(async_load, key)) {
		  var keyVal = async_load[? key];
		  
		  documentText+=string(key)+" : "+string(keyVal)+" - ";
		  
		}
		documentText+="\n";
	
	}
	break;
	
}
