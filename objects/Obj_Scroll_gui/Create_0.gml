/// @description Insert description here
// You can write your code in this editor

event_inherited()
surf	= noone;
width = noone;
heigth = noone;

marginX	= 0;
marginY	= 0;
V_H = noone;
Holder_step = noone;
sep = noone;

Back = noone;
Next = noone;
Slider = noone;

//////////////////////////

master = self.id;

Xstart = x;
Ystart = y;

drag_enable = true;

justCreated = true;

Step = noone;

list = noone;

length = noone; // Total lenght of all items
move_lenght = noone; //max move of the "view" (change if is in parallel)
move_lenght_mine = noone; //max move of this specific "view"

parallelObj = noone;

porcent_transfer = 0;
pixels_transfer = 0;

porcent = 0;
pixels = 0;

pressed = false;
press_steps = 0;
dragging_abs = noone;
dragging_x = noone;
dragging_y = noone;
surfaceUpdate	= true;
