if(surface_exists(surf))
{
	surface_free(surf);
}
if(ds_exists(list,ds_type_list))
{
	for(var _i=0; _i<ds_list_size(list); _i++)
	{
		var _inst	= list[| _i];
		if(instance_exists(_inst))
		{
			instance_destroy(_inst);
		}
	}
	ds_list_destroy(list);
}