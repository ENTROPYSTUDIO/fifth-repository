myWindowLayer	= enumWindowLayer.characterSell;
forceInput	= false;
drawMain	= function(){
	static btColor	= enumColor.skyBlue;
	static btName	= method_struct_get_deep_value(global.structGameText,["common","sell",global.languageCode]);
	static nameScale	= mac_default_font_scale-0.2;
	draw_sprite_ext(sprite_index,0,x,y,1,1,0,btColor,1);
	preSetForDrawText(fa_center,fa_middle,c_white,1,global.fontMainBold);
	draw_text_transformed(x,y,btName,nameScale,nameScale,0);
};

touchEvent	= function(){
	if(method_function_input_mouse_pressed_gui_single(id,myWindowLayer,noone) || (myWindowLayer == global.windowLayer && forceInput))
	{
		forceInput	= false;
		if(instance_exists(obj_menu_character_sell_manager))
		{
			var _message	= method_struct_get_deep_value(global.structGameText,["common","itemSellQuestion",global.languageCode]);
			popup_question(_message,questionConfirm,undefined);
		}
	}
};

questionConfirm	= function(){
	//var _popupName	= method_struct_get_deep_value(global.structGameText,["common","waitingResult",global.languageCode]);
	//var _functionData	= new firebase_cloud_function_builder("characterSell",{
	//	itemUid	: global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex],
	//	sameGradeSell : obj_menu_character_sell_manager.optionSameGradeSell
	//});
	
	//beforeHash	= sha1_string_utf8(json_stringify(global.structUserData));
	//firebase_waiting_for_cloud_function_result(id,_functionData,_popupName,cfCallbackDone,cfCallbackFail);
	
	//if(os_get_config() == "pc")
	//{
	//	cfCallbackDone(json_stringify({
	//		success : true,
	//		message : "dd"
	//	}));
	//}
	
	//const itemSellMeta = (await databaseMeta.ref("/gameMetaData/sellReward/" + itemSellMetaKey).once("value")).val();
	var _userInventory	= method_struct_get_deep_value(global.structUserData,["inventory","character"]);
	if(!is_undefined(_userInventory))
	{
		var _sellGrade	= global.gridCharInventory[# enumGridCharacterInventory.grade,global.inventorySelectedIndex];

		var _sellCount	= 0;
		if(obj_menu_character_sell_manager.optionSameGradeSell)
		{
			var _size	= ds_grid_height(global.gridCharInventory);
			for(var _i=0; _i<_size; ++_i)
			{
				var _grade	= global.gridCharInventory[# enumGridCharacterInventory.grade,_i];
				var _equip	= global.gridCharInventory[# enumGridCharacterInventory.equip,_i];
				if(_sellGrade == _grade && _equip == "none")
				{
					var _uid	= global.gridCharInventory[# enumGridCharacterInventory.uid,_i];
					if(variable_struct_exists(_userInventory,_uid))
					{
						variable_struct_remove(_userInventory,_uid);
						firebase_userdata_pipeline_add("inventory/character/"+_uid,{});
						_sellCount++;
					}
				}
			}
		}
		else
		{
			var _uid	= global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
			if(variable_struct_exists(_userInventory,_uid))
			{
				variable_struct_remove(_userInventory,_uid);
				firebase_userdata_pipeline_add("inventory/character/"+_uid,{});
				_sellCount++;
			}
		}
		
		var _meta	= global.structGameMeta.sellReward[$ mac_equip_character_key_prefix+string(_sellGrade)];
		var _names	= variable_struct_get_names(_meta);
		var _namesSize	= array_length(_names);
		for(var _i=0; _i<_namesSize; ++_i)
		{
			var _currency	= _names[_i];
			var _reward	= _meta[$ _currency]*_sellCount;
			var _now	= user_resource_get_real(_currency);
			var _finalResource	= string(_now+_reward);
			global.structUserData.resource[$ _currency]	= _finalResource;
			firebase_userdata_pipeline_add("resource/"+_currency,_finalResource);
		}
		
		firebase_userdata_pipeline_upload();
	}
	
	var _text	= method_struct_get_deep_value(global.structGameText,["common","itemSellSuccess",global.languageCode]);
	popup_message(_text,120);
	with(obj_menu_character_sell_parent)
	{
		instance_destroy();
	}
	with(obj_rm_character_inventory_manager)
	{
		structInventory.inventoryReset();
	}
};

//cfCallbackDone	= function(_value){
//	if(instance_exists(id))
//	{
//		global.windowLayer	= myWindowLayer;
//	}
//	asyncProcess	= false;
//	var _result	= json_parse(_value);
//	var _success	= _result.success;
//	if(_success)
//	{
//		var _nowHash	= sha1_string_utf8(json_stringify(global.structUserData));
//		if(_nowHash == beforeHash)
//		{
//			var _userInventory	= method_struct_get_deep_value(global.structUserData,["inventory","character"]);
//			if(!is_undefined(_userInventory))
//			{
//				if(obj_menu_character_sell_manager.optionSameGradeSell)
//				{
//					var _sellGrade	= global.gridCharInventory[# enumGridCharacterInventory.grade,global.inventorySelectedIndex];
//					var _size	= ds_grid_height(global.gridCharInventory);
//					for(var _i=0; _i<_size; ++_i)
//					{
//						var _grade	= global.gridCharInventory[# enumGridCharacterInventory.grade,_i];
//						var _equip	= global.gridCharInventory[# enumGridCharacterInventory.equip,_i];
//						if(_sellGrade == _grade && _equip == "none")
//						{
//							var _uid	= global.gridCharInventory[# enumGridCharacterInventory.uid,_i];
//							if(variable_struct_exists(_userInventory,_uid))
//							{
//								variable_struct_remove(_userInventory,_uid);
//							}
//						}
//					}
//				}
//				else
//				{
//					var _sellUid	= global.gridCharInventory[# enumGridCharacterInventory.uid,global.inventorySelectedIndex];
//					if(variable_struct_exists(_userInventory,_sellUid))
//					{
//						variable_struct_remove(_userInventory,_sellUid);
//					}
//				}
//			}
//		}
		
//		var _text	= method_struct_get_deep_value(global.structGameText,["common",_result.message,global.languageCode]);
//		popup_message(_text,120);
//		with(obj_menu_character_sell_parent)
//		{
//			instance_destroy();
//		}
//		with(obj_rm_character_inventory_manager)
//		{
//			structInventory.inventoryReset();
//		}
//	}
//	else
//	{
//		cfCallbackFail(_value);
//	}
	
//	delete(_result);
//};

//cfCallbackFail	= function(_value){
//	if(instance_exists(id))
//	{
//		global.windowLayer	= myWindowLayer;
//	}
//	asyncProcess	= false;
//	if(_value != "error")
//	{
//		var _result	= json_parse(_value);
//		var _text	= method_struct_get_deep_value(global.structGameText,["common",_result.message,global.languageCode]);
//	}
//	else
//	{
//		var _text	= method_struct_get_deep_value(global.structGameText,["error","networkError",global.languageCode]);
//	}
//	popup_message(_text,120);
//	delete(_result);
//};

stepMain	= function(){
	touchEvent();
};
