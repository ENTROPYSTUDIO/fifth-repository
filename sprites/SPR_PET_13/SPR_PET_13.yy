{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 2,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 120,
  "bbox_top": 0,
  "bbox_bottom": 92,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 121,
  "height": 93,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"de159a35-16c3-4348-af9a-ad440da16daf","path":"sprites/SPR_PET_13/SPR_PET_13.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"de159a35-16c3-4348-af9a-ad440da16daf","path":"sprites/SPR_PET_13/SPR_PET_13.yy",},"LayerId":{"name":"627dd052-9986-41fc-a62e-690afd23604f","path":"sprites/SPR_PET_13/SPR_PET_13.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_PET_13","path":"sprites/SPR_PET_13/SPR_PET_13.yy",},"resourceVersion":"1.0","name":"de159a35-16c3-4348-af9a-ad440da16daf","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_PET_13","path":"sprites/SPR_PET_13/SPR_PET_13.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"7c332706-8430-464e-83f7-be570e48b5b3","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"de159a35-16c3-4348-af9a-ad440da16daf","path":"sprites/SPR_PET_13/SPR_PET_13.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 60,
    "yorigin": 96,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_PET_13","path":"sprites/SPR_PET_13/SPR_PET_13.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"627dd052-9986-41fc-a62e-690afd23604f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "PET",
    "path": "folders/Sprites/CHARACTER/PLAYER/PET.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_PET_13",
  "tags": [],
  "resourceType": "GMSprite",
}