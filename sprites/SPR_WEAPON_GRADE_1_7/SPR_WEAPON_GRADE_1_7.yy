{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 91,
  "bbox_top": 0,
  "bbox_bottom": 179,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 92,
  "height": 180,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"533a8e39-0417-4283-ae7c-57e04eac694e","path":"sprites/SPR_WEAPON_GRADE_1_7/SPR_WEAPON_GRADE_1_7.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"533a8e39-0417-4283-ae7c-57e04eac694e","path":"sprites/SPR_WEAPON_GRADE_1_7/SPR_WEAPON_GRADE_1_7.yy",},"LayerId":{"name":"49d01d86-a40f-49cd-9e2e-17611c0ae345","path":"sprites/SPR_WEAPON_GRADE_1_7/SPR_WEAPON_GRADE_1_7.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_WEAPON_GRADE_1_7","path":"sprites/SPR_WEAPON_GRADE_1_7/SPR_WEAPON_GRADE_1_7.yy",},"resourceVersion":"1.0","name":"533a8e39-0417-4283-ae7c-57e04eac694e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_WEAPON_GRADE_1_7","path":"sprites/SPR_WEAPON_GRADE_1_7/SPR_WEAPON_GRADE_1_7.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ec9db49a-d125-4518-b1e0-d5d6e9d79dc7","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"533a8e39-0417-4283-ae7c-57e04eac694e","path":"sprites/SPR_WEAPON_GRADE_1_7/SPR_WEAPON_GRADE_1_7.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 34,
    "yorigin": 145,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_WEAPON_GRADE_1_7","path":"sprites/SPR_WEAPON_GRADE_1_7/SPR_WEAPON_GRADE_1_7.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"49d01d86-a40f-49cd-9e2e-17611c0ae345","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_1",
    "path": "folders/Sprites/CHARACTER/WEAPON/GRADE_1.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_WEAPON_GRADE_1_7",
  "tags": [],
  "resourceType": "GMSprite",
}