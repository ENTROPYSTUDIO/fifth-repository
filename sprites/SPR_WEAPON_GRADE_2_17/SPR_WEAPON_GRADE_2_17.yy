{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 45,
  "bbox_top": 0,
  "bbox_bottom": 165,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 46,
  "height": 166,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"04c52713-60d3-4f8a-a6c1-f7418aa764ef","path":"sprites/SPR_WEAPON_GRADE_2_17/SPR_WEAPON_GRADE_2_17.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"04c52713-60d3-4f8a-a6c1-f7418aa764ef","path":"sprites/SPR_WEAPON_GRADE_2_17/SPR_WEAPON_GRADE_2_17.yy",},"LayerId":{"name":"83421186-9c52-4a84-acc4-a17f690f3cf6","path":"sprites/SPR_WEAPON_GRADE_2_17/SPR_WEAPON_GRADE_2_17.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_WEAPON_GRADE_2_17","path":"sprites/SPR_WEAPON_GRADE_2_17/SPR_WEAPON_GRADE_2_17.yy",},"resourceVersion":"1.0","name":"04c52713-60d3-4f8a-a6c1-f7418aa764ef","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_WEAPON_GRADE_2_17","path":"sprites/SPR_WEAPON_GRADE_2_17/SPR_WEAPON_GRADE_2_17.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"01465a76-2f01-45d7-926f-93da49d682c8","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"04c52713-60d3-4f8a-a6c1-f7418aa764ef","path":"sprites/SPR_WEAPON_GRADE_2_17/SPR_WEAPON_GRADE_2_17.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 13,
    "yorigin": 142,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_WEAPON_GRADE_2_17","path":"sprites/SPR_WEAPON_GRADE_2_17/SPR_WEAPON_GRADE_2_17.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"83421186-9c52-4a84-acc4-a17f690f3cf6","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_2",
    "path": "folders/Sprites/CHARACTER/WEAPON/GRADE_2.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_WEAPON_GRADE_2_17",
  "tags": [],
  "resourceType": "GMSprite",
}