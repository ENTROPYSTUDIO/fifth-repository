{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 48,
  "bbox_top": 0,
  "bbox_bottom": 188,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 49,
  "height": 189,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"3df6b9b2-8fd0-463f-b2aa-d5e7f89511eb","path":"sprites/SPR_WEAPON_GRADE_5_3/SPR_WEAPON_GRADE_5_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"3df6b9b2-8fd0-463f-b2aa-d5e7f89511eb","path":"sprites/SPR_WEAPON_GRADE_5_3/SPR_WEAPON_GRADE_5_3.yy",},"LayerId":{"name":"ba241c4f-8f92-431c-bc6b-0a9c48218697","path":"sprites/SPR_WEAPON_GRADE_5_3/SPR_WEAPON_GRADE_5_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_WEAPON_GRADE_5_3","path":"sprites/SPR_WEAPON_GRADE_5_3/SPR_WEAPON_GRADE_5_3.yy",},"resourceVersion":"1.0","name":"3df6b9b2-8fd0-463f-b2aa-d5e7f89511eb","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_WEAPON_GRADE_5_3","path":"sprites/SPR_WEAPON_GRADE_5_3/SPR_WEAPON_GRADE_5_3.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"ca0c38ea-4080-49df-b50c-335c1cf5424d","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"3df6b9b2-8fd0-463f-b2aa-d5e7f89511eb","path":"sprites/SPR_WEAPON_GRADE_5_3/SPR_WEAPON_GRADE_5_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 25,
    "yorigin": 153,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_WEAPON_GRADE_5_3","path":"sprites/SPR_WEAPON_GRADE_5_3/SPR_WEAPON_GRADE_5_3.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"ba241c4f-8f92-431c-bc6b-0a9c48218697","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_5",
    "path": "folders/Sprites/CHARACTER/WEAPON/GRADE_5.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_WEAPON_GRADE_5_3",
  "tags": [],
  "resourceType": "GMSprite",
}