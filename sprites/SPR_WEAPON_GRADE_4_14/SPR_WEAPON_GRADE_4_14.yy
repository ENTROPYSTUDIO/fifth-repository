{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 58,
  "bbox_top": 0,
  "bbox_bottom": 183,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 59,
  "height": 184,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"fb924270-5378-4225-83bc-69400223193b","path":"sprites/SPR_WEAPON_GRADE_4_14/SPR_WEAPON_GRADE_4_14.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fb924270-5378-4225-83bc-69400223193b","path":"sprites/SPR_WEAPON_GRADE_4_14/SPR_WEAPON_GRADE_4_14.yy",},"LayerId":{"name":"5e8de5aa-3c2b-4cdb-b437-4469576da0b9","path":"sprites/SPR_WEAPON_GRADE_4_14/SPR_WEAPON_GRADE_4_14.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_WEAPON_GRADE_4_14","path":"sprites/SPR_WEAPON_GRADE_4_14/SPR_WEAPON_GRADE_4_14.yy",},"resourceVersion":"1.0","name":"fb924270-5378-4225-83bc-69400223193b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_WEAPON_GRADE_4_14","path":"sprites/SPR_WEAPON_GRADE_4_14/SPR_WEAPON_GRADE_4_14.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e3eab202-3d7c-463b-8328-9458fe08cb7e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fb924270-5378-4225-83bc-69400223193b","path":"sprites/SPR_WEAPON_GRADE_4_14/SPR_WEAPON_GRADE_4_14.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 30,
    "yorigin": 154,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_WEAPON_GRADE_4_14","path":"sprites/SPR_WEAPON_GRADE_4_14/SPR_WEAPON_GRADE_4_14.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"5e8de5aa-3c2b-4cdb-b437-4469576da0b9","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_4",
    "path": "folders/Sprites/CHARACTER/WEAPON/GRADE_4.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_WEAPON_GRADE_4_14",
  "tags": [],
  "resourceType": "GMSprite",
}