{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 2,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 143,
  "bbox_top": 0,
  "bbox_bottom": 115,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 149,
  "height": 116,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"d8236a37-0355-4ef7-9c48-59e48f93378a","path":"sprites/SPR_CHARACTER_GRADE_3_3/SPR_CHARACTER_GRADE_3_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d8236a37-0355-4ef7-9c48-59e48f93378a","path":"sprites/SPR_CHARACTER_GRADE_3_3/SPR_CHARACTER_GRADE_3_3.yy",},"LayerId":{"name":"4fca2ee3-0f0c-4114-9744-a06ade95d563","path":"sprites/SPR_CHARACTER_GRADE_3_3/SPR_CHARACTER_GRADE_3_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_CHARACTER_GRADE_3_3","path":"sprites/SPR_CHARACTER_GRADE_3_3/SPR_CHARACTER_GRADE_3_3.yy",},"resourceVersion":"1.0","name":"d8236a37-0355-4ef7-9c48-59e48f93378a","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_CHARACTER_GRADE_3_3","path":"sprites/SPR_CHARACTER_GRADE_3_3/SPR_CHARACTER_GRADE_3_3.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"0e7681e2-bf9e-4ab8-8606-f61f598c645a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d8236a37-0355-4ef7-9c48-59e48f93378a","path":"sprites/SPR_CHARACTER_GRADE_3_3/SPR_CHARACTER_GRADE_3_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 63,
    "yorigin": 115,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_CHARACTER_GRADE_3_3","path":"sprites/SPR_CHARACTER_GRADE_3_3/SPR_CHARACTER_GRADE_3_3.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"4fca2ee3-0f0c-4114-9744-a06ade95d563","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_3",
    "path": "folders/Sprites/CHARACTER/PLAYER/GRADE_3.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_CHARACTER_GRADE_3_3",
  "tags": [],
  "resourceType": "GMSprite",
}