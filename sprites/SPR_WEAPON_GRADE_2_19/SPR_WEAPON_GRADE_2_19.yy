{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 30,
  "bbox_top": 0,
  "bbox_bottom": 164,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 31,
  "height": 165,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"66ad013c-4593-49ff-81ba-a57f00a0c171","path":"sprites/SPR_WEAPON_GRADE_2_19/SPR_WEAPON_GRADE_2_19.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"66ad013c-4593-49ff-81ba-a57f00a0c171","path":"sprites/SPR_WEAPON_GRADE_2_19/SPR_WEAPON_GRADE_2_19.yy",},"LayerId":{"name":"61158659-ca34-4611-838c-07c4194b88d1","path":"sprites/SPR_WEAPON_GRADE_2_19/SPR_WEAPON_GRADE_2_19.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_WEAPON_GRADE_2_19","path":"sprites/SPR_WEAPON_GRADE_2_19/SPR_WEAPON_GRADE_2_19.yy",},"resourceVersion":"1.0","name":"66ad013c-4593-49ff-81ba-a57f00a0c171","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_WEAPON_GRADE_2_19","path":"sprites/SPR_WEAPON_GRADE_2_19/SPR_WEAPON_GRADE_2_19.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"3f81700a-3a2a-4afa-840c-d4d801a64e2a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"66ad013c-4593-49ff-81ba-a57f00a0c171","path":"sprites/SPR_WEAPON_GRADE_2_19/SPR_WEAPON_GRADE_2_19.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 15,
    "yorigin": 139,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_WEAPON_GRADE_2_19","path":"sprites/SPR_WEAPON_GRADE_2_19/SPR_WEAPON_GRADE_2_19.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"61158659-ca34-4611-838c-07c4194b88d1","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_2",
    "path": "folders/Sprites/CHARACTER/WEAPON/GRADE_2.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_WEAPON_GRADE_2_19",
  "tags": [],
  "resourceType": "GMSprite",
}