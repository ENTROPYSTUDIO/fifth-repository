{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 65,
  "bbox_top": 0,
  "bbox_bottom": 224,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 66,
  "height": 225,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"32640487-3240-4218-8090-cff26ee5a93a","path":"sprites/SPR_WEAPON_GRADE_4_15/SPR_WEAPON_GRADE_4_15.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"32640487-3240-4218-8090-cff26ee5a93a","path":"sprites/SPR_WEAPON_GRADE_4_15/SPR_WEAPON_GRADE_4_15.yy",},"LayerId":{"name":"9a8a188d-938d-45e3-8816-e407c00ca70e","path":"sprites/SPR_WEAPON_GRADE_4_15/SPR_WEAPON_GRADE_4_15.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_WEAPON_GRADE_4_15","path":"sprites/SPR_WEAPON_GRADE_4_15/SPR_WEAPON_GRADE_4_15.yy",},"resourceVersion":"1.0","name":"32640487-3240-4218-8090-cff26ee5a93a","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_WEAPON_GRADE_4_15","path":"sprites/SPR_WEAPON_GRADE_4_15/SPR_WEAPON_GRADE_4_15.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"4f080161-3dae-4ef2-9851-c82d275bb762","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"32640487-3240-4218-8090-cff26ee5a93a","path":"sprites/SPR_WEAPON_GRADE_4_15/SPR_WEAPON_GRADE_4_15.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 34,
    "yorigin": 195,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_WEAPON_GRADE_4_15","path":"sprites/SPR_WEAPON_GRADE_4_15/SPR_WEAPON_GRADE_4_15.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"9a8a188d-938d-45e3-8816-e407c00ca70e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_4",
    "path": "folders/Sprites/CHARACTER/WEAPON/GRADE_4.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_WEAPON_GRADE_4_15",
  "tags": [],
  "resourceType": "GMSprite",
}