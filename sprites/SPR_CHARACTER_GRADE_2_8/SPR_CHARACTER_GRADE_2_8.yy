{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 2,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 130,
  "bbox_top": 0,
  "bbox_bottom": 106,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 136,
  "height": 107,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"9df97e30-d8c6-483c-a5ba-d48bcee67107","path":"sprites/SPR_CHARACTER_GRADE_2_8/SPR_CHARACTER_GRADE_2_8.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9df97e30-d8c6-483c-a5ba-d48bcee67107","path":"sprites/SPR_CHARACTER_GRADE_2_8/SPR_CHARACTER_GRADE_2_8.yy",},"LayerId":{"name":"6f591915-8ed4-40a6-ae49-70720035fac3","path":"sprites/SPR_CHARACTER_GRADE_2_8/SPR_CHARACTER_GRADE_2_8.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_CHARACTER_GRADE_2_8","path":"sprites/SPR_CHARACTER_GRADE_2_8/SPR_CHARACTER_GRADE_2_8.yy",},"resourceVersion":"1.0","name":"9df97e30-d8c6-483c-a5ba-d48bcee67107","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_CHARACTER_GRADE_2_8","path":"sprites/SPR_CHARACTER_GRADE_2_8/SPR_CHARACTER_GRADE_2_8.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"27e5a8ca-35a0-4b02-ac34-6448e5146dc1","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9df97e30-d8c6-483c-a5ba-d48bcee67107","path":"sprites/SPR_CHARACTER_GRADE_2_8/SPR_CHARACTER_GRADE_2_8.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 44,
    "yorigin": 106,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_CHARACTER_GRADE_2_8","path":"sprites/SPR_CHARACTER_GRADE_2_8/SPR_CHARACTER_GRADE_2_8.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"6f591915-8ed4-40a6-ae49-70720035fac3","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_2",
    "path": "folders/Sprites/CHARACTER/PLAYER/GRADE_2.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_CHARACTER_GRADE_2_8",
  "tags": [],
  "resourceType": "GMSprite",
}