{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 71,
  "bbox_top": 0,
  "bbox_bottom": 153,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 72,
  "height": 154,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"701db822-6e73-4a9e-836b-267678dabac3","path":"sprites/SPR_WEAPON_GRADE_2_10/SPR_WEAPON_GRADE_2_10.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"701db822-6e73-4a9e-836b-267678dabac3","path":"sprites/SPR_WEAPON_GRADE_2_10/SPR_WEAPON_GRADE_2_10.yy",},"LayerId":{"name":"2681a6e7-a7bd-44ea-a0c8-b11894d8acca","path":"sprites/SPR_WEAPON_GRADE_2_10/SPR_WEAPON_GRADE_2_10.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_WEAPON_GRADE_2_10","path":"sprites/SPR_WEAPON_GRADE_2_10/SPR_WEAPON_GRADE_2_10.yy",},"resourceVersion":"1.0","name":"701db822-6e73-4a9e-836b-267678dabac3","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_WEAPON_GRADE_2_10","path":"sprites/SPR_WEAPON_GRADE_2_10/SPR_WEAPON_GRADE_2_10.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"f1c6d25d-daf9-4d25-b9e2-c98b0c615e49","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"701db822-6e73-4a9e-836b-267678dabac3","path":"sprites/SPR_WEAPON_GRADE_2_10/SPR_WEAPON_GRADE_2_10.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 37,
    "yorigin": 123,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_WEAPON_GRADE_2_10","path":"sprites/SPR_WEAPON_GRADE_2_10/SPR_WEAPON_GRADE_2_10.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"2681a6e7-a7bd-44ea-a0c8-b11894d8acca","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_2",
    "path": "folders/Sprites/CHARACTER/WEAPON/GRADE_2.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_WEAPON_GRADE_2_10",
  "tags": [],
  "resourceType": "GMSprite",
}