{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 53,
  "bbox_top": 0,
  "bbox_bottom": 199,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 54,
  "height": 200,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e69a8040-99dd-45d6-bfd4-f96ef2978601","path":"sprites/SPR_WEAPON_GRADE_3_20/SPR_WEAPON_GRADE_3_20.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e69a8040-99dd-45d6-bfd4-f96ef2978601","path":"sprites/SPR_WEAPON_GRADE_3_20/SPR_WEAPON_GRADE_3_20.yy",},"LayerId":{"name":"1b7febb1-82a9-47cd-8236-3ddcb54112ec","path":"sprites/SPR_WEAPON_GRADE_3_20/SPR_WEAPON_GRADE_3_20.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_WEAPON_GRADE_3_20","path":"sprites/SPR_WEAPON_GRADE_3_20/SPR_WEAPON_GRADE_3_20.yy",},"resourceVersion":"1.0","name":"e69a8040-99dd-45d6-bfd4-f96ef2978601","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_WEAPON_GRADE_3_20","path":"sprites/SPR_WEAPON_GRADE_3_20/SPR_WEAPON_GRADE_3_20.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"d789b69f-acf4-4299-8114-1f41e70a6c27","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e69a8040-99dd-45d6-bfd4-f96ef2978601","path":"sprites/SPR_WEAPON_GRADE_3_20/SPR_WEAPON_GRADE_3_20.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 29,
    "yorigin": 100,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_WEAPON_GRADE_3_20","path":"sprites/SPR_WEAPON_GRADE_3_20/SPR_WEAPON_GRADE_3_20.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"1b7febb1-82a9-47cd-8236-3ddcb54112ec","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_3",
    "path": "folders/Sprites/CHARACTER/WEAPON/GRADE_3.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_WEAPON_GRADE_3_20",
  "tags": [],
  "resourceType": "GMSprite",
}