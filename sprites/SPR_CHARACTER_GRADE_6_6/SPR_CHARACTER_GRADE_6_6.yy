{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 2,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 143,
  "bbox_top": 0,
  "bbox_bottom": 127,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 147,
  "height": 128,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"821ddb8d-fb1f-4173-8c8a-60a99a619548","path":"sprites/SPR_CHARACTER_GRADE_6_6/SPR_CHARACTER_GRADE_6_6.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"821ddb8d-fb1f-4173-8c8a-60a99a619548","path":"sprites/SPR_CHARACTER_GRADE_6_6/SPR_CHARACTER_GRADE_6_6.yy",},"LayerId":{"name":"d4336b2f-d03a-43c1-9c3c-7e034e0d8f54","path":"sprites/SPR_CHARACTER_GRADE_6_6/SPR_CHARACTER_GRADE_6_6.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_CHARACTER_GRADE_6_6","path":"sprites/SPR_CHARACTER_GRADE_6_6/SPR_CHARACTER_GRADE_6_6.yy",},"resourceVersion":"1.0","name":"821ddb8d-fb1f-4173-8c8a-60a99a619548","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_CHARACTER_GRADE_6_6","path":"sprites/SPR_CHARACTER_GRADE_6_6/SPR_CHARACTER_GRADE_6_6.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 15.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a7ea3a1c-211f-494c-87fa-5948d22ebf52","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"821ddb8d-fb1f-4173-8c8a-60a99a619548","path":"sprites/SPR_CHARACTER_GRADE_6_6/SPR_CHARACTER_GRADE_6_6.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 38,
    "yorigin": 127,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_CHARACTER_GRADE_6_6","path":"sprites/SPR_CHARACTER_GRADE_6_6/SPR_CHARACTER_GRADE_6_6.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"d4336b2f-d03a-43c1-9c3c-7e034e0d8f54","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_6",
    "path": "folders/Sprites/CHARACTER/PLAYER/GRADE_6.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_CHARACTER_GRADE_6_6",
  "tags": [],
  "resourceType": "GMSprite",
}