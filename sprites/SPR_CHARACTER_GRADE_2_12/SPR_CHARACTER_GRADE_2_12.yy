{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 2,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 111,
  "bbox_top": 0,
  "bbox_bottom": 111,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 112,
  "height": 112,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"cb9148c0-205a-4eab-984c-593a66174ba9","path":"sprites/SPR_CHARACTER_GRADE_2_12/SPR_CHARACTER_GRADE_2_12.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cb9148c0-205a-4eab-984c-593a66174ba9","path":"sprites/SPR_CHARACTER_GRADE_2_12/SPR_CHARACTER_GRADE_2_12.yy",},"LayerId":{"name":"95d07e16-c460-47a6-8b6b-90b7e7ad6ec0","path":"sprites/SPR_CHARACTER_GRADE_2_12/SPR_CHARACTER_GRADE_2_12.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_CHARACTER_GRADE_2_12","path":"sprites/SPR_CHARACTER_GRADE_2_12/SPR_CHARACTER_GRADE_2_12.yy",},"resourceVersion":"1.0","name":"cb9148c0-205a-4eab-984c-593a66174ba9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_CHARACTER_GRADE_2_12","path":"sprites/SPR_CHARACTER_GRADE_2_12/SPR_CHARACTER_GRADE_2_12.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"e42f0ce1-3417-4858-8c8e-cf26a7f2de75","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cb9148c0-205a-4eab-984c-593a66174ba9","path":"sprites/SPR_CHARACTER_GRADE_2_12/SPR_CHARACTER_GRADE_2_12.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 40,
    "yorigin": 111,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_CHARACTER_GRADE_2_12","path":"sprites/SPR_CHARACTER_GRADE_2_12/SPR_CHARACTER_GRADE_2_12.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"95d07e16-c460-47a6-8b6b-90b7e7ad6ec0","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_2",
    "path": "folders/Sprites/CHARACTER/PLAYER/GRADE_2.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_CHARACTER_GRADE_2_12",
  "tags": [],
  "resourceType": "GMSprite",
}