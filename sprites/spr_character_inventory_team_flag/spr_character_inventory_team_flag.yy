{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 4,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 28,
  "bbox_top": 0,
  "bbox_bottom": 23,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 29,
  "height": 24,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"85e27225-f58f-40ee-9626-e9ef2443338f","path":"sprites/spr_character_inventory_team_flag/spr_character_inventory_team_flag.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"85e27225-f58f-40ee-9626-e9ef2443338f","path":"sprites/spr_character_inventory_team_flag/spr_character_inventory_team_flag.yy",},"LayerId":{"name":"e4117c96-395c-41f3-a731-27cb36227360","path":"sprites/spr_character_inventory_team_flag/spr_character_inventory_team_flag.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_character_inventory_team_flag","path":"sprites/spr_character_inventory_team_flag/spr_character_inventory_team_flag.yy",},"resourceVersion":"1.0","name":"85e27225-f58f-40ee-9626-e9ef2443338f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_character_inventory_team_flag","path":"sprites/spr_character_inventory_team_flag/spr_character_inventory_team_flag.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"07aaaa44-5eca-492b-b860-ee648c1ee93f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"85e27225-f58f-40ee-9626-e9ef2443338f","path":"sprites/spr_character_inventory_team_flag/spr_character_inventory_team_flag.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 14,
    "yorigin": 12,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_character_inventory_team_flag","path":"sprites/spr_character_inventory_team_flag/spr_character_inventory_team_flag.yy",},
    "resourceVersion": "1.3",
    "name": "spr_character_inventory_team_flag",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"e4117c96-395c-41f3-a731-27cb36227360","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "CHARACTER",
    "path": "folders/Sprites/ROOM/INVENTORY/CHARACTER.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_character_inventory_team_flag",
  "tags": [],
  "resourceType": "GMSprite",
}