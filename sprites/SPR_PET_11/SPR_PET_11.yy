{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 2,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 267,
  "bbox_top": 0,
  "bbox_bottom": 200,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 268,
  "height": 201,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"d49925ff-5511-4a40-8fb1-565accc61e2e","path":"sprites/SPR_PET_11/SPR_PET_11.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d49925ff-5511-4a40-8fb1-565accc61e2e","path":"sprites/SPR_PET_11/SPR_PET_11.yy",},"LayerId":{"name":"6cc09cdf-6490-4330-b0b9-18bc9072d73d","path":"sprites/SPR_PET_11/SPR_PET_11.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_PET_11","path":"sprites/SPR_PET_11/SPR_PET_11.yy",},"resourceVersion":"1.0","name":"d49925ff-5511-4a40-8fb1-565accc61e2e","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_PET_11","path":"sprites/SPR_PET_11/SPR_PET_11.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"571e5354-4fce-4d9e-a6e7-f993857a750a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d49925ff-5511-4a40-8fb1-565accc61e2e","path":"sprites/SPR_PET_11/SPR_PET_11.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 86,
    "yorigin": 197,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_PET_11","path":"sprites/SPR_PET_11/SPR_PET_11.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"6cc09cdf-6490-4330-b0b9-18bc9072d73d","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "PET",
    "path": "folders/Sprites/CHARACTER/PLAYER/PET.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_PET_11",
  "tags": [],
  "resourceType": "GMSprite",
}