{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 2,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 8,
  "bbox_right": 117,
  "bbox_top": 0,
  "bbox_bottom": 132,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 118,
  "height": 134,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"5fbfec70-21a1-43a3-9825-6c9b49243608","path":"sprites/SPR_PET_7/SPR_PET_7.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5fbfec70-21a1-43a3-9825-6c9b49243608","path":"sprites/SPR_PET_7/SPR_PET_7.yy",},"LayerId":{"name":"e9a2d000-ea4f-4ea0-b2b9-b9d5e64ce07e","path":"sprites/SPR_PET_7/SPR_PET_7.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_PET_7","path":"sprites/SPR_PET_7/SPR_PET_7.yy",},"resourceVersion":"1.0","name":"5fbfec70-21a1-43a3-9825-6c9b49243608","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_PET_7","path":"sprites/SPR_PET_7/SPR_PET_7.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"dc4c0baa-cc3a-4dd0-b8fe-f56aadd9c4a3","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5fbfec70-21a1-43a3-9825-6c9b49243608","path":"sprites/SPR_PET_7/SPR_PET_7.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 66,
    "yorigin": 159,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_PET_7","path":"sprites/SPR_PET_7/SPR_PET_7.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"e9a2d000-ea4f-4ea0-b2b9-b9d5e64ce07e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "PET",
    "path": "folders/Sprites/CHARACTER/PLAYER/PET.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_PET_7",
  "tags": [],
  "resourceType": "GMSprite",
}