{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 39,
  "bbox_top": 0,
  "bbox_bottom": 143,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 40,
  "height": 144,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"9934edc3-de10-4d83-9bb7-cbe5f8dc7bff","path":"sprites/SPR_WEAPON_GRADE_5_0/SPR_WEAPON_GRADE_5_0.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9934edc3-de10-4d83-9bb7-cbe5f8dc7bff","path":"sprites/SPR_WEAPON_GRADE_5_0/SPR_WEAPON_GRADE_5_0.yy",},"LayerId":{"name":"49fd0e22-fa7f-43b2-98cd-5b7f1407265c","path":"sprites/SPR_WEAPON_GRADE_5_0/SPR_WEAPON_GRADE_5_0.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_WEAPON_GRADE_5_0","path":"sprites/SPR_WEAPON_GRADE_5_0/SPR_WEAPON_GRADE_5_0.yy",},"resourceVersion":"1.0","name":"9934edc3-de10-4d83-9bb7-cbe5f8dc7bff","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_WEAPON_GRADE_5_0","path":"sprites/SPR_WEAPON_GRADE_5_0/SPR_WEAPON_GRADE_5_0.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"163f47f0-7ad7-4b91-ae66-cc4239621589","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9934edc3-de10-4d83-9bb7-cbe5f8dc7bff","path":"sprites/SPR_WEAPON_GRADE_5_0/SPR_WEAPON_GRADE_5_0.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 20,
    "yorigin": 123,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_WEAPON_GRADE_5_0","path":"sprites/SPR_WEAPON_GRADE_5_0/SPR_WEAPON_GRADE_5_0.yy",},
    "resourceVersion": "1.3",
    "name": "",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"49fd0e22-fa7f-43b2-98cd-5b7f1407265c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_5",
    "path": "folders/Sprites/CHARACTER/WEAPON/GRADE_5.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_WEAPON_GRADE_5_0",
  "tags": [],
  "resourceType": "GMSprite",
}