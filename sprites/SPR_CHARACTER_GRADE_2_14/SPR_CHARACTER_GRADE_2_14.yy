{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 2,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 116,
  "bbox_top": 1,
  "bbox_bottom": 125,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 121,
  "height": 126,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"4a383aa2-429c-4765-9ded-cea459457af9","path":"sprites/SPR_CHARACTER_GRADE_2_14/SPR_CHARACTER_GRADE_2_14.yy",},"LayerId":null,"resourceVersion":"1.0","name":"imported","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4a383aa2-429c-4765-9ded-cea459457af9","path":"sprites/SPR_CHARACTER_GRADE_2_14/SPR_CHARACTER_GRADE_2_14.yy",},"LayerId":{"name":"11fb7f2d-bb10-4437-9f50-d8cd99c7873f","path":"sprites/SPR_CHARACTER_GRADE_2_14/SPR_CHARACTER_GRADE_2_14.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"SPR_CHARACTER_GRADE_2_14","path":"sprites/SPR_CHARACTER_GRADE_2_14/SPR_CHARACTER_GRADE_2_14.yy",},"resourceVersion":"1.0","name":"4a383aa2-429c-4765-9ded-cea459457af9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"SPR_CHARACTER_GRADE_2_14","path":"sprites/SPR_CHARACTER_GRADE_2_14/SPR_CHARACTER_GRADE_2_14.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 1.0,
    "playbackSpeedType": 1,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"2b4be787-c770-4bfe-af07-fcdd5f0c36bb","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4a383aa2-429c-4765-9ded-cea459457af9","path":"sprites/SPR_CHARACTER_GRADE_2_14/SPR_CHARACTER_GRADE_2_14.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": {"x":0.0,"y":0.0,},
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1920,
    "backdropHeight": 1080,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 36,
    "yorigin": 126,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"SPR_CHARACTER_GRADE_2_14","path":"sprites/SPR_CHARACTER_GRADE_2_14/SPR_CHARACTER_GRADE_2_14.yy",},
    "resourceVersion": "1.3",
    "name": "SPR_CHARACTER_GRADE_2_14",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"11fb7f2d-bb10-4437-9f50-d8cd99c7873f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GRADE_2",
    "path": "folders/Sprites/CHARACTER/PLAYER/GRADE_2.yy",
  },
  "resourceVersion": "1.0",
  "name": "SPR_CHARACTER_GRADE_2_14",
  "tags": [],
  "resourceType": "GMSprite",
}