#import <objc/runtime.h>
#import <UIKit/UIKit.h>

@interface YYShowMessage:NSObject
    -(int)__showMessage:(id)arg1;
@end

@interface RunnerViewController:UIViewController
    @property (retain, nonatomic) YYShowMessage* uiMessage;
@end

@interface gml_ex:NSObject
-(NSObject*)init;
-(int)__showMessage:(id)Arg1;
@end