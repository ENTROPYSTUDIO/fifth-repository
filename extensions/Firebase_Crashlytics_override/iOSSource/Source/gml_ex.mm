#import "gml_ex.h"
#import "FirebaseGM.h"

extern RunnerViewController *g_controller;

@implementation gml_ex

Method origShowMessage,newShowMessage;

-(int)__showMessage:(id)Arg1{
    method_exchangeImplementations(origShowMessage,newShowMessage);
    NSString *msg=Arg1;
	
	
	msg = [msg stringByReplacingOccurrencesOfString:@"___________________________________________" withString:@""];
    
    msg = [msg stringByReplacingOccurrencesOfString:@"############################################################################################" withString:@""];
    
    msg = [msg stringByReplacingOccurrencesOfString:@"--------------------------------------------------------------------------------------------" withString:@""];
	
	//msg = [msg stringByReplacingOccurrencesOfString:@"\r\n" withString:@" "];
    
	//Show on console
    CLSLog(@"%@",msg);

	//////////////////////////////////////HEADER FOR CRASH REPORT

    NSRange mRange = [msg rangeOfString:@":"];
    int start = (int) mRange.location;
    start += 2;

    msg = [msg substringFromIndex:start];
    
    mRange = [msg rangeOfString:@"stack frame is"];
    int end = (int) mRange.location;
    
    msg = [msg substringToIndex:end];
    
    msg = [msg stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    
    //NSLog(msg);
    
	NSDictionary *userInfo = @{};
    NSError *error = [NSError errorWithDomain:msg code:-1001 userInfo:userInfo];
    [CrashlyticsKit recordError:error];
   
    int rs = [g_controller.uiMessage __showMessage:@"\nInternal application error! :(\n"];
	method_exchangeImplementations(origShowMessage,newShowMessage);
    return rs;
     
}

-(NSObject*)init {
	self=[super init];
	SEL sel=@selector(__showMessage:);
    origShowMessage=class_getInstanceMethod([g_controller.uiMessage class], sel);
    newShowMessage=class_getInstanceMethod([self class], sel);
    method_exchangeImplementations(origShowMessage,newShowMessage);
	return self;
}

@end
