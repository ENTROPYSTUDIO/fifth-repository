
package ${YYAndroidPackageName};

//import ${YYAndroidPackageName}.RunnerActivity;
import ${YYAndroidPackageName}.R;
import com.yoyogames.runner.RunnerJNILib;
import java.lang.RuntimeException;
import android.util.Log;
import android.app.AlertDialog;
import android.content.DialogInterface;
import java.util.concurrent.CountDownLatch;
import java.lang.Thread;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import android.content.Context;
import ${YYAndroidPackageName}.DemoRenderer;
import java.util.LinkedList;
import java.util.List;
import java.lang.StackTraceElement;

import androidx.annotation.Keep;
@Keep
public class GMLException extends RuntimeException {
	// private FirebaseCrashlytics crashlytics;
	// //crashlytics
	// crashlytics = FirebaseCrashlytics.getInstance();

	private static final long serialVersionUID = 3221337228232807009L;

	public GMLException() {
	}

	public GMLException(String message) {
		super(message);
	}

	public GMLException(Throwable cause) {
		super(cause);
	}

	public GMLException(String message, Throwable cause) {
		super(message, cause);
	}

	public static void gmlMessage(String _message) {// , Context _ms_context) {
		// FirebaseCrashlytics crashlytics = FirebaseCrashlytics.getInstance();
		// final Context ms_context;
		// ms_context = RunnerJNILib.GetApplicationContext();
		Log.i("yoyo", "gmlMessage start");
		final String omsg;

		omsg = "\nInternal application error! :(\n";

		String sMessage = _message.replace("___________________________________________", "<");
		sMessage = sMessage.replace(
				"############################################################################################", "");
		sMessage = sMessage.replace(
				"--------------------------------------------------------------------------------------------", ">");
		// sMessage=sMessage.replace("\r\n"," ");

		FirebaseCrashlytics.getInstance().log(sMessage);
		Log.i("yoyo", sMessage);

		GMLException ge = new GMLException(sMessage);// Integer.toHexString(sMessage.hashCode()));
		StackTraceElement[] strace = new StackTraceElement[1];
		strace[0] = new StackTraceElement("", "", sMessage, -1); // result: FILE obj.method
		ge.setStackTrace(strace);

		// FirebaseCrashlytics.getInstance().recordException(ge);
		FirebaseCrashlytics.getInstance().recordException(ge);

		Log.i("yoyo", "gmlMessage end");
		// messageToUser(sMessage/* omsg */, ms_context);

	}

	// public static void messageToUser(final String msg, final Context context) {
	// final CountDownLatch latch = new CountDownLatch(1);
	// RunnerActivity.ViewHandler.post(new Runnable() {
	// public void run() {
	// AlertDialog.Builder builder = new AlertDialog.Builder(context);
	// builder.setMessage(msg).setCancelable(false).setPositiveButton("OK",
	// new DialogInterface.OnClickListener() {
	// public void onClick(DialogInterface dialog, int id) {
	// latch.countDown();
	// }
	// });
	// AlertDialog alert = builder.create();
	// alert.show();
	// }
	// });

	// try {
	// latch.await();
	// } catch (InterruptedException e) {
	// Thread.currentThread().interrupt();
	// }

	// }
}
