package ${YYAndroidPackageName};

//Game Maker Studio 2 Packages
import ${YYAndroidPackageName}.R;
import com.yoyogames.runner.RunnerJNILib;
import ${YYAndroidPackageName}.RunnerActivity;

//Some Android Packages
import java.lang.String;
import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import android.net.Uri;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import androidx.annotation.NonNull;

//Dynamic Links Library
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;
import com.google.firebase.dynamiclinks.ShortDynamicLink;

import androidx.annotation.Keep;
@Keep
public class gmdevbloggamemakerfirebasedynamiclinks extends RunnerActivity {

	private double valueListernerInd = 0;

	// Game Maker Async Event Code
	private static final int EVENT_OTHER_SOCIAL = 70;

	// Debug Modifier
	public static boolean dynaDebugMode = true;

	// For the custom built Dynamic Link, is edited afterwards;
	public static String dynaLinkStrExt = "";

	public double firebase_dynamic_links_debug_mode(double arg0) {

		// You might ask wtf is this code. With this function, I imitate converting GML
		// boolean to Java boolean.
		if (arg0 >= 0.5) {
			dynaDebugMode = true;
		} else {
			dynaDebugMode = false;
		}

		return 0;
	}

	private double getListenerInd() {
		valueListernerInd++;
		return (valueListernerInd);
	}

	// Deciding whether the app is opened from a deepLink.
	public double firebase_dynamic_links_init() {
		final double listenerInd = getListenerInd();
		Log.i("yoyo",
				"Game Maker Firebase Dynamic Links Extension. For more information check the blog post : https://gmdevblog.com/game-maker-firebase-dynamic-links/");
		Log.i("yoyo",
				"For any questions and suggestions, please comment to the post. Credits would be appreciated. Have fun!");

		FirebaseDynamicLinks.getInstance().getDynamicLink(RunnerActivity.CurrentActivity.getIntent())
				.addOnSuccessListener(RunnerActivity.CurrentActivity, new OnSuccessListener<PendingDynamicLinkData>() {
					@Override
					public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
						Log.i("yoyo", "maperrortest - firebase_dynamic_links_init");
						if (pendingDynamicLinkData != null) {

							Uri deepLink = pendingDynamicLinkData.getLink();
							String deepLinkString = deepLink.toString();
							// I'm leaving this data as Uri here, because we might add creative ideas here.
							// Anyways, we will return this data as string.
							final int dsMapIndex = RunnerJNILib.dsMapCreate();
							RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
							RunnerJNILib.DsMapAddString(dsMapIndex, "event", "dynamic_links_deeplink");
							RunnerJNILib.DsMapAddString(dsMapIndex, "data", deepLinkString);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "status", 1);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
							RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

							if (dynaDebugMode == true) {
								Log.i("yoyo", "Game Maker Firebase | Dynamic Links : The app is opened from an URL : "
										+ deepLinkString);
							}
						} else {
							final int dsMapIndex = RunnerJNILib.dsMapCreate();
							RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
							RunnerJNILib.DsMapAddString(dsMapIndex, "event", "dynamic_links_deeplink");
							RunnerJNILib.DsMapAddString(dsMapIndex, "data", "N/A");
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "status", 0);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
							RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

							if (dynaDebugMode == true) {
								Log.i("yoyo",
										"Game Maker Firebase | Dynamic Links : The app is not opened from a DeepLink");
							}
						}
					}
				}).addOnFailureListener(RunnerActivity.CurrentActivity, new OnFailureListener() {
					@Override
					public void onFailure(@NonNull Exception e) {
						Log.i("yoyo", "maperrortest - firebase_dynamic_links_initaddOnFailureListener");
						final int dsMapIndex = RunnerJNILib.dsMapCreate();
						RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
						RunnerJNILib.DsMapAddString(dsMapIndex, "event", "dynamic_links_deeplink");
						RunnerJNILib.DsMapAddString(dsMapIndex, "data", "FAILED");
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "status", -1);
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
						RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

						if (dynaDebugMode == true) {
							Log.i("yoyo",
									"Game Maker Firebase | Dynamic Links : Failed to get the DeepLink data of the app. Error : "
											+ e.toString());
						}
					}
				});
		return listenerInd;
	}

	public double firebase_dynamic_links_generate_short(String toLink, String fromLink, String androidPackage,
			String iosPackage, String title, String description, String imageUrl) {
		// [START create_short_link]
		final double listenerInd = getListenerInd();

		Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
				.setLink(Uri.parse(toLink)).setDomainUriPrefix(fromLink)
				.setAndroidParameters(new DynamicLink.AndroidParameters.Builder(androidPackage).build())
				.setIosParameters(new DynamicLink.IosParameters.Builder(iosPackage).build())
				.setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder().setTitle(title)
						.setDescription(description).setImageUrl(Uri.parse(imageUrl))// Your// url// HERE
						.build())
				.buildShortDynamicLink().addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
					@Override
					public void onComplete(@NonNull Task<ShortDynamicLink> task) {
						Log.i("yoyo", "maperrortest - firebase_dynamic_links_generate_short");
						if (task.isSuccessful()) {
							// Short link created
							Uri shortLink = task.getResult().getShortLink();

							if (dynaDebugMode == true) {
								Log.i("yoyo",
										"Game Maker Firebase | Dynamic Links : Basic Dynamic Link created. You may preview your link via : "
												+ task.getResult().getPreviewLink());
								Log.i("yoyo",
										"Game Maker Firebase | Dynamic Links : The dynamic link is  : " + shortLink);
							}

							final int dsMapIndex = RunnerJNILib.dsMapCreate();
							RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
							RunnerJNILib.DsMapAddString(dsMapIndex, "event", "dynamic_link_short");
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "status", 1);
							RunnerJNILib.DsMapAddString(dsMapIndex, "data", shortLink.toString());
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
							RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

						} else {

							if (dynaDebugMode == true) {
								Log.i("yoyo",
										"Game Maker Firebase | Dynamic Links : Error creating a Basic Dynamic Link. Possibly the device has no internet connection!");
							}

							final int dsMapIndex = RunnerJNILib.dsMapCreate();
							RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
							RunnerJNILib.DsMapAddString(dsMapIndex, "event", "dynamic_link_short");
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "status", 0);
							RunnerJNILib.DsMapAddString(dsMapIndex, "data", "N/A");
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
							RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
						}
					}
				});
		return listenerInd;
	}

	public String firebase_dynamic_links_generate_basic(String toLink, String fromLink, String androidPackage,
			String iosPackage) {
		// [START create_short_link]
		DynamicLink dynamicLink = FirebaseDynamicLinks.getInstance().createDynamicLink().setLink(Uri.parse(toLink))
				.setDomainUriPrefix(fromLink)
				.setAndroidParameters(new DynamicLink.AndroidParameters.Builder(androidPackage).build())
				.setIosParameters(new DynamicLink.IosParameters.Builder(iosPackage).build())
				.setSocialMetaTagParameters(new DynamicLink.SocialMetaTagParameters.Builder()
						.setTitle("Example of a Dynamic Link")
						.setDescription("This link works whether the app is installed or not!")
						.setImageUrl(Uri.parse(
								"https://firebasestorage.googleapis.com/v0/b/stickmansurvival-23a0c.appspot.com/o/spr_Hat_1.png?alt=media&token=86a85d2c-d816-4077-abc3-f2181e3bd39c"))// Your
						// url
						// HERE
						.build())
				.buildDynamicLink();

		Uri dynamicLinkUri = dynamicLink.getUri();

		if (dynaDebugMode == true) {
			Log.i("yoyo", "Game Maker Firebase | Dynamic Links : The dynamic link is  : " + dynamicLinkUri);
		}
		return dynamicLinkUri.toString();
	}

	/*
	 * WORK IN PROGRESS //Advanced URL Building public void
	 * firebase_dynamic_links_builder_begin(String arg0, String arg1) { //Example :
	 * firebase_dynamic_links_builder_begin(domain,baseTargetURL); dynaLinkStrExt =
	 * ""; dynaLinkStrExt = arg0 + "/?link=" + arg1; }
	 *
	 * //Set up Advanced URL Android Params public void
	 * firebase_dynamic_links_builder_android_params(String arg0, String arg1,
	 * String arg2) { //Example :
	 * firebase_dynamic_links_builder_android_params(androidPackageName,
	 * androidTargetURL,androidMinimumVer);
	 *
	 * if (arg0!="") { dynaLinkStrExt += "&apn=" + arg0; }
	 *
	 * if (arg1!="") { dynaLinkStrExt += "&afl=" + arg1; }
	 *
	 * if (arg2!="") { dynaLinkStrExt += "&amv=" + arg2; }
	 *
	 * }
	 *
	 * public void firebase_dynamic_links_builder_ios_params(String arg0, String
	 * arg1, String arg2, String arg3) { //Example :
	 * firebase_dynamic_links_builder_ios_params(androidPackageName,androidTargetURL
	 * ,androidMinimumVer);
	 *
	 * if (arg0!="") { dynaLinkStrExt += "&ibi=" + arg0; }
	 *
	 * if (arg1!="") { dynaLinkStrExt += "&isi=" + arg1; }
	 *
	 * if (arg2!="") { dynaLinkStrExt += "&ifl=" + arg2; }
	 *
	 * if (arg3!="") { dynaLinkStrExt += "&imv=" + arg2; }
	 *
	 * }
	 *
	 * //Set up Social Meta Tag Params public void
	 * firebase_dynamic_links_builder_social_params(String arg0, String arg1, String
	 * arg2) { //Example :
	 * firebase_dynamic_links_builder_social_meta_params(title,description,imageURL)
	 * ;
	 *
	 * if (arg0!="") { dynaLinkStrExt += "&st=" + Uri.parse(arg0).toString(); }
	 *
	 * if (arg1!="") { dynaLinkStrExt += "&sd=" + Uri.parse(arg1).toString(); }
	 *
	 * if (arg2!="") { dynaLinkStrExt += "&si=" + Uri.parse(arg2).toString(); }
	 *
	 * }
	 *
	 * public void firebase_dynamic_links_builder_other_params(String arg0) {
	 *
	 * if (arg0!="") { dynaLinkStrExt += "&ofl=" + arg0; }
	 *
	 * } //Set up Analytics Param public void
	 * firebase_dynamic_links_builder_google_analytics_params(String arg0, String
	 * arg1, String arg2, String arg3, String arg4) { //Example :
	 * firebase_dynamic_links_builder_analytics_params(source,medium,campaign);
	 *
	 * if (arg0!="") { dynaLinkStrExt += "&utm_source=" + arg0; }
	 *
	 * if (arg1!="") { dynaLinkStrExt += "&utm_medium=" + arg1; }
	 *
	 * if (arg2!="") { dynaLinkStrExt += "&utm_campaign=" + arg2; }
	 *
	 * if (arg3!="") { dynaLinkStrExt += "&utm_term=" + arg2; }
	 *
	 * if (arg4!="") { dynaLinkStrExt += "&utm_content=" + arg2; }
	 *
	 * }
	 *
	 * public void firebase_dynamic_links_builder_itunes_connect_params(String arg0,
	 * String arg1, String arg2) { //Example :
	 * firebase_dynamic_links_builder_analytics_params(source,medium,campaign);
	 *
	 * if (arg0!="") { dynaLinkStrExt += "&at=" + arg0; }
	 *
	 * if (arg1!="") { dynaLinkStrExt += "&ct=" + arg1; }
	 *
	 * if (arg2!="") { dynaLinkStrExt += "&pt=" + arg2; }
	 *
	 *
	 * }
	 *
	 *
	 * public void firebase_dynamic_links_generate_build() { //Example :
	 * firebase_dynamic_links_shorturl(domain,targetURL,androidPackageName); if
	 * (dynaDebugMode==true) { Log.i("yoyo",
	 * "Game Maker Firebase | Dynamic Links : Long URL is created as " +
	 * dynaLinkStrExt + ". Shortening now..."); }
	 *
	 * Task<ShortDynamicLink> createLinkTask =
	 * FirebaseDynamicLinks.getInstance().createDynamicLink()
	 * .setLongLink(Uri.parse(dynaLinkStrExt)) .buildShortDynamicLink()
	 * .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
	 *
	 * @Override public void onComplete(@NonNull Task<ShortDynamicLink> task) { if
	 * (task.isSuccessful()) { // Short link created Uri shortLink =
	 * task.getResult().getShortLink(); Uri flowchartLink =
	 * task.getResult().getPreviewLink(); //flowchart link is a debugging URL
	 *
	 * if (dynaDebugMode==true) { Log.i("yoyo",
	 * "Game Maker Firebase | Dynamic Links : Short URL is created as " +
	 * shortLink.toString() + "For debugging purposes, use " +
	 * flowchartLink.toString()); }
	 *
	 * final int dsMapIndex = RunnerJNILib.dsMapCreate();
	 * RunnerJNILib.DsMapAddDouble( dsMapIndex, "id", 6161 );
	 * RunnerJNILib.DsMapAddString( dsMapIndex, "category", "dynamic_link_build");
	 * RunnerJNILib.DsMapAddDouble( dsMapIndex, "status", 1);
	 * RunnerJNILib.DsMapAddString( dsMapIndex, "data", shortLink.toString());
	 * RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
	 *
	 * } else {
	 *
	 * if (dynaDebugMode==true) { Log.i("yoyo",
	 * "GMFirebase | Dynamic Links : Couldn't create the Short URL!"); }
	 *
	 * final int dsMapIndex = RunnerJNILib.dsMapCreate();
	 * RunnerJNILib.DsMapAddDouble( dsMapIndex, "id", 6161 );
	 * RunnerJNILib.DsMapAddString( dsMapIndex, "category", "dynamic_link_build");
	 * RunnerJNILib.DsMapAddDouble( dsMapIndex, "status", 0);
	 * RunnerJNILib.DsMapAddString( dsMapIndex, "data", "N/A");
	 * RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL); } }
	 * });
	 *
	 * dynaLinkStrExt = ""; }
	 */
}