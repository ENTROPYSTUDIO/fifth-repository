package ${YYAndroidPackageName};

//Game Maker Studio 2 Packages
import ${YYAndroidPackageName}.R;
import com.yoyogames.runner.RunnerJNILib;
import ${YYAndroidPackageName}.RunnerActivity;

//Some Android Packages
import java.lang.String;
import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;

//Firebase Core Library (Analytics)
import com.google.firebase.analytics.FirebaseAnalytics;

import androidx.annotation.Keep;
//Starts Here
@Keep
public class gmdevbloggamemakerfirebaseanalytics extends RunnerActivity { 
	
	//Debug Modifier
	public static boolean debugMode = false;

	//Firebase Instances
	private FirebaseAnalytics mFirebaseAnalytics;

	public void firebase_analytics_debug_mode(double arg0) {

		//You might ask wtf is this code. With this function, I imitate converting GML boolean to Java boolean.
		if (arg0 >= 0.5)
		{
			debugMode = true;
		}
		else
		{
			debugMode = false;
		}
	}

	/////--------- ANALYTICS INIT ----------////
	public void firebase_analytics_init() {
		mFirebaseAnalytics = FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity.getApplicationContext());
        mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
        Log.i("yoyo", "Game Maker Firebase Analytics Extension. For more information check the blog post : https://gmdevblog.com/game-maker-firebase-analytics/");
        Log.i("yoyo", "For any questions and suggestions, please comment to the post. Credits would be appreciated. Have fun!");
        if (debugMode==true)
        {
        	Log.i("yoyo", "Game Maker Firebase | Analytics : Initiated and Debug mode enabled!");
    	}
		/* The Following Functions are deprecated with the latest API. Just leaving them here.
		if (arg0 != "")
		{
		long minDur = Long.parseLong(arg0);
		mFirebaseAnalytics.setMinimumSessionDuration(minDur);
		}
		
		if (arg1 != "")
		{
		long timeOut = Long.parseLong(arg1);
		mFirebaseAnalytics.setSessionTimeoutDuration(timeOut);
		}*/
	}
	
	//Disable Analytics
	public void firebase_analytics_disable() {
		mFirebaseAnalytics.setAnalyticsCollectionEnabled(false);
		if (debugMode==true)
        {
			Log.i("yoyo", "Game Maker Firebase | Analytics : Disabled data collection on the runtime.");
		}
	}
	
	//Enable Analytics
	public void firebase_analytics_enable() {
		mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
		if (debugMode==true)
        {
			Log.i("yoyo", "Game Maker Firebase | Analytics : Enabled data collection on the runtime.");
		}
	}
	
	//Set User ID
	public void firebase_analytics_set_userid(String arg0) {
		mFirebaseAnalytics.setUserId(arg0);
		if (debugMode==true)
        {
			Log.i("yoyo", "Game Maker Firebase | Analytics : The User ID is set as : " + arg0);
		}
	}
	
	//Set User Property
	public void firebase_analytics_set_userproperty(String arg0, String arg1) {
		mFirebaseAnalytics.setUserProperty(arg0, arg1);
		if (debugMode==true)
        {
			Log.i("yoyo", "Game Maker Firebase | Analytics : The User Property " + arg0 + " is set as " + arg1);
		}
	}
	
	//Set Current Screen
	public void firebase_analytics_set_current_screen(final String arg0) {
		FirebaseAnalytics.getInstance(RunnerActivity.CurrentActivity).setCurrentScreen(RunnerActivity.CurrentActivity,arg0,arg0);
		if (debugMode==true)
		{
			Log.i("yoyo", "Game Maker Firebase | Analytics : The current screen is set as " + arg0);
		}
	}
	
	//New Events ********************
	public void firebase_analytics_event_string(String arg0, String arg1, String arg2) {
		Bundle bundle = new Bundle();
		bundle.putString(arg0, arg1);
		mFirebaseAnalytics.logEvent(arg2, bundle);
		if (debugMode==true)
	    {
	    	Log.i("yoyo", "Game Maker Firebase | Analytics : Logged [STRING] " + arg0 + " : " + arg1 + " to the event " + arg2);
		}
	}
	
	public void firebase_analytics_event_real(String arg0, double arg1, String arg2) {
		Bundle bundle = new Bundle();
		bundle.putDouble(arg0, arg1);
		mFirebaseAnalytics.logEvent(arg2, bundle);
		if (debugMode==true)
	    {
	    Log.i("yoyo", "Game Maker Firebase | Analytics : Logged [REAL] " + arg0 + " : " + arg1 + " to the event " + arg2);
		}
	}

	public void firebase_analytics_event_post_score(double arg0, double arg1, String arg2) {

		double d = arg0;
		long score = (long) d;

		Bundle bundle = new Bundle();
		bundle.putLong(FirebaseAnalytics.Param.SCORE, score);
		
		//Log Level >> Long
		if (arg1 > 0.5)
		{
			double da = arg1;
			long level = (long) da;
			bundle.putLong(FirebaseAnalytics.Param.LEVEL, level);
		}

		//Log Character >> String
		if (arg2 != "") {

			bundle.putString(FirebaseAnalytics.Param.CHARACTER, arg2);
		}
		mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.POST_SCORE, bundle);

		if (debugMode==true)
	    {
	    Log.i("yoyo", "Game Maker Firebase | Analytics : Logged POST_SCORE event");
		}

	}

	public void firebase_analytics_event_earn_virtual_currency(String arg0, double arg1) {

		Bundle bundle = new Bundle();
		bundle.putString(FirebaseAnalytics.Param.VIRTUAL_CURRENCY_NAME, arg0);
		bundle.putDouble(FirebaseAnalytics.Param.VALUE, arg1);
		mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.EARN_VIRTUAL_CURRENCY, bundle);

		if (debugMode==true)
	    {
	    Log.i("yoyo", "Game Maker Firebase | Analytics : Logged EARN_VIRTUAL_CURRENCY event");
		}
	}

	public void firebase_analytics_event_spend_virtual_currency(String arg0, String arg1, double arg2) {

		Bundle bundle = new Bundle();
		bundle.putString(FirebaseAnalytics.Param.VIRTUAL_CURRENCY_NAME, arg0);
		bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, arg1);
		bundle.putDouble(FirebaseAnalytics.Param.VALUE, arg2);
		mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SPEND_VIRTUAL_CURRENCY, bundle);

		if (debugMode==true)
	    {
	    Log.i("yoyo", "Game Maker Firebase | Analytics : Logged SPEND_VIRTUAL_CURRENCY event");
		}
	}

	public void firebase_analytics_event_unlock_achievement(String arg0) {

		Bundle bundle = new Bundle();
		bundle.putString(FirebaseAnalytics.Param.ACHIEVEMENT_ID, arg0);
		mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.UNLOCK_ACHIEVEMENT, bundle);

		if (debugMode==true)
	    {
	    Log.i("yoyo", "Game Maker Firebase | Analytics : Logged UNLOCK_ACHIEVEMENT event");
		}

	}

	public void firebase_analytics_event_select_content(String arg0, String arg1) {

		Bundle bundle = new Bundle();
		bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, arg0);
		bundle.putString(FirebaseAnalytics.Param.ITEM_ID, arg0);
		mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

		if (debugMode==true)
	    {
	    Log.i("yoyo", "Game Maker Firebase | Analytics : Logged SELECT_CONTENT event");
		}

	}

	public void firebase_analytics_event_login(String arg0) {

		Bundle bundle = new Bundle();
		bundle.putString(FirebaseAnalytics.Param.METHOD, arg0);
		mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LOGIN, bundle);

		if (debugMode==true)
	    {
			Log.i("yoyo", "Game Maker Firebase | Analytics : Logged LOGIN event");
		}

	}

	public void firebase_analytics_event_level_start(String arg0) {

		Bundle bundle = new Bundle();
		bundle.putString(FirebaseAnalytics.Param.LEVEL_NAME, arg0);
		mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LEVEL_START, bundle);

		if (debugMode==true)
	    {
	    Log.i("yoyo", "Game Maker Firebase | Analytics : Logged LEVEL_START event");
		}

	}

	public void firebase_analytics_event_level_end(String arg0, double arg1) {

		Bundle bundle = new Bundle();
		bundle.putString(FirebaseAnalytics.Param.LEVEL_NAME, arg0);
		if (arg1 >= 0.5)
		{
			bundle.putLong(FirebaseAnalytics.Param.SUCCESS, 1);
		}
		else
		{
			bundle.putLong(FirebaseAnalytics.Param.SUCCESS, 0);
		}

		mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.LEVEL_END, bundle);

		if (debugMode==true)
	    {
	    Log.i("yoyo", "Game Maker Firebase | Analytics : Logged LEVEL_END event");
		}

	}

	public void firebase_analytics_event_share(String arg0, String arg1, String arg2) {

		Bundle bundle = new Bundle();
		bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, arg0);
		bundle.putString(FirebaseAnalytics.Param.ITEM_ID, arg1);
		bundle.putString(FirebaseAnalytics.Param.METHOD, arg2);
		mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SHARE, bundle);

		if (debugMode==true)
	    {
	    Log.i("yoyo", "Game Maker Firebase | Analytics : Logged SHARE event");
		}
	}


	// public void Firebase_Analytics_Enabled(double enabled)
	// {
	// 	if(enabled >= .5)
	// 		mFirebaseAnalytics.setAnalyticsCollectionEnabled(true);
	// 	else{
	// 		mFirebaseAnalytics.setAnalyticsCollectionEnabled(false);

	// 	}
	// }

	// public void Firebase_Analytics_logEvent(String event, String jsonValues, String jsonKinds)
	// {
	// 	try 
	// 	{
	// 		JSONObject jsonObject = new JSONObject(jsonValues);
	// 		JSONObject jsonObjKinds = new JSONObject(jsonKinds);	
	// 		Bundle bundle = new Bundle();
	// 		Iterator iter = jsonObject.keys();
	// 		while(iter.hasNext())
	// 		{
	// 			String key = (String)iter.next();	
	// 			switch(jsonObjKinds.getString(key))
	// 			{
	// 				case "string":
	// 					Log.i("yoyo","KAGUVA --->" + key + " String: " + jsonObject.getString(key));
	// 					bundle.putString(key,jsonObject.getString(key));
	// 				break;
					
	// 				case "double":					
	// 					Log.i("yoyo","KAGUVA --->" + key + " Double: " + String.valueOf(jsonObject.getDouble(key)));
	// 					bundle.putDouble(key,jsonObject.getDouble(key));
	// 				break;
					
	// 				case "long":
	// 					Log.i("yoyo","KAGUVA --->" + key + " Long: " + String.valueOf( (long) jsonObject.getDouble(key)));
	// 					bundle.putLong(key,(long)jsonObject.getDouble(key));
	// 				break;
	// 			}
	// 		}
	// 	}
	// 	catch (JSONException ignored) 
	// 	{
			
	// 	}
		
	// 	mFirebaseAnalytics.logEvent(event,jsonStringToBundle(jsonValues));
			
	// }
	
	// public static Bundle jsonStringToBundle(String jsonString)
	// {
	// 	try 
	// 	{
	// 		JSONObject jsonObject = new JSONObject(jsonString);
	// 		Bundle bundle = new Bundle();
	// 		Iterator iter = jsonObject.keys();
	// 		while(iter.hasNext())
	// 		{
	// 			String key = (String)iter.next();
	// 			Object aObj = jsonObject.get(key);
	// 			if(aObj instanceof String)
	// 				bundle.putString(key,jsonObject.getString(key));
	// 			else{
	// 				bundle.putDouble(key,jsonObject.getDouble(key));

	// 			}
	// 		}
	// 		return bundle;
	// 	} 
	// 	catch (JSONException ignored) 
	// 	{
	// 	}
	// 	return new Bundle();
	// }
	
}
