{
  "optionsFile": "options.json",
  "options": [],
  "exportToGame": true,
  "supportedTargets": 562949953421320,
  "extensionVersion": "1.0.2",
  "packageId": "",
  "productId": "",
  "author": "",
  "date": "2019-02-25T00:20:39",
  "license": "",
  "description": "",
  "helpfile": "",
  "iosProps": false,
  "tvosProps": false,
  "androidProps": true,
  "installdir": "",
  "files": [
    {"filename":"gmdevbloggamemakerfirebaseanalytics.ext","origname":"","init":"","final":"","kind":4,"uncompress":false,"functions":[
        {"externalName":"firebase_analytics_init","kind":4,"help":"firebase_analytics_init();","hidden":false,"returnType":1,"argCount":0,"args":[],"resourceVersion":"1.0","name":"firebase_analytics_init","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_disable","kind":4,"help":"firebase_analytics_disable();","hidden":false,"returnType":1,"argCount":0,"args":[],"resourceVersion":"1.0","name":"firebase_analytics_disable","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_enable","kind":4,"help":"firebase_analytics_enable();","hidden":false,"returnType":1,"argCount":0,"args":[],"resourceVersion":"1.0","name":"firebase_analytics_enable","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_set_userid","kind":4,"help":"firebase_analytics_set_userid(String userID);","hidden":false,"returnType":1,"argCount":0,"args":[
            1,
          ],"resourceVersion":"1.0","name":"firebase_analytics_set_userid","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_set_userproperty","kind":4,"help":"firebase_analytics_set_userproperty(String key, String value);","hidden":false,"returnType":1,"argCount":0,"args":[
            1,
            1,
          ],"resourceVersion":"1.0","name":"firebase_analytics_set_userproperty","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_set_current_screen","kind":4,"help":"firebase_analytics_set_current_screen(String screenName);","hidden":false,"returnType":1,"argCount":0,"args":[
            1,
          ],"resourceVersion":"1.0","name":"firebase_analytics_set_current_screen","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_event_string","kind":4,"help":"firebase_analytics_event_string(String Key, String Value, String event);","hidden":false,"returnType":1,"argCount":0,"args":[
            1,
            1,
            1,
          ],"resourceVersion":"1.0","name":"firebase_analytics_event_string","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_event_real","kind":4,"help":"firebase_analytics_event_real(String key, Real value, String event);","hidden":false,"returnType":1,"argCount":0,"args":[
            1,
            2,
            1,
          ],"resourceVersion":"1.0","name":"firebase_analytics_event_real","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_debug_mode","kind":4,"help":"firebase_analytics_debug_mode(true/false);","hidden":false,"returnType":1,"argCount":0,"args":[
            2,
          ],"resourceVersion":"1.0","name":"firebase_analytics_debug_mode","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_event_post_score","kind":4,"help":"firebase_analytics_event_post_score(Real Score, Real Level, String Character);","hidden":false,"returnType":1,"argCount":0,"args":[
            2,
            2,
            1,
          ],"resourceVersion":"1.0","name":"firebase_analytics_event_post_score","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_event_earn_virtual_currency","kind":4,"help":"firebase_analytics_event_earn_virtual_currency(String CurrencyName, Real Value);","hidden":false,"returnType":1,"argCount":0,"args":[
            1,
            2,
          ],"resourceVersion":"1.0","name":"firebase_analytics_event_earn_virtual_currency","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_event_spend_virtual_currency","kind":4,"help":"firebase_analytics_event_spend_virtual_currency(String CurrencyName, String itemName, Real Value);","hidden":false,"returnType":1,"argCount":0,"args":[
            1,
            1,
            2,
          ],"resourceVersion":"1.0","name":"firebase_analytics_event_spend_virtual_currency","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_event_unlock_achievement","kind":4,"help":"firebase_analytics_event_unlock_achievement(String AchievementID);","hidden":false,"returnType":1,"argCount":0,"args":[
            1,
          ],"resourceVersion":"1.0","name":"firebase_analytics_event_unlock_achievement","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_event_select_content","kind":4,"help":"firebase_analytics_event_select_content(String ContentType, String ItemID);","hidden":false,"returnType":1,"argCount":0,"args":[
            1,
            1,
          ],"resourceVersion":"1.0","name":"firebase_analytics_event_select_content","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_event_login","kind":4,"help":"firebase_analytics_event_login(String LoginService);","hidden":false,"returnType":1,"argCount":0,"args":[
            1,
          ],"resourceVersion":"1.0","name":"firebase_analytics_event_login","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_event_level_start","kind":4,"help":"firebase_analytics_event_level_start(String LevelName);","hidden":false,"returnType":1,"argCount":0,"args":[
            1,
          ],"resourceVersion":"1.0","name":"firebase_analytics_event_level_start","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_event_level_end","kind":4,"help":"firebase_analytics_event_level_end(String LevelName, True/False Success);","hidden":false,"returnType":1,"argCount":0,"args":[
            1,
            2,
          ],"resourceVersion":"1.0","name":"firebase_analytics_event_level_end","tags":[],"resourceType":"GMExtensionFunction",},
        {"externalName":"firebase_analytics_event_share","kind":4,"help":"firebase_analytics_event_share(String ContentType, String ItemID, String Method);","hidden":false,"returnType":1,"argCount":0,"args":[
            1,
            1,
            1,
          ],"resourceVersion":"1.0","name":"firebase_analytics_event_share","tags":[],"resourceType":"GMExtensionFunction",},
      ],"constants":[],"ProxyFiles":[],"copyToTargets":562949953421320,"order":[
        {"name":"firebase_analytics_init","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_disable","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_enable","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_set_userid","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_set_userproperty","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_set_current_screen","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_event_string","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_event_real","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_debug_mode","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_event_post_score","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_event_earn_virtual_currency","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_event_spend_virtual_currency","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_event_unlock_achievement","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_event_select_content","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_event_login","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_event_level_start","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_event_level_end","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
        {"name":"firebase_analytics_event_share","path":"extensions/GameMakerFirebaseAnalytics/GameMakerFirebaseAnalytics.yy",},
      ],"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMExtensionFile",},
  ],
  "classname": "",
  "tvosclassname": "",
  "tvosdelegatename": "",
  "iosdelegatename": "",
  "androidclassname": "gmdevbloggamemakerfirebaseanalytics",
  "sourcedir": "",
  "androidsourcedir": "",
  "macsourcedir": "",
  "maccompilerflags": "",
  "tvosmaccompilerflags": "",
  "maclinkerflags": "",
  "tvosmaclinkerflags": "",
  "iosplistinject": "",
  "tvosplistinject": "",
  "androidinject": "<meta-data android:name=\"firebase_analytics_collection_enabled\" android:value=\"true\" />",
  "androidmanifestinject": "",
  "androidactivityinject": "",
  "gradleinject": "",
  "iosSystemFrameworkEntries": [],
  "tvosSystemFrameworkEntries": [],
  "iosThirdPartyFrameworkEntries": [],
  "tvosThirdPartyFrameworkEntries": [],
  "IncludedResources": [],
  "androidPermissions": [],
  "copyToTargets": 12,
  "iosCocoaPods": "",
  "tvosCocoaPods": "",
  "iosCocoaPodDependencies": "",
  "tvosCocoaPodDependencies": "",
  "parent": {
    "name": "FIREBASE",
    "path": "folders/Extensions/FIREBASE.yy",
  },
  "resourceVersion": "1.2",
  "name": "GameMakerFirebaseAnalytics",
  "tags": [],
  "resourceType": "GMExtension",
}