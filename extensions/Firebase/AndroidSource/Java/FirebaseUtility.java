package ${YYAndroidPackageName};

import java.util.Map;
import java.util.HashMap;

import org.json.JSONObject;
import org.json.JSONException;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;

import android.util.Log;
import java.lang.String;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import org.json.JSONArray;
import java.lang.NullPointerException;

import androidx.annotation.Keep;

@Keep
public class FirebaseUtility {

    public static DatabaseReference Firebase_DataBase_DecodeRef(String jsonStr) {
        try {
            DatabaseReference dataRef;
            JSONObject reader = new JSONObject(jsonStr);
            String databaseInstance = reader.getString(String.valueOf(0));

            if (databaseInstance.equals("main")) {
                dataRef = FirebaseDatabase.getInstance().getReference();
            } else {
                dataRef = FirebaseDatabase.getInstance(databaseInstance).getReference();
            }

            int num = reader.length();
            int a;
            for (a = 1; a < num; a++) {
                dataRef = dataRef.child(reader.getString(String.valueOf(a)));
            }
            return (dataRef);
        } catch (JSONException e) {
            e.printStackTrace();
            return (null);
        }
    }

    public String ref2json(final DatabaseReference dataRef) {
        int num = 0;
        DatabaseReference auxRef = dataRef;
        while (true) {
            if (auxRef == null) {
                num--;// root
                break;
            }
            num++;
            auxRef = auxRef.getParent();
        }
        HashMap<String, Object> value = new HashMap<>();
        auxRef = dataRef;
        for (int a = num - 1; a >= 0; a--) {
            value.put(String.valueOf(a), auxRef.getKey());
            auxRef = auxRef.getParent();
        }
        return (MapToJSON(value));
        // String result = new JSONObject(value).toString();
        // return (result);
    }

    ///////////////////// THIS 3 FUNCTIONS ONLY FOR CAN USE
    ///////////////////// Firebase_DataBase_setNode(); . . . . .
    public static String MapToJSON(Map map) {
        try {
            return (new JSONObject(map).toString());
        } catch (Exception e) {
            return "{}";
        }
    }

    public static Map<String, Object> jsonToMap(String jsonStr) {
        try {
            JSONObject json = new JSONObject(jsonStr);
            Map<String, Object> retMap = new HashMap<String, Object>();
            if (json != JSONObject.NULL)
                retMap = toMap(json);
            return retMap;
        } catch (JSONException e) {
            return new HashMap<String, Object>();
        }
    }

    public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();
        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else {
                if (value instanceof JSONObject) {
                    value = toMap((JSONObject) value);
                }
            }

            map.put(key, value);
        }
        return map;
    }

    public static List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else {
                if (value instanceof JSONObject) {
                    value = toMap((JSONObject) value);
                }
            }

            list.add(value);
        }
        return list;
    }
}
