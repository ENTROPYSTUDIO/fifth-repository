package ${YYAndroidPackageName};
import com.yoyogames.runner.RunnerJNILib;
import ${YYAndroidPackageName}.FirebaseGM;

import android.app.Activity;
import android.os.Build;

import android.content.Intent;

import java.util.Map;
import java.util.HashMap;
import org.json.JSONObject;

import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.PlayGamesAuthProvider;

import androidx.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import android.util.Log;
import java.lang.String;
import java.lang.NullPointerException;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.ActionCodeSettings;//ENTROPY

import androidx.annotation.Keep;
@Keep
public class FirebaseGmAuth {
    private static final int EVENT_OTHER_SOCIAL = 70;
    private static Activity activity;
    
    public FirebaseGmAuth() {
		activity = RunnerActivity.CurrentActivity;
		new FirebaseAuth.AuthStateListener() {
			@Override
			public void onAuthStateChanged(@NonNull FirebaseAuth FirebaseAuth) {
				//Log.i("yoyo", "maperrortest - onAuthStateChanged");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "AuthStateChanged");
				FirebaseUser user = FirebaseAuth.getCurrentUser();
				if (user != null)
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		};
    }
    
    
	public double Firebase_Auth_getIdToken() {
		final double listenerInd = FirebaseGM.getListenerInd();
		FirebaseUser mUser = FirebaseAuth.getInstance().getCurrentUser();
		mUser.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
			public void onComplete(@NonNull Task<GetTokenResult> task) {
				//Log.i("yoyo", "maperrortest - Firebase_Auth_getIdToken");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "getIdToken");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (task.isSuccessful()) {
					String idToken = task.getResult().getToken();
					RunnerJNILib.DsMapAddString(dsMapIndex, "value", idToken);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
				} else {
					RunnerJNILib.DsMapAddString(dsMapIndex, "value", "null");
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public String Firebase_Auth_getUserInfo() {
		try {
			FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
			if (user != null) {
				HashMap<String, String> UserMap = new HashMap<String, String>();
				UserMap.put("Name", user.getDisplayName());
				UserMap.put("Email", user.getEmail());
				UserMap.put("Uid", user.getUid());

				if (user.isEmailVerified())
					UserMap.put("EmailVerified", "true");
				else {
					UserMap.put("EmailVerified", "false");

				}

				if (user.isAnonymous())
					UserMap.put("Anonymous", "true");
				else {
					UserMap.put("Anonymous", "false");

				}

				int a = 0;
				for (UserInfo userInfo : FirebaseAuth.getInstance().getCurrentUser().getProviderData()) {
					UserMap.put("Provider" + String.valueOf(a), userInfo.getProviderId());
				}

				JSONObject obj = new JSONObject(UserMap);

				return obj.toString();
			} else {
				return "NULL";

			}
		} catch (NullPointerException e) {
			return "NULL";
		}
	}

	public void Firebase_Auth_signOut() {
		FirebaseAuth.getInstance().signOut();
	}

	public double Firebase_Auth_updateProfile(String StringUser) {
		final double listenerInd = FirebaseGM.getListenerInd();
		FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
		UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(StringUser)
				.build();
		user.updateProfile(profileUpdates).addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				//Log.i("yoyo", "maperrortest - Firebase_Auth_updateProfile");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();

				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "updateProfile");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (task.isSuccessful())
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public double Firebase_Auth_updateEmail(String StringEmail) {
		final double listenerInd = FirebaseGM.getListenerInd();
		FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
		user.updateEmail(StringEmail).addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				//Log.i("yoyo", "maperrortest - Firebase_Auth_updateEmail");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "updateEmail");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (task.isSuccessful())
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public double Firebase_Auth_sendEmailVerification() {
		final double listenerInd = FirebaseGM.getListenerInd();
		FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
		user.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				//Log.i("yoyo", "maperrortest - Firebase_Auth_sendEmailVerification");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();

				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "sendEmailVerification");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (task.isSuccessful())
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public double Firebase_Auth_updatePassword(String newPassword) {
		final double listenerInd = FirebaseGM.getListenerInd();
		FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
		user.updatePassword(newPassword).addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				//Log.i("yoyo", "maperrortest - Firebase_Auth_updatePassword");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();

				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "updatePassword");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (task.isSuccessful())
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public double Firebase_Auth_sendPasswordResetEmail(String emailAddress) {
		final double listenerInd = FirebaseGM.getListenerInd();
		FirebaseAuth auth = FirebaseAuth.getInstance();
		auth.sendPasswordResetEmail(emailAddress).addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				//Log.i("yoyo", "maperrortest - Firebase_Auth_sendPasswordResetEmail");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "sendPasswordResetEmail");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (task.isSuccessful())
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public double Firebase_Auth_deleteAccount() {
		final double listenerInd = FirebaseGM.getListenerInd();
		FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
		user.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				//Log.i("yoyo", "maperrortest - Firebase_Auth_deleteAccount");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "deleteAccount");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (task.isSuccessful())
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public double Firebase_Auth_reauthenticate(String email, String password) {
		final double listenerInd = FirebaseGM.getListenerInd();
		FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
		AuthCredential credential = EmailAuthProvider.getCredential(email, password);
		user.reauthenticate(credential).addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				//Log.i("yoyo", "maperrortest - Firebase_Auth_reauthenticate");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "reauthenticate");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public double Firebase_Auth_signInWithEmailAndPassword(String email, String password) {
		final double listenerInd = FirebaseGM.getListenerInd();
		FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password).addOnCompleteListener(activity,
				new OnCompleteListener<AuthResult>() {
					@Override
					public void onComplete(@NonNull Task<AuthResult> task) {
						//Log.i("yoyo", "maperrortest - Firebase_Auth_signInWithEmailAndPassword");
						final int dsMapIndex = RunnerJNILib.dsMapCreate();
						RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
						RunnerJNILib.DsMapAddString(dsMapIndex, "event", "signInWithEmailAndPassword");
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
						if (task.isSuccessful()) {
							Firebase_Auth_getUserInfo();
							Firebase_Auth_getIdToken();
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
						} else {
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);

						}
						RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
					}
				});
		return listenerInd;
	}

	public double Firebase_Auth_createUserWithEmailAndPassword(String email, String password) {
		final double listenerInd = FirebaseGM.getListenerInd();
		FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnCompleteListener(activity,
				new OnCompleteListener<AuthResult>() {
					@Override
					public void onComplete(@NonNull Task<AuthResult> task) {
						//Log.i("yoyo", "maperrortest - Firebase_Auth_createUserWithEmailAndPassword");
						final int dsMapIndex = RunnerJNILib.dsMapCreate();
						RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
						RunnerJNILib.DsMapAddString(dsMapIndex, "event", "createUserWithEmailAndPassword");
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
						if (task.isSuccessful()) {
							Firebase_Auth_getUserInfo();
							Firebase_Auth_getIdToken();
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
						} else {
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);

						}
						RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
					}
				});
		return listenerInd;
	}

	public double Firebase_Auth_signInFacebook(String accessToken_) {
		final double listenerInd = FirebaseGM.getListenerInd();
		AuthCredential mCredential = FacebookAuthProvider.getCredential(accessToken_);
		FirebaseAuth.getInstance().signInWithCredential(mCredential).addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
			@Override
			public void onComplete(@NonNull Task<AuthResult> task) {
				//Log.i("yoyo", "maperrortest - Firebase_Auth_signInFacebook");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "signInFacebook");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (task.isSuccessful()) {
					Firebase_Auth_getUserInfo();
					Firebase_Auth_getIdToken();
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public double Firebase_Auth_signInAnonymously() {
		final double listenerInd = FirebaseGM.getListenerInd();
		FirebaseAuth.getInstance().signInAnonymously().addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
			@Override
			public void onComplete(@NonNull Task<AuthResult> task) {
				//Log.i("yoyo", "maperrortest - Firebase_Auth_signInAnonymously");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "signInAnonymously");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (task.isSuccessful()) {
					Firebase_Auth_getUserInfo();
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
    }
    
	public double Firebase_Auth_signInGooglePlay(String serverAuthCode) {
		final double listenerInd = FirebaseGM.getListenerInd();
		final FirebaseAuth auth = FirebaseAuth.getInstance();
		AuthCredential credential = PlayGamesAuthProvider.getCredential(serverAuthCode);
		auth.signInWithCredential(credential).addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
			@Override
			public void onComplete(@NonNull Task<AuthResult> task) {
				//Log.i("yoyo", "maperrortest - Firebase_Auth_signInGooglePlay");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "signInGooglePlay");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (task.isSuccessful())
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public double Firebase_Auth_signInAnonymouslyToProviderEmail(String email, String emailLink) {
		final double listenerInd = FirebaseGM.getListenerInd();
		AuthCredential mCredential = EmailAuthProvider.getCredentialWithLink(email, emailLink);

		FirebaseAuth.getInstance().getCurrentUser().linkWithCredential(mCredential).addOnCompleteListener(activity,
				new OnCompleteListener<AuthResult>() {
					@Override
					public void onComplete(@NonNull Task<AuthResult> task) {
						if (task.isSuccessful()) {
							//Log.i("yoyo", "maperrortest - Firebase_Auth_signInAnonymouslyToProviderEmail");
							Firebase_Auth_getUserInfo();

							final int dsMapIndex = RunnerJNILib.dsMapCreate();
							RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
							RunnerJNILib.DsMapAddString(dsMapIndex, "event", "signAnonToEmail");
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
							RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

						} else {
							//Log.i("yoyo", "maperrortest - Firebase_Auth_signInAnonymouslyToProviderEmail");
							// ERROR SIGNING IN
							final int dsMapIndex = RunnerJNILib.dsMapCreate();
							RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
							RunnerJNILib.DsMapAddString(dsMapIndex, "event", "signAnonToEmail");
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
							RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
						}
					}
				});
		return listenerInd;
	}

	public double Firebase_Auth_signInAnonymouslyToProviderFacebook(String access_auth_id__) {
		final double listenerInd = FirebaseGM.getListenerInd();
		AuthCredential mCredential = FacebookAuthProvider.getCredential(access_auth_id__);

		FirebaseAuth.getInstance().getCurrentUser().linkWithCredential(mCredential).addOnCompleteListener(activity,
				new OnCompleteListener<AuthResult>() {
					@Override
					public void onComplete(@NonNull Task<AuthResult> task) {
						//Log.i("yoyo", "maperrortest - Firebase_Auth_signInAnonymouslyToProviderFacebook");
						if (task.isSuccessful()) {
							Firebase_Auth_getUserInfo();

							final int dsMapIndex = RunnerJNILib.dsMapCreate();
							RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
							RunnerJNILib.DsMapAddString(dsMapIndex, "event", "signAnonToFacebook");
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
							RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

						} else {
							// ERROR SIGNING IN

							final int dsMapIndex = RunnerJNILib.dsMapCreate();
							RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
							RunnerJNILib.DsMapAddString(dsMapIndex, "event", "signAnonToFacebook");
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
							RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
						}
					}
				});
		return listenerInd;
	}

	public double Firebase_Auth_signInAnonymouslyToProviderGooglePlay(String access_auth_id___) {
		final double listenerInd = FirebaseGM.getListenerInd();
		AuthCredential mCredential = PlayGamesAuthProvider.getCredential(access_auth_id___);

		FirebaseAuth.getInstance().getCurrentUser().linkWithCredential(mCredential).addOnCompleteListener(activity,
				new OnCompleteListener<AuthResult>() {
					@Override
					public void onComplete(@NonNull Task<AuthResult> task) {
						//Log.i("yoyo", "maperrortest - Firebase_Auth_signInAnonymouslyToProviderGooglePlay");
						if (task.isSuccessful()) {
							Firebase_Auth_getUserInfo();

							final int dsMapIndex = RunnerJNILib.dsMapCreate();
							RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
							RunnerJNILib.DsMapAddString(dsMapIndex, "event", "signAnonToPlayGame");
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
							RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

						} else {
							// ERROR SIGNING IN

							final int dsMapIndex = RunnerJNILib.dsMapCreate();
							RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
							RunnerJNILib.DsMapAddString(dsMapIndex, "event", "signAnonToPlayGame");
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
							RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
						}
					}
				});
		return listenerInd;
    }
    
    public void FirebaseSendSignInLink(String email, String deep_link, String Package_name) {

		ActionCodeSettings actionCodeSettings = ActionCodeSettings.newBuilder()
				.setUrl("https://entropyworks.page.link/FirebaseSendSignInLink")
				// This must be true
				.setHandleCodeInApp(true)
				// .setIOSBundleId("com.example.ios")
				.setAndroidPackageName(Package_name, true, /* installIfNotAvailable */
						"19" /* minimumVersion */)
				.setDynamicLinkDomain(deep_link).build();

		// [START auth_send_sign_in_link]
		FirebaseAuth auth = FirebaseAuth.getInstance();
		auth.sendSignInLinkToEmail(email, actionCodeSettings).addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				//Log.i("yoyo", "maperrortest - FirebaseSendSignInLink");
				if (task.isSuccessful()) {

					final int dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
					RunnerJNILib.DsMapAddString(dsMapIndex, "event", "SendSignInLink");
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
				} else {
					// Log.i("yoyo", "Error auth_send_sign_in_link", task.getException());
					final int dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
					RunnerJNILib.DsMapAddString(dsMapIndex, "event", "SendSignInLink");
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
				}
			}
		});
		// [END auth_send_sign_in_link]
	}

	public void FirebaseVerifySignInLink(String email, String emailLink) {
		// [START auth_verify_sign_in_link]
		FirebaseAuth auth = FirebaseAuth.getInstance();
		// Intent intent = RunnerActivity.CurrentActivity.getIntent();
		// String emailLink = intent.getData().toString();

		// Confirm the link is a sign-in with email link.
		if (auth.isSignInWithEmailLink(emailLink)) {
			// Retrieve this from wherever you stored it
			// String email = "someemail@domain.com";

			// The client SDK will parse the code from the link for you.
			auth.signInWithEmailLink(email, emailLink).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
				@Override
				public void onComplete(@NonNull Task<AuthResult> task) {
					//Log.i("yoyo", "maperrortest - FirebaseVerifySignInLink");
					if (task.isSuccessful()) {
						Log.d("yoyo", "Successfully signed in with email link!");
						AuthResult result = task.getResult();
						// You can access the new user via result.getUser()
						// Additional user info profile *not* available via:
						// result.getAdditionalUserInfo().getProfile() == null
						// You can check if the user is new or existing:
						// result.getAdditionalUserInfo().isNewUser()

						final int dsMapIndex = RunnerJNILib.dsMapCreate();
						RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
						RunnerJNILib.DsMapAddString(dsMapIndex, "event", "FirebaseVerifySignInLink");
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
						RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

					} else {
						// Log.i("yoyo", "Error signing in with email link", task.getException());
						final int dsMapIndex = RunnerJNILib.dsMapCreate();
						RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
						RunnerJNILib.DsMapAddString(dsMapIndex, "event", "FirebaseVerifySignInLink");
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
						RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
					}
				}
			});
		}
		// [END auth_verify_sign_in_link]
	}
	// ENTROPY END
}
