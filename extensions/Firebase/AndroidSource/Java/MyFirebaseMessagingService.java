package ${YYAndroidPackageName};

import ${YYAndroidPackageName}.R;
import ${YYAndroidPackageName}.RunnerActivity;
import com.yoyogames.runner.RunnerJNILib;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import androidx.core.app.NotificationCompat;//import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.util.Map;
import android.os.Bundle;

import org.json.JSONObject;

import androidx.annotation.Keep;
@Keep
public class MyFirebaseMessagingService extends FirebaseMessagingService {

  private static final int EVENT_OTHER_SOCIAL = 70;

  @Override
  public void onNewToken(String token) {
    Log.i("yoyo", "maperrortest - onNewToken");
    final int dsMapIndex = RunnerJNILib.dsMapCreate();
    RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
    RunnerJNILib.DsMapAddString(dsMapIndex, "event", "FCM_onNewToken");
    RunnerJNILib.DsMapAddString(dsMapIndex, "value", token);
    RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
  }

  @Override
  public void onMessageReceived(RemoteMessage remoteMessage) {
    Log.i("yoyo", "maperrortest - onMessageReceived");
    // sendMyNotification(message.getNotification().getBody());
    final int dsMapIndex = RunnerJNILib.dsMapCreate();
    RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
    RunnerJNILib.DsMapAddString(dsMapIndex, "event", "FCM_onMessageReceived");
    RunnerJNILib.DsMapAddString(dsMapIndex, "value", new JSONObject(remoteMessage.getData()).toString());
    RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

    String title = remoteMessage.getNotification().getTitle();
    String message = remoteMessage.getNotification().getBody();

    sendNotification(title, message, remoteMessage.getData());
  }

  @Override
  public void onDeletedMessages() {
    Log.i("yoyo", "maperrortest - onDeletedMessages");
    final int dsMapIndex = RunnerJNILib.dsMapCreate();
    RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
    RunnerJNILib.DsMapAddString(dsMapIndex, "event", "FCM_onDeletedMessages");
    RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
  }
  ////////////////////////////////////////////////////////////////////////////////// WEB
  ////////////////////////////////////////////////////////////////////////////////// dont
  ////////////////////////////////////////////////////////////////////////////////// support
  ////////////////////////////////////////////////////////////////////////////////// asscending
  ////////////////////////////////////////////////////////////////////////////////// messages

  @Override
  public void onMessageSent(String msgId) {
    Log.i("yoyo", "maperrortest - onMessageSent");
    final int dsMapIndex = RunnerJNILib.dsMapCreate();
    RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
    RunnerJNILib.DsMapAddString(dsMapIndex, "event", "FCM_onMessageSent");
    RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

  }

  @Override
  public void onSendError(String msgId, Exception exception) {
    Log.i("yoyo", "maperrortest - onSendError");
    final int dsMapIndex = RunnerJNILib.dsMapCreate();
    RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
    RunnerJNILib.DsMapAddString(dsMapIndex, "event", "FCM_onSendError");
    RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

  }

  ///////////////////////////////////////////////////////////////////// NOTIFICATION

  // From here:
  // https://github.com/firebase/quickstart-android/blob/ab0315ab4c7a630ca9c0b8d2a28cdf9c997a3fc6/messaging/app/src/main/java/com/google/firebase/quickstart/fcm/java/MyFirebaseMessagingService.java#L47-L90

  private void sendNotification(String title, String messageBody, Map<String, String> map) {

    Intent intent = new Intent(this, RunnerActivity.class);

    Log.d("yoyo", "STARTHERE");
    // Add the data from cloud
    for (Map.Entry<String, String> entry : map.entrySet()) {
      Log.d("yoyo", entry.getKey() + entry.getValue());
      intent.putExtra(entry.getKey(), entry.getValue());
    }

    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
        PendingIntent.FLAG_ONE_SHOT);

    String channelId = "${YYAndroidPackageName}";// getString(R.string.default_notification_channel_id);
    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
        .setSmallIcon(R.drawable.ic_stat_66).setContentTitle(title).setContentText(messageBody)
        .setAutoCancel(true).setSound(defaultSoundUri).setContentIntent(pendingIntent);
    // .addExtras(bundle);

    NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    // Since android Oreo notification channel is needed.
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title",
          NotificationManager.IMPORTANCE_DEFAULT);
      notificationManager.createNotificationChannel(channel);
    }

    notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
  }
}
