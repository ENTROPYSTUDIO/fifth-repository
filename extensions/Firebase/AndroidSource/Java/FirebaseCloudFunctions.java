package ${YYAndroidPackageName};
import com.yoyogames.runner.RunnerJNILib;

import ${YYAndroidPackageName}.FirebaseGM;
import ${YYAndroidPackageName}.FirebaseUtility;
import java.util.Map;

import androidx.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Continuation;

import android.util.Log;
import java.lang.String;

import com.google.firebase.functions.FirebaseFunctions;//ENTROPY
import com.google.firebase.functions.FirebaseFunctionsException;//ENTROPY
import com.google.firebase.functions.HttpsCallableResult;//ENTROPY

import androidx.annotation.Keep;
@Keep
public class FirebaseCloudFunctions {
	private static final int EVENT_OTHER_SOCIAL = 70;
    // entropy
	// cloud functions
	private Task<String> firebase_cloud_functions_Bulider(final Map<String, Object> value, final String functionsName) {
		return FirebaseFunctions.getInstance().getHttpsCallable(functionsName).call(value)
				.continueWith(new Continuation<HttpsCallableResult, String>() {
					@Override
					public String then(@NonNull Task<HttpsCallableResult> task) throws Exception {
						// This continuation runs on either success or failure, but if the task
						// has failed then getResult() will throw an Exception which will be
						// propagated down.
						String result = (String) task.getResult().getData();
						return result;
					}
				});
	}

	public double firebase_cloud_functions(String json, String functionsName, final double Callback) {
		final double listenerInd = FirebaseGM.getListenerInd();
		// [START firebase_cloud_functions]
		firebase_cloud_functions_Bulider(FirebaseUtility.jsonToMap(json), functionsName)
				.addOnCompleteListener(new OnCompleteListener<String>() {
					@Override
					public void onComplete(@NonNull Task<String> task) {
						//Log.i("yoyo", "maperrortest - firebase_cloud_functions");
						if (Callback > 0.5) {
							final int dsMapIndex = RunnerJNILib.dsMapCreate();
							RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
							RunnerJNILib.DsMapAddString(dsMapIndex, "event", "cloudFunctions");
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);

							if (task.isSuccessful()) {
								String result = task.getResult();
								//Log.i("yoyo", "firebase_cloud_functions:result " + result);
								RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
								RunnerJNILib.DsMapAddString(dsMapIndex, "value", result);
							} else {
								RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
								RunnerJNILib.DsMapAddString(dsMapIndex, "value", "error");
								Exception e = task.getException();
								if (e instanceof FirebaseFunctionsException) {
									FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
									FirebaseFunctionsException.Code code = ffe.getCode();
									Object details = ffe.getDetails();
								}

								// [START_EXCLUDE]
								//Log.i("yoyo", "firebase_cloud_functions:onFailure", e);
								// [END_EXCLUDE]
							}
							RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
						}
					}
				});
		// [END firebase_cloud_functions]

		return listenerInd;
	}
    // cloud functions
}