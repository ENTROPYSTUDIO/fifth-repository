package ${YYAndroidPackageName};

import com.yoyogames.runner.RunnerJNILib;
import ${YYAndroidPackageName}.FirebaseGM;
import ${YYAndroidPackageName}.FirebaseUtility;
import android.os.Handler;

import java.util.Map;
import java.util.HashMap;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;

import androidx.annotation.NonNull;

import android.util.Log;
import java.lang.String;

import androidx.annotation.Keep;
@Keep
public class FirebaseRealtimeDatabaseTransaction {
    private static final int EVENT_OTHER_SOCIAL = 70;

    public double Firebase_Transaction_User_PickObject(String jsonStr, final String userUid) {
        final double listenerInd = FirebaseGM.getListenerInd();

        final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
        if (dataRef == null) {

            return -4;
        }
        dataRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                HashMap currentData = (HashMap) (mutableData.getValue());

                if (currentData == null) {
                    return Transaction.success(mutableData);
                }
                if (!currentData.containsKey("owner")) {
                    currentData.put("owner", userUid);
                } else {
                    Transaction.abort();
                }
                mutableData.setValue(currentData);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean committed, @NonNull DataSnapshot dataSnapshot) {
                // Log.i("yoyo", "maperrortest - Firebase_Transaction_User_PickObject");
                final int dsMapIndex = RunnerJNILib.dsMapCreate();
                // Log.i("yoyo", "Firebase_Transaction_User_PickObject : " +
                // dataSnapshot.getRef().toString());

                RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
                RunnerJNILib.DsMapAddString(dsMapIndex, "event", "transaction");
                RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);

                boolean exists = dataSnapshot.exists();
                if (committed && exists) {
                    HashMap snapMap = (HashMap) (dataSnapshot.getValue());
                    snapMap.put("key", dataSnapshot.getKey());
                    RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
                    RunnerJNILib.DsMapAddString(dsMapIndex, "value", FirebaseUtility.MapToJSON(snapMap));

                    // dataSnapshot.getRef().removeValue();
                } else {
                    // dataSnapshot.getRef().removeValue();
                    RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
                    RunnerJNILib.DsMapAddString(dsMapIndex, "value", "");
                }
                RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

            }
        });
        return listenerInd;
    }

    // fourth-project
    public double Firebase_Transaction_team3v3_occupation(String jsonStr, final String myTeam, final String uid,
            final String enemyTeam, final double servertime, final double occupationDelay) {
        final double listenerInd = FirebaseGM.getListenerInd();

        final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
        if (dataRef == null) {
            return -4;
        }
        dataRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {

                if (mutableData.getValue() == null) {
                    // Map<String, Object> map = new HashMap<String, Object>();
                    // map.put(myTeam, ServerValue.TIMESTAMP);

                    HashMap<String, HashMap<String, Object>> outter = new HashMap<String, HashMap<String, Object>>();
                    HashMap<String, Object> inner = new HashMap<String, Object>();
                    inner.put("timestamp", ServerValue.TIMESTAMP);
                    inner.put("uid", uid);
                    outter.put(myTeam, inner);
                    mutableData.setValue(outter);
                    return Transaction.success(mutableData);
                }
                HashMap currentData = (HashMap) (mutableData.getValue());

                boolean enemyExists = currentData.containsKey(enemyTeam);
                boolean myTeamExists = currentData.containsKey(myTeam);

                if (!enemyExists && myTeamExists) {
                    HashMap mapMyteam = (HashMap) currentData.get(myTeam);
                    long myTeamTimestamp = (long) mapMyteam.get("timestamp");

                    if (servertime > myTeamTimestamp + occupationDelay) {
                        currentData.put("occupation", myTeam);
                    }
                }

                if (!myTeamExists) {
                    Log.i("yoyo", "!myTeamExists");
                    // currentData.put(myTeam, ServerValue.TIMESTAMP);
                    HashMap<String, HashMap<String, Object>> outter = new HashMap<String, HashMap<String, Object>>();
                    HashMap<String, Object> inner = new HashMap<String, Object>();
                    inner.put("timestamp", ServerValue.TIMESTAMP);
                    inner.put("uid", uid);
                    outter.put(myTeam, inner);
                    mutableData.setValue(outter);
                    return Transaction.success(mutableData);
                }
                if (enemyExists) {
                    currentData.remove(enemyTeam);
                    if (myTeamExists) {
                        currentData.remove(myTeam);
                    }
                }

                mutableData.setValue(currentData);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean committed, @NonNull DataSnapshot dataSnapshot) {
                // Log.i("yoyo", "maperrortest - Firebase_Transaction_team3v3_occupation");
                final int dsMapIndex = RunnerJNILib.dsMapCreate();

                // Log.i("yoyo", "Firebase_Transaction_team3v3_occupation : " +
                // dataSnapshot.getRef().toString());

                RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
                RunnerJNILib.DsMapAddString(dsMapIndex, "event", "team3v3Transaction");
                RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);

                Log.i("yoyo", "postTransaction:onComplete:" + databaseError);
                // if(committed && dataSnapshot.exists()){
                // HashMap snapMap = (HashMap)dataSnapshot.getValue();
                // if(snapMap.containsKey("occupation")){
                // String teamName = currentData.get();
                // if(teamName == myTeam){
                // RunnerJNILib.DsMapAddDouble( dsMapIndex, "occupation" , 1);
                // }
                // }
                // }
                RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
            }
        });
        return listenerInd;
    }

    public double Firebase_Transaction_team3v3_occupation_out(String jsonStr, final String myTeam) {
        final double listenerInd = FirebaseGM.getListenerInd();

        final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
        if (dataRef == null) {
            return -4;
        }
        dataRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {

                if (mutableData.getValue() == null) {
                    return Transaction.success(mutableData);
                }
                HashMap currentData = (HashMap) (mutableData.getValue());
                boolean myTeamExists = currentData.containsKey(myTeam);
                if (myTeamExists) {
                    currentData.remove(myTeam);
                }
                mutableData.setValue(currentData);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean committed, @NonNull DataSnapshot dataSnapshot) {
                // Log.i("yoyo", "maperrortest - onComplete");
                if (databaseError == null) {
                    final int dsMapIndex = RunnerJNILib.dsMapCreate();
                    // Log.i("yoyo", "Firebase_Transaction_team3v3_occupation_out : " +
                    // dataSnapshot.getRef().toString());
                    RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
                    RunnerJNILib.DsMapAddString(dsMapIndex, "event", "team3v3Transaction");
                    RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
                    RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
                }
            }
        });
        return listenerInd;
    }

    public double Firebase_Transaction_Count_Change(String jsonStr, final double amount) {
        final double listenerInd = FirebaseGM.getListenerInd();

        final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
        if (dataRef == null) {
            return -4;
        }
        dataRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Integer currentData = mutableData.getValue(Integer.class);
                if (currentData == null) {
                    mutableData.setValue(amount);
                } else {
                    mutableData.setValue(currentData + amount);
                }
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean committed, @NonNull DataSnapshot dataSnapshot) {
                // Log.i("yoyo", "maperrortest - Firebase_Transaction_Count_Change");
                final int dsMapIndex = RunnerJNILib.dsMapCreate();
                // Log.i("yoyo", "Firebase_Transaction_Count_Change : " +
                // dataSnapshot.getRef().toString());
                RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
                RunnerJNILib.DsMapAddString(dsMapIndex, "event", "transaction");
                RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);

                boolean exists = dataSnapshot.exists();
                if (committed && exists) {
                    RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
                } else {
                    RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
                }
                RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

            }
        });
        return listenerInd;
    }

    public double firebase_transaction_resource_change(String jsonStr, final double amount) {
        final double listenerInd = FirebaseGM.getListenerInd();
        Log.i("yoyo", Double.toString(amount));
        final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
        if (dataRef == null) {
            return -4;
        }
        dataRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {

                if (mutableData.getValue() == null) {
                    Log.i("yoyo", "null");
                    return Transaction.success(mutableData);
                }

                String currentData  = mutableData.getValue(String.class);
                double parseResource    = Double.parseDouble(currentData);
                double value    = Math.floor(parseResource+amount);
                if(value <= 0){
                    Log.i("yoyo", "abort");
                    Transaction.abort();
                }

                String finalValue   = Double.toString(value);
                mutableData.setValue(finalValue);
                Log.i("yoyo", "finalValue");
                Log.i("yoyo", finalValue);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean committed, @NonNull DataSnapshot dataSnapshot) {
                final int dsMapIndex = RunnerJNILib.dsMapCreate();

                RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
                RunnerJNILib.DsMapAddString(dsMapIndex, "event", "transaction");
                RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);

                boolean exists = dataSnapshot.exists();
                if (committed && exists) {
                    RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
                } else {
                    RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
                }
                RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
            }
        });
        return listenerInd;
    }
}