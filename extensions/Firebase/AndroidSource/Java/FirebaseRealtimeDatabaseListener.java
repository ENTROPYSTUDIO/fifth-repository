package ${YYAndroidPackageName};

import com.yoyogames.runner.RunnerJNILib;
import ${YYAndroidPackageName}.FirebaseGM;
import ${YYAndroidPackageName}.FirebaseUtility;
import java.util.Map;
import java.util.HashMap;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Query;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.util.Log;
import java.lang.String;

import androidx.annotation.Keep;
@Keep
public class FirebaseRealtimeDatabaseListener {
	private static final int EVENT_OTHER_SOCIAL = 70;

	@Nullable
	private static HashMap<String, ValueEventListener> valueListernerMap;
	@Nullable	
	private static HashMap<String, ChildEventListener> childListernerMap;
	@Nullable	
	private static HashMap<String, DatabaseReference> valueListernerRefMap;
	@Nullable	
	private static HashMap<String, DatabaseReference> childListernerRefMap;
	
	private static double valueListernerInd = 0;
	private static double childListenerInd = 0;

	public FirebaseRealtimeDatabaseListener() {
		valueListernerMap = new HashMap<String, ValueEventListener>();
		valueListernerRefMap = new HashMap<String, DatabaseReference>();

		childListernerMap = new HashMap<String, ChildEventListener>();
		childListernerRefMap = new HashMap<String, DatabaseReference>();
	}


	//////////////////////////////////////////////////// Firebase DataBase
	// info connected
	public double firebase_connection_manager(String ref1, String ref2) {
		final double listenerInd = FirebaseGM.getListenerInd();
		// [START rtdb_full_connection_example]
		// Since I can connect from multiple devices, we store each connection instance
		// separately
		// any time that connectionsRef's value is null (i.e. has no children) I am
		// offline
		final DatabaseReference myConnectionsRef = FirebaseUtility.Firebase_DataBase_DecodeRef(ref1);
		// Stores the timestamp of my last disconnect (the last time I was seen online)
		final DatabaseReference lastOnlineRef = FirebaseUtility.Firebase_DataBase_DecodeRef(ref2);

		final DatabaseReference connectedRef = FirebaseDatabase.getInstance().getReference().child(".info/connected");
		connectedRef.addValueEventListener(new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot snapshot) {
				//Log.i("yoyo", "maperrortest - firebase_connection_manager");
				if(snapshot.exists())
				{
					boolean connected = snapshot.getValue(Boolean.class);
					// final int dsMapIndex = RunnerJNILib.dsMapCreate();
					// RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
					// RunnerJNILib.DsMapAddString(dsMapIndex, "event", "infoConnected");
					// RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
					if (connected) {
						Log.i("yoyo", "connected");
						// RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
	
						// DatabaseReference con = myConnectionsRef.push();
	
						// When this device disconnects, remove it
						// con.onDisconnect().removeValue();
	
						// When I disconnect, update the last time I was seen online
						lastOnlineRef.onDisconnect().setValue(ServerValue.TIMESTAMP);
	
						// Add this device to my connections list
						// this value could contain info about the device or a timestamp too
						myConnectionsRef.setValue(ServerValue.TIMESTAMP);
					} else {
						Log.i("yoyo", "not connected");
						// RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
					}
					// RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
				}
			}

			@Override
			public void onCancelled(DatabaseError error) {
				Log.i("yoyo", "Listener was cancelled at .info/connected");
			}
		});
		// [END rtdb_full_connection_example]
		return listenerInd;
	}

	static private void valueEventListenerToMaps(final DatabaseReference dataRef, final ValueEventListener valueEventListener,
			double ind) {
		dataRef.addValueEventListener(valueEventListener);
		valueListernerMap.put(String.valueOf(ind), valueEventListener);
		valueListernerRefMap.put(String.valueOf(ind), dataRef);
	}

	static private void childEventListenerToMaps(final DatabaseReference dataRef, final ChildEventListener ChildEventListener,
			double ind) {
		dataRef.addChildEventListener(ChildEventListener);

		childListernerMap.put(String.valueOf(ind), ChildEventListener);
		childListernerRefMap.put(String.valueOf(ind), dataRef);
	}

	public void Firebase_DataBase_RemoveListener(double ind) {
		if (valueListernerRefMap.containsKey(String.valueOf(ind))) {
			DatabaseReference dataRef = valueListernerRefMap.remove(String.valueOf(ind));
			dataRef.getRef().removeEventListener(valueListernerMap.remove(String.valueOf(ind)));
		} else if (childListernerRefMap.containsKey(String.valueOf(ind))) {
			DatabaseReference dataRef = childListernerRefMap.remove(String.valueOf(ind));
			dataRef.getRef().removeEventListener(childListernerMap.remove(String.valueOf(ind)));
		}
	}

	public void Firebase_DataBase_RemoveAllListeners(double t) {
		for (String key : valueListernerRefMap.keySet()) {
			DatabaseReference dataRef = valueListernerRefMap.get(key);
			dataRef.removeEventListener(valueListernerMap.get(key));
		}
		for (String key : childListernerRefMap.keySet()) {
			DatabaseReference dataRef = childListernerRefMap.get(key);
			dataRef.removeEventListener(childListernerMap.get(key));
		}
		valueListernerMap.clear();
		childListernerMap.clear();
		valueListernerRefMap.clear();
		childListernerRefMap.clear();

		Log.i("yoyo", "Firebase_DataBase_RemoveAllListeners");
	}

	public double Firebase_DataBase_ReferenceExists(String jsonStr) {
		final double listenerInd = FirebaseGM.getListenerInd();
		final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}
		dataRef.addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				//Log.i("yoyo", "maperrortest - Firebase_DataBase_ReferenceExists");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ReferenceExists");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (dataSnapshot.exists())
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
				else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}

			@Override
			public void onCancelled(DatabaseError error) {
				Log.i("yoyo", "Firebase_DataBase_ReferenceExists " + error.getMessage());
				// final int dsMapIndex = RunnerJNILib.dsMapCreate();
				// RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				// RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ReferenceExists");
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "error", 1);

				// RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}


	public double Firebase_DataBase_startListener_String(String jsonStr) {
		final double listenerInd = FirebaseGM.getListenerInd();
		final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}
		ValueEventListener valueEventListener = new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				//Log.i("yoyo", "maperrortest - Firebase_DataBase_startListener_String");

				if (dataSnapshot.exists()) {
					final int dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
					RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_String");
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);

					String value = dataSnapshot.getValue(String.class);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					RunnerJNILib.DsMapAddString(dsMapIndex, "value", value);
					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
				} else {
					final int dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
					RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_String");
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);

					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
				}
				
			}

			@Override
			public void onCancelled(DatabaseError error) {
				Log.i("yoyo", "Firebase_DataBase_startListener_String " + error.getMessage());
				// final int dsMapIndex = RunnerJNILib.dsMapCreate();
				// RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				// RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_String");
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "error", 1);

				// RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		};
		valueEventListenerToMaps(dataRef, valueEventListener, listenerInd);
		return listenerInd;
	}

	public double Firebase_DataBase_startListener_Double(String jsonStr) {
		final double listenerInd = FirebaseGM.getListenerInd();
		final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}
		ValueEventListener valueEventListener = new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				//Log.i("yoyo", "maperrortest - Firebase_DataBase_startListener_Double");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_Double");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (dataSnapshot.exists()) {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					double value = (double) (dataSnapshot.getValue(long.class));
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", value);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}

			@Override
			public void onCancelled(DatabaseError error) {
				Log.i("yoyo", "Firebase_DataBase_startListener_Double " + error.getMessage());
				// final int dsMapIndex = RunnerJNILib.dsMapCreate();
				// RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				// RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_Double");
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "error", 1);

				// RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		};
		valueEventListenerToMaps(dataRef, valueEventListener, listenerInd);
		return (listenerInd);
	}

	public double Firebase_DataBase_startListener_Node(String jsonStr) {
		final double listenerInd = FirebaseGM.getListenerInd();
		final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}
		ValueEventListener valueEventListener = new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				if (dataSnapshot.exists()) {
					int dsMapIndex	= -1;
					dsMapIndex = RunnerJNILib.dsMapCreate();
					if(dsMapIndex != -1) {
						RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
						RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_Node");
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);

						//Log.i("yoyo", "maperrortest - Firebase_DataBase_startListener_Node - "+dataSnapshot.getValue());
						if ((dataSnapshot.getValue()) instanceof Map) {
							HashMap value = (HashMap)(dataSnapshot.getValue());
							if(value != null)
							{
								RunnerJNILib.DsMapAddString(dsMapIndex, "value", FirebaseUtility.MapToJSON(value));
								RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 3);
							}
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
						} else if ((dataSnapshot.getValue()) instanceof Long) {
							double value = (double) (dataSnapshot.getValue(long.class));
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", value);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 0);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
						} else if ((dataSnapshot.getValue()) instanceof Double) {
							double value = dataSnapshot.getValue(double.class);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", value);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 1);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
						} else if ((dataSnapshot.getValue()) instanceof String) {
							String value = dataSnapshot.getValue(String.class);
							RunnerJNILib.DsMapAddString(dsMapIndex, "value", value);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 2);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
						} else {
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
						}
						RunnerJNILib.DsMapAddString(dsMapIndex, "childKey", dataSnapshot.getKey());
						RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
						//Log.i("yoyo", "maperrortest - Firebase_DataBase_startListener_Nodeok");
					}
				}
				else{
					final int dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
					RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_Node");
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
				}
			}

			@Override
			public void onCancelled(DatabaseError error) {
				// Log.i("yoyo", "Firebase_DataBase_startListener_Node " + error.getMessage() +
				// " - " + jsonStr);

				// final int dsMapIndex = RunnerJNILib.dsMapCreate();
				// RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				// RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_Node");
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "error", 1);

				// RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		};
		valueEventListenerToMaps(dataRef, valueEventListener, listenerInd);
		return listenerInd;
	}

	public double Firebase_DataBase_startListenerForSingle_String(String jsonStr) {
		final double listenerInd = FirebaseGM.getListenerInd();
		final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}
		dataRef.addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				//Log.i("yoyo", "maperrortest - Firebase_DataBase_startListenerForSingle_String");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_String");
				// RunnerJNILib.DsMapAddString( dsMapIndex, "ref",
				// ref2json(dataSnapshot.getRef()));
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (dataSnapshot.exists()) {
					String value = dataSnapshot.getValue(String.class);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					RunnerJNILib.DsMapAddString(dsMapIndex, "value", value);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

				}

				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}

			@Override
			public void onCancelled(DatabaseError error) {
				Log.i("yoyo", "Firebase_DataBase_startListenerForSingle_String " + error.getMessage());
				// final int dsMapIndex = RunnerJNILib.dsMapCreate();
				// RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				// RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_String");
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "error", 1);

				// RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public double Firebase_DataBase_startListenerForSingle_Double(String jsonStr) {
		final double listenerInd = FirebaseGM.getListenerInd();
		final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}
		dataRef.addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				//Log.i("yoyo", "maperrortest - Firebase_DataBase_startListenerForSingle_Double");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_Double");
				// RunnerJNILib.DsMapAddString( dsMapIndex, "ref",
				// ref2json(dataSnapshot.getRef()) );
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (dataSnapshot.exists()) {
					double value = (double) (dataSnapshot.getValue(long.class));
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", value);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

				}

				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}

			@Override
			public void onCancelled(DatabaseError error) {
				Log.i("yoyo", "Firebase_DataBase_startListenerForSingle_Double " + error.getMessage());
				// final int dsMapIndex = RunnerJNILib.dsMapCreate();
				// RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				// RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_Double");
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "error", 1);

				// RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public double Firebase_DataBase_startListenerForSingle_Node(String jsonStr) {
		final double listenerInd = FirebaseGM.getListenerInd();
		final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}
		dataRef.addListenerForSingleValueEvent(new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				//Log.i("yoyo", "maperrortest - Firebase_DataBase_startListenerForSingle_Node");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_Node");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (dataSnapshot.exists()) {
					if ((dataSnapshot.getValue()) instanceof Map) {
						HashMap value = (HashMap) (dataSnapshot.getValue());
						if(value != null)
						{
							RunnerJNILib.DsMapAddString(dsMapIndex, "value", FirebaseUtility.MapToJSON(value));
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 3);
						}
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					} else if ((dataSnapshot.getValue()) instanceof Long) {
						double value = (double) (dataSnapshot.getValue(long.class));
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", value);
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 0);
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					} else if ((dataSnapshot.getValue()) instanceof Double) {
						double value = dataSnapshot.getValue(double.class);
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", value);
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 1);
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					} else if ((dataSnapshot.getValue()) instanceof String) {
						String value = dataSnapshot.getValue(String.class);
						RunnerJNILib.DsMapAddString(dsMapIndex, "value", value);
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 2);
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					} else {
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
					}
					RunnerJNILib.DsMapAddString(dsMapIndex, "childKey", dataSnapshot.getKey());
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}

			@Override
			public void onCancelled(DatabaseError error) {
				Log.i("yoyo", "Firebase_DataBase_startListenerForSingle_Node " + error.getMessage());
				// final int dsMapIndex = RunnerJNILib.dsMapCreate();
				// RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				// RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_Node");
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "error", 1);
				// RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public double Firebase_DataBase_startListener_Ordened(double OrderType, String OrderKey, double FilterType,
			String FilterKey, double FilterType2, String FilterKey2, double LimitType, double LimitValue,
			String jsonStr) {
		final double listenerInd = FirebaseGM.getListenerInd();
		final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}
		Query mQuery = (Query) dataRef;
		switch ((int) OrderType) {
			case 0:
				mQuery = mQuery.orderByChild(OrderKey);
				break;
			case 1:
				mQuery = mQuery.orderByKey();
				break;
			case 2:
				mQuery = mQuery.orderByValue();
				break;
		}

		switch ((int) FilterType) {
			case 0:
				mQuery = mQuery.startAt(FilterKey);
				break;
			case 1:
				mQuery = mQuery.endAt(FilterKey);
				break;
			case 2:
				mQuery = mQuery.equalTo(FilterKey);
				break;
		}

		switch ((int) FilterType2) {
			case 0:
				mQuery = mQuery.startAt(FilterKey2);
				break;
			case 1:
				mQuery = mQuery.endAt(FilterKey2);
				break;
			case 2:
				mQuery = mQuery.equalTo(FilterKey2);
				break;
		}

		switch ((int) LimitType) {
			case 0:
				mQuery = mQuery.limitToFirst((int) LimitValue);
				break;
			case 1:
				mQuery = mQuery.limitToLast((int) LimitValue);
				break;
		}

		ValueEventListener valueEventListener = new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				//Log.i("yoyo", "maperrortest - Firebase_DataBase_startListener_Ordened");
				int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_Ordened_Start");
				// RunnerJNILib.DsMapAddString( dsMapIndex, "ref",
				// ref2json(dataSnapshot.getRef()) );
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (dataSnapshot.exists()) {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

				for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
					String nodeStr = "";
					if ((postSnapshot.getValue()) instanceof Map) {
						HashMap value = (HashMap) (postSnapshot.getValue());
						nodeStr = FirebaseUtility.MapToJSON(value);
					} else {
						nodeStr = postSnapshot.getValue().toString();
					}

					dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
					RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_Ordened");
					// RunnerJNILib.DsMapAddString( dsMapIndex, "ref",
					// ref2json(dataSnapshot.getRef()) );
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
					if (dataSnapshot.exists()) {
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
						RunnerJNILib.DsMapAddString(dsMapIndex, "value", nodeStr);
					} else {
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

					}
					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

				}
				dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_Ordened_End");
				// RunnerJNILib.DsMapAddString( dsMapIndex, "ref",
				// ref2json(dataSnapshot.getRef()) );
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (dataSnapshot.exists()) {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}

			@Override
			public void onCancelled(DatabaseError error) {
				Log.i("yoyo", "Firebase_DataBase_startListener_Ordened " + error.getMessage());
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_Ordened_Start");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "error", 1);

				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}

		};
		valueEventListenerToMaps(dataRef, valueEventListener, listenerInd);
		return listenerInd;
	}

	public double Firebase_DataBase_startListenerForSingle_Ordened(double OrderType, String OrderKey, double FilterType,
			String FilterKey, double FilterType2, String FilterKey2, double LimitType, double LimitValue,
			String jsonStr) {
		final double listenerInd = FirebaseGM.getListenerInd();
		final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);

		if (dataRef == null) {
			return -4;
		}
		Query mQuery = (Query) dataRef;

		switch ((int) OrderType) {
			case 0:
				mQuery = mQuery.orderByChild(OrderKey);
				break;
			case 1:
				mQuery = mQuery.orderByKey();
				break;
			case 2:
				mQuery = mQuery.orderByValue();
				break;
		}

		switch ((int) FilterType) {
			case 0:
				mQuery = mQuery.startAt(FilterKey);
				break;
			case 1:
				mQuery = mQuery.endAt(FilterKey);
				break;
			case 2:
				mQuery = mQuery.equalTo(FilterKey);
				break;
		}

		switch ((int) FilterType2) {
			case 0:
				mQuery = mQuery.startAt(FilterKey2);
				break;
			case 1:
				mQuery = mQuery.endAt(FilterKey2);
				break;
			case 2:
				mQuery = mQuery.equalTo(FilterKey2);
				break;
		}

		switch ((int) LimitType) {
			case 0:
				mQuery = mQuery.limitToFirst((int) LimitValue);
				break;
			case 1:
				mQuery = mQuery.limitToLast((int) LimitValue);
				break;
		}

		ValueEventListener valueEventListener = new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_Ordened_Start");
				// RunnerJNILib.DsMapAddString( dsMapIndex, "ref",
				// ref2json(dataSnapshot.getRef()) );
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (dataSnapshot.exists()) {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

				for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
					String nodeStr = "";
					if ((postSnapshot.getValue()) instanceof Map) {
						HashMap value = (HashMap) (postSnapshot.getValue());
						nodeStr = FirebaseUtility.MapToJSON(value);
					} else {
						nodeStr = postSnapshot.getValue().toString();
					}

					dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
					RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_Ordened");
					// RunnerJNILib.DsMapAddString( dsMapIndex, "ref",
					// ref2json(dataSnapshot.getRef()) );
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
					if (dataSnapshot.exists()) {
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
						RunnerJNILib.DsMapAddString(dsMapIndex, "value", nodeStr);
					} else {
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

					}

					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

				}
				dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_Ordened_End");
				// RunnerJNILib.DsMapAddString( dsMapIndex, "ref",
				// ref2json(dataSnapshot.getRef()) );
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (dataSnapshot.exists()) {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}

			@Override
			public void onCancelled(DatabaseError error) {
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_Ordened");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "error", 1);

				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		};
		mQuery.addListenerForSingleValueEvent(valueEventListener);
		return listenerInd;
	}

	public double Firebase_DataBase_startListenerForSingle_Ordened_FilterDouble(double OrderType, String OrderKey,
			double FilterType, double FilterKey, double FilterType2, double FilterKey2, double LimitType,
			double LimitValue, String jsonStr) {
		final double listenerInd = FirebaseGM.getListenerInd();
		final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}
		Query mQuery = (Query) dataRef;

		switch ((int) OrderType) {
			case 0:
				mQuery = mQuery.orderByChild(OrderKey);
				break;
			case 1:
				mQuery = mQuery.orderByKey();
				break;
			case 2:
				mQuery = mQuery.orderByValue();
				break;
		}

		switch ((int) FilterType) {
			case 0:
				mQuery = mQuery.startAt(FilterKey);
				break;
			case 1:
				mQuery = mQuery.endAt(FilterKey);
				break;
			case 2:
				mQuery = mQuery.equalTo(FilterKey);
				break;
		}

		switch ((int) FilterType2) {
			case 0:
				mQuery = mQuery.startAt(FilterKey2);
				break;
			case 1:
				mQuery = mQuery.endAt(FilterKey2);
				break;
			case 2:
				mQuery = mQuery.equalTo(FilterKey2);
				break;
		}

		switch ((int) LimitType) {
			case 0:
				mQuery = mQuery.limitToFirst((int) LimitValue);
				break;
			case 1:
				mQuery = mQuery.limitToLast((int) LimitValue);
				break;
		}

		ValueEventListener valueEventListener = new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
				int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_Ordened_Start");
				// RunnerJNILib.DsMapAddString( dsMapIndex, "ref",
				// ref2json(dataSnapshot.getRef()) );
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (dataSnapshot.exists()) {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

				for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
					String nodeStr = "";
					if ((postSnapshot.getValue()) instanceof Map) {
						HashMap value = (HashMap) (postSnapshot.getValue());
						nodeStr = FirebaseUtility.MapToJSON(value);
					} else {
						nodeStr = postSnapshot.getValue().toString();
					}

					dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
					RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_Ordened");
					// RunnerJNILib.DsMapAddString( dsMapIndex, "ref",
					// ref2json(dataSnapshot.getRef()) );
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
					if (dataSnapshot.exists()) {
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
						RunnerJNILib.DsMapAddString(dsMapIndex, "value", nodeStr);
					} else {
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

					}

					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

				}
				dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_Ordened_End");
				// RunnerJNILib.DsMapAddString( dsMapIndex, "ref",
				// ref2json(dataSnapshot.getRef()) );
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				if (dataSnapshot.exists()) {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}

			@Override
			public void onCancelled(DatabaseError error) {
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_ForSingle_Ordened_Start");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "error", 1);

				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		};
		mQuery.addListenerForSingleValueEvent(valueEventListener);
		return listenerInd;
	}


    
	public double logListenerCount(String dd) {
		Log.i("yoyo", "valueListernerMap - " + valueListernerMap.size());
		Log.i("yoyo", "valueListernerRefMap - " + valueListernerRefMap.size());
		Log.i("yoyo", "childListernerMap - " + childListernerMap.size());
		Log.i("yoyo", "childListernerRefMap - " + childListernerRefMap.size());
		return 0;
    }
    
	public double Firebase_Database_ServerTimeOffset() {
		final double listenerInd = FirebaseGM.getListenerInd();
		final DatabaseReference dataRef = FirebaseDatabase.getInstance().getReference(".info/serverTimeOffset");
		ValueEventListener valueEventListener = new ValueEventListener() {
			@Override
			public void onDataChange(@NonNull DataSnapshot snapshot) {
				if(snapshot.exists()){
					//Log.i("yoyo", "maperrortest - Firebase_Database_ServerTimeOffset");
					double offset = snapshot.getValue(Double.class);
					// double estimatedServerTimeMs = System.currentTimeMillis() + offset;

					final int dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
					RunnerJNILib.DsMapAddString(dsMapIndex, "event", "ServerTimeOffset");
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", offset);
					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
				}
			}

			@Override
			public void onCancelled(@NonNull DatabaseError error) {
				Log.i("yoyo", "Listener was cancelled");
			}
		};
		valueEventListenerToMaps(dataRef, valueEventListener, listenerInd);
		return listenerInd;
	}

	public double Firebase_DataBase_startListener_Child(String jsonStr) {
		final double listenerInd = FirebaseGM.getListenerInd();
		final DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}
		ChildEventListener ChildEventListener = new ChildEventListener() {
			@Override
			public void onChildAdded(@NonNull DataSnapshot dataSnapshot, String prevChildKey) {
				if (dataSnapshot.exists()) {
					int dsMapIndex	= -1;
					dsMapIndex = RunnerJNILib.dsMapCreate();
					if(dsMapIndex != -1) {
						RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
						RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_onChildAdded");
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);

						if ((dataSnapshot.getValue()) instanceof Map) {
							HashMap value = (HashMap)(dataSnapshot.getValue());
							if(value != null)
							{
								RunnerJNILib.DsMapAddString(dsMapIndex, "value", FirebaseUtility.MapToJSON(value));
								RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 3);
							}
						} else if ((dataSnapshot.getValue()) instanceof Double) {
							double value = dataSnapshot.getValue(double.class);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", value);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 1);
						} else if ((dataSnapshot.getValue()) instanceof String) {
							String value = dataSnapshot.getValue(String.class);
							RunnerJNILib.DsMapAddString(dsMapIndex, "value", value);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 2);
						} else if ((dataSnapshot.getValue()) instanceof Long) {
							double value = (double) (dataSnapshot.getValue(long.class));
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", value);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 0);
						} else {
							Log.i("yoyo", "childListener Error Key " + dataSnapshot.getKey());
							Log.i("yoyo", "childListener Error value " + dataSnapshot.getValue());
						}

						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
						RunnerJNILib.DsMapAddString(dsMapIndex, "childKey", dataSnapshot.getKey());
						RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
					}
				}				
				else {
					final int dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
					RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_onChildAdded");
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
				}
			}

			@Override
			public void onChildChanged(@NonNull DataSnapshot dataSnapshot, String prevChildKey) {
				if (dataSnapshot.exists()) {
					int dsMapIndex	= -1;
					dsMapIndex = RunnerJNILib.dsMapCreate();
					if(dsMapIndex != -1) {
						RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
						RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_onChildChanged");
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);

						if ((dataSnapshot.getValue()) instanceof Map) {
							HashMap value = (HashMap) (dataSnapshot.getValue());
							if(value != null)
							{
								RunnerJNILib.DsMapAddString(dsMapIndex, "value", FirebaseUtility.MapToJSON(value));
								RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 3);
							}
						} else if ((dataSnapshot.getValue()) instanceof Double) {
							double value = dataSnapshot.getValue(double.class);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", value);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 1);
						} else if ((dataSnapshot.getValue()) instanceof Long) {
							double value = (double) (dataSnapshot.getValue(long.class));
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", value);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 0);
						} else if ((dataSnapshot.getValue()) instanceof String) {
							String value = dataSnapshot.getValue(String.class);
							RunnerJNILib.DsMapAddString(dsMapIndex, "value", value);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 2);
						} else {
							Log.i("yoyo", "childListener Error Key " + dataSnapshot.getKey());
							Log.i("yoyo", "childListener Error value " + dataSnapshot.getValue());
						}

						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
						RunnerJNILib.DsMapAddString(dsMapIndex, "childKey", dataSnapshot.getKey());
						RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
					}
				} 	
				else {
					final int dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
					RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_onChildChanged");
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
				}
			}

			@Override
			public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
				if (dataSnapshot.exists()) {
					int dsMapIndex	= -1;
					dsMapIndex = RunnerJNILib.dsMapCreate();
					if(dsMapIndex != -1) {
						RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
						RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_onChildRemoved");
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);

						if ((dataSnapshot.getValue()) instanceof Map) {
							HashMap value = (HashMap) (dataSnapshot.getValue());
							if(value != null)
							{
								RunnerJNILib.DsMapAddString(dsMapIndex, "value", FirebaseUtility.MapToJSON(value));
								RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 3);
							}

						} else if ((dataSnapshot.getValue()) instanceof Long) {
							double value = (double) (dataSnapshot.getValue(long.class));
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", value);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 0);
						} else if ((dataSnapshot.getValue()) instanceof Double) {
							double value = dataSnapshot.getValue(double.class);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", value);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 1);
						} else if ((dataSnapshot.getValue()) instanceof String) {
							String value = dataSnapshot.getValue(String.class);
							RunnerJNILib.DsMapAddString(dsMapIndex, "value", value);
							RunnerJNILib.DsMapAddDouble(dsMapIndex, "dataType", 2);
						}

						else {
							Log.i("yoyo", "childListener Error Key " + dataSnapshot.getKey());
							Log.i("yoyo", "childListener Error value " + dataSnapshot.getValue());
						}

						RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
						RunnerJNILib.DsMapAddString(dsMapIndex, "childKey", dataSnapshot.getKey());
						RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
					}
				}				
				else {
					final int dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
					RunnerJNILib.DsMapAddString(dsMapIndex, "event", "DataBase_onChildRemoved");
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);

					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
				}
			}

			@Override
			public void onChildMoved(@NonNull DataSnapshot dataSnapshot, String prevChildKey) {
			}

			@Override
			public void onCancelled(@NonNull DatabaseError databaseError) {

			}
		};

		childEventListenerToMaps(dataRef, ChildEventListener, listenerInd);
		return listenerInd;
	}
	// [ENTROPY]
}
