package ${YYAndroidPackageName};

import ${YYAndroidPackageName}.R;
import ${YYAndroidPackageName}.RunnerActivity;

import com.yoyogames.runner.RunnerJNILib;

import android.view.View;
import android.app.Activity;
import android.os.Build;
import android.os.Handler;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;

import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnSuccessListener;

import android.util.Log;
import java.lang.String;

import androidx.annotation.Keep;
@Keep
public class FirebaseGM extends RunnerSocial {
	private static final int EVENT_OTHER_SOCIAL = 70;

	public static Activity activity;

	@Nullable	
	private FirebaseRemoteConfig mFirebaseRemoteConfig;
	// private FirebaseAnalytics mFirebaseAnalytics;
	
	private static double valueListernerInd = 0;
	private static double childListenerInd = 0;

	public FirebaseGM() {
		activity = RunnerActivity.CurrentActivity;
		mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
	}

	@Override
	public void onStart() {

		/////////////////////////////////////////////////////// Firebase FCM
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() {
				// do something

				RunnerActivity.intentHandle(activity.getIntent());

			}
		}, 2000);// time in milisecond
	}

	@Override
	public void onStop() {
	}

	@Override
	public void onPause() {
	}

	@Override
	public void onResume() {
	}

	static public double getListenerInd() {
		valueListernerInd++;
		return (valueListernerInd);
	}

	public void Firebase_UserMessage(final String message) {
		RunnerActivity.ViewHandler.post(new Runnable() {
			public void run() {
				Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
			}
		});
	}

	//////////////////////////////////////////////////// Firebase RemoteConfig
	//////////////////////////////////////////////////// ////////////////////////////////////////////////////
	public void Firebase_remote_config_init(double debugMode) {
		Log.i("yoyo", "FIrebaseRemoteConfigInit");
		boolean modeEnabled = false;
		if (debugMode > 0.5) {
			modeEnabled = true;
		}
		long IntevalSeconds = modeEnabled ? 300L : 10800L;		
		
		FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
				.setMinimumFetchIntervalInSeconds(IntevalSeconds)
				.build();
		mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);
		mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_default);
		Log.i("yoyo", "FIrebaseRemoteConfigInstance - "+mFirebaseRemoteConfig);
	}

	public void Firebase_RemoteConfig_fetch() {
		Log.i("yoyo", "Firebase_RemoteConfig_fetch_ Start");
		mFirebaseRemoteConfig.fetchAndActivate().addOnCompleteListener(activity, new OnCompleteListener<Boolean>() {
			@Override
			public void onComplete(@NonNull Task<Boolean> task) {
				//Log.i("yoyo", "maperrortest - Firebase_RemoteConfig_fetch");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "RemoteConfig_fetch");
				if (task.isSuccessful()) {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
	}

	public String Firebase_RemoteConfig_getString(String key) {
		return mFirebaseRemoteConfig.getString(key);
	}

	public double Firebase_RemoteConfig_getDouble(String key) {
		return mFirebaseRemoteConfig.getDouble(key);
	}

	///////////////////////////////// Firebase Cloud Messaging
	public void Firebase_FCM_getToken() {
		// FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(new OnSuccessListener<InstanceIdResult>() {
		// 	@Override
		// 	public void onSuccess(InstanceIdResult instanceIdResult) {
		// 		//Log.i("yoyo", "maperrortest - Firebase_FCM_getToken");
		// 		String token = instanceIdResult.getToken();
		// 		final int dsMapIndex = RunnerJNILib.dsMapCreate();
		// 		RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
		// 		RunnerJNILib.DsMapAddString(dsMapIndex, "event", "FCM_getToken");
		// 		RunnerJNILib.DsMapAddString(dsMapIndex, "value", token);
		// 		RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
		// 	}
		// });
		FirebaseMessaging.getInstance().getToken()
		.addOnCompleteListener(new OnCompleteListener<String>() {
			@Override
			public void onComplete(@NonNull Task<String> task) {
			  if (!task.isSuccessful()) {
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "FCM_getToken");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			  }
			  else
			  {
				// Get new FCM registration token
				String token = task.getResult();
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "FCM_getToken");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
				RunnerJNILib.DsMapAddString(dsMapIndex, "value", token);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			  }
			}
		});
	}

	public void Firebase_FCM_subscribeToTopic(String Topic) {
		FirebaseMessaging.getInstance().subscribeToTopic(Topic).addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				//Log.i("yoyo", "maperrortest - Firebase_FCM_subscribeToTopic");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "FCM_subscribeToTopic");
				if (task.isSuccessful()) {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
	}

	public void Firebase_FCM_unsubscribeFromTopic(String Topic) {
		FirebaseMessaging.getInstance().unsubscribeFromTopic(Topic).addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				//Log.i("yoyo", "maperrortest - Firebase_FCM_unsubscribeFromTopic");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", "FCM_unsubscribeFromTopic");
				if (task.isSuccessful()) {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
				} else {
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				}
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
	}

	public double Firebase_FCM_isAutoInitEnabled() {
		if (FirebaseMessaging.getInstance().isAutoInitEnabled()) {
			return (1);
		} else {
			return (0);
		}
	}

	public void Firebase_FCM_setAutoInitEnabled(double eneable) {
		if (eneable > .5) {
			FirebaseMessaging.getInstance().setAutoInitEnabled(true);

		} else {
			FirebaseMessaging.getInstance().setAutoInitEnabled(false);

		}
	}

	///////////////////////////////////////////////////////////////////////////ENTROPY
	// ENTROPY START
	public double testMethod(String _temp) {
		String varS	= null;
		//Log.i("yoyo", "maperrortest - testMethod");
		final double listenerInd = getListenerInd();
		final int dsMapIndex = RunnerJNILib.dsMapCreate();
		RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
		RunnerJNILib.DsMapAddString(dsMapIndex, "event", "asyncTest");
		RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);

		RunnerJNILib.DsMapAddString(dsMapIndex, "test", varS);
		RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
		
		return listenerInd;
	}
	// entropy
	public static void android_view_BRIGHTNESS_OVERRIDE_FULL(boolean _enable) {
		final boolean enable = _enable;
		RunnerActivity.ViewHandler.post(new Runnable() {
			public void run() {
				if (enable) {
					android.view.WindowManager.LayoutParams params = RunnerActivity.CurrentActivity.getWindow()
							.getAttributes();
					params.screenBrightness = 1.0f;
					RunnerActivity.CurrentActivity.getWindow().setAttributes(params);
					// RunnerActivity.CurrentActivity.getWindow().clearFlags(android.view.WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL);
				} // end if

			}
		});
	}
	// [ENTROPY]
}
