package ${YYAndroidPackageName};

import com.yoyogames.runner.RunnerJNILib;
import ${YYAndroidPackageName}.FirebaseGM;
import ${YYAndroidPackageName}.FirebaseUtility;
import java.util.Map;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;

import androidx.annotation.NonNull;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import android.util.Log;
import java.lang.String;

import androidx.annotation.Keep;
@Keep
public class FirebaseRealtimeDatabaseSet {
	private static final int EVENT_OTHER_SOCIAL = 70;

	public String firebaseDatabaseGetPushKey(String jsonStr) {
		DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		dataRef = dataRef.push();
		String pushKey = dataRef.getKey();
		return pushKey;
	}

	private double databaseUpdateValueNoCallback(final DatabaseReference dataRef, final Map<String, Object> value,
			double push) {
		final double listenerInd = FirebaseGM.getListenerInd();
		DatabaseReference ref = dataRef;
		if (push > 0.5)
			ref = ref.push();
		ref.updateChildren(value);
		return listenerInd;
	}

	private double databaseSetValueNoCallback(final DatabaseReference dataRef, final Object value, double push) {
		final double listenerInd = FirebaseGM.getListenerInd();
		DatabaseReference ref = dataRef;
		if (push > 0.5)
			ref = ref.push();
		ref.setValue(value);
		return listenerInd;
	}

	private double databaseSetValueNoCallback(final DatabaseReference dataRef, final Map<String, Object> value,
			double push) {
		final double listenerInd = FirebaseGM.getListenerInd();
		DatabaseReference ref = dataRef;
		if (push > 0.5)
			ref = ref.push();
		ref.setValue(value);
		return listenerInd;
	}

	private double databaseSetValue(final DatabaseReference dataRef, final Object value, final String asyncEvent,
			double push) {
		final double listenerInd = FirebaseGM.getListenerInd();
		DatabaseReference ref = dataRef;
		if (push > 0.5)
			ref = ref.push();
		ref.setValue(value).addOnSuccessListener(new OnSuccessListener<Void>() {
			@Override
			public void onSuccess(Void aVoid) {
				// Log.i("yoyo", "maperrortest - databaseSetValue");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();

				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", asyncEvent);
				//// RunnerJNILib.DsMapAddString( dsMapIndex, "ref",ref2json(dataRef));
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		}).addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception e) {
				Log.i("yoyo", e.getMessage());
				// final int dsMapIndex = RunnerJNILib.dsMapCreate();
				// RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				// RunnerJNILib.DsMapAddString(dsMapIndex, "event", asyncEvent);
				// //// RunnerJNILib.DsMapAddString( dsMapIndex, "ref",ref2json(dataRef));
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				// RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	private double databaseSetValue(final DatabaseReference dataRef, final Map<String, Object> value,
			final String asyncEvent, double push) {
		final double listenerInd = FirebaseGM.getListenerInd();
		DatabaseReference ref = dataRef;
		if (push == 1)
			ref = ref.push();
		ref.setValue(value).addOnSuccessListener(new OnSuccessListener<Void>() {
			@Override
			public void onSuccess(Void aVoid) {
				// Log.i("yoyo", "maperrortest - databaseSetValueMap<String, Object>");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", asyncEvent);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

			}
		}).addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception e) {
				Log.i("yoyo", e.getMessage());
				// final int dsMapIndex = RunnerJNILib.dsMapCreate();
				// RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				// RunnerJNILib.DsMapAddString(dsMapIndex, "event", asyncEvent);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				// RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	private double databaseUpdateValue(final DatabaseReference dataRef, final Map<String, Object> value,
			final String asyncEvent, double push) {
		final double listenerInd = FirebaseGM.getListenerInd();
		DatabaseReference ref = dataRef;
		if (push == 1)
			ref = ref.push();
		ref.updateChildren(value).addOnSuccessListener(new OnSuccessListener<Void>() {
			@Override
			public void onSuccess(Void aVoid) {
				// Log.i("yoyo", "maperrortest - databaseUpdateValue");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", asyncEvent);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		}).addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception e) {
				Log.i("yoyo", e.getMessage());
				// final int dsMapIndex = RunnerJNILib.dsMapCreate();
				// RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				// RunnerJNILib.DsMapAddString(dsMapIndex, "event", asyncEvent);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				// RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	private double databaseRemoveValue(final DatabaseReference dataRef, final String asyncEvent) {
		final double listenerInd = FirebaseGM.getListenerInd();
		dataRef.removeValue().addOnSuccessListener(new OnSuccessListener<Void>() {
			@Override
			public void onSuccess(Void aVoid) {
				// Log.i("yoyo", "maperrortest - databaseRemoveValue");
				final int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				RunnerJNILib.DsMapAddString(dsMapIndex, "event", asyncEvent);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 1);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 1);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		}).addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception e) {
				// final int dsMapIndex = RunnerJNILib.dsMapCreate();
				// RunnerJNILib.DsMapAddString(dsMapIndex, "type", "Firebase");
				// RunnerJNILib.DsMapAddString(dsMapIndex, "event", asyncEvent);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "exists", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "value", 0);
				// RunnerJNILib.DsMapAddDouble(dsMapIndex, "Id", listenerInd);
				// RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});
		return listenerInd;
	}

	public double Firebase_DataBase_setString(String jsonStr, String str, double push, double callback) {
		DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}

		if (callback > 0.5) {
			return (databaseSetValue(dataRef, str, "DataBase_setString", push));
		} else {
			return (databaseSetValueNoCallback(dataRef, str, push));
		}

	}

	public double Firebase_DataBase_setDouble(String jsonStr, double value, double push, double callback) {
		DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}

		if (callback > 0.5) {
			return (databaseSetValue(dataRef, value, "DataBase_setDouble", push));
		} else {
			return (databaseSetValueNoCallback(dataRef, value, push));
		}

	}

	public double Firebase_DataBase_setTimeStamp(String jsonStr, double push, double callback) {
		DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}

		if (callback > 0.5) {
			return (databaseSetValue(dataRef, ServerValue.TIMESTAMP, "DataBase_setTimeStamp", push));
		} else {
			return (databaseSetValueNoCallback(dataRef, ServerValue.TIMESTAMP, push));
		}
	}

	public double Firebase_DataBase_setNode(String jsonStr, String json, double push, double callback,
			double update_bool) {
		DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}
		if (update_bool > 0.5) {
			if (callback > 0.5) {
				return (databaseUpdateValue(dataRef, FirebaseUtility.jsonToMap(json), "DataBase_setNode", push));
			} else {
				return (databaseUpdateValueNoCallback(dataRef, FirebaseUtility.jsonToMap(json), push));
			}
		} else {
			if (callback > 0.5) {
				return (databaseSetValue(dataRef, FirebaseUtility.jsonToMap(json), "DataBase_setNode", push));
			} else {
				return (databaseSetValueNoCallback(dataRef, FirebaseUtility.jsonToMap(json), push));
			}
		}
	}

	public double Firebase_DataBase_removeValue(String jsonStr) {
		DatabaseReference dataRef = FirebaseUtility.Firebase_DataBase_DecodeRef(jsonStr);
		if (dataRef == null) {
			return -4;
		}
		return (databaseRemoveValue(dataRef, "DataBase_removeValue"));
	}
}