package ${YYAndroidPackageName};

//Game Maker Studio 2 Packages
import ${YYAndroidPackageName}.R;
import com.yoyogames.runner.RunnerJNILib;
import ${YYAndroidPackageName}.RunnerActivity;

//Some Android & Java Packages
import java.lang.String;
import android.app.Activity;
import android.os.Bundle;
import android.content.Intent;
import android.util.Log;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import androidx.annotation.NonNull;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;

//Firestore Packages
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.SetOptions;

import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.Query.Direction;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

//Starts Here
import androidx.annotation.Keep;
@Keep
public class gmdevbloggamemakerfirebasefirestore extends RunnerActivity {

	// Social Async Event Code
	final static int EVENT_OTHER_SOCIAL = 70;
	// Debug Modifier
	public static boolean debMod = false;

	// Singleton for Firestore
	private FirebaseFirestore db;

	// Static Map for Writing Operations
	static final Map<String, Object> WRITING_LIST = new HashMap<String, Object>();
	// static final List<Object> listenerRegList = new ArrayList<>();

	// Reference Global for Querying
	public static Query QUERY_REF = null;

	public void firebase_firestore_init() {
		// AUTOMATICALLY INITIALIZED BY GAME MAKER STUDIO 2. Don't do it manually.
		// Firebase Instance
		db = FirebaseFirestore.getInstance();
	}

	public void firebase_firestore_debug_mode(double arg0) {

		// You might ask wtf is this code. With this function, I imitate converting GML
		// boolean to Java boolean.
		if (arg0 >= 0.5) {
			debMod = true;
		} else {
			debMod = false;
		}

	}

	// Used for console debug logging.
	public void debugLog(String category, String debugLog) {
		if (debMod == true) {
			Log.i("yoyo", "GMFirebase | Firestore [" + category + "]: " + debugLog);
		}
	}

	// #### ENABLE & DISABLE NETWORK.
	public void firebase_firestore_disable_network() {

		db.disableNetwork().addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				debugLog("Disable Network",
						"All network operations are disabled. Firestore is forced to use local cache.");
				int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "[category]", "firestore_network_disabled");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "[status]", 1);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});

	}

	public void firebase_firestore_enable_network() {

		db.enableNetwork().addOnCompleteListener(new OnCompleteListener<Void>() {
			@Override
			public void onComplete(@NonNull Task<Void> task) {
				debugLog("Enable Network",
						"All network operations are enabled. Firestore is now using online database primarily.");
				int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "[category]", "firestore_network_enabled");
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "[status]", 1);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});

	}

	/*
	 * I'M JUST PUTTING THIS HERE CUZ IDK.
	 * 
	 * ░░░░░░░░░♚░░░░░░░░░░ ░░░░░░ ( ͡° ͜ʖ ͡°)░░░░░░░░░ ░░░░░░███████ ]▄▄▄▄▄▄▄
	 * ░▂▄▅█████████▅▄▃▂ I███████████████████]. ░◥⊙▲⊙▲⊙▲⊙▲⊙▲⊙▲⊙◤░
	 */

	/// ## DATA WRITING OPERATIONS
	// Clear the Map
	public void firebase_firestore_write_begin() {

		WRITING_LIST.clear();

	}

	// Write String
	public void firebase_firestore_write_string(String arg0, String arg1) {

		WRITING_LIST.put(arg0, arg1);

	}

	// Write Double
	public void firebase_firestore_write_double(String arg0, double arg1) {

		WRITING_LIST.put(arg0, arg1);

	}

	// Write the Data >> With Set
	public void firebase_firestore_write_set(final String arg0) {

		db.document(arg0).set(WRITING_LIST).addOnSuccessListener(new OnSuccessListener<Void>() {
			@Override
			public void onSuccess(Void aVoid) {
				debugLog("Write Data/Set", "The document " + arg0 + " is successfully SET.");
				int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "[category]", "firestore_write_set");
				RunnerJNILib.DsMapAddString(dsMapIndex, "[location]", arg0);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "[status]", 1);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

			}
		}).addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception e) {
				String errCode = e.toString();
				debugLog("Write Data/Set", "Error while SETting the document " + arg0 + ". Error code : " + errCode);
				int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "[category]", "firestore_write_set");
				RunnerJNILib.DsMapAddString(dsMapIndex, "[location]", arg0);
				RunnerJNILib.DsMapAddString(dsMapIndex, "[error]", errCode);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "[status]", 0);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});

	}

	// Write the Data >> With Add
	public void firebase_firestore_write_add(final String arg0) {

		db.collection(arg0).add(WRITING_LIST).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
			@Override
			public void onSuccess(DocumentReference documentReference) {
				debugLog("Write Data/Add",
						"The document " + arg0 + " is successfully ADDED to the collection in " + arg0);

				int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "[category]", "firestore_write_add");
				RunnerJNILib.DsMapAddString(dsMapIndex, "[location]", arg0);
				RunnerJNILib.DsMapAddString(dsMapIndex, "[document_id]", documentReference.getId());
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "[status]", 1);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		}).addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception e) {
				String errCode = e.toString();
				debugLog("Write Data/Add",
						"Error while ADDing the document into the collection " + arg0 + ". Error code : " + errCode);

				int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "[category]", "firestore_write_add");
				RunnerJNILib.DsMapAddString(dsMapIndex, "[location]", arg0);
				RunnerJNILib.DsMapAddString(dsMapIndex, "[error]", errCode);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "[status]", 0);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});

	}

	// Write the Data >> With Merge
	public void firebase_firestore_write_merge(final String arg0) {

		db.document(arg0).set(WRITING_LIST, SetOptions.merge()).addOnSuccessListener(new OnSuccessListener<Void>() {
			@Override
			public void onSuccess(Void aVoid) {
				debugLog("Write Data/Merge", "The document " + arg0 + " is successfully MERGED.");

				int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "[category]", "firestore_write_merge");
				RunnerJNILib.DsMapAddString(dsMapIndex, "[location]", arg0);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "[status]", 1);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

			}
		}).addOnFailureListener(new OnFailureListener() {
			@Override
			public void onFailure(@NonNull Exception e) {
				String errCode = e.toString();
				debugLog("Write Data/Merge", "Error while MERGING the document " + arg0 + ". Error code : " + errCode);

				int dsMapIndex = RunnerJNILib.dsMapCreate();
				RunnerJNILib.DsMapAddString(dsMapIndex, "[category]", "firestore_write_merge");
				RunnerJNILib.DsMapAddString(dsMapIndex, "[location]", arg0);
				RunnerJNILib.DsMapAddString(dsMapIndex, "[error]", errCode);
				RunnerJNILib.DsMapAddDouble(dsMapIndex, "[status]", 0);
				RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
			}
		});

	}

	// ### BASIC DATA READING OPERATIONS (NOT QUERIES)

	// Get a simple document
	public void firebase_firestore_get_document(final String arg0) {

		db.document(arg0).get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
			@Override
			public void onComplete(@NonNull Task<DocumentSnapshot> task) {
				if (task.isSuccessful()) {
					DocumentSnapshot document = task.getResult();
					if (document.exists()) {

						debugLog("Read/Get Document", "Successfully read the document " + arg0);
						debugLog("Read/Get Document", "Data <> Key values from " + arg0 + " are >>>");
						int dsMapIndex = RunnerJNILib.dsMapCreate();
						Map<String, Object> map = document.getData();

						for (Map.Entry<String, Object> entry : map.entrySet()) {
							String key = entry.getKey();
							String val = entry.getValue().toString();
							RunnerJNILib.DsMapAddString(dsMapIndex, key, val);
							if (debMod == true) {
								Log.i("yoyo", key + " : " + val);
							}
						}

						RunnerJNILib.DsMapAddString(dsMapIndex, "[category]", "firestore_get_document");
						RunnerJNILib.DsMapAddString(dsMapIndex, "[location]", arg0);
						RunnerJNILib.DsMapAddString(dsMapIndex, "[id]", document.getId());
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "[status]", 1);
						RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

					} else {
						debugLog("Read/Get Document", "The document " + arg0 + " does not exist!");

						int dsMapIndex = RunnerJNILib.dsMapCreate();
						RunnerJNILib.DsMapAddString(dsMapIndex, "[category]", "firestore_get_document");
						RunnerJNILib.DsMapAddString(dsMapIndex, "[location]", arg0);
						RunnerJNILib.DsMapAddString(dsMapIndex, "[id]", "N/A");
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "[status]", 0);
						RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);

					}
				} else {

					String errCode = task.getException().toString();
					debugLog("Read/Get Document", "Error when reading the document. Error code : " + errCode);

					int dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "[category]", "firestore_get_document");
					RunnerJNILib.DsMapAddString(dsMapIndex, "[location]", arg0);
					RunnerJNILib.DsMapAddString(dsMapIndex, "[id]", "N/A");
					RunnerJNILib.DsMapAddString(dsMapIndex, "[error]", errCode);
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "[status]", -1);
					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
				}
			}
		});

		// get document end
	}

	/// ## QUERY OPERATIONS

	// Indicating collection address
	public void firebase_firestore_query_create(String arg0) {
		debugLog("Query Create", "Started building query.");
		QUERY_REF = db.collection(arg0);

	}

	// Adding "==" to the query.
	public void firebase_firestore_query_equal_to(String arg0, String arg1, double arg2) {

		if (arg2 == 0) {

			QUERY_REF = QUERY_REF.whereEqualTo(arg0, arg1);
		} else {

			double val = Double.parseDouble(arg1);
			QUERY_REF = QUERY_REF.whereEqualTo(arg0, val);
		}

	}

	// Adding ">=" to the query.
	public void firebase_firestore_query_greater_than_or_equal_to(String arg0, double arg1) {

		QUERY_REF = QUERY_REF.whereGreaterThanOrEqualTo(arg0, arg1);

	}

	// Adding "<=" to the query.
	public void firebase_firestore_query_less_than_or_equal_to(String arg0, double arg1) {

		QUERY_REF = QUERY_REF.whereLessThanOrEqualTo(arg0, arg1);

	}

	// Adding ">" to the query.
	public void firebase_firestore_query_greater_than(String arg0, double arg1) {

		QUERY_REF = QUERY_REF.whereGreaterThan(arg0, arg1);
	}

	// Adding "<" to the query.
	public void firebase_firestore_query_less_than(String arg0, double arg1) {

		QUERY_REF = QUERY_REF.whereLessThan(arg0, arg1);
	}

	// Limiting the amount of document fetched.
	public void firebase_firestore_query_limit(double arg0) {

		int val = (int) arg0;
		QUERY_REF = QUERY_REF.limit(val);

	}

	// Reformatting the fetching order by "Value" (String)
	public void firebase_firestore_query_orderby(String arg0) {

		QUERY_REF = QUERY_REF.orderBy(arg0);

	}

	// Reformatting the order by "Value" (Real) & Ascending or Descending order
	public void firebase_firestore_query_orderby_ext(String arg0, double arg1) {

		if (arg1 == 0) {
			QUERY_REF = QUERY_REF.orderBy(arg0, Direction.ASCENDING);
		} else {
			QUERY_REF = QUERY_REF.orderBy(arg0, Direction.DESCENDING);
		}

	}

	// Indicating StartAt, which position to start query.
	public void firebase_firestore_query_startat(String arg0, String arg1, double arg2) {

		if (arg2 == 0) {

			QUERY_REF = QUERY_REF.startAt(arg0, arg1);
		} else {

			double val = Double.parseDouble(arg1);
			QUERY_REF = QUERY_REF.startAt(arg0, val);
		}

	}

	/// Indicating EndAt, which position to end the query
	public void firebase_firestore_query_endat(String arg0, String arg1, double arg2) {

		if (arg2 == 0) {

			QUERY_REF = QUERY_REF.endAt(arg0, arg1);
		} else {

			double val = Double.parseDouble(arg1);
			QUERY_REF = QUERY_REF.endAt(arg0, val);
		}

	}

	/// Simply get the results of the Query!
	public void firebase_firestore_query_execute_get(final String arg0) {

		QUERY_REF.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
			@Override
			public void onComplete(@NonNull Task<QuerySnapshot> task) {
				if (task.isSuccessful()) {
					debugLog("Query Execute/Get", "Query results marked as #" + arg0
							+ " are fetched successfully! The documents are fetched(If there are any)...");

					for (QueryDocumentSnapshot document : task.getResult()) {
						String docID, docData;
						docID = document.getId();
						// docData = document.getData();
						debugLog("Query Document/Get", "#" + arg0 + " Marked Query >> + Document ID : " + docID);

						int dsMapIndex = RunnerJNILib.dsMapCreate();
						RunnerJNILib.DsMapAddString(dsMapIndex, "[category]", "firestore_query_document");
						RunnerJNILib.DsMapAddString(dsMapIndex, "[query_mark]", arg0);
						RunnerJNILib.DsMapAddString(dsMapIndex, "[id]", docID);
						RunnerJNILib.DsMapAddDouble(dsMapIndex, "[status]", 1);

						Map<String, Object> map = document.getData();

						for (Map.Entry<String, Object> entry : map.entrySet()) {
							String key = entry.getKey();
							String val = entry.getValue().toString();
							RunnerJNILib.DsMapAddString(dsMapIndex, key, val);
							if (debMod == true) {
								Log.i("yoyo", key + " : " + val);
							}
						}
						RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
					}
				} else {
					debugLog("Query Execute/Get", "Error while executing the query for the #Mark : " + arg0);
					int dsMapIndex = RunnerJNILib.dsMapCreate();
					RunnerJNILib.DsMapAddString(dsMapIndex, "[category]", "firestore_query_document");
					RunnerJNILib.DsMapAddString(dsMapIndex, "[query_mark]", arg0);
					RunnerJNILib.DsMapAddString(dsMapIndex, "[id]", "N/A");
					RunnerJNILib.DsMapAddDouble(dsMapIndex, "[status]", 0);
					RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
				}
			}
		});
	}
}

/*
 * ///Listen to the Query! public void firebase_firestore_query_execute_listen()
 * {
 * 
 * QUERY_REF.addSnapshotListener(new EventListener<QuerySnapshot>() {
 * 
 * @Override public void onEvent(@Nullable QuerySnapshot snapshots,
 * 
 * @Nullable FirebaseFirestoreException e) {
 * 
 * if (e != null) { if (debMod==true) { Log.i("yoyo",
 * "GMFirebase | Firestore : QUERY LISTENER ERROR"); } return; }
 * 
 * for (DocumentChange dChange : snapshots.getDocumentChanges()) { switch
 * (dChange.getType()) { case ADDED: if (debMod==true) { Log.i("yoyo",
 * "GMFirebase | Firestore[Query Listener]. New document is added : " +
 * dChange.getDocument().getData()); } break; case MODIFIED: if (debMod==true) {
 * Log.i("yoyo",
 * "GMFirebase | Firestore[Query Listener]. A document is modified : " +
 * dChange.getDocument().getData()); } break; case REMOVED: if (debMod==true) {
 * Log.i("yoyo",
 * "GMFirebase | Firestore[Query Listener]. A document is removed : " +
 * dChange.getDocument().getData()); } break; } }
 * 
 * } }); } }
 * 
 * 
 * public void firebase_firestore_listen_document(final String arg0) {
 * DocumentReference noteRef = db.document(arg0); ListenerRegistration
 * registration = noteRef.addSnapshotListener(RunnerActivity.CurrentActivity,
 * new EventListener<DocumentSnapshot>() {
 * 
 * @Override public void onEvent(DocumentSnapshot documentSnapshot,
 * FirebaseFirestoreException e) { listenerRegList.add(registration); int regVal
 * = listenerRegList.size();
 * 
 * if (e != null) { if (debMod==true) { Log.i("yoyo",
 * "GMFirebase | Firestore[Listen/Document] : Error while listening to the document. Error code "
 * + e.toString()); } int dsMapIndex =
 * RunnerJNILib.dsMapCreate(); RunnerJNILib.DsMapAddString(
 * dsMapIndex, "[category]", "firestore_listen_document");
 * RunnerJNILib.DsMapAddString( dsMapIndex, "[location]", arg0);
 * RunnerJNILib.DsMapAddString( dsMapIndex, "[id]", "N/A");
 * RunnerJNILib.DsMapAddDouble( dsMapIndex, "[listenerID]", regVal);
 * RunnerJNILib.DsMapAddString( dsMapIndex, "[data]", e.toString());
 * RunnerJNILib.DsMapAddDouble( dsMapIndex, "[status]", 0);
 * RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
 * return; }
 * 
 * if (documentSnapshot.exists()) {
 * 
 * if (debMod==true) { Log.i("yoyo",
 * "GMFirebase | Firestore[Listen/Document] : Successfully received the document from "
 * + arg0); Log.i("yoyo",
 * "GMFirebase | Firestore[Listen/Document] : Data Key <> Values from " + arg0 +
 * "are;"); }
 * 
 * int dsMapIndex = RunnerJNILib.dsMapCreate();
 * 
 * Map<String, Object> map = documentSnapshot.getData();
 * 
 * for (Map.Entry<String, Object> entry : map.entrySet()) { String key =
 * entry.getKey(); String val = entry.getValue().toString();
 * RunnerJNILib.DsMapAddString( dsMapIndex, key, val); if (debMod==true) {
 * Log.i("yoyo", key + " : " + val); } }
 * 
 * 
 * RunnerJNILib.DsMapAddString( dsMapIndex, "[category]",
 * "firestore_listen_document"); RunnerJNILib.DsMapAddString( dsMapIndex,
 * "[location]", arg0); RunnerJNILib.DsMapAddDouble( dsMapIndex, "[listenerID]",
 * regVal); RunnerJNILib.DsMapAddString( dsMapIndex, "[id]",
 * documentSnapshot.getId()); RunnerJNILib.DsMapAddDouble( dsMapIndex,
 * "[status]", 1); RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex,
 * EVENT_OTHER_SOCIAL); } else { if (debMod==true) { Log.i("yoyo",
 * "GMFirebase | Firestore[Listen/Document] : Document "+ arg0
 * +" does not exist"); } int dsMapIndex =
 * RunnerJNILib.dsMapCreate(); RunnerJNILib.DsMapAddString(
 * dsMapIndex, "[category]", "firestore_listen_document");
 * RunnerJNILib.DsMapAddString( dsMapIndex, "[location]", arg0);
 * RunnerJNILib.DsMapAddDouble( dsMapIndex, "[listenerID]", regVal);
 * RunnerJNILib.DsMapAddString( dsMapIndex, "[id]", "N/A");
 * RunnerJNILib.DsMapAddDouble( dsMapIndex, "[status]", -1);
 * RunnerJNILib.CreateAsynEventWithDSMap(dsMapIndex, EVENT_OTHER_SOCIAL);
 * 
 * } } }); //LISTENER_LIST.put() }
 * 
 * public void firebase_firestore_detach_listener(double arg0) { int pos =
 * (int)arg0; listenerRegList.get(pos).remove(); listenerRegList.remove(pos); }
 * 
 * 
 * 
 * }
 */