function system_shader_create_blur(_create,_blurAmount) {
	_blurAmount	= is_undefined(_blurAmount) ? 1 : _blurAmount;
	var _return	= false;
	if(_create)
	{
		if(!instance_exists(obj_system_shader_blur))
		{
			with(instance_create_depth(0,0,0,obj_system_shader_blur))
			{
				var_blur_amount	= _blurAmount;
			}
			_return	= true;
		}
	}
	else
	{
		with(obj_system_shader_blur)
		{
			instance_destroy();
		}
	}
	return (_return);
}