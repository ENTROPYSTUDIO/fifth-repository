function method_Scroll_create(X, Y, Depth, width, heigth, list, V_H, Holder_step, sep, marginX, marginY) {
	var ins = instance_create_depth(X,Y,Depth-1,Obj_Scroll_gui);

	ins.width = width;
	ins.heigth = heigth;

	ins.list = list;
	ins.V_H = V_H;
	ins.Holder_step = Holder_step;
	ins.sep = sep;
	ins.marginX	= marginX;
	ins.marginY	= marginY;

	method_Scroll_refresh(ins);

	return(ins);
}
