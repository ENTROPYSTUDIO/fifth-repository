// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function method_skeleton_animation_speed(_speedType){
	if(is_undefined(_speedType)) { return 1; }
	var _return	= 1;
	switch(_speedType)
	{
		case "attackSpeed":
		{
			_return	= structAbility.attackSpeed;
			break;
		}
	}
	return _return;
}