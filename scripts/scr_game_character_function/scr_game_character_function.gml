#region character_get_stat_upgrade_price
function character_get_stat_upgrade_price(_characterUid,_statName,_returnType,_purchaseAmount){
	var _level	= method_struct_get_deep_value(global.structUserData,["inventory","character",_characterUid,_statName]);
	var _defaultPrice	= method_struct_get_deep_value(global.structGameMeta,["characterStatus",_statName,"price"]);
	var _base	= method_struct_get_deep_value(global.structGameMeta,["characterStatus",_statName,"priceBase"]);
	var _value	= 0;
	repeat(_purchaseAmount)
	{
		_value	+= floor(power(_base,_level)*_defaultPrice);
		_level	+= 1;
	}
	var _return	= "";
	switch(_returnType)
	{
		case "display" :
		{
			_return	= string_number_abbrev(_value,3);
			break;
		}
		case "default":
		{
			_return = _value;
			break;
		}
	}
	return _return;
}
#endregion

#region character_upgrade_level
function character_upgrade_level(_characterUid,_statName,_returnType,_purchaseAmount){
	var _level	= method_struct_get_deep_value(global.structUserData,["inventory","character",_characterUid,_statName])+_purchaseAmount;
	var _maxLevel = method_struct_get_deep_value(global.structGameMeta,["characterStatus",_statName,"maxUpgrade"]);
	
	var _return	= "";
	switch(_returnType)
	{
		case "display" :
		{
			if(_level < _maxLevel)
			{
				_return	= "Lv."+string(_level+1);
			}
			else
			{
				_return	= "Lv.Max";
			}
			break;
		}
		case "check":
		{
			if(_level < _maxLevel)
			{
				_return	= true;
			}
			else
			{
				_return	= false;
			}
			break;
		}
	}
	return _return;
}
#endregion

#region character_get_stat_upgrade_amount
function character_get_stat_upgrade_amount(_characterUid,_statName,_returnType,_purchaseAmount){
	var _grade	= character_get_static_ability(_characterUid,"grade","string");
	var _level	= method_struct_get_deep_value(global.structUserData,["inventory","character",_characterUid,_statName]);
	var _default = method_struct_get_deep_value(global.structGameMeta,["characterStatus",_statName,"amount"]);
	var _base	= method_struct_get_deep_value(global.structGameMeta,["characterStatus",_statName,"amountBase_"+_grade]);
	var _next	= power(_base,_level+_purchaseAmount)*_default;
	var _now	= power(_base,_level)*_default;
	var _final	= _next-_now;
	var _dec	= _final > 0.009 ? 2 : 3;
	
	var _return	= "";
	switch(_returnType)
	{
		case "display" :
		{
			var _surffix	= "";
			switch(_statName)
			{
				case "drainHpChance":
				case "drainHpRatio":
				case "critical":
				case "evasion":
				{
					_surffix	= "%";
					break;
				}
				case "criticalPower":
				{
					_surffix	= "%";
					_final*=100;
					break;
				}
			}
			_return	= string_number_abbrev_remain_dec(_final,_dec)+_surffix;
			break;
		}
	}
	return _return;
}
#endregion

#region character_get_mutable_ability
function character_get_mutable_ability(_characterUid,_statName,_returnType){
	var _grade	= character_get_static_ability(_characterUid,"grade","string");
	var _level	= method_struct_get_deep_value(global.structUserData,["inventory","character",_characterUid,_statName]);
	var _default	= method_struct_get_deep_value(global.structGameMeta,["characterStatus",_statName,"amount"]);
	var _base	= method_struct_get_deep_value(global.structGameMeta,["characterStatus",_statName,"amountBase_"+_grade]);
	var _now	= power(_base,_level)*_default;
	var _dec	= _now > 0.9 ? 2 : 3;
	
	var _return	= "";
	switch(_returnType)
	{
		case "battle" :
		{
			_return	= _now;
			break;
		}
		case "display" :
		{
			var _surffix	= "";
			switch(_statName)
			{
				case "drainHpChance":
				case "drainHpRatio":
				case "critical":
				case "evasion":
				{
					_surffix	= "%";
					break;
				}
				case "criticalPower":
				{
					_surffix	= "%";
					_now*=100;
					break;
				}
			}
			//_dec	= frac(_now)*power(10,_dec) == 0 && _now < 1000? 0 : _dec;
			_return	= string_number_abbrev(_now,_dec)+_surffix;
			break;
		}
	}
	return _return;
}
#endregion

#region character_get_static_ability
function character_get_static_ability(_characterUid,_statName,_returnType){
	var _return	= 0;
	var _heroKey	= method_struct_get_deep_value(global.structUserData,["inventory","character",_characterUid,"key"]);
	switch(_returnType)
	{
		case "display":
		{
			_return	= method_struct_get_deep_value(global.structGameMeta,["heroBase",_heroKey,_statName]);
			break;
		}
		case "battle":
		{
			_return	= method_struct_get_deep_value(global.structGameMeta,["heroBase",_heroKey,_statName]);
			break;
		}
		case "string":
		{
			_return	= string(method_struct_get_deep_value(global.structGameMeta,["heroBase",_heroKey,_statName]));
			break;
		}
	}
	
	return _return;
}
#endregion

#region character_get_name
function character_get_name(_characterUid) {
	var _structStatus	= method_struct_get_deep_value(global.structUserData,["inventory","character",_characterUid]);
	var _heroKey	= _structStatus.key;
	return method_struct_get_deep_value(global.structGameText,["heroName",_heroKey,global.languageCode]);
}
#endregion

#region character_status_upgrade_update
function character_status_upgrade_update(_characterUid,_statName){
	with(obj_menu_character_ability_button)
	{
		structDraw.updateStruct(characterUid,statName);
		//structDraw.increase = "+"+string(character_get_stat_upgrade_amount(characterUid,statName,"display"));
		//structDraw.price = character_get_stat_upgrade_price(characterUid,statName,"display");
		//structDraw.level = "Lv."+string(method_struct_get_deep_value(global.structUserData,["inventory","character",characterUid,statName])+1);
		surfaceUpdate	= true;
	}
	with(obj_menu_character_main_control)
	{
		if(characterUid != "empty" && characterUid != "lock")
		{
			structStatus	= method_struct_get_deep_value(global.structUserData,["inventory","character",characterUid]);
			for(var _i=0; _i<array_length(structDraw.statProperty); ++_i)
			{
				var _property	= structDraw.statProperty[_i];
				variable_struct_set(structDraw.statName,_property,method_struct_get_deep_value(global.structGameText,["characterAbility",_property,global.languageCode]));
				variable_struct_set(structDraw.statValue,_property,character_get_mutable_ability(characterUid,_property,"display"));
			}
			for(var _i=0; _i<array_length(structDraw.abilityProperty); ++_i)
			{
				var _property	= structDraw.abilityProperty[_i];
				variable_struct_set(structDraw.abilityName,_property,method_struct_get_deep_value(global.structGameText,["characterAbility",_property,global.languageCode]));
				variable_struct_set(structDraw.abbiltyValue,_property,character_get_static_ability(characterUid,_property,"display"));
			}
			surfaceUpdate	= true;
		}
	}
	with(obj_room_stage_character_slot)
	{
		if(characterUid != "lock" && characterUid != "empty")
		{
			structAbility.updateAbility(_statName);
		}
	}
}
#endregion

#region ability_get_name_and_value
function ability_get_name_and_value(_arrayAbility,_type,_characterUid) constructor {
	var _getFunction	= _type == "static" ? character_get_static_ability : character_get_mutable_ability;
	for(var _i=0; _i<array_length(_arrayAbility); ++_i)
	{
		var _property	= _arrayAbility[_i];
		variable_struct_set(self,_property,{
			name : method_struct_get_deep_value(global.structGameText,["characterAbility",_property,global.languageCode]),
			value : _getFunction(_characterUid,_property,"display")
		});
	}
}
#endregion

