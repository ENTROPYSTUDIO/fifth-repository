function method_Scroll_add_slider(scroll, Obj_Scroll_slider_child, start_, end_) {

	scroll.Slider = Obj_Scroll_slider_child;
	Obj_Scroll_slider_child.owner = scroll;

	Obj_Scroll_slider_child.start_ = start_;
	Obj_Scroll_slider_child.end_ = end_;

	Obj_Scroll_slider_child.V_H = scroll.V_H;

	with(Obj_Scroll_slider_child)
	{
		event_perform(ev_other,ev_user0);
	}
	


}
