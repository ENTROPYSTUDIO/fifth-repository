function firebase_waiting_for_cloud_function_result(_callId,_functionData,_popupName,_callbackDone,_callbackFail) {
	if(os_get_config() == "pc") { return 0; }
	var _inst	= instance_create_depth(0,0,depth-10,obj_firbase_cloud_function_callback);
	with(_inst)
	{
		callId	= _callId;
		popupName	= _popupName;
		callbackDone	= _callbackDone;
		callbackFail	= _callbackFail;
		popupEnabled	= _popupName != "";
		if(popupEnabled)
		{
			drawEnabled	= true;
			global.windowLayer	= enumWindowLayer.cloudFunction;
		}
		
		lid	= firebase_cloud_functions_call(_functionData.functionData,_functionData.functionName,_callbackDone != undefined);
		if(lid == -1)
		{
			_callbackFail();
			instance_destroy();
		}
	}
}

function firebase_cloud_function_builder(_functionName,_functionData) constructor {
	functionName	= _functionName;
	functionData	= _functionData == undefined ? json_stringify({
		data : ""
	}) : json_stringify(_functionData);
}

function firebase_cloud_functions_call(_jsonPost,_functionsName,_callback) {
	if(global.debugMode && os_get_config() == "pc") { return -1; }
	var _jsonFunction	= 	Firebase_RemoteConfig_getString("cloudFunctionName");
	var _structFunction	= json_parse(_jsonFunction);

	if(is_struct(_structFunction))
	{
		var _convertTrueFunction	= variable_struct_get(_structFunction,_functionsName);
		var _return	= firebase_cloud_functions(_jsonPost,_convertTrueFunction,_callback);
	}
	else
	{
		var _return	= -1;
	}

	return(_return);
}