function firebase_rtdb_transaction_resource_change_builder(_ref,_value,_callback,_doneFunction,_failFunction){
	if(os_get_config() == "pc") { return 0; }
	var _lid	= firebase_transaction_resource_change(_ref,_value);
	if(_callback)
	{
		with(instance_create_depth(0,0,0,obj_firebase_rtdb_transaction_callback))
		{
			callbackDone	= _doneFunction;
			callbackFail	= _failFunction;
			lid	= _lid;
		}
	}
}

function firebase_rtdb_set_double_builder(_ref,_value,_push,_callback,_doneFunction,_failFunction){
	if(os_get_config() == "pc") { return 0; }
	var _lid	= Firebase_DataBase_setDouble(_ref,_value,_push,_callback);
	if(_callback)
	{
		with(obj_firebase_rtdb_set_double_callback)
		{
			callbackDone	= _doneFunction;
			callbackFail	= _failFunction;
			lid	= _lid;
		}
	}
}

function firebase_rtdb_set_string_builder(_ref,_value,_push,_callback,_doneFunction,_failFunction){
	var _lid	= Firebase_DataBase_setString(_ref,_value,_push,_callback);
	if(_callback)
	{
		with(obj_firebase_rtdb_set_string_callback)
		{
			callbackDone	= _doneFunction;
			callbackFail	= _failFunction;
			lid	= _lid;
		}
	}
}

function firebase_rtdb_node_builder(_ref,_value,_push,_callback,_update,_callbackDone,_callbackFail){
	var _lid	= Firebase_DataBase_setNode(_ref,_value,_push,_callback,_update);
	if(_callback)
	{
		with(obj_firebase_rtdb_node_callback)
		{
			variable_struct_set(structListener,string(_lid),new firebase_async_callback_constructor(_callbackDone,_callbackFail,undefined));
		}
	}
}

function firebase_rtdb_listener_child_builder(_ref,_callbackDone,_callbackFail,_ownerId) {
	var _lid	= Firebase_DataBase_startListener_Child(_ref);
	method_show_debug(["firebase_rtdb_listener_child_builder",_lid]);

	with(obj_firebase_rtdb_listener_child_callback)
	{
		variable_struct_set(structListener,string(_lid),new firebase_async_callback_constructor(_callbackDone,_callbackFail,_ownerId));
	}
	return _lid;
}
function firebase_rtdb_remove_listener_child(_listenerId){
	with(obj_firebase_rtdb_listener_child_callback)
	{
		var _stringLid	= string(_listenerId);
		if(variable_struct_exists(structListener,_stringLid))
		{
			variable_struct_remove(structListener,_stringLid);
			Firebase_DataBase_RemoveListener(_listenerId);
		}
	}
}

function firebase_async_callback_constructor(_callbackDone,_callbackFail,_ownerId) constructor {
	callbackDone	= _callbackDone;
	callbackFail	= _callbackFail;
	ownerId	= _ownerId;
}



	
