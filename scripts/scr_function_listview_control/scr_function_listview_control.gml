#region constructDragItem
function constructListViewDrag(_moveType,_margin,_moveInter,_moveSpeed) constructor {
	moveType	= _moveType;
	pressedMouseX	= noone;
	pressedMouseY	= noone;
	originalMouseX	= noone;
	originalMouseY	= noone;
	dragNowX	= noone;
	dragNowY	= noone;
	dragGoalX	= noone;
	dragGoalY	= noone;
	pressed		= 0;
	released	= true;
	drag	= false;
	marginX	= _margin;
	marginY = _margin;
	moveInter	= is_undefined(_moveInter) ? mac_listview_move_inter : _moveInter;
	moveSpeed	= is_undefined(_moveSpeed) ? mac_listview_move_speed : _moveSpeed;
	
	orientationX	= enumListDragOrientation.inverseHorizon;
	orientationY	= enumListDragOrientation.inverseVertical;
	
	static checkInput	= function() {
		if(mouse_check_button_pressed(mb_left) && pressed == 0)
		{
			pressedMouseX	= device_mouse_x_to_gui(0);
			pressedMouseY	= device_mouse_y_to_gui(0);
			if(point_in_rectangle(pressedMouseX,pressedMouseY,other.inventoryLeft,other.inventoryTop,other.inventoryRight,other.inventoryBottom))
			{
				originalMouseX	= device_mouse_x_to_gui(0);
				originalMouseY	= device_mouse_y_to_gui(0);
				pressed	= current_time;
				drag		= false;
				dragGoalX	= 0;
				dragGoalY	= 0;
				released	= true;
				backToBegin	= false;
			}
		}
	}
	
	static checkDrag	= function() {
		if(mouse_check_button(mb_left) && pressed != 0)
		{
			if(!drag)
			{
				var _x	= device_mouse_x_to_gui(0);
				var _y	= device_mouse_y_to_gui(0);
				if((abs(originalMouseY-_y) > mac_listview_drag_distance_min) || (abs(originalMouseX-_x) > mac_listview_drag_distance_min) && current_time-pressed > mac_listview_drag_time_min)
				{
					drag	= true;
					released	= false;
				}
			}
			if(drag)
			{
				var _x	= device_mouse_x_to_gui(0);
				var _y	= device_mouse_y_to_gui(0);
				dragGoalX	= (originalMouseX-_x)*moveInter*orientationX;
				dragGoalY	= (originalMouseY-_y)*moveInter*orientationY;
				
				originalMouseX	= lerp(originalMouseX,_x,0.05);
				originalMouseY	= lerp(originalMouseY,_y,0.05);
			}
		}
	}
	static dragItem	= function(_grid,_objectIndex) {
		if(drag)
		{
			other.surfaceUpdate	= true;
			
			var _firstItem	= ds_grid_get(_grid,0,0);
			var _lastItem	= ds_grid_get(_grid,0,ds_grid_height(_grid)-1);
				
			if(moveType == enumListDragOrientation.typeVertical)
			{				
				var _firstY	= _firstItem.bbox_top;
				var _lastY	= _lastItem.bbox_bottom;
				var _goalY	= dragGoalY;
				var _sign	= sign(dragGoalY);
				if(_sign > 0)
				{
					if(_firstY < other.inventoryTop+marginY)
					{
						with(_objectIndex)
						{
							y += _goalY;
						}
					}
				}
				else if(_sign < 0)
				{
					if(_lastY > other.inventoryBottom-marginY)
					{
						with(_objectIndex)
						{
							y += _goalY;
						}
					}
				}
			}
			else if(moveType == enumListDragOrientation.typeHorizon)
			{
				var _firstX	= _firstItem.bbox_left;
				var _lastX	= _lastItem.bbox_right;
				var _goalX	= dragGoalX;
				var _sign	= sign(dragGoalX);
				if(_sign > 0)
				{
					if(_firstX < other.inventoryLeft+marginX)
					{
						with(_objectIndex)
						{
							x += _goalX;
						}
					}
				}
				else if(_sign < 0)
				{
					if(_lastX > other.inventoryRight-marginX)
					{
						with(_objectIndex)
						{
							x += _goalX;
						}
					}
				}
			}
			
			dragGoalX	= lerp(dragGoalX,0,moveSpeed);
			dragGoalY	= lerp(dragGoalY,0,moveSpeed);
			
			if(abs(dragGoalX) < 1) { dragGoalX	= 0; }
			if(abs(dragGoalY) < 1) { dragGoalY	= 0; }
			if(dragGoalX == 0 && dragGoalY == 0)
			{
				drag	= false;
			}
		}
	}
	
	static checkRelesed	= function(_objectIndex) {
		if(mouse_check_button_released(mb_left))
		{
			if(released)
			{
				if(point_in_rectangle(pressedMouseX,pressedMouseY,other.inventoryLeft,other.inventoryTop,other.inventoryRight,other.inventoryBottom))
				{
					var _inst	= instance_position(pressedMouseX,pressedMouseY,_objectIndex);
					if(_inst != noone)
					{
						with(_inst)
						{
							touchEvent();
						}
					}
				}
			}
			pressed	= 0;
		}
	}
}
#endregion