function lerpGoalY(_y,_goal,_amount) {
	if(_y != _goal)
	{
		_y	= lerp(_y,_goal,_amount);
		if(abs(_y-_goal) < 1)
		{
			_y	= _goal;
		}
	}
	return _y;
}

function constructBottomGuiHideAnimation() constructor {
	originX	= other.x;
	originY	= other.y;
	originDepth	= other.depth;
	bottomBarPass	= false;
	static hideAndVisible	= function(_state) {
		if(!_state)
		{
			other.depth	= originDepth;
			var _originY	= originY;
			if(other.y != _originY)
			{
				other.visible	= true;
				bottomBarPass	= false;
				other.y	= lerp(other.y,_originY-10,0.05);
				if(abs(other.y-_originY) < 2)
				{
					other.y	= _originY;
				}
			}
		}
		else
		{
			if(!bottomBarPass)
			{
				other.y	= lerp(other.y,display_get_gui_height()/2,0.025);
				if(other.bbox_bottom < display_get_gui_height()-120)
				{
					bottomBarPass	= true;
				}
			}
			else
			{
				other.depth	= 10000;
				other.y	= lerp(other.y,display_get_gui_height()+200,0.05);
				if(other.bbox_top > display_get_gui_height())
				{
					other.visible	= false;
				}
			}
		}
	}
}