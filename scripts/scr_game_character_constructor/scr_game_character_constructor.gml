#region constructPlayerAbility
function constructPlayerAbility(_characterUid) constructor {
	characterUid		= _characterUid;
	
	static updateAbility	= function(_statName){
		variable_struct_set(self,_statName,character_get_mutable_ability(characterUid,_statName,"battle"))
	}
		
	#region show static ability
	grade = character_get_static_ability(characterUid,"grade","battle");
	index = character_get_static_ability(characterUid,"index","battle");
	aggroRange	= character_get_static_ability(characterUid,"aggroRange","battle");
	aggroPower	= character_get_static_ability(characterUid,"aggroPower","battle");
	aggroSpeed	= character_get_static_ability(characterUid,"aggroSpeed","battle");
	luck	= character_get_static_ability(characterUid,"luck","battle");
	attackSpeed	= character_get_static_ability(characterUid,"attackSpeed","battle");
	moveSpeed	= character_get_static_ability(characterUid,"moveSpeed","battle");
	multipleTarget	= character_get_static_ability(characterUid,"multipleTarget","battle");
	healthWeight	= method_struct_get_deep_value(global.structUserData,["inventory","character",characterUid,"healthWeight"]);
	#endregion
	
	#region not show static ability
	attackDelay	= character_get_static_ability(characterUid,"attackDelay","battle");
	attackWave	= character_get_static_ability(characterUid,"attackWave","battle");
	#endregion
	
	#region show mutable ability
	attack	= character_get_mutable_ability(characterUid,"attack","battle");
	defence	= character_get_mutable_ability(characterUid,"defence","battle");
	critical	= character_get_mutable_ability(characterUid,"critical","battle");
	criticalPower = character_get_mutable_ability(characterUid,"criticalPower","battle");
	evasion	= character_get_mutable_ability(characterUid,"evasion","battle");
	healthPointMax	= character_get_mutable_ability(characterUid,"healthPointMax","battle");
	drainHpChance = character_get_mutable_ability(characterUid,"drainHpChance","battle");
	drainHpRatio = character_get_mutable_ability(characterUid,"drainHpRatio","battle");
	
	weapon = method_struct_get_deep_value(global.structUserData,["inventory","character",characterUid,"weapon"]);
	equip = method_struct_get_deep_value(global.structUserData,["inventory","character",characterUid,"equip"]);
	#endregion
	
	#region battle var
	healthPoint	= healthPointMax*healthWeight;
	#endregion
	
}
#endregion

#region constructEnemyAbility
function constructEnemyAbility(_stageKey,_type) constructor {
	attack	= game_play_enemy_get_ability_attack(_stageKey,_type);
	attackWave	= game_play_enemy_get_ability_attackWave(_stageKey,_type);
	defence		= 0;
	critical	= game_play_enemy_get_ability_critical(_stageKey,_type);
	criticalPower	= game_play_enemy_get_ability_criticalPower(_stageKey,_type);
	attackSpeed	= game_play_enemy_get_ability_attackSpeed(_stageKey,_type);
	attackDelay	= game_play_enemy_get_ability_attackDelay(_stageKey,_type);
	multipleTarget	= game_play_enemy_get_ability_multipleTarget(_stageKey,_type);
	moveSpeed	= game_play_enemy_get_ability_moveSpeed(_stageKey,_type);
	healthPointMax	= game_play_enemy_get_ability_healthPointMax(_stageKey,_type);
	healthPoint	= healthPointMax;
}
#endregion

#region constructCharacterDraw
function constructCharacterDraw(_healthbarSub,_auraSprite) constructor {
	starIcon	= noone;
	starIconX	= 0;
	starIconY	= -20;
	
	heroName	= "";
	heroNameX	= 0;
	heroNameY	= -20;
	
	healthBarDrawX	= -sprite_get_width(spr_health_bar_back)/2;
	healthBarDrawY	= -sprite_get_height(spr_health_bar_back)-8;
	healthBarWidth	= sprite_get_width(spr_health_bar_back);
	healthBarHeight	= sprite_get_height(spr_health_bar_back);
	healthBarSub	= _healthbarSub;
	
	//avatarSprite	= SPR_CHARACTER_HEAD_GRADE_5;
	avatarSub		= 0;
	
	aggroX	= 0;
	aggroY	= -60;
	aggroSprite	= noone;
	aggroAlpha	= 0;
	aggroScale	= 1;
	aggroSub	= 0;
	
	maxScale = 0.8;
	minScale = 0.6;
	nowScale = 1;
	changeValue = 1;
	changeAmount = 0.04;
	auraSprite = _auraSprite;
	shadowSprite	= spr_character_player_shadow;

	defaultDamage	= noone;
	defaultDamageText	= "";
	defaultDamageX	= noone;
	defaultDamageFinalX	= noone;
	defaultDamageY	= noone;
	defaultDamageAlpha	= noone;
	defaultDamageColor	= c_red;
	defaultDamageOrientation	= 0;
	
	criticalDamage	= noone;
	criticalDamageText	= "";
	criticalDamageX	= noone;
	criticalDamageFinalX	= noone;
	criticalDamageY	= noone;
	criticalDamageAlpha	= noone;
	criticalDamageColor	= c_red;
	criticalDamageOrientation	= 0;
	
	criticalInfo	= noone;
	criticalInfoText	= "";
	criticalInfoX	= noone;
	criticalInfoFinalX	= noone;
	criticalInfoY	= noone;
	criticalInfoAlpha	= noone;
	criticalInfoColor	= c_red;
	criticalInfoOrientation	= 0;
	criticalInfoScale	= 1;
	
	hitTime	= 0;
	hitShader	= noone;
	
	static auraScale	= function() {
		if(nowScale > maxScale)
		{ 
			changeValue = minScale-0.1;
		}
		else if(nowScale < minScale)
		{
			changeValue = maxScale+0.1;
		}
		nowScale	= lerp(nowScale,changeValue,changeAmount);
	};
	
	static hitSet	= function(_time,_shader) {
		hitTime	= _time;
		hitShader	= _shader;
	};
	
	static hitUpdate	= function() {
		hitTime--;
		return hitTime > 0;
	}
	
	static healthBarDraw	= function(_x,_y,_healthRatio,_healthPoint) {
		var _healthBarWidth	= healthBarWidth;
		var _barWidth	= _healthBarWidth*_healthRatio;
		var _healthBarX	= healthBarDrawX;
		//var _healthBarY	= healthBarDrawY;
		var _healthBarSub	= healthBarSub;
		var _healthBarHeight	= healthBarHeight;
			
		draw_set_alpha(1);
		draw_set_halign(fa_left);
		draw_set_valign(fa_middle);
		draw_set_color(c_white);
		draw_sprite(spr_health_bar_back,0,_x+_healthBarX,_y);
		//var _color	= make_color_rgb(floor(255-(255*_barForeRatio)),floor(230*_barForeRatio),0);
		draw_sprite_part(spr_health_bar_fore,_healthBarSub,0,0,_barWidth,_healthBarHeight,_x+_healthBarX,_y);
		draw_set_font(font_player_health_bar);
		draw_text(_x+_healthBarX+4,_y+(_healthBarHeight/2)-2,string_number_abbrev(_healthPoint,1));
		//draw_text(_x+_healthBarX+4,_y+(_healthBarHeight/2)-2,string(_healthRatio*100)+"%");//string_number_abbrev(_healthPoint,1));
	};
	
	static drawDefaultDamage	= function()
	{
		if(defaultDamage != noone)
		{
			draw_set_alpha(defaultDamageAlpha);
			draw_set_halign(fa_center);
			draw_set_valign(fa_bottom);
			draw_set_font(font_default_damage);
		
			var _x	= defaultDamageFinalX;
			var _scale	= defaultDamageScale;
			//draw_set_color(c_dkgray);
			//draw_text_transformed(_x,defaultDamageY,defaultDamageText,_scale,_scale,0);
			draw_set_color(defaultDamageColor);
			draw_text_transformed(_x,defaultDamageY,defaultDamageText,_scale,_scale,0);
			defaultDamageY	= lerp(defaultDamageY,defaultDamageFinalY,0.15);
			defaultDamageAlpha	= lerp(defaultDamageAlpha,-0.5,0.03);
		
			if(_scale > 1)
			{
				defaultDamageScale	= lerp(defaultDamageScale,0.8,0.2);
			}
			if(defaultDamageAlpha < 0)
			{
				defaultDamage	= noone;
			}
		}
	};
	static drawCriticalDamage	= function(){
		if(criticalDamage != noone)
		{
			draw_set_alpha(criticalDamageAlpha);
			draw_set_halign(fa_center);
			draw_set_valign(fa_bottom);
			draw_set_font(font_critical_damage);

			var _x	= criticalDamageX;
			var _scale	= criticalDamageScale;
			//draw_set_color(c_dkgray);
			//draw_text_transformed(_x,criticalDamageFinalY,criticalDamageText,_scale,_scale,0);
			draw_set_color(criticalDamageColor);
			draw_text_transformed(_x,criticalDamageFinalY,criticalDamageText,_scale,_scale,0);
			criticalDamageX	= lerp(criticalDamageX,criticalDamageFinalX,0.1);
			criticalDamageAlpha	= lerp(criticalDamageAlpha,-0.5,0.02);
		
			if(criticalDamageAlpha < 0)
			{
				criticalDamage	= noone;
			}
		}
	};
	static drawCriticalInfo	= function(){
		if(criticalInfo != noone)
		{
			draw_set_alpha(criticalInfoAlpha);
			draw_set_halign(fa_center);
			draw_set_valign(fa_bottom);
			draw_set_font(font_critical_damage);
		
			var _x	= criticalInfoFinalX;
			var _scale	= criticalInfoScale;
			draw_set_color(criticalInfoColor);
			draw_text_transformed(_x,criticalInfoY,criticalInfoText,_scale,_scale,0);
		
			if(_scale > 1)
			{
				criticalInfoScale	= lerp(criticalInfoScale,0.8,0.35);
			}
			if(_scale < 1.5)
			{
				criticalInfoAlpha	= lerp(criticalInfoAlpha,-0.5,0.04);
			}
		
			if(criticalInfoAlpha < 0)
			{
				criticalInfo	= noone;
			}
		}
	};
	static drawAggroSprite	= function(){
		if(aggroSprite != noone)
		{
			var _scale	= aggroScale;
			var _alpha	= aggroAlpha;
			draw_sprite_ext(aggroSprite,aggroSub,other.x+aggroX,other.bbox_top+aggroY,_scale,_scale,0,c_white,aggroAlpha);
		
			if(_scale > 0.5)
			{
				aggroScale	= lerp(aggroScale,0.3,0.3);
			}
			else
			{
				aggroAlpha -= 0.02;
			}
			if(_alpha < 0)
			{
				aggroSprite	= noone;
			}
		}
	};
}
#endregion

#region constructCharacterState
function constructCharacterState(_id,_objectType,_category,_scale) constructor {
	animationNow	= "none";//enumSkeletonAninmationState.none;
	animationSet	= "none";//enumSkeletonAninmationState.none;
	forceAnimation	= "none";//noone;
	animationName	= "";
	animationCommite	= true;
	target	= noone;
	prowlDelay	= 3000;
	imageScale	= _scale;
	aggroTime	= 0;
	prowlTime	= 0;
	attackTime	= 0;
	aggroPoint	= 0;
	healthRatio	= 1;
	hsp	= 0;
	vsp	= 0;
	lerpHsp	= 0;
	lerpVsp	= 0;
	prowlSpeed	= 3;
	die	= false;
	stun	= false;
	roomIn	= false;
	knockBack	= false;
	standing	= false;
	orientation	= false;
	objectType	= _objectType;
	category	= _category;
	spriteHalfWidth	= 1;
	attack	= false;
	
	static applySpeed	= function(_clamp) {
		var _hspAmount	= sign(lerpHsp-hsp) >= 0 ? 1 : 0.1;
		var _vspAmount	= sign(lerpVsp-vsp) >= 0 ? 1 : 0.1;
		lerpHsp	= lerp(lerpHsp,hsp,_hspAmount);
		lerpVsp	= lerp(lerpVsp,vsp,_vspAmount);
	
		var _nextPositionX	= other.x+hsp;
		var _nextPositionY	= other.y+vsp;
		if(hsp != 0 || vsp != 0)
		{
			if(orientation)
			{
				other.image_xscale	= imageScale*sign(hsp);
			}
			var _animation	= abs(max(hsp,vsp)) < mac_run_or_walk ? "walk" : "run";
			animationSetPriority(_animation,animationSet,global.structHumanAnimation);
		}
		else
		{
			var _animation	= healthRatio > mac_animation_tired ? "idle" : "tired";
			animationSetPriority(_animation,animationSet,global.structHumanAnimation);
		}
		other.x	= _clamp ? clamp(_nextPositionX,spriteHalfWidth,room_width-spriteHalfWidth) : _nextPositionX;
		other.y	= _clamp ? clamp(_nextPositionY,global.roomTop,room_height) : _nextPositionY;
	};
	
	static moveDisable = function() {
		if(die || knockBack || stun || standing)
		{
			hsp	= 0;
			vsp	= 0;
			return true;
		}
		else
		{
			return false;
		}
	};
	
	static actionDisable	= function() {
		if(die || knockBack || stun || standing)
		{
			hsp	= 0;
			vsp	= 0;
		}
	};
	
	static healthRatioUpdate	= function(_other){
		healthRatio	= _other.healthPoint/_other.healthPointMax;
	};
	
	static scaleUpdate = function(_other){
		_other.image_xscale	= imageScale;
		_other.image_yscale	= imageScale;
		spriteHalfWidth	= abs(_other.sprite_width/2);
	};
	
	static animationSetPriority = function(_animation_1,_animation_2,_struct) {
		var _pri_1	= variable_struct_get(_struct,_animation_1).priority;
		var _pri_2	= variable_struct_get(_struct,_animation_2).priority;
		animationSet	=  _pri_1 > _pri_2 ? _animation_1 : _animation_2;
	}
	
	scaleUpdate(other);
}
#endregion