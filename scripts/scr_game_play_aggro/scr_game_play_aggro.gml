// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function method_aggro_calculate(_target,_caller,_aggroPower){
	with(_target)
	{
		if(structState.target == _caller)
		{
			break;
		}
		else if(structState.target == noone)
		{
			structState.aggroPoint	= _aggroPower;
			structState.target	= _caller;
			structDraw.aggroSprite	= _caller.structDraw.avatarSprite;
			structDraw.aggroSub	= _caller.structDraw.avatarSub;
			structDraw.aggroAlpha	= 1;
			structDraw.aggroScale	= 1;
		}
		else if(_target != _caller)
		{
			if(structState.aggroPoint < _aggroPower)
			{
				structState.aggroPoint	= _aggroPower;
				structState.target	= _caller;
				structDraw.aggroSprite	= _caller.structDraw.avatarSprite;
				structDraw.aggroSub	= _caller.structDraw.avatarSub;
				structDraw.aggroAlpha	= 1;
				structDraw.aggroScale	= 1;
			}
			else
			{
				structState.aggroPoint	-= _aggroPower*0.3;
			}
		}
	}
}
/*
	aggroX	= 0;
	aggroY	= -50;
	aggroSprite	= noone;
	aggroAlpha	= 0;
	aggroScale	= 1;
*/