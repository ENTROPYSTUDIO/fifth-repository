function shader_premultiply(_now,_src,_dest) {
	if(_now)
	{
		var _blendSrc	= is_undefined(_src) ? bm_one : _src;
		var _blendDest	= is_undefined(_dest) ? bm_inv_src_alpha : _dest;
		shader_set(shd_premultiply);
		gpu_set_blendmode_ext(_blendSrc, _blendDest);
	}
	else
	{
		shader_reset();
		gpu_set_blendmode(bm_normal);
	}
}