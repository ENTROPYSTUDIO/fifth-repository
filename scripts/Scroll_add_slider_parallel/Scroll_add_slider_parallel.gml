function method_Scroll_add_slider_parallel(list, Obj_Scroll_slider_child, start_, end_) {

	var a = 0;
	while(true)
	{
		var scroll = list[|a]
		if(is_undefined(scroll))
		{
			break;
		}
		
		scroll.Slider = Obj_Scroll_slider_child;
		a++;
	
	}

	Obj_Scroll_slider_child.list = list;

	Obj_Scroll_slider_child.start_ = start_;
	Obj_Scroll_slider_child.end_ = end_;

	Obj_Scroll_slider_child.V_H = list[|0].V_H;

	with(Obj_Scroll_slider_child)
	{
		event_perform(ev_other,ev_user0);
	}
	


}
