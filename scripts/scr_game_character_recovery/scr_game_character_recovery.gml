#region getRecoveryPrice
function getCharacterRecoveryPrice(_characterKey){
	var _structInventory	= global.structUserData.inventory.character;
	var _price	= 0;
	if(_characterKey == "")
	{
		var _names	= variable_struct_get_names(_structInventory);
		var _size	= array_length(_names);
		for(var _i=0; _i<_size; ++_i)
		{
			var _key	= _names[_i];
			var _healthWeight	= 1-_structInventory[$ _key].healthWeight;
			var _totalHealth	= character_get_mutable_ability(_key,"healthPointMax","battle");
			_price += _totalHealth*_healthWeight;
		}
	}
	else
	{
		var _healthWeight	= 1-_structInventory[$ _characterKey].healthWeight;
		var _totalHealth	= character_get_mutable_ability(_characterKey,"healthPointMax","battle");
		_price += _totalHealth*_healthWeight;
	}
	
	return ceil(_price);
};
#endregion

#region getResurrectionPrice
function getResurrectionPrice(_characterKey){
	var _structInventory	= global.structUserData.inventory.character;
	var _metaData	= global.structGameMeta.heroSetting;
	var _price	= 0;
	if(_characterKey == "")
	{
		var _names	= variable_struct_get_names(_structInventory);
		var _size	= array_length(_names);
		for(var _i=0; _i<_size; ++_i)
		{
			var _key	= _names[_i];
			var _state	= _structInventory[$ _key].state;
			if(_state == "dead")
			{
				var _grade	= character_get_static_ability(_key,"grade","string");
				_price += _metaData[$ mac_equip_character_key_prefix+_grade].resurrectionPrice;
			}
		}
	}
	else
	{
		var _state	= _structInventory[$ _characterKey].state;
		if(_state == "dead")
		{
			var _grade	= character_get_static_ability(_characterKey,"grade","string");
			_price += _metaData[$ mac_equip_character_key_prefix+_grade].resurrectionPrice;	
		}
	}
	return ceil(_price);
};
#endregion

#region characterResurrection
function characterResurrectionAll(_callbackDone,_callbackFail){
	var _price	= getResurrectionPrice("");
	var _currency	= global.structGameMeta.gameSetting.value.resurrectionCurrency;
	var _nowResource	= real(global.structUserData.resource[$ _currency]);
	if(_nowResource >= _price)
	{
		var _ref	= method_firebase_ref_addChildren(mac_firebase_ref_my_uid,["resource",_currency]);
		firebase_rtdb_transaction_resource_change_builder(_ref,-abs(_price),true,_callbackDone,_callbackFail);
		global.windowLayer	= enumWindowLayer.callbackWaiting;
		return true;
	}
	else
	{
		return false;
	}
};

function characterResurrectionIndividual(_key,_callbackDone,_callbackFail){
	var _price	= getResurrectionPrice(_key);
	var _currency	= global.structGameMeta.gameSetting.value.resurrectionCurrency;
	var _nowResource	= real(global.structUserData.resource[$ _currency]);
	if(_nowResource >= _price)
	{
		var _ref	= method_firebase_ref_addChildren(mac_firebase_ref_my_uid,["resource",_currency]);
		firebase_rtdb_transaction_resource_change_builder(_ref,-abs(_price),true,_callbackDone,_callbackFail);
		global.windowLayer	= enumWindowLayer.callbackWaiting;
		return true;
	}
	else
	{
		return false;
	}
};
#endregion

#region characterRecovery
function characterRecoveryAll(_callbackDone,_callbackFail){
	var _price	= getCharacterRecoveryPrice("");
	var _nowResource	= real(global.structUserData.resource.carrot);
	if(_nowResource >= _price)
	{
		var _ref	= method_firebase_ref_addChildren(mac_firebase_ref_my_uid,["resource","carrot"]);
		firebase_rtdb_transaction_resource_change_builder(_ref,-abs(_price),true,_callbackDone,_callbackFail);
		global.windowLayer	= enumWindowLayer.callbackWaiting;
		return true;
	}
	else
	{
		return false;
	}
};

function characterRecoveryIndividual(_key,_callbackDone,_callbackFail){
	var _price	= getCharacterRecoveryPrice(_key);
	var _nowResource	= real(global.structUserData.resource.carrot);
	if(_nowResource >= _price)
	{
		var _ref	= method_firebase_ref_addChildren(mac_firebase_ref_my_uid,["resource","carrot"]);
		firebase_rtdb_transaction_resource_change_builder(_ref,-abs(_price),true,_callbackDone,_callbackFail);
		global.windowLayer	= enumWindowLayer.callbackWaiting;
		return true;
	}
	else
	{
		return false;
	}
};
#endregion