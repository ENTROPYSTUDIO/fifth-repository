function ex_camera_clear_bounds(_name) {

	var _list = obj_system_camera._ex_cameras;
	// check name column
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	            ////show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return 0;
	}
	_list[# _ex_camera._bound_x,      _table_row] = -1;
	_list[# _ex_camera._bound_y,      _table_row] = -1;
	_list[# _ex_camera._bound_width,  _table_row] = -1;
	_list[# _ex_camera._bound_height, _table_row] = -1;
	return 1;
}

function ex_camera_count() {
	gml_pragma("forceinline");
	var _list = obj_system_camera._ex_cameras;
	if (_list == -1) {
	    return 0;
	}
	var _ds_height = ds_grid_height(_list);
	if (_ds_height < 2) {
	    if (_list[# 0, 0] == "") {
	        return 0;
	    }
	}
	return _ds_height;
}

function ex_camera_create(_name,_view,_argXport,_argYport,_argWidth,_argHeight,_argX,_argY,_argAngle,_argZoom,_argInstance,_argLimitBounds,_argBoundX,_argBoundY,_argBoundWidth,_argBoundHeight) {

	var _table           = obj_system_camera._ex_cameras;
	var _table_max_size  = _ex_camera._length;
	var _autoincrement   = 0;
	var _instance        = noone;
	var _x               = 0;
	var _y               = 0;
	var _xport           = 0;
	var _yport           = 0;
	var _width           = camera_get_view_width(_view);
	var _height          = camera_get_view_height(_view);
	var _angle           = camera_get_view_angle(_view);
	var _zoom            = 100;
	var _limit_bounds    = true;
	var _bound_x         = 0;
	var _bound_y         = 0;
	var _bound_width     = room_width;
	var _bound_height    = room_height;
	if (!is_undefined(_argXport)) {
	    _xport = _argXport;
	}
	if (!is_undefined(_argYport)) {
	    _yport = _argYport;
	}
	if (!is_undefined(_argWidth)) {
	    _width = _argWidth;
	    camera_set_view_size(_view,_width,_height);
	    view_set_wport(_view,_width);
	}
	if (!is_undefined(_argHeight)) {
	    _height = _argHeight;
	    camera_set_view_size(_view,_width,_height);
	    view_set_hport(_view,_height);
	}
	if (!is_undefined(_argX)) {
	    _x = _argX;
	}
	if (!is_undefined(_argY)) {
	    _y = _argY;
	}
	if (!is_undefined(_argAngle)) {
	    _angle = _argAngle;
	}
	if (!is_undefined(_argZoom)) {
	    _zoom = _argZoom;
	}
	if (!is_undefined(_argInstance)) {
	    _instance = _argInstance;
	}
	if (!is_undefined(_argLimitBounds)) {
	    _limit_bounds = _argLimitBounds;
	}
	if (!is_undefined(_argBoundX)) {
	    _bound_x = _argBoundX;
	}
	if (!is_undefined(_argBoundY)) {
	    _bound_y = _argBoundY;
	}
	if (!is_undefined(_argBoundWidth)) {
	    _bound_width = _argBoundWidth;
	}
	if (!is_undefined(_argBoundHeight)) {
	    _bound_height = _argBoundHeight;
	}
	// create or update the list
	if (ds_exists(_table, ds_type_grid)) {
    
	    // workaround
	    if (_table[# 0, 0] == "") {
    
	    } else {
    
	        ds_grid_resize(_table, _table_max_size, ds_grid_height(_table)+1);
	        _autoincrement = ds_grid_height(_table)-1;
    
	    }
	} else {
	    obj_system_camera._ex_cameras = ds_grid_create(_table_max_size, 0);
	    _table = obj_system_camera._ex_cameras;
	    ds_grid_resize(_table, _table_max_size, ds_grid_height(_table)+1);
	}
	// check if item with the same name exists
	var _row = ds_grid_value_y(_table, 0, 0, ds_grid_width(_table), ds_grid_height(_table), _name);
	if (_row > -1) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, camera name \""+string( _name )+"\" already exists, camera names must be unique");
	    }
	    return -1;
	}
	view_set_xport(_view,_xport);
	view_set_yport(_view,_yport);
	camera_set_view_angle(_view,_angle);
	if (_limit_bounds == false) {
	    _bound_x      = -1;
	    _bound_y      = -1;
	    _bound_width  = -1;
	    _bound_height = -1;
	}
	// function_big_number_add item to list
	_table[# _ex_camera._name,            _autoincrement] = _name;
	_table[# _ex_camera._view,            _autoincrement] = _view;
	_table[# _ex_camera._width,           _autoincrement] = _width;
	_table[# _ex_camera._height,          _autoincrement] = _height;
	_table[# _ex_camera._bound_x,         _autoincrement] = _bound_x;
	_table[# _ex_camera._bound_y,         _autoincrement] = _bound_y;
	_table[# _ex_camera._bound_width,     _autoincrement] = _bound_width;
	_table[# _ex_camera._bound_height,    _autoincrement] = _bound_height;
	_table[# _ex_camera._safe_area,  _autoincrement] = 128;
	_table[# _ex_camera._fade_start, _autoincrement] = -1;
	_table[# _ex_camera._fade_end, _autoincrement] = -1;
	_table[# _ex_camera._fade_tween_position, _autoincrement] = -1;
	_table[# _ex_camera._fade_tween_duration, _autoincrement] = -1;
	_table[# _ex_camera._fade_tween_easing, _autoincrement] = -1;
	_table[# _ex_camera._fade_color, _autoincrement] = c_black;
	_table[# _ex_camera._fade_blend, _autoincrement] = bm_normal;
	_table[# _ex_camera._fade_alpha, _autoincrement] = 0;
	_table[# _ex_camera._fade_oncomplete, _autoincrement] = -1;
	_table[# _ex_camera._fade_oncomplete_arguments, _autoincrement] = ex_camera_arguments_undefined;
	_table[# _ex_camera._fade_draw_target, _autoincrement] = ex_camera_draw_target_event;
	_table[# _ex_camera._flash_start, _autoincrement] = -1;
	_table[# _ex_camera._flash_end, _autoincrement] = -1;
	_table[# _ex_camera._flash_tween_position, _autoincrement] = -1;
	_table[# _ex_camera._flash_tween_duration, _autoincrement] = -1;
	_table[# _ex_camera._flash_tween_easing, _autoincrement] = -1;
	_table[# _ex_camera._flash_color, _autoincrement] = c_white;
	_table[# _ex_camera._flash_blend, _autoincrement] = bm_add;
	_table[# _ex_camera._flash_draw_target, _autoincrement] = ex_camera_draw_target_event;
	_table[# _ex_camera._flash_alpha, _autoincrement] = 1;
	_table[# _ex_camera._flash_oncomplete, _autoincrement] = -1;
	_table[# _ex_camera._flash_oncomplete_arguments, _autoincrement] = ex_camera_arguments_undefined;
	_table[# _ex_camera._flash_start, _autoincrement] = -1;
	_table[# _ex_camera._shake_start, _autoincrement] = -1;
	_table[# _ex_camera._shake_end, _autoincrement] = -1;
	_table[# _ex_camera._shake_tween_duration, _autoincrement] = -1;
	_table[# _ex_camera._shake_tween_position, _autoincrement] = -1;
	_table[# _ex_camera._shake_tween_easing, _autoincrement] = -1;
	_table[# _ex_camera._shake_power_x, _autoincrement] = 0;
	_table[# _ex_camera._shake_power_y, _autoincrement] = 0;
	_table[# _ex_camera._shake_power_angle, _autoincrement] = 0;
	_table[# _ex_camera._shake_oncomplete, _autoincrement] = -1;
	_table[# _ex_camera._shake_oncomplete_arguments, _autoincrement] = ex_camera_arguments_undefined;
	_table[# _ex_camera._zoom_start_scale, _autoincrement] = -1;
	_table[# _ex_camera._zoom_end_scale, _autoincrement] = -1;
	_table[# _ex_camera._zoom_tween_duration, _autoincrement] = -1;
	_table[# _ex_camera._zoom_tween_position, _autoincrement] = -1;
	_table[# _ex_camera._zoom_tween_easing, _autoincrement] = -1;
	_table[# _ex_camera._zoom_scale, _autoincrement] = 100;
	_table[# _ex_camera._zoom_offset_x, _autoincrement] = 0;
	_table[# _ex_camera._zoom_offset_y, _autoincrement] = 0;
	_table[# _ex_camera._zoom_start_x, _autoincrement] = 0;
	_table[# _ex_camera._zoom_start_y, _autoincrement] = 0;
	_table[# _ex_camera._zoom_end_x, _autoincrement] = 0;
	_table[# _ex_camera._zoom_end_y, _autoincrement] = 0;
	_table[# _ex_camera._zoom_oncomplete, _autoincrement] = -1;
	_table[# _ex_camera._zoom_oncomplete_arguments, _autoincrement] = ex_camera_arguments_undefined;
	_table[# _ex_camera._tilt_start, _autoincrement] = -1;
	_table[# _ex_camera._tilt_end, _autoincrement] = -1;
	_table[# _ex_camera._tilt_tween_duration, _autoincrement] = -1;
	_table[# _ex_camera._tilt_tween_position, _autoincrement] = -1;
	_table[# _ex_camera._tilt_tween_easing, _autoincrement] = -1;
	_table[# _ex_camera._tilt_angle, _autoincrement] = _angle;
	_table[# _ex_camera._tilt_oncomplete, _autoincrement] = -1;
	_table[# _ex_camera._tilt_oncomplete_arguments, _autoincrement] = ex_camera_arguments_undefined;
	_table[# _ex_camera._scroll_x, _autoincrement] = _x;
	_table[# _ex_camera._scroll_y, _autoincrement] = _y;
	_table[# _ex_camera._scroll_start_x, _autoincrement] = 0;
	_table[# _ex_camera._scroll_start_y, _autoincrement] = 0;
	_table[# _ex_camera._scroll_end_x, _autoincrement] = 0;
	_table[# _ex_camera._scroll_end_y, _autoincrement] = 0;
	_table[# _ex_camera._scroll_tween_duration, _autoincrement] = -1;
	_table[# _ex_camera._scroll_tween_easing, _autoincrement] = -1;
	_table[# _ex_camera._scroll_tween_position, _autoincrement] = -1;
	_table[# _ex_camera._scroll_oncomplete, _autoincrement] = -1;
	_table[# _ex_camera._scroll_oncomplete_arguments, _autoincrement] = ex_camera_arguments_undefined;
	_table[# _ex_camera._is_scrolling, _autoincrement] = false;
	_table[# _ex_camera._is_zooming, _autoincrement] = false;
	_table[# _ex_camera._is_flashing, _autoincrement] = false;
	_table[# _ex_camera._is_fading, _autoincrement] = false;
	_table[# _ex_camera._is_tilting, _autoincrement] = false;
	_table[# _ex_camera._is_shaking, _autoincrement] = false;
	_table[# _ex_camera._is_resizing, _autoincrement] = false;
	_table[# _ex_camera._resize_tween_position, _autoincrement] = 0;
	_table[# _ex_camera._resize_tween_duration, _autoincrement] = 0;
	_table[# _ex_camera._resize_tween_easing, _autoincrement] = -1;
	_table[# _ex_camera._resize_start_width, _autoincrement] = 0;
	_table[# _ex_camera._resize_start_height, _autoincrement] = 0;
	_table[# _ex_camera._resize_end_width, _autoincrement] = 0;
	_table[# _ex_camera._resize_end_height, _autoincrement] = 0;
	_table[# _ex_camera._resize_start_x, _autoincrement] = 0;
	_table[# _ex_camera._resize_end_x, _autoincrement] = 0;
	_table[# _ex_camera._resize_start_y, _autoincrement] = 0;
	_table[# _ex_camera._resize_end_y, _autoincrement] = 0;
	// base
	var _view_w_half = _width/2;
	var _view_h_half = _height/2;
	var _ratio_x = (_view_w_half)/100;
	var _ratio_y = (_view_h_half)/100;
	// base
	if (_instance != noone and instance_exists(_instance)) {
	    // unclamped
	    var _focus_x_u = _instance.x-(_view_w_half)+(_instance.sprite_width/2)+_x;
	    var _focus_y_u = _instance.y-(_view_h_half)+(_instance.sprite_height/2)+_y;   
        
	    // clamped
	    var _focus_x = clamp(_instance.x-(_view_w_half)+(_instance.sprite_width/2)+_x,  _bound_x, _bound_width-_width);
	    var _focus_y = clamp(_instance.y-(_view_h_half)+(_instance.sprite_height/2)+_y, _bound_y, _bound_height-_height);
    
	    var _sprite_width_scaled  = _instance.sprite_width/2*(_zoom/100);
	    var _sprite_height_scaled = _instance.sprite_height/2*(_zoom/100);
	} else {
	    // unclamped
	    var _focus_x_u = camera_get_view_x(_view);
	    var _focus_y_u = camera_get_view_y(_view);   
        
	    // clamped
	    var _focus_x = clamp(camera_get_view_x(_view), _bound_x, _bound_width-_width);
	    var _focus_y = clamp(camera_get_view_y(_view), _bound_y, _bound_height-_height);
    
	    var _sprite_width_scaled  = 0;
	    var _sprite_height_scaled = 0;
    
	}
	if (_limit_bounds == true) {
    
	    ////show_debug_message("---CLAMPED");
	    _focus_x += _x;
	    _focus_y += _y;
    
	    // first move
	    camera_set_view_pos(_view,_focus_x,_focus_y);
    
	    // update the grid
	    _table[# _ex_camera._scroll_x, _autoincrement] = _focus_x;
	    _table[# _ex_camera._scroll_y, _autoincrement] = _focus_y;    
    
	    // zoom
		var _Wvalue	= view_get_wport(_view)* (_zoom / 100);
		var _Hvalue	= view_get_hport(_view)* (_zoom / 100);
		camera_set_view_size(_view,_Wvalue,_Hvalue);
    
	    // get final zoom offset
	    var _zoom_end_x = camera_get_view_x(_view)+(100 - _zoom)*( _ratio_x )-_x;
	    var _zoom_end_y = camera_get_view_y(_view)+(100 - _zoom)*( _ratio_y )-_y;
    
	    _zoom_end_x = _zoom_end_x-_focus_x;
	    _zoom_end_y = _zoom_end_y-_focus_y;
    
	    // function_big_number_add the zoom offset
		camera_set_view_pos(_view,camera_get_view_x(_view) + (_zoom_end_x),camera_get_view_y(_view) + (_zoom_end_y));
	
	    // update the grid
	    _table[# _ex_camera._zoom_scale,    _autoincrement] = _zoom;
	    _table[# _ex_camera._zoom_offset_x, _autoincrement] = _zoom_end_x;
	    _table[# _ex_camera._zoom_offset_y, _autoincrement] = _zoom_end_y;    
    
	} else {
    
	    ////show_debug_message("---NOT CLAMPED");
	    // no bounds active
	    _table[# _ex_camera._bound_x,      _autoincrement] = -1;
	    _table[# _ex_camera._bound_y,      _autoincrement] = -1;    
	    _table[# _ex_camera._bound_width,  _autoincrement] = -1;
	    _table[# _ex_camera._bound_height, _autoincrement] = -1;
    
	    _focus_x_u += _x;
	    _focus_y_u += _y;
    
	    // first move
		camera_set_view_pos(_view,_focus_x_u,_focus_y_u);
	
	    // update the grid
	    _table[# _ex_camera._scroll_x, _autoincrement] = _focus_x_u;
	    _table[# _ex_camera._scroll_y, _autoincrement] = _focus_y_u;    
    
	    // zoom
		var _wValue	= view_get_wport(_view)*(_zoom / 100);
		var _hValue	= view_get_hport(_view)*(_zoom / 100);
	
		camera_set_view_size(_view, _wValue,_hValue);
    
	    // get zoom offset
	    var _zoom_end_x = ((_focus_x_u)+( 100 - _zoom )*( _ratio_x ) )-_focus_x_u;
	    var _zoom_end_y = ((_focus_y_u)+( 100 - _zoom )*( _ratio_y ) )-_focus_y_u;
    
	    // scale to the center of the sprite (if applicable)
	    _zoom_end_x -= _sprite_width_scaled;
	    _zoom_end_y -= _sprite_height_scaled;
    
	    // function_big_number_add the zoom offset
		camera_set_view_pos(_view,camera_get_view_x(_view) + (_zoom_end_x),camera_get_view_y(_view) + (_zoom_end_y));
    
	    // update the grid
	    _table[# _ex_camera._zoom_scale,    _autoincrement] = _zoom;
	    _table[# _ex_camera._zoom_offset_x, _autoincrement] = _zoom_end_x;
	    _table[# _ex_camera._zoom_offset_y, _autoincrement] = _zoom_end_y;    
	}
    
	if (view_enabled == false) {
	    view_enabled = true;
	}
	view_set_visible(_view,true);
	if (ex_camera_get_debug_mode()) {
	    var _row = ds_grid_value_y(_table, 0, 0, 1, ds_grid_height(_table), string( _name ));
	    //show_debug_message("exCamera: Created camera with name \""+_name+"\" ["+string( _row )+"]");
	}
	// return grid y position
	return _autoincrement;
}

function ex_camera_destroy(_name) {
	var _list = obj_system_camera._ex_cameras;
	if(_list < 0) { return 0; }
	// check name column
	var _y = ds_grid_value_y(_list, 0, 0, 1, ds_grid_height(_list), _name);
	if (_y < 0) {
	if (ex_camera_get_debug_mode()) {
	    //show_debug_message("exCamera: Error, could not find camera with name \""+string( _name )+"\"");
	}
	    return 0;
	}
	// remove item
	if (ds_grid_height(_list) < 2) {
	    ds_grid_clear(_list, "");
	    ds_grid_resize(_list, ds_grid_width(_list), 1);
	} else {
	    ex_camera_ds_grid_delete_y(_list, _y, true);
	}
	if (ex_camera_get_debug_mode()) {
	    //show_debug_message("exCamera: Removed camera with name \""+_name+"\"");
	}
	return 1;
}

function ex_camera_fade(_name,_alpha,_duration,_argEasing,color,_argOncomplete,_argArgu) {

	var _list   = obj_system_camera._ex_cameras;
	var _easing = -1;
	var _oncomplete = -1;
	var _arguments  = ex_camera_arguments_undefined;
	var _color = c_black;
	// arguments
	if(!is_undefined(color))
	{
		_color	= color;
	}
	if (!is_undefined(_argEasing)) {
	    _easing = _argEasing;
	}
	if (!is_undefined(_argOncomplete)) {
	    _oncomplete = _argOncomplete;
	}
	if (!is_undefined(_argArgu)) {
	    _arguments = _argArgu;
	}
	// check name column
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        ////show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return 0;
	}
	if (!is_undefined(_color)) {
	    _color = _color;
	    _list[# _ex_camera._fade_color, _table_row] = _color;
	}
	_list[# _ex_camera._fade_start, _table_row] = _list[# _ex_camera._fade_alpha, _table_row];
	_list[# _ex_camera._fade_end, _table_row] = _alpha;
	_list[# _ex_camera._fade_tween_duration, _table_row] = _duration;
	_list[# _ex_camera._fade_tween_position, _table_row] = -1;
	_list[# _ex_camera._fade_tween_easing, _table_row] = _easing;
	_list[# _ex_camera._fade_oncomplete, _table_row] = _oncomplete;
	_list[# _ex_camera._fade_oncomplete_arguments, _table_row] = _arguments;
	_list[# _ex_camera._is_fading, _table_row] = true;
	if (ex_camera_get_debug_mode()) {
	    //show_debug_message("exCamera: camera \""+_name+"\" has started fading");
	}
	return 1;
}

function ex_camera_fade_in(_cameraName,_duration,_argEasing,_argColor,_argOncomplete,_argOncompleteArgu) {

	var _easing     = -1;
	var _oncomplete = -1;
	var _oncomplete_arguments = ex_camera_arguments_undefined;
	var _color      = ex_camera_get_fade_color(_cameraName);
	if (!is_undefined(_argEasing)) {
	    _easing = _argEasing;
	}
	if (!is_undefined(_argColor)) {
	    _color = _argColor;
	}
	if (!is_undefined(_argOncomplete)) {
	    _oncomplete = _argOncomplete
	}
	if (!is_undefined(_argOncompleteArgu)) {
	    _oncomplete_arguments = _argOncompleteArgu;
	}
	return ex_camera_fade(_cameraName, 0, _duration, _easing, _color, _oncomplete, _oncomplete_arguments);
}

function ex_camera_fade_out(cameraName, duration, _argEasing, _argColor, _argOnComplete, _argOnCompleteArguments) {

	var _easing     = -1;
	var _oncomplete = -1;
	var _oncomplete_arguments = ex_camera_arguments_undefined;
	var _color      = ex_camera_get_fade_color(cameraName);
	if (!is_undefined(_argEasing)) {
	    _easing = _argEasing;
	}
	if (!is_undefined(_argColor)) {
	    _color = _argColor;
	}
	if (!is_undefined(_argOnComplete)) {
	    _oncomplete = _argOnComplete;
	}
	if (!is_undefined(_argOnCompleteArguments)) {
	    _oncomplete_arguments = _argOnCompleteArguments;
	}
	return ex_camera_fade(cameraName, 1, duration, _easing, _color, _oncomplete, _oncomplete_arguments);
}

function ex_camera_flash(cameraName, _argIntensity, _argDuration, _argEasing, _argColor, _argBlendMode, _argOnComplete, _argOnCompleteArguments){

	var _name       = cameraName;
	var _list       = obj_system_camera._ex_cameras;
	var _intensity  = 1.0;
	var _duration   = 12;
	var _easing     = -1;
	var _color      = c_white;
	var _blend      = bm_add;
	var _oncomplete = -1;
	var _oncomplete_arguments = ex_camera_arguments_undefined;
	if (!is_undefined(_argIntensity)) {
	    _intensity = _argIntensity;
	}
	if (!is_undefined(_argDuration)) {
	    _duration = _argDuration;
	}
	if (!is_undefined(_argEasing)) {
	    _easing = _argEasing;
	}
	// check name column
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	return 0;
	}
	if (!is_undefined(_argColor)) {
	    _color = _argColor;
	    _list[# _ex_camera._flash_color, _table_row] = _color;
	}
	if (!is_undefined(_argBlendMode)) {
	    _blend = _argBlendMode;
	    _list[# _ex_camera._flash_blend, _table_row] = _blend;
	}
	if (!is_undefined(_argOnComplete)) {
	    _oncomplete = _argOnComplete;
	    _list[# _ex_camera._flash_oncomplete, _table_row] = _oncomplete;
	}
	if (!is_undefined(_argOnCompleteArguments)) {
	    _oncomplete_arguments = _argOnCompleteArguments;
	    _list[# _ex_camera._flash_oncomplete_arguments, _table_row] = _oncomplete_arguments;
	}
	_list[# _ex_camera._flash_start, _table_row] = _intensity;
	_list[# _ex_camera._flash_end, _table_row] = _duration;
	_list[# _ex_camera._flash_tween_duration, _table_row] = _duration
	_list[# _ex_camera._flash_tween_position, _table_row] = -1;
	_list[# _ex_camera._flash_tween_easing, _table_row] = _easing;
	_list[# _ex_camera._flash_alpha, _table_row] = _intensity;
	_list[# _ex_camera._is_flashing, _table_row] = true;
	if (ex_camera_get_debug_mode()) {
	    //show_debug_message("exCamera: camera \""+_name+"\" has started flashing");
	}
	return 1;
}

function ex_camera_get_debug_mode(){
	return obj_system_camera._ex_camera_debug_mode;
}

function ex_camera_get_fade_color(_name){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._fade_color;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// get table row
	return _table[# _table_column, _table_row];
}

function ex_camera_get_height(_name){
	gml_pragma("forceinline");
	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._height;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// get table row
	return _table[# _table_column, _table_row];
}

function ex_camera_get_index(_name) {
	gml_pragma("forceinline");
	var _list = obj_system_camera._ex_cameras;
	// check if exists first
	if (ex_camera_count() < 1) {
		return -1;
	}
	var _y = ds_grid_value_y(_list, 0, 0, 1, ds_grid_height(_list), _name);
	if (_y < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    _y = -1;
	}
	return _y;
}

function ex_camera_get_name(_index) {

	var _list = obj_system_camera._ex_cameras;
	if (_list < 0) {
	    return "";
	}
	if (_index < 0 or _index > ds_grid_height(_list)) {
	    return "";
	}
	return _list[# _ex_camera._name, _index];
}

function ex_camera_get_safe_area(_name) {

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._safe_area;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// get table row
	return _table[# _table_column, _table_row];
}

function ex_camera_get_scroll_position_x(_name) {

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._scroll_x;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// get table row
	return _table[# _table_column, _table_row];
}

function ex_camera_get_scroll_position_y(_name){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._scroll_y;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// get table row
	return _table[# _table_column, _table_row];
}

function ex_camera_get_tilt_angle(_name){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._tilt_angle;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// get table row
	return _table[# _table_column, _table_row];
}

function ex_camera_get_view_index(_name){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._view;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// get table row
	return _table[# _table_column, _table_row];
}

function ex_camera_get_width(_name){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._width;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// get table row
	return _table[# _table_column, _table_row];
}

function ex_camera_get_x(_name){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._view;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	var _view = _table[# _table_column, _table_row];
	return view_get_xport(_view);
}

function ex_camera_get_y(_name){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._view;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	var _view = _table[# _table_column, _table_row];
	return view_get_yport(_view);
}

function ex_camera_get_zoom_offset_x(_name){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._zoom_offset_x;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	return _table[# _table_column, _table_row];
}

function ex_camera_get_zoom_offset_y(_name){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._zoom_offset_y;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	return _table[# _table_column, _table_row];
}

function ex_camera_get_zoom_scale(_name,_argNormalize){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._zoom_scale;
	var _default      = 0;
	var _percent      = false;
	// arguments
	if (!is_undefined(_argNormalize)) {
	    _percent = _argNormalize; 
	}
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	if (_percent == true) {
	    var _value = _table[# _table_column, _table_row];
	    return _value/100.00;
	} else {
	    return _table[# _table_column, _table_row];
	}
}

function ex_camera_initialize(){
	if (instance_exists(obj_system_camera)) {
	if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Warning, Camera system is already initialized");
	    }
	    return 0;
	}
	// create exCamera object
	instance_create_depth(0, 0,0, obj_system_camera);
	return 1;
}

function ex_camera_is_shaking(_name){
	gml_pragma("forceinline");
	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._is_shaking;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// get table row
	return _table[# _table_column, _table_row];
}

function ex_camera_is_visible(_name){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._view;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	var _view = _table[# _table_column, _table_row];
	return view_get_visible(_view);
}

function ex_camera_resize(_name, _value_x, _value_y, _value_width, _value_height, _argDuration, _argEasing, _argOnComplete, _argOnCompleteArguments){

	// smooth resize camera with animation
	var _table               = obj_system_camera._ex_cameras;
	var _table_column_width  = _ex_camera._width;
	var _table_column_height = _ex_camera._height;
	var _table_column_view   = _ex_camera._view;
	var _default             = 0;

	// arguments

	var _oncomplete = -1;
	var _duration	= 10;
	var _easing	= -1;
	var _oncomplete_arguments = ex_camera_arguments_undefined;
	if (!is_undefined(_argDuration)) {
	    _duration = _argDuration;
	}
	if (!is_undefined(_argEasing)) {
	    _easing = _argEasing;
	}
	if (!is_undefined(_argOnComplete)) {
	    _oncomplete = _argOnComplete;
	}
	if (!is_undefined(_argOnCompleteArguments)) {
	    _oncomplete_arguments = _argOnCompleteArguments;
	}
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+string(_name)+"\"");
	    }
	    return _default;
	}
	var _view = _table[# _table_column_view,  _table_row];
	_table[# _ex_camera._resize_tween_duration,  _table_row] = _duration;
	_table[# _ex_camera._resize_tween_easing,  _table_row] = _easing;
	_table[# _ex_camera._resize_start_x,  _table_row] = view_get_xport(_view);
	_table[# _ex_camera._resize_start_y,  _table_row] = view_get_yport(_view);
	_table[# _ex_camera._resize_end_x,  _table_row] = _value_x;
	_table[# _ex_camera._resize_end_y,  _table_row] = _value_y;
	_table[# _ex_camera._resize_start_width,  _table_row] = view_get_wport(_view);
	_table[# _ex_camera._resize_start_height,  _table_row] = view_get_hport(_view);
	_table[# _ex_camera._resize_end_width,  _table_row] = _value_width;
	_table[# _ex_camera._resize_end_height,  _table_row] = _value_height;
	_table[# _ex_camera._resize_oncomplete,  _table_row] = _oncomplete;
	_table[# _ex_camera._resize_oncomplete_arguments,  _table_row] = _oncomplete_arguments;
	_table[# _ex_camera._is_resizing,  _table_row] = true;
	return 1;
}

function ex_camera_scroll_by(_name, _target_x, _target_y, _argDuration, _argEasing, _argOnComplete, _argOnCompleteArguments){
	gml_pragma("forceinline");
	var _easing   = -1;
	var _list     = obj_system_camera._ex_cameras;
	// arguments
	var _oncomplete = -1;
	var _duration	= 10;
	var _oncomplete_arguments = ex_camera_arguments_undefined;
	if (!is_undefined(_argDuration)) {
	    _duration = _argDuration;
	}
	if (!is_undefined(_argEasing)) {
	    _easing = _argEasing;
	}
	if (!is_undefined(_argOnComplete)) {
	    _oncomplete = _argOnComplete;
	}
	if (!is_undefined(_argOnCompleteArguments)) {
	    _oncomplete_arguments = _argOnCompleteArguments;
	}
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return 0;
	}
	var _view      = _list[# _ex_camera._view, _table_row];
	var _current_x = _list[# _ex_camera._scroll_x, _table_row];
	var _current_y = _list[# _ex_camera._scroll_y, _table_row];
	_target_x = _current_x+_target_x;
	_target_y = _current_y+_target_y;
	ex_camera_scroll_to_point(_name, _target_x, _target_y, _duration, _easing, false, _oncomplete, _oncomplete_arguments);
	if (ex_camera_get_debug_mode()) {
	    //show_debug_message("exCamera: camera \""+_name+"\" has started scrolling by "+string(_target_x)+", "+string(_target_y));
	}
	return 1;
}

function ex_camera_scroll_to_object(_name, _instance, _duration, _argEasing, _argCenter, _argOnComplete, _argOnCompleteArguments){
	gml_pragma("forceinline");
	var _center       = true;
	var _easing       = -1;
	var _oncomplete   = -1;
	var _oncomplete_arguments = ex_camera_arguments_undefined;
	// arguments
	if (!is_undefined(_argEasing)) {
	    _easing = _argEasing;
	}
	if (!is_undefined(_argCenter)) {
	    _center = _argCenter;
	}
	if (!is_undefined(_argOnComplete)) {
	    _oncomplete = _argOnComplete;
	}
	if (!is_undefined(_argOnCompleteArguments)) {
	    _oncomplete_arguments = _argOnCompleteArguments;
	}
	if (instance_exists(_instance)) {
	    var _target_x = _instance.x;
	    var _target_y = _instance.y;
    
    
	    ex_camera_scroll_to_point(_name,  _target_x , _target_y, _duration, _easing, _center, _oncomplete, _oncomplete_arguments);    
    
	    if (ex_camera_get_debug_mode()) {
	    //    //show_debug_message('exCamera: camera "'+string(_name)+'" has started scrolling to object');
	    }
	    return 1;
	} else {
	    return 0;
	}
}

function ex_camera_scroll_to_point(_name, _target_x, _target_y, _duration, easing, center, onComplete, onCompleteArguments){
	gml_pragma("forceinline");
	var _list   = obj_system_camera._ex_cameras;
	var _center = false;
	var _easing = -1;
	var _oncomplete = -1;
	var _oncomplete_arguments = ex_camera_arguments_undefined;
	// arguments
	if (!is_undefined(easing)) {
	    _easing = easing
	}
	if (!is_undefined(center)) {
	    _center = center
	}
	if (!is_undefined(onComplete)) {
	    _oncomplete = onComplete
	}
	if (!is_undefined(onCompleteArguments)) {
	    _oncomplete_arguments = onCompleteArguments;
	}
	// check name column
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+string(_name)+"\"");
	    }
	    return 0;
	}
	var _view         = _list[# _ex_camera._view, _table_row];
	var _bound_x      = _list[# _ex_camera._bound_x, _table_row];
	var _bound_y      = _list[# _ex_camera._bound_y, _table_row];
	var _bound_width  = _list[# _ex_camera._bound_width, _table_row];
	var _bound_height = _list[# _ex_camera._bound_height, _table_row];
	var _width   = _list[# _ex_camera._width, _table_row];
	var _height  = _list[# _ex_camera._height, _table_row];
	if (_center == true) {
	    _target_x = _target_x-(_width/2);
	    _target_y = _target_y-(_height/2);
	    if (_bound_x != -1 and _bound_y != -1 and _bound_width != -1 and _bound_height != -1) {
	        _target_x = clamp(_target_x, _bound_x, _bound_width-_width);
	        _target_y = clamp(_target_y, _bound_y, _bound_height-_height);
	    }
	} else {
	    if (_bound_x != -1 and _bound_y != -1 and _bound_width != -1 and _bound_height != -1) {
	        _target_x = clamp(_target_x, _bound_x, _bound_width-_width);
	        _target_y = clamp(_target_y, _bound_y, _bound_height-_height);
	    }
	}
	// cancel motion on no position change
	if (round(_target_x) == round(_list[# _ex_camera._scroll_x, _table_row]-_list[# _ex_camera._zoom_offset_x, _table_row]) 
	    && round(_target_y) == round(_list[# _ex_camera._scroll_y, _table_row]-_list[# _ex_camera._zoom_offset_y, _table_row])
	    ) {
    
	    //if (ex_camera_get_debug_mode()) {
	        ////show_debug_message('exCamera: camera "'+string(_name)+'" has no need to scroll (same x,y position detected)');
	    //}
    
	    return 0;
	}
	var _follow_x = 0, _follow_y = 0;
	/* if (_follow != noone) {
	    _follow_x = _follow.x-(_width/2);
	    _follow_y = _follow.y-(_height/2);
    
	}*/
	var _zoom_x_offset = _list[# _ex_camera._zoom_offset_x, _table_row];
	var _zoom_y_offset = _list[# _ex_camera._zoom_offset_y, _table_row];
	_list[# _ex_camera._scroll_start_x, _table_row] = camera_get_view_x(_view)-_follow_x-_zoom_x_offset;
	_list[# _ex_camera._scroll_end_x, _table_row] = _target_x-_follow_x;
	_list[# _ex_camera._scroll_start_y, _table_row] = camera_get_view_y(_view)-_follow_y-_zoom_y_offset;
	_list[# _ex_camera._scroll_end_y, _table_row] = _target_y-_follow_y;
	_list[# _ex_camera._scroll_tween_position, _table_row] = 0;
	_list[# _ex_camera._scroll_tween_duration, _table_row] = _duration;
	_list[# _ex_camera._scroll_tween_easing,   _table_row] = _easing;
	_list[# _ex_camera._scroll_oncomplete, _table_row] = _oncomplete;
	_list[# _ex_camera._scroll_oncomplete_arguments, _table_row] = _oncomplete_arguments;
	_list[# _ex_camera._is_scrolling, _table_row] = true;
	if (ex_camera_get_debug_mode()) {
	    ////show_debug_message('exCamera: camera "'+string(_name)+'" has started scrolling to point: '+string(_target_x)+', '+string(_target_y));
	}
	return 1;
}

function ex_camera_set_bounds(_name, _argX, _argY, _argWidth, _argHeight){

	var _list = obj_system_camera._ex_cameras;
	// check name column
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	            //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return 0;
	}
	_list[# _ex_camera._bound_x,      _table_row] = _argX;
	_list[# _ex_camera._bound_y,      _table_row] = _argY;
	_list[# _ex_camera._bound_width,  _table_row] = _argWidth;
	_list[# _ex_camera._bound_height, _table_row] = _argHeight;
	return 1;
}

function ex_camera_set_debug_mode(enabled){

	obj_system_camera._ex_camera_debug_mode = enabled;
}
	
function ex_camera_set_fade_alpha(cameraName, alpha){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._fade_alpha;
	var _default      = 0;
	// arguments
	var _name  = cameraName;
	var _value = alpha;
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// set table row
	_table[# _table_column, _table_row] = _value;
	return 1;
}

function ex_camera_set_fade_color(cameraName, color){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._fade_color;
	var _default      = 0;
	// arguments
	var _name  = cameraName;
	var _value = color;
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// set table row
	_table[# _table_column, _table_row] = _value;
	return 1;
}

function ex_camera_set_fade_draw_target(cameraName, drawTarget){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._fade_draw_target;
	var _default      = 0;
	// arguments
	var _name  = cameraName;
	var _value = drawTarget;
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// set table row
	_table[# _table_column, _table_row] = _value;
	return 1;
}

function ex_camera_set_flash_blend(cameraName, blendMode){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._flash_blend;
	var _default      = 0;
	// arguments
	var _name  = cameraName;
	var _value = blendMode;
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// set table row
	_table[# _table_column, _table_row] = _value;
	return 1;
}

function ex_camera_set_flash_color(cameraName, color){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._flash_color;
	var _default      = 0;
	// arguments
	var _name  = cameraName;
	var _value = color;
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// set table row
	_table[# _table_column, _table_row] = _value;
	return 1;
}

function ex_camera_set_flash_draw_target(cameraName, drawTarget){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._flash_draw_target;
	var _default      = 0;
	// arguments
	var _name  = cameraName;
	var _value = drawTarget;
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// set table row
	_table[# _table_column, _table_row] = _value;
	return 1;
}

function ex_camera_set_position(cameraName, _argX, _argY){
	gml_pragma("forceinline");
	var _table               = obj_system_camera._ex_cameras;
	var _table_column_view   = _ex_camera._view;
	var _default             = 0;
	// arguments
	var _name         = cameraName;
	var _value_x      = _argX;
	var _value_y      = _argY;
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	var _view = _table[# _table_column_view,  _table_row];
	// set table row
	view_set_xport(_view,_value_x);
	view_set_yport(_view,_value_y);
	return 1;
}

function ex_camera_set_safe_area(_name, _value){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._safe_area;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	// set table row
	_table[# _table_column, _table_row] = _value;
	return 1;
}

function ex_camera_set_scroll_position_at_object(_name, _instance, center){
	gml_pragma("forceinline");
	var _table               = obj_system_camera._ex_cameras;
	var _table_column_x      = _ex_camera._scroll_x;
	var _table_column_y      = _ex_camera._scroll_y;
	var _table_column_view   = _ex_camera._view;
	var _default             = 0;
	var _center              = false;
	// arguments
	if (!is_undefined(center)) {
	    _center = center;
	}
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	var _view         = _table[# _table_column_view,  _table_row];
	var _width        = _table[# _ex_camera._width, _table_row];
	var _height       = _table[# _ex_camera._height, _table_row];
	var _bound_x      = _table[# _ex_camera._bound_x, _table_row];
	var _bound_y      = _table[# _ex_camera._bound_y, _table_row];
	var _bound_width  = _table[# _ex_camera._bound_width, _table_row];
	var _bound_height = _table[# _ex_camera._bound_height, _table_row];
	if (_center == true) {
	    _target_x = _instance.x-(_width/2);
	    _target_y = _instance.y-(_height/2);
	} else {
	    _target_x = _instance.x;
	    _target_y = _instance.y;
	}
	if (_bound_x != -1 and _bound_y != -1 and _bound_width != -1 and _bound_height != -1) {
	    _target_x = clamp(_target_x, _bound_x, _bound_width-_width);
	    _target_y = clamp(_target_y, _bound_y, _bound_height-_height);
	}
	// set table row
	_table[# _table_column_x, _table_row] = _target_x;
	_table[# _table_column_y, _table_row] = _target_y;
	return 1;
}

function ex_camera_set_scroll_position_at_point(_name, _target_x, _target_y, center){
	gml_pragma("forceinline");
	var _table               = obj_system_camera._ex_cameras;
	var _table_column_x      = _ex_camera._scroll_x;
	var _table_column_y      = _ex_camera._scroll_y;
	var _table_column_view   = _ex_camera._view;
	var _default             = 0;
	var _center              = false;
	// arguments
	if (!is_undefined(center)) {
	    _center = center;
	}
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	var _view         = _table[# _table_column_view,  _table_row];
	var _width        = _table[# _ex_camera._width, _table_row];
	var _height       = _table[# _ex_camera._height, _table_row];
	var _bound_x      = _table[# _ex_camera._bound_x, _table_row];
	var _bound_y      = _table[# _ex_camera._bound_y, _table_row];
	var _bound_width  = _table[# _ex_camera._bound_width, _table_row];
	var _bound_height = _table[# _ex_camera._bound_height, _table_row];
	if (_center == true) {
	    _target_x = _target_x-(_width/2);
	    _target_y = _target_y-(_height/2);
	}
	if (_bound_x != -1 and _bound_y != -1 and _bound_width != -1 and _bound_height != -1) {
	    _target_x = clamp(_target_x, _bound_x, _bound_width-_width);
	    _target_y = clamp(_target_y, _bound_y, _bound_height-_height);
	}
	// set table row
	_table[# _table_column_x, _table_row] = _target_x;
	_table[# _table_column_y, _table_row] = _target_y;
	return 1;
}

function ex_camera_set_size(_name, _value_width, _value_height) {

	var _table               = obj_system_camera._ex_cameras;
	var _table_column_width  = _ex_camera._width;
	var _table_column_height = _ex_camera._height;
	var _table_column_view   = _ex_camera._view;
	var _default             = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	var _view = _table[# _table_column_view,  _table_row];
	// set view
	camera_set_view_size(_view,_value_width,_value_height);
	view_set_wport(_view,_value_width);
	view_set_hport(_view,_value_height);

	// set table row
	_table[# _table_column_width,  _table_row] = _value_width;
	_table[# _table_column_height, _table_row] = _value_height;
	return 1;
}

function ex_camera_set_tilt_angle(_name, _value){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._tilt_angle;
	var _default      = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	var _view = _table[# _ex_camera._view, _table_row];
	// set table row
	_table[# _table_column, _table_row] = _value;
	// set angle

	camera_set_view_angle(_view,_value);
	return 1;
}

function ex_camera_set_visible(_name, _value){

	var _table        = obj_system_camera._ex_cameras;
	var _table_column = _ex_camera._view;
	var _default      = 1;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	var _view = _table[# _table_column, _table_row];
	view_set_visible(_view,_value);
}

function ex_camera_set_zoom_scale(_name, _zoom_scale){

	var _table              = obj_system_camera._ex_cameras;
	var _table_column_scale = _ex_camera._zoom_scale;
	var _table_column_view  = _ex_camera._view;
	//var _table_column_x     = _ex_camera._scroll_x;
	//var _table_column_y     = _ex_camera._scroll_y;
	var _default            = 0;
	// arguments
	// check if item exists first
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return _default;
	}
	var _view  = _table[# _table_column_view,  _table_row];
	var _scale = _table[# _ex_camera._zoom_scale, _table_row];
	var _scroll_x_offset = _table[# _ex_camera._scroll_x, _table_row];
	var _scroll_y_offset = _table[# _ex_camera._scroll_y, _table_row];
	var _view_width  = _table[# _ex_camera._width, _table_row];
	var _view_height = _table[# _ex_camera._height, _table_row];
	var _ratio_x = (( ( _view_width  / 2) ) / 100 );
	var _ratio_y = (( ( _view_height / 2) ) / 100 );
	var _zoom_end_x = (( camera_get_view_x(_view))+( _scale - _zoom_scale )*( _ratio_x ) )-_scroll_x_offset; 
	var _zoom_end_y = (( camera_get_view_y(_view))+( _scale - _zoom_scale )*( _ratio_y ) )-_scroll_y_offset;
	_table[# _ex_camera._zoom_offset_x, _table_row] = _zoom_end_x;
	_table[# _ex_camera._zoom_offset_y, _table_row] = _zoom_end_y;

	var _wValue	= view_get_wport(_view) * (_zoom_scale / 100);
	var _hValue	= view_get_hport(_view) * (_zoom_scale / 100);
	camera_set_view_size(_view,_wValue,_hValue);

	// set table row
	_table[# _table_column_scale, _table_row] = _zoom_scale;
	return 1;
}

function ex_camera_shake(cameraName, shakeX, shakeY, shakeAngle, duration, easing, onComplete, onCompleteArguments){
	gml_pragma("forceinline");
	var _name = cameraName;
	var _list = obj_system_camera._ex_cameras;
	var _shake_x = 0;
	var _shake_y = 0;
	var _shake_angle = 0;
	var _duration = 12;
	var _easing = -1;
	var _oncomplete = -1;
	var _oncomplete_arguments = ex_camera_arguments_undefined;
	if (!is_undefined(shakeX)) {
	    _shake_x = shakeX;
	}
	if (!is_undefined(shakeY)) {
	    _shake_y = shakeY;
	}
	if (!is_undefined(shakeAngle)) {
	    _shake_angle = shakeAngle;
	}
	if (!is_undefined(duration)) {
	    _duration = duration;
	}
	if (!is_undefined(easing)) {
	    _easing = easing;
	}
	if (!is_undefined(onComplete)) {
	    _oncomplete = onComplete;
	}
	if (!is_undefined(onCompleteArguments)) {
	    _oncomplete = onCompleteArguments;
	}
	// check name column
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
	    return 0;
	}
	_list[# _ex_camera._shake_start, _table_row] = 0;
	_list[# _ex_camera._shake_end, _table_row] = _duration;
	_list[# _ex_camera._shake_tween_duration, _table_row] = _duration;
	_list[# _ex_camera._shake_tween_position, _table_row] = 0;
	_list[# _ex_camera._shake_tween_easing, _table_row] = _easing;
	_list[# _ex_camera._shake_power_x, _table_row] = _shake_x;
	_list[# _ex_camera._shake_power_y, _table_row] = _shake_y;
	_list[# _ex_camera._shake_power_angle, _table_row] = _shake_angle;
	_list[# _ex_camera._shake_oncomplete, _table_row] = _oncomplete;
	_list[# _ex_camera._shake_oncomplete_arguments, _table_row] = _oncomplete_arguments;
	_list[# _ex_camera._is_shaking, _table_row] = true;
	if (ex_camera_get_debug_mode()) {
	//show_debug_message("exCamera: camera \""+_name+"\" has started shaking");
	}
	return 1;
}

function ex_camera_tilt(cameraName, value, duration, easing, onComplete){

	var _name = cameraName;
	var _list = obj_system_camera._ex_cameras;
	var _easing = -1;
	var _value = value;
	var _duration = duration;
	var _oncomplete = -1;
	var _oncomplete_arguments = ex_camera_arguments_undefined;
	if (!is_undefined(easing)) {
	    _easing = easing;
	}
	if (!is_undefined(onComplete)) {
	    _oncomplete = onComplete;
	}
	// check name column
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
    
	    return 0;
	}
	_list[# _ex_camera._tilt_start, _table_row] = _list[# _ex_camera._tilt_angle, _table_row];
	_list[# _ex_camera._tilt_end, _table_row] = _value;
	_list[# _ex_camera._tilt_tween_duration, _table_row] = _duration;
	_list[# _ex_camera._tilt_tween_position, _table_row] = -1;
	_list[# _ex_camera._tilt_tween_easing, _table_row] = _easing;
	_list[# _ex_camera._tilt_oncomplete, _table_row] = _oncomplete;
	_list[# _ex_camera._tilt_oncomplete_arguments, _table_row] = _oncomplete_arguments;
	_list[# _ex_camera._is_tilting, _table_row] = true;
	if (ex_camera_get_debug_mode()) {
	//show_debug_message("exCamera: camera \""+_name+"\" has started tilting");
	}
	return 1;
}

function ex_camera_zoom(cameraName, scale, duration, easing, onComplete, onCompleteArguments) {
	
	var _name = cameraName;
	var _list = obj_system_camera._ex_cameras;
	var _zoom_scale = scale;
	var _duration = duration;
	var _easing = -1;
	var _oncomplete = -1;
	var _oncomplete_arguments = ex_camera_arguments_undefined;
	if (!is_undefined(easing)) {
	    _easing = easing;
	}
	if (!is_undefined(onComplete)) {
	    _oncomplete = onComplete;
	}
	if (!is_undefined(onCompleteArguments)) {
	    _oncomplete_arguments = onCompleteArguments;
	}
	// check name column
	var _table_row = ex_camera_get_index(_name);
	if (_table_row < 0) {
	    if (ex_camera_get_debug_mode()) {
	        //show_debug_message("exCamera: Error, could not find camera with name \""+_name+"\"");
	    }
    
	    return 0;
	}
	var _scale = _list[# _ex_camera._zoom_scale, _table_row];
	var _view  = _list[# _ex_camera._view, _table_row];
	var _view_width  = _list[# _ex_camera._width, _table_row];
	var _view_height = _list[# _ex_camera._height, _table_row];
	var _scroll_x_offset = _list[# _ex_camera._scroll_x, _table_row];
	var _scroll_y_offset = _list[# _ex_camera._scroll_y, _table_row];

	var _zoom_start_x = camera_get_view_x(_view)-_scroll_x_offset;
	var _zoom_start_y = camera_get_view_y(_view)-_scroll_y_offset;

	var _ratio_x = (( ( _view_width /2) ) /100 );
	var _ratio_y = (( ( _view_height/2) ) /100 );
	var _zoom_end_x = (( camera_get_view_x(_view))+( _scale - _zoom_scale )*( _ratio_x ) )-_scroll_x_offset; // *6.4 in 720p
	var _zoom_end_y = (( camera_get_view_y(_view))+( _scale - _zoom_scale )*( _ratio_y ) )-_scroll_y_offset; // *3.6 in 720p
	//if(_zoom_end_x < 0 || _zoom_end_x+_view_width > room_width || _zoom_end_y < 0 || _zoom_end_y+_view_height > room_height)
	//{
	//	return 1;
	//}

	_list[# _ex_camera._zoom_start_scale, _table_row]    = _list[# _ex_camera._zoom_scale, _table_row];
	_list[# _ex_camera._zoom_end_scale, _table_row]      = _zoom_scale;
	_list[# _ex_camera._zoom_tween_duration, _table_row] = _duration;
	_list[# _ex_camera._zoom_tween_position, _table_row]   = 0;
	_list[# _ex_camera._zoom_tween_easing, _table_row]     = _easing;
	_list[# _ex_camera._zoom_oncomplete, _table_row] = _oncomplete;
	_list[# _ex_camera._zoom_oncomplete_arguments, _table_row] = _oncomplete_arguments;
	/// setup center for zoom
	var _ox = ((room_width  - (camera_get_view_x(_view)+camera_get_view_width(_view))));
	var _oy = ((room_height - (camera_get_view_y(_view)+camera_get_view_height(_view))));
	var _oxc = ((room_width  - (camera_get_view_x(_view)+camera_get_view_width(_view)))/2);
	var _oyc = ((room_height - (camera_get_view_y(_view)+camera_get_view_height(_view)))/2);

	// find view ratio (don't use view w/h but stored value because former changes as you zoom)


	// start-end values for the zoom
	_list[# _ex_camera._zoom_start_x, _table_row] = _zoom_start_x;
	_list[# _ex_camera._zoom_start_y, _table_row] = _zoom_start_y;
	_list[# _ex_camera._zoom_end_x, _table_row]   = _zoom_end_x;
	_list[# _ex_camera._zoom_end_y, _table_row]   = _zoom_end_y;
	_list[# _ex_camera._is_zooming, _table_row] = true;

	return 1;
}

function ex_camera_math_smoothstep(_a, _b, _t){
    gml_pragma("forceinline");
 
	var _p;
	if (_t < _a) { 
	    return 0;
	}
	if (_t >= _b) {
	    return 1;
	}
	if (_a == _b) {
	    return -1;
	}
	_p = ((_t - _a) / (_b - _a));
	return (_p * _p * (3 - 2 * _p));
}

function ex_camera_ds_grid_delete_y(DSGridIndex, _argY, shift){
	gml_pragma("forceinline");
	var _grid   = DSGridIndex;
	var _y      = _argY;
	var _shift  = false;
	if (!is_undefined(shift)) {
	    _shift = shift;
	}
	var _grid_width  = ds_grid_width(_grid);
	var _grid_height = ds_grid_height(_grid);
	if (_grid_height < 2) {
	    ds_grid_clear(_grid, "");
	    ds_grid_resize(_grid, ds_grid_width(_grid), 1);
	    return 0;
	}
	if (_shift == true) {
	    ds_grid_set_grid_region(_grid, _grid, 0, _y+1, _grid_width-1, _y+1, 0, _y);
	    for (var _i=_y; _i <= ds_grid_height(_grid); ++_i) {
	        ds_grid_set_grid_region(_grid, _grid, 0, _i+1, _grid_width-1, _i+1, 0, _i);    
	    }
    
	} else {
    
	    ds_grid_set_grid_region(_grid, _grid, 0, _y+1, _grid_width-1, _grid_height-_y, 0, _y);
    
	}
	ds_grid_resize(_grid, _grid_width, _grid_height-1);
	return 1;
}

