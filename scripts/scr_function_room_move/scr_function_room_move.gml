/// @param next_Room
/// @param persistant
function method_function_room_move(_room, _persis) {
	if(instance_exists(obj_system_room_move))
	{
		instance_destroy(obj_system_room_move);
	}
	var _inst	= instance_create_depth(x,y,-1000,obj_system_room_move);
	with(_inst)
	{
		roomDestination	= _room;
	}
	global.beforeRoom	= room;
}
