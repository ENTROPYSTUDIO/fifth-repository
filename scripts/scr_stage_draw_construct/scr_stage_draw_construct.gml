function constructStageDrawAvatar(_frameSub,_sprAvatar,_avatarSub,_avatarScale,_avatarMargin,_starIcon,_avatarColor,_frameColor,_surfaceScale) constructor {
	surfaceMask	= noone;
	surfaceUpdate	= true;
	surfaceColor	= c_white;
	surfaceScale	= _surfaceScale;
	surfaceSize		= 128;
	surfaceCenterX	= 64;
	surfaceCenterY	= 64;
	frameSub	= _frameSub;
	sprAvatar	= _sprAvatar;
	avatarSub	= _avatarSub;
	avatarScale	= _avatarScale;
	avatarColor	= _avatarColor
	avatarX		= surfaceCenterX;
	avatarY		= surfaceCenterY-_avatarMargin;
	starX		= surfaceCenterX;
	starY		= surfaceCenterY+sprite_get_height(spr_room_stage_avatar_frame)/2-18;
	starIcon	= _starIcon;
	frameColor	= _frameColor;
	lerpGoalY	= other.y;
	
	static surfaceScaleChange	= function(_type){
		if(_type)
		{
			surfaceScale	= 1;
		}
		else
		{
			surfaceScale	= 110/surfaceSize;
		}
		surfaceUpdate	= true;
	};
	
	static drawSurface	= function() {
		
		if(!surface_exists(surfaceMask)) 
		{
			surfaceMask = surface_create(surfaceSize, surfaceSize);
			surfaceUpdate	= true;
		}
		
		if(surfaceUpdate)
		{
			surfaceUpdate	= false;
			surface_set_target(surfaceMask);
			draw_clear_alpha(c_white,0);
			draw_set_alpha(1);
			draw_set_color(frameColor);
			draw_sprite(spr_room_stage_avatar_frame,frameSub,surfaceCenterX,surfaceCenterY);
			
			if(sprAvatar != noone)
			{
				draw_sprite_ext(sprAvatar,avatarSub,avatarX,avatarY,avatarScale,avatarScale,0,avatarColor,1);
			}
			surface_reset_target();
		}
		
		static surfaceCenterMarginX	= surfaceCenterX*surfaceScale;
		static surfaceCenterMarginY	= surfaceCenterY*surfaceScale;
		draw_surface_ext(surfaceMask,other.x-surfaceCenterMarginX,other.y-surfaceCenterMarginY,surfaceScale,surfaceScale,0,surfaceColor,1);
	}
	
	static frameLessDrawSurface	= function(_myCharacterInst) {
		if(!surface_exists(surfaceMask)) 
		{
			surfaceMask = surface_create(surfaceSize, surfaceSize);
			surfaceUpdate	= true;
		}

		if(surfaceUpdate)
		{
			surfaceUpdate	= false;
			surface_set_target(surfaceMask);
			draw_clear_alpha(c_black,0);
			draw_set_alpha(1);
			//draw_sprite(spr_room_stage_avatar_frame,frameSub,surfaceCenterX,surfaceCenterY);

			if(sprAvatar != noone)
			{
				draw_sprite_ext(sprAvatar,avatarSub,avatarX,avatarY,avatarScale,avatarScale,0,c_white,1);
			}
			if(starIcon != noone)
			{
				draw_sprite_ext(starIcon,0,starX,starY,0.5,0.5,0,c_white,1);
			}

			var _centerX	= surfaceCenterX+2;
			var _surfaceSize	= surfaceSize-15;
			with(_myCharacterInst)
			{
				structDraw.healthBarDraw(_centerX,_surfaceSize,structState.healthRatio,structAbility.healthPoint);
			}

			surface_reset_target();
		}
		
		static surfaceCenterMarginX	= surfaceCenterX*surfaceScale;
		static surfaceCenterMarginY	= surfaceCenterY*surfaceScale;
		draw_surface_ext(surfaceMask,other.x-surfaceCenterMarginX,other.y-surfaceCenterMarginY,surfaceScale,surfaceScale,0,surfaceColor,1);
	}
}

function constructorCharacterSlotMeta() constructor {
	var _names	= variable_struct_get_names(global.structUserData.equip.character);
	for(var _i=0; _i<array_length(_names); ++_i)
	{
		variable_struct_set(self,_names[_i],{
			uid : "",
			inst : ""
		});
	}
}


