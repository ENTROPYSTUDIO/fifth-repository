/// unix_timestamp([datetime])
function method_function_time_datetime_to_timestamp(datetime) {
	//
	//  Returns a Unix timestamp for the current time
	//  or optionally given GameMaker datetime value.
	//
	//      datetime    GameMaker datetime, real
	//
	/// GMLscripts.com/license

	var timezone = date_get_timezone();
	date_set_timezone(timezone_utc);
	var timestamp = round(date_second_span(25569, datetime));
	date_set_timezone(timezone);
	return timestamp*1000;


}
