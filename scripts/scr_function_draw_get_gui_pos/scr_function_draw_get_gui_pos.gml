function get_x_pos_for_gui_ratio(_xPos,_option) {
	var _baseW	= MAC_GAME_BASE_WIDTH;
	var _guiW	= display_get_gui_width();
	var _baseHalfW	= _baseW/2;
	var _guiHalfW	= _guiW/2;
	var _gap	= _baseW-_xPos;
	var _forceAlignment	= false;
	_option	= is_undefined(_option) ? "" : _option;
	if(_option != "")
	{
		switch(_option)
		{
			case "center":
			{
				var _reuturn	= _xPos+(abs(_guiW-_baseW)/2);
				break;
			}
			case "alignment":
			{
				var _reuturn	= _xPos*(_baseW/_guiW);
				break;
			}
			case "scale":
			{
				var _off_x_percent	= _xPos/_guiW;
				var _reuturn	= _off_x_percent*_guiW;//(_guiW/_baseW);
				break;
			}
		}
		return (_reuturn);
	}

	//가운데를 중심으로 나눔
	if(_xPos > _baseHalfW || _forceAlignment)
	{
		return(_guiW-_gap);
	}
	else if(_xPos < _baseHalfW)
	{
		return(_xPos);
	}
	else
	{
		return(_guiHalfW);
	}
}

function get_y_pos_for_gui_ratio(_yPos) {
	//var _baseH	= MAC_GAME_BASE_HEIGHT;
	var _guiH	= display_get_gui_height();
	//var _baseHalfH	= _baseH/2;
	//var _guihalfH	= _guiH/2;
	//var _gap	= _baseH-_yPos;

	var _off_y_percent = _yPos / _guiH;
	var _gui_y = _off_y_percent * _guiH;

	return(_gui_y);
}
