function method_string_pad_right(_string,_length,_pad) {

	var _pad_size, _padding, _out;

	_pad_size = string_length(_pad);
	_padding = max(0, _length - string_length(_string));

	_out  = _string;
	_out += string_repeat(_pad, _padding div _pad_size);
	_out += string_copy(_pad, 1, _padding mod _pad_size);
	_out  = string_copy(_out, 1, _length);

	return _out;
}

function method_ds_map_print(_map,_filename) {

	// note: only prints non-nested maps
	if(!global.debugMode) { exit; }
	var _file_handle = -1;
	var _size = ds_map_size(_map);
	var _key = ds_map_find_first(_map);

	if (!is_undefined(_filename)) {
	    _file_handle = file_text_open_append(_filename);
	    file_text_writeln(_file_handle);
	    file_text_write_string(_file_handle, "  DS Map: "+string(_map)); 
	    file_text_writeln(_file_handle);
	    file_text_write_string(_file_handle, "  Size: "+string(ds_map_size(_map))); 
	    file_text_writeln(_file_handle);
	} else {
	    show_debug_message("");
	    show_debug_message("  DS Map: "+string(_map)); 
	    show_debug_message("  Size: "+string(ds_map_size(_map)));
	}

	// find longest key string in map items
	var _found   = "";
	var _longest = "";

	for (var _i=0; _i < _size; ++_i) {
    
	    _found = string(_key);
	    _key = ds_map_find_next(_map, _key);
    
	    if (string_length(_found) > string_length(_longest)) {
	        _longest = _found;
	    }
	}

	var _maxlength = string_length(_longest);

	if (!is_undefined(_filename)) {
	    file_text_write_string(_file_handle, "  { "); 
	    file_text_writeln(_file_handle);    
	} else {
	    show_debug_message("  { ");
	}

	// iterate top map keys
	_key = ds_map_find_first(_map);
	for (var _i=0; _i < _size; ++_i) {
    
	    var _value = ds_map_find_value(_map, _key);
    
	    if (!is_undefined(_filename)) {
	        file_text_write_string(_file_handle, "      "+method_string_pad_right(string(_key), _maxlength, " ") +" : "+string( _value )); 
	        file_text_writeln(_file_handle);
	    } else {
	        show_debug_message("      "+method_string_pad_right(string(_key), _maxlength, " ") +" : "+string( _value ));
	    }
    
	    _key = ds_map_find_next(_map, _key);
    
	}

	if (!is_undefined(_filename)) {
	    file_text_write_string(_file_handle, "  }"); 
	    file_text_writeln(_file_handle);
	    file_text_writeln(_file_handle);
	    file_text_close(_file_handle);
	} else {
	    show_debug_message("  }");
	    show_debug_message(""); 
	}




}
function method_ds_grid_print(_grid,_filename,_column_names) {

 
	if(!global.debugMode) { exit; }
	var _width, _height, _string, _row, _column;

	var _file_handle = -1;

	_width  = ds_grid_width(_grid);
	_height = ds_grid_height(_grid);

	if (!is_undefined(_filename)) {
	    _file_handle = file_text_open_append(_filename);
	}

	if (is_undefined(_filename)) {
	    show_debug_message("");
    
	    show_debug_message("  DS Grid: "+string(_grid));
	    show_debug_message("  Size: "+string(_width)+" x "+string(_height));
    
	    show_debug_message("");
	} else {
    
	    file_text_writeln(_file_handle);
	    file_text_write_string(_file_handle, "  DS Grid: "+string(_grid));
	    file_text_writeln(_file_handle);
	    file_text_write_string(_file_handle, "  Size: "+string(_width)+" x "+string(_height));
	    file_text_writeln(_file_handle);
	    file_text_writeln(_file_handle);

	}

	// find longest string in grid items
	var _found = "";
	var _longest = "";

	for (_row = 0; _row < _height; ++_row) {

	    for (_column = 0; _column < _width; ++_column) {
        
	        _found = string( _grid[# _column, _row]);
	        if (string_length(_found) > string_length(_longest)) { _longest = _found; }
    
	    }
	}

	var _maxlength = string_length(_longest)+1;
	var _height_digits = string_length(string(_height));

	// print column indexes
	var _number_of_columns = 0;
	var _columns = " "+method_string_pad_right(" ", _height_digits, " ")+" |";
	for (_column = 0; _column < _width; ++_column) {
	    if (!is_undefined(_column_names)) {
	        _columns += " "+method_string_pad_right(string(_column)+" "+_column_names[_column], _maxlength, " ")+"|";
	    } else {
	        _columns += " "+method_string_pad_right(string(_column), _maxlength, " ")+"|";
	    }
	    _number_of_columns += 1;
	}

	// print separator
	var _separator = " "+method_string_pad_right(" ", _height_digits, " ")+" ";
	repeat(_number_of_columns+1) { repeat(_maxlength) { _separator += "-"; } }

	if (is_undefined(_filename)) {
	    show_debug_message(_columns);
	    show_debug_message(_separator);
	} else {
	   file_text_write_string(_file_handle, _columns);
	   file_text_writeln(_file_handle);
	   file_text_write_string(_file_handle, _separator);
	   file_text_writeln(_file_handle);
	}

	// print row indexes and items
	for (_row = 0; _row < _height; ++_row) {
    
	    _string = " ";
	    _string += method_string_pad_right(string(_row), _height_digits, " ")+" | ";
    
	    for (_column = 0; _column < _width; ++_column) {
    
	        if (_column >= _width-1) {
	            _delimiter = "|";
	        } else {
	            _delimiter = ", ";
	        }
         
	        if (_grid[# _column, _row] == 0) {
	            _string += method_string_pad_right("0", _maxlength, " ")+_delimiter;
	        } else {
	            _string += method_string_pad_right(string( _grid[# _column, _row] ), _maxlength, " ")+_delimiter;
	        }
        
	    }
    
	    if (is_undefined(_filename)) {
	        show_debug_message(_string);
	    } else {
	        file_text_write_string(_file_handle, _string); 
	        file_text_writeln(_file_handle);
	    }
    
	}

	// print separator
	if (is_undefined(_filename)) {
	    show_debug_message(_separator);
	    show_debug_message("");
	} else {
	   file_text_write_string(_file_handle, _separator);    
	   file_text_writeln(_file_handle); 
	   file_text_writeln(_file_handle);
	}

	if (!is_undefined(_filename)) {
	    file_text_close(_file_handle);
	}


}
function method_ds_list_print(_list,_filename) {

	// find longest string in list items
	var _found = "";
	var _longest = "";
	var _file_handle	= noone;
	for (var _i=0; _i < ds_list_size(_list); ++_i) {
	    _found = string(_i);
	    if (string_length(_found) > string_length(_longest)) { _longest = _found; }
	}

	var _maxlength = string_length(_longest);

	if (!is_undefined(_filename)) {
	    _file_handle = file_text_open_append(_filename);
	    file_text_writeln(_file_handle);
	    file_text_write_string(_file_handle, "  DS List: "+string(_list)); 
	    file_text_writeln(_file_handle);
	    file_text_write_string(_file_handle, "  Size: "+string(ds_list_size(_list))); 
	    file_text_writeln(_file_handle);
	} else {
	    show_debug_message(""); 
	    show_debug_message("  DS List: "+string(_list)); 
	    show_debug_message("  Size: "+string(ds_list_size(_list))); 
	    show_debug_message(""); 
	}

	for (var _i=0; _i < ds_list_size(_list); ++_i) {
    
	    if (!is_undefined(_filename)) {
	        file_text_write_string(_file_handle, " "+method_string_pad_right(string(_i), _maxlength, " ")+" : "+string(_list[| _i]) );
	        file_text_writeln(_file_handle);
	        file_text_writeln(_file_handle);
	    } else {
	        show_debug_message( " "+method_string_pad_right(string(_i), _maxlength, " ")+" : "+string(_list[| _i]) );
	    }
    
	}


	if (!is_undefined(_filename)) {
	    file_text_close(_file_handle);
	} else {
	    show_debug_message(""); 
	}





}
