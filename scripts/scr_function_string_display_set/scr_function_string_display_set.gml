///@param text
///@param width
///@param height
///@param size
///@param optionFont
function method_function_string_display_set(_original_text,_draw_width,_draw_height,_draw_f_size,_font) {
	_original_text	= string(_original_text);

	if(!is_undefined(_font))
	{
		draw_set_font(_font);
	}
	else
	{
		draw_set_font(enumFont.loadout_stat);
	}

	var _i;
	while(true)
	{
		var _return_text		= "";
		var _string_width		= string_width(_original_text)*_draw_f_size;
		var _string_height		= string_height(_original_text)*_draw_f_size;
		var _string_length		= string_length(_original_text);
		var _over_draw_width	= _string_width >= _draw_width;
		var _output				= [];
		var _slice_text			= "";

		if(_over_draw_width)
		{
			var _new_line		= 0;
			for(_i=0; _i<_string_length; _i++)
			{
				_slice_text			+= string_copy(_original_text,_i+1,1);
				_string_width		= string_width(_slice_text)*_draw_f_size;
			
				if(_string_width >= _draw_width)
				{
					if(string_pos(" ",_slice_text) == 1)
					{
						_slice_text	= string_delete(_slice_text,1,1);
					}
				
				
					var _next_first	= string_char_at(_original_text,_i+2);
					if(_next_first == "." || _next_first == "?" || _next_first == "!" || _next_first == ",")
					{
						_output[_new_line]	= _slice_text+_next_first+"\n"
						_i++;
						//_output[_new_line]	= string_insert("\n",_slice_text,string_length(_slice_text)-2);
					}
					else if(_next_first == " ")
					{
						_output[_new_line]	= _slice_text+"\n";
						_i++;
					}
					else
					{
						_output[_new_line]	= _slice_text+"\n";
					}
				
					_slice_text	= "";
					_new_line	++;
				}
			}
		
			if(_slice_text != "")
			{
				_output[_new_line]	= _slice_text;
			}
		
			for(_i=0; _i<array_length(_output); _i++)
			{
				_return_text	+= string(_output[_i]);
			}
		
			_string_height	= string_height(_return_text)*_draw_f_size;
			if(_string_height >= _draw_height)
			{
				_draw_f_size	-= 0.05;
			}
			else
			{
				break;
			}
		}
		else
		{
			_return_text	= _original_text;
			break;
		}
	}
	
	return([_return_text,_draw_f_size]);
}
