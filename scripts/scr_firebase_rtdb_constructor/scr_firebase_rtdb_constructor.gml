function firebaseRtdbUpdateMultiPathConstructor() constructor {
	//var _size	= array_length(_arrPath);
	//for(var _i=0; _i<_size; ++_i)
	//{
	//	var _path	= _arrPath[_i];
	//	var _value	= _arrValue[_i];
	//	variable_struct_set(self,_path,_value);
	//}
	
	static add	= function(_path,_value){
		variable_struct_set(self,_path,_value);
	};
	static getJson	= function(){
		return json_stringify(self);
	};
	static clear	= function(){
		method_show_debug([self]);
		var _names	= variable_struct_get_names(self);
		var _size	= array_length(_names);
		for(var _i=0; _i<_size; ++_i)
		{
			variable_struct_remove(self,_names[_i]);
		}
	};
	static existsCheck	= function(){
		if(variable_struct_names_count(self) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	};
}

function firebase_userdata_pipeline_add(_path,_value){
	global.firebaseUserDataPipeLine.add(_path,_value);
}

function firebase_userdata_pipeline_upload(){
	with(obj_system_control)
	{
		structUserDataUploader.forceUpdate();
	}
};


