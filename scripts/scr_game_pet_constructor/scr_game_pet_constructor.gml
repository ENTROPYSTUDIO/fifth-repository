#region constructPetAbility
function constructPetAbility(_uid) constructor {
	range	= pet_get_static_ability(_uid,"range","battle");
	lootDelay	= pet_get_static_ability(_uid,"lootDelay","battle");
	moveSpeed	= pet_get_static_ability(_uid,"moveSpeed","battle");
	multi	= pet_get_static_ability(_uid,"multi","battle");
	heal	= pet_get_static_ability(_uid,"heal","battle");
	healDelay	= pet_get_static_ability(_uid,"healDelay","battle");
}
#endregion

#region constructPetDraw
function constructPetDraw() constructor {
	maxScale = 0.6;
	minScale = 0.4;
	nowScale = 1;
	changeValue = 1;
	changeAmount = 0.04;
	auraSprite = spr_character_pet_aura;
	shadowSprite	= spr_character_pet_shadow;
	
	static auraScale	= function() {
		if(nowScale > maxScale)
		{ 
			changeValue = minScale-0.1;
		}
		else if(nowScale < minScale)
		{
			changeValue = maxScale+0.1;
		}
		nowScale	= lerp(nowScale,changeValue,changeAmount);
	}
}
#endregion

#region constructPetState
function constructPetState() constructor {
	animationNow	= "none";//enumSkeletonAninmationState.none;
	animationSet	= "none";//enumSkeletonAninmationState.none;
	forceAnimation	= "none";//noone;
	animationName	= "";
	animationCommite	= true;
	target	= noone;
	prowlDelay	= 1000;
	imageScale	= 0.7;
	aggroTime	= 0;
	prowlTime	= 0;
	lootTime	= 0;
	aggroPoint	= 0;
	healthRatio	= 1;
	hsp	= 0;
	vsp	= 0;
	die	= false;
	stun	= false;
	roomIn	= false;
	knockBack	= false;
	standing	= false;
	orientation	= false;
	objectType	= "pet";
	category	= "player";
}
#endregion




























