/// @param url host
/// @param param map
function method_dynamic_link_create_deep_url(_map) {

	var _defaultUrl	= "https://play.google.com/store/apps/details?id=com.entropy.stickmanSurvival"

	var _arrayNames	= variable_struct_get_names(_map);
	for(var _i=0; _i<array_length(_arrayNames); _i++)
	{
		var _param	= _arrayNames[_i];
		var _paramValue	= variable_struct_get(_map,_param);
		var _final	= "&"+_param+"="+string(_paramValue);
		_defaultUrl	= _defaultUrl+_final;
	}
	method_show_debug(["_defaultUrl",_defaultUrl]);
	//delete _map;
	return(_defaultUrl);
}
