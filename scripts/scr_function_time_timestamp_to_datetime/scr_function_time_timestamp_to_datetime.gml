function method_function_time_timestamp_to_datetime(_arg) {

	var _timestamp = _arg/1000;

	var _timezone = date_get_timezone();
	date_set_timezone(timezone_utc);

	var _datetime = date_inc_second(date_create_datetime(2000,1,1,0,0,0), _timestamp - 946684800);

	date_set_timezone(_timezone)

	return _datetime;


}
