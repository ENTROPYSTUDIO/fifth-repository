#region string_number_abbrev
function string_number_abbrev(_real,_dec){
	var _suffix	= "";
	var _chrAtStart	= 96;
	var _chrAtEnd	= 122;
	
	var _k		= power(10,3);
	var _index	= 0;
	while(true)
	{
		var _calcul	= _real/_k;
		if(_calcul >= 1)
		{
			_real	= _calcul;
			_index++;
		}
		else
		{
			//if(_index < 1) { _dec	= 0; }
			break;
		}
	}
	if(_index > 0)
	{
		while(_index > 26)
		{
			var _div	= _index div 26;
			_suffix	= chr(_chrAtStart+_div);
			_index -= 26*_div;
		}
		if(_index > 0)
		{
			_suffix += chr(_chrAtStart+_index);
		}
	}
	if(frac(_real) == 0) { _dec = 0; }
	var _return	= string_format(_real,1,_dec)+_suffix;
	return _return;
}
#endregion 

#region string_number_resource_abbrev
function string_number_resource_abbrev(_real,_dec){
	var _suffix	= "";
	var _chrAtStart	= 96;
	var _chrAtEnd	= 122;
	
	var _k		= power(10,3);
	var _index	= 0;
	while(true)
	{
		var _calcul	= _real/_k;
		if(_calcul >= 1)
		{
			_real	= _calcul;
			_index++;
		}
		else
		{
			if(_index < 1) { _dec	= 0; }
			break;
		}
	}
	if(_index > 0)
	{
		while(_index > 26)
		{
			var _div	= _index div 26;
			_suffix	= chr(_chrAtStart+_div);
			_index -= 26*_div;
		}
		if(_index > 0)
		{
			_suffix += chr(_chrAtStart+_index);
		}
	}
	if(frac(_real) == 0) { _dec = 0; }
	var _return	= string_format(_real,1,_dec)+_suffix;
	return _return;
}
#endregion 

#region string_number_abbrev_remain_dec
function string_number_abbrev_remain_dec(_real,_dec){
	var _suffix	= "";
	var _chrAtStart	= 96;
	var _chrAtEnd	= 122;
	
	var _k		= power(10,3);
	var _index	= 0;
	while(true)
	{
		var _calcul	= _real/_k;
		if(_calcul >= 1)
		{
			_real	= _calcul;
			_index++;
		}
		else
		{
			break;
		}
	}
	
	if(_index > 0)
	{
		while(_index > 26)
		{
			var _div	= _index div 26;
			_suffix	= chr(_chrAtStart+_div);
			_index -= 26*_div;
		}
		if(_index > 0)
		{
			_suffix += chr(_chrAtStart+_index);
		}
	}
	
	var _return	= string_format(_real,1,_dec)+_suffix;
	return _return;
}
#endregion

#region string_number_add
function string_number_add(_stringNumber,_amount){
	var _real	= real(_stringNumber);
	var _final	= _real+_amount;
	var _reutrn	= string(_final);
	return _reutrn;
}
#endregion

#region string_number_resource_display
function string_number_resource_display(_struct,_now) {
	var _before	= _struct.beforeAmount;
	var _realNow	= real(_now);
	if(_before != _realNow)
	{
		if(abs(_realNow-_before) > 1)
		{
			_struct.beforeAmount	= lerp(_struct.beforeAmount,_realNow,0.05);
			_struct.drawAmount	= string_number_resource_abbrev(_struct.beforeAmount,3);
		}
		else
		{
			_struct.beforeAmount	= _realNow;
			_struct.drawAmount	= string_number_resource_abbrev(_realNow,3);
		}
		return true;
	}
	else
	{
		return false;
	}
}
#endregion

#region string_number_format_thousand
function string_number_format_thousand(n, places, dec, sep) {
	var out,pos,i;
	if (!is_string(dec)) dec = ".";
	if (!is_string(sep)) sep = ",";
	out = string_format(abs(n),0,places);
	pos = string_pos(".",out);
	if(pos == 0)
	{
	pos = string_length(out)+1;
	}
	else
	{
		out = string_replace(out,".",dec);
	}
	for (i=pos-3; i>1; i-=3) out = string_insert(sep,out,i);
	if (n < 0) out = "-" + out;
	return out;


}
#endregion