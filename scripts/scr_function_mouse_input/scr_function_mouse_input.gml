function method_function_input_mouse_released_gui(_call_id,_win_layer,_freeId) {
	for(var _i=0; _i<4; _i++)
	{
		if(device_mouse_check_button_released(_i,mb_left))
		{
			var _mouseX	= device_mouse_x_to_gui(_i);
			var _mouseY	= device_mouse_y_to_gui(_i);
			if(_freeId != noone && !position_meeting(_mouseX,_mouseY,_freeId))
			{
				return true;
			}
			else if(position_meeting(_mouseX,_mouseY,_call_id) && _win_layer == global.windowLayer)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
function method_function_input_mouse_released_gui_single(_call_id,_win_layer,_freeId) {
	var _i	= 0;
	if(device_mouse_check_button_released(_i,mb_left))
	{
		var _mouseX	= device_mouse_x_to_gui(_i);
		var _mouseY	= device_mouse_y_to_gui(_i);
		if(_freeId != noone && !position_meeting(_mouseX,_mouseY,_freeId))
		{
			return true;
		}
		else if(position_meeting(_mouseX,_mouseY,_call_id) && _win_layer == global.windowLayer)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
function method_function_input_mouse_released(_call_id,_win_layer,_freeId) {
	var _i;
	for(_i=0; _i<4; _i++)
	{
		if(device_mouse_check_button_released(_i,mb_left))
		{
			var _mouseX	= device_mouse_x(_i);
			var _mouseY	= device_mouse_y(_i);
			if(_freeId != noone && !position_meeting(_mouseX,_mouseY,_freeId))
			{
				return true;
			}
			else if(position_meeting(_mouseX,_mouseY,_call_id) && _win_layer == global.windowLayer)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
}
function method_function_input_mouse_pressed_gui(_call_id,_win_layer,_freeId) {
	var _i;
	for(_i=0; _i<4; _i++)
	{
		if(device_mouse_check_button_pressed(_i,mb_left))
		{
			var _mouseX	= device_mouse_x_to_gui(_i);
			var _mouseY	= device_mouse_y_to_gui(_i);
			if(_freeId != noone && !position_meeting(_mouseX,_mouseY,_freeId))
			{
				return true;
			}
			else if(position_meeting(_mouseX,_mouseY,_call_id) && _win_layer == global.windowLayer)
			{
				return true;
			}
		}
	}
	return false;
}

function method_function_input_mouse_pressed_gui_single(_call_id,_win_layer,_freeId) {
	var _i	= 0;
	if(device_mouse_check_button_pressed(_i,mb_left))
	{
		var _mouseX	= device_mouse_x_to_gui(_i);
		var _mouseY	= device_mouse_y_to_gui(_i);
		if(_freeId != noone && !position_meeting(_mouseX,_mouseY,_freeId))
		{
			return true;
		}
		else if(position_meeting(_mouseX,_mouseY,_call_id) && _win_layer == global.windowLayer)
		{
			return true;
		}
	}
	return false;
}

function method_function_input_mouse_pressed(_call_id,_win_layer,_freeId) {
	var _i;
	for(_i=0; _i<4; _i++)
	{
		if(device_mouse_check_button_pressed(_i,mb_left))
		{
			var _mouseX	= device_mouse_x(_i);
			var _mouseY	= device_mouse_y(_i);
			if(_freeId != noone && !position_meeting(_mouseX,_mouseY,_freeId))
			{
				return true;
			}
			else if(position_meeting(_mouseX,_mouseY,_call_id) && _win_layer == global.windowLayer)
			{
				return true;
			}
		}
	}
	return false;
}
function method_function_input_mouse_check_gui(_call_id,_win_layer,_freeId) {
	for(var _i=0; _i<4; _i++)
	{
		if(device_mouse_check_button(_i,mb_left))
		{
			var _mouseX	= device_mouse_x_to_gui(_i);
			var _mouseY	= device_mouse_y_to_gui(_i);
			if(_freeId != noone && !position_meeting(_mouseX,_mouseY,_freeId))
			{
				return [true,_i];
			}
			else if(position_meeting(_mouseX,_mouseY,_call_id) && _win_layer == global.windowLayer)
			{
				return [true,_i];
			}
		}
	}
	return [false,_i];
}
