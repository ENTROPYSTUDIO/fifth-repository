function system_room_camera_create(){
	with(obj_system_camera) { instance_destroy(); }
	switch(room)
	{
		case ROOM_STAGE:
		{
			instance_create_layer(0,0,"manager",obj_system_camera);
			
			var VIEW_HEIGHT = 820;
			var VIEW_WIDTH = round(VIEW_HEIGHT * global.displayAspect);

			var layer_id = layer_get_id("manager");
			layer_add_instance(layer_id,obj_system_camera.id);
			ex_camera_create(MAC_CAMERA_NAME_MAIN, view_camera[0], 0, -40,VIEW_WIDTH, VIEW_HEIGHT, 0, 0, 0, 100, noone, true , 0, 0, room_width, room_height);
			
			//window_set_size(view_wport[0],view_hport[0]);
			//surface_resize(application_surface,view_wport[0],view_hport[0]);
			break;
		}
		case ROOM_CHARACTER_INVENTORY:
		{
			instance_create_layer(0,0,"manager",obj_system_camera);
			
			var VIEW_HEIGHT = MAC_GAME_BASE_HEIGHT;
			var VIEW_WIDTH = round(VIEW_HEIGHT * global.displayAspect);
			ex_camera_create(MAC_CAMERA_NAME_MAIN, view_camera[0], 0, 0,MAC_GAME_BASE_WIDTH, MAC_GAME_BASE_HEIGHT, 0, 0, 0, 100, noone, true , 0, 0, room_width, room_height);
			
			//window_set_size(view_wport[0],view_hport[0]);
			//surface_resize(application_surface,view_wport[0],view_hport[0]);
			break;
		}
	}
}
