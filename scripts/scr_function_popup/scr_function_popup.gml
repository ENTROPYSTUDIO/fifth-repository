function popup_message(_message, _time) {
	with(obj_utility_window_popup_message)
	{
	    var ranOut	= irandom(3);
		if(ranOut == 0) then { hspeed	= -30; }
		else if(ranOut == 1) then { hspeed	= 30; }
		else if(ranOut == 2) then { vspeed	= 30; }
		else if(ranOut == 3) then { vspeed	= -30; }
		destroy	= true;
	}
    
	var _inst    = instance_create_depth(get_x_pos_for_gui_ratio(640,""),get_y_pos_for_gui_ratio(360),0,obj_utility_window_popup_message);
	with(_inst)
	{
	    stringMessage   = _message;
	    alarm[0]        = _time;

	    if(is_undefined(stringMessage))
	    {
	        stringMessage = "ERROR";
	    }
	    x_speed         = image_xscale/10;
	    y_speed         = image_yscale/10;
	
		width           = sprite_width-150;
		height          = sprite_height-20;
		font			= global.fontMainMedium;
		var _set		= method_function_string_display_set(stringMessage,width,height,mac_default_font_scale,global.fontMainMedium);
		stringMessage	= _set[0];
		font_size		= _set[1];
		fontColor		= c_white;
	}
	return(_inst);
}

function popup_question(_message,_callbackConfirm,_callbackCancle) {
	var _x	= display_get_gui_width()/2-sprite_get_width(spr_question_window)/2;
	var _y	= display_get_gui_height()/2-sprite_get_height(spr_question_window)/2;
	with(instance_create_depth(_x,_y,depth-1,obj_utility_question_message))
	{
		callbackCancle	= _callbackCancle;	
		callbackConfirm	= _callbackConfirm;
		questionMessage	= _message;
	}
}
