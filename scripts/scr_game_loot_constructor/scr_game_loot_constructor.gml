#region constructLootDraw
function constructLootDraw() constructor {
	shadowSprite	= spr_character_loot_shadow;
	
}
#endregion

#region constructLootState
function constructLootState() constructor {
	hsp	= random_range(-2,2);
	vsp	= random_range(-5,-12);
	grv	= 0.5;
	finalY	= 0;
	imageScale	= 1;
	ready	= false;
	objectType	= "loot";
	amount	= 0;
	category	= "gold";
	target	= noone;
	die	= false;
	static destroyProcess	= function(){
		var _now	= user_resource_get_real(category);
		var _amount	= amount;
		var _final	= string(floor(_now+_amount));
		global.structUserData.resource[$ category]	= _final;
		firebase_userdata_pipeline_add("resource/"+category,_final);
	};
}
#endregion

