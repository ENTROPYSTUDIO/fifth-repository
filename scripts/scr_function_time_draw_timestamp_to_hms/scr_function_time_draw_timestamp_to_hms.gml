function method_function_time_draw_timestamp_to_hms(_arg) {
	/*
	** Input    : Real   : _steps
	** Output   : String : 00:00:00
	*/
 
	//Set-up of script local variables
	var _steps			= _arg*0.001;
	if(_steps < 0)
	{
		return("00:00:00");
	}
	var _minute_check	= 60;
	var _hour_check		= 3600;
	if(_steps < _minute_check) // 1hour <
	{
		var _decimal	= floor(frac(_steps)*100);
		var _seconds	= floor(_steps);
		var _minutes	= floor(_seconds/60);
		var _hours		= floor(_minutes/60);
		//Calculate the number of minutes and second
 
		_minutes = _minutes - _hours * 60;
		_seconds = _seconds - _hours * 3600 - _minutes*60;
	
		//Insert additional zeroes to mantain format
		if (_minutes < 10)
		{
			_minutes = "0" + string(_minutes);
		}
		if (_seconds < 10)
		{
			_seconds = "0" + string(_seconds);
		}

		return /*"00:"+*/string(_minutes) + ":" + string(_seconds);
	}
	else if(_steps < _hour_check) // 1hour <
	{
		var _decimal	= floor(frac(_steps)*100);
		var _seconds	= floor(_steps);
		var _minutes	= floor(_seconds/60);
		var _hours		= floor(_minutes/60);
		//Calculate the number of minutes and second
 
		_minutes = _minutes - _hours * 60;
		_seconds = _seconds - _hours * 3600 - _minutes*60;
	
		//Insert additional zeroes to mantain format
		if (_minutes < 10)
		{
			_minutes = "0" + string(_minutes);
		}
		if (_seconds < 10)
		{
			_seconds = "0" + string(_seconds);
		}

		return /*"00:"+*/string(_minutes) + ":" + string(_seconds);
	}
	else // 1hour >
	{
		var _seconds = floor(_steps);
		var _minutes = floor(_seconds/60);
		var _hours   = floor(_minutes/60);
	
		//Calculate the number of minutes and second
 
		_minutes = _minutes - _hours * 60;
		_seconds = _seconds - _hours * 3600 - _minutes*60;
	
		//Insert additional zeroes to mantain format
		if (_hours < 10)
		{
			_hours = "0" + string(_hours);
		}
		if (_minutes < 10)
		{
			_minutes = "0" + string(_minutes);
		}
		if (_seconds < 10)
		{
			_seconds = "0" + string(_seconds);
		}

		return string(_hours) + ":" + string(_minutes) + ":" + string(_seconds);
	}
}
