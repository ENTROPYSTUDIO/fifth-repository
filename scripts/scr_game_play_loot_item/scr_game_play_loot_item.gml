// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function method_game_play_create_loot(_amount){
	if(instance_number(obj_parent_loot) < 50)
	{
		with(instance_create_depth(random_range(bbox_left,bbox_right),bbox_top,0,obj_loot_carrot))
		{
			structState.category	= "carrot";
			structState.amount	= floor(_amount);
		}
	}
}

function game_play_loot_log_construct(_category,_amount,_key) constructor{
	alpha	= 1;
	destroyStart	= current_time+3000;
	goalY	= other.y;
	itemEndX	= 256;
	surface	= noone;
	surfaceUpdate	= false;
	static drawItem	= function(_x,_y){
		if(!surface_exists(surface))
		{
			surface	= surface_create(itemEndX,itemEndY);
			surfaceUpdate	= true;
		}
		if(surfaceUpdate)
		{
			surfaceUpdate	= false;
			surface_set_target(surface);
			draw_clear_alpha(c_black,0.05);
			
			draw_sprite_ext(icon,iconSub,iconX,iconY,iconScale,iconScale,0,c_white,1);
			
			gpu_set_blendenable(false);
			preSetForDrawText(fa_left,fa_middle,prefixTextColor,1,global.fontMainBold);
			draw_text_transformed(prefixTextX,textY,prefixText,textScale,textScale,0);
			draw_set_color(mainTextColor);
			draw_text_transformed(mainTextX,textY,mainText,textScale,textScale,0);
			draw_set_color(suffixTextColor);
			draw_text_transformed(suffixTextX,textY,suffixText,textScale,textScale,0);
			gpu_set_blendenable(true);
			
			//draw_sprite_ext(icon,iconSub,_x+iconX,_y+iconY,iconScale,iconScale,0,c_white,alpha);
			
			//preSetForDrawText(fa_left,fa_middle,textColor,alpha,global.fontMainBold);
			//draw_text_transformed(_x+textX,_y+textY,text,textScale,textScale,0);
			
			surface_reset_target();
		}
		draw_surface_ext(surface,_x,_y,1,1,0,c_white,alpha);
	};
	
	static initialised	= function(_category,_amount,_key){
		switch(_category)
		{
			case "carrot" :
			case "gold" :
			case "gem" :
			{
				draw_set_font(global.fontMainBold);
				draw_set_halign(fa_left);
				draw_set_halign(fa_top);
				
				icon	= asset_get_index("spr_icon_currency_"+_category);
				iconScale	= 24/sprite_get_height(icon);
				iconSub	= 0;
				iconX	= sprite_get_width(icon)*iconScale/2+10;
				iconY	= sprite_get_height(icon)/2;
				
				textScale	= mac_default_font_scale-0.3;
				textY		= iconY;
				
				prefixTextX	= iconX+(sprite_get_width(icon)*iconScale/2+5);
				prefixText	= string(method_struct_get_deep_value(global.structGameText,["obtain",_category,global.languageCode]));
				prefixTextColor	= _category == "carrot" ? c_white : (_category == "gold" ? c_yellow : c_purple);
				
				mainTextX	= prefixTextX+(string_width(prefixText)*textScale);
				mainText	= " "+string_number_abbrev(_amount,3)+" ";
				mainTextColor	= c_white;
				
				suffixTextX	= mainTextX+(string_width(mainText)*textScale);
				suffixText	= " "+string(method_struct_get_deep_value(global.structGameText,["obtain","obtain",global.languageCode]));
				suffixTextColor	= c_white;
				
				itemEndX	= suffixTextX+(string_width(suffixText)*textScale)+10;
				itemEndY	= string_height(prefixText);
			}
		}
	};
	
	static move	= function(_id){
		var _goalY	= goalY;
		with(_id)
		{
			if(abs(x-xstart) > 1)
			{
				x	= lerp(x,xstart,0.3);
			}
			if(abs(y-_goalY) > 1)
			{
				y	= lerp(y,_goalY,0.05);
			}
			if(y < 400) { instance_destroy(); }
		}
	};
	
	static alphaChange	= function(){
		if(current_time > destroyStart)
		{
			alpha	-= 0.02;
			if(alpha < 0) { instance_destroy(other); }
		}
	};
	
	static changeGoalY	= function(){
		goalY -= 36;
	};
	
	static clean	= function(){
		if(surface_exists(surface))
		{
			surface_free(surface);
		}
	};
	initialised(_category,_amount,_key);
};
function game_play_loot_log_create(_category,_amount,_key) {
	with(obj_room_stage_loot_log_item)
	{
		structItem.changeGoalY();
		structItem.destroyStart	= true;
	}
		
	with(instance_create_layer(32,550,"loot_log",obj_room_stage_loot_log_item))
	{
		structItem	= new game_play_loot_log_construct(_category,_amount,_key);
		x	-= 400;
	}
}







