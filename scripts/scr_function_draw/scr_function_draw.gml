#region drawTextConstructor
function drawTextConstructor(_text,_x,_y,_h,_v,_font,_scale,_color,_alpha) constructor {
	text	= _text;
	textX	= _x;
	textY	= _y;
	textH	= _h;
	textV	= _v;
	textFont	= _font;
	textScale	= _scale;
	textColor	= _color;
	textAlpha	= _alpha;
	
	static draw	= function(_x,_y,_scale,_alpha,_color) {
		textX	= is_undefined(_x) ? textX : _x;
		textY	= is_undefined(_y) ? textY : _y;
		textScale	= is_undefined(_scale) ? textScale : _scale;
		textAlpha	= is_undefined(_alpha) ? textAlpha : _alpha;
		textColor	= is_undefined(_color) ? textColor : _color;
		draw_set_font(textFont);
		draw_set_color(textColor);
		draw_set_halign(textH);
		draw_set_valign(textV);
		draw_set_alpha(textAlpha);
		draw_text_transformed(textX,textY,text,textScale,textScale,0);
	}
}
#endregion

#region drawSpriteConstructor
function drawSpriteConstructor(_sprite,_subimg,_x,_y,_scale,_rot,_color,_alpha) constructor {
	sprite	= _sprite;
	subimg	= _subimg;
	drawX	= _x;
	drawY	= _y;
	scale	= _scale;
	rot	= _rot;
	color	= _color;
	alpha	= _alpha;
	static draw	= function(_x,_y,_scale,_alpha,_color) {
		drawX	= is_undefined(_x) ? drawX : _x;
		drawY	= is_undefined(_y) ? drawY : _y;
		scale	= is_undefined(_scale) ? scale : _scale;
		alpha	= is_undefined(_alpha) ? alpha : _alpha;
		color	= is_undefined(_color) ? color : _color;
		draw_sprite_ext(sprite,subimg,drawX,drawY,scale,scale,rot,color,alpha)
	}
}
#endregion

#region grade color
function constructGradeColor() constructor {
	grade_0	= $c9c9c9;
	grade_1	= $c9c9c9;
	grade_2	= $00ff1e;
	grade_3	= $00ff1e;
	grade_4	= $dd7000;
	grade_5	= $dd7000;
	grade_6	= $ee35a3;
	grade_7	= $00eaff;
	grade_8	= $0080ff;
	grade_9	= $3030fb;
}
#endregion

#region pre set for draw text
function preSetForDrawText(_halign,_valign,_color,_alpha,_font) {
	draw_set_halign(_halign);
	draw_set_valign(_valign);
	draw_set_color(_color);
	draw_set_alpha(_alpha);
	draw_set_font(_font);
}
#endregion