#region pet_get_static_ability
function pet_get_static_ability(_uid,_statName,_returnType){
	var _return	= 0;
	var _grade	= method_struct_get_deep_value(global.structUserData,["inventory","pet",_uid,"grade"]);
	var _key	= mac_equip_pet_key_prefix+_grade;
	switch(_returnType)
	{
		case "display":
		{
			_return	= method_struct_get_deep_value(global.structGameMeta,["petBase",_key,_statName]);
			break;
		}
		case "battle":
		{
			_return	= method_struct_get_deep_value(global.structGameMeta,["petBase",_key,_statName]);
			if(_statName == "lootDelay")
			{
				_return	*= 1000;
			}
			break;
		}
		case "string":
		{
			_return	= string(method_struct_get_deep_value(global.structGameMeta,["petBase",_key,_statName]));
			break;
		}
	}
	return _return;
}
#endregion