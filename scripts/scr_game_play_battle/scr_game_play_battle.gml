function game_play_default_attack(_target){
	if(!instance_exists(_target)) { return false; }
	var _sign	= sign(_target.x-x);
	var _critical	= random(100) < structAbility.critical ? true : false;
	var _attackPower	= structAbility.attack;
	var _criticalPower	= structAbility.criticalPower;
	var _targetDie	= false;
	
	if(!_critical)
	{
		var _forceAnimation	= "hit";
		var _wave	= structAbility.attackWave;
		var _attackWave		= random_range(1-_wave,1+_wave);
		var _hitShader	= shd_hit_flash_default;
		_attackPower	= floor(_attackWave*_attackPower);
	}
	else
	{
		var _forceAnimation	= "bigHit";
		var _hitShader	= shd_hit_flash_critical;
		_attackPower = floor(_attackPower*structAbility.criticalPower);
		if(!ex_camera_is_shaking(MAC_CAMERA_NAME_MAIN) && global.structUserData.option.screenShake)
		{
			ex_camera_shake(MAC_CAMERA_NAME_MAIN, 1, 1, 1, 2,undefined,undefined,undefined);
		}
	}
	
	with(_target)
	{
		structDraw.hitSet(mac_hit_duration,_hitShader);
		var _targetCategory	= structState.category;
		var _nowHealth	= structAbility.healthPoint;
		
		if(_targetCategory == "enemy")
		{
			structAbility.healthPoint = clamp(_nowHealth-_attackPower,0,_nowHealth);		
			if(structAbility.healthPoint <= 0)
			{
				_targetDie	= true;
				_forceAnimation	= "die";
				structState.target	= noone;
				structState.die	= true;
			}
			if(structState.target == noone)
			{
				structState.target	= other.id;
			}
			
			structState.healthRatio	= structAbility.healthPoint/structAbility.healthPointMax;
			var _loot	= round(clamp(_attackPower,1,_nowHealth));
			method_game_play_create_loot(_loot);
			
			var _x	= random_range(bbox_left+20,bbox_right-20);
			var _y	= random_range(bbox_top+20,bbox_bottom-20);
			with(obj_system_particle_main_maneger)
			{
				makerFunction(enumParticleBurst.hitEffect,_x,_y);
			}
			if(_critical)
			{
				var _x	= x;
				var _y	= y;
				with(obj_system_particle_main_maneger)
				{
					makerFunction(enumParticleBurst.criticalEffect,_x,_y);
				}
			}
		}
		if(_targetCategory == "player")
		{
			with(motherInst)
			{
				surfaceUpdate	= true;
			}
		}
		
		structState.forceAnimation	= _forceAnimation;

		//var _choose	= choose(ef_ellipse,ef_ring);
		
		if(!_critical)
		{
			var _text	= string_number_abbrev(_attackPower,2);
			structDraw.defaultDamage	= 1;
			structDraw.defaultDamageText	= _text;
			structDraw.defaultDamageX	= x;//x+(100*_sign);
			structDraw.defaultDamageFinalX	= random_range(x-10,x+10);
			structDraw.defaultDamageY	= bbox_top-100;
			structDraw.defaultDamageFinalY	= bbox_top-30;
			structDraw.defaultDamageAlpha	= 2;
			structDraw.defaultDamageColor	= _targetCategory == "enemy" ? c_white : c_white;
			structDraw.defaultDamageOrientation	= 0;//irandom(1);
			structDraw.defaultDamageScale	= _targetCategory == "enemy" ? 2.5 : 1;
		}
		else
		{			
			//effect_create_below(ef_ellipse,x,y,1,c_white);
			var _text	= string_number_abbrev(_attackPower,2);
			structDraw.criticalDamage	= 1;
			structDraw.criticalDamageText	= _text;
			structDraw.criticalDamageX	= x+(150*_sign);
			structDraw.criticalDamageFinalX	= x;
			structDraw.criticalDamageY	= bbox_top-90;
			structDraw.criticalDamageFinalY	= bbox_top-30;
			structDraw.criticalDamageAlpha	= 2;
			structDraw.criticalDamageColor	= c_red;//_targetCategory == "enemy" ? c_red : $c9c9c9;
			structDraw.criticalDamageOrientation	= 1;//irandom(1);
			structDraw.criticalDamageScale	= 1.2;
			
			var _text	= "x"+string_number_abbrev(_criticalPower,2);
			structDraw.criticalInfo	= 1;
			structDraw.criticalInfoText	= _text;
			structDraw.criticalInfoX	= x;//x+(100*_sign);
			structDraw.criticalInfoFinalX	= x;
			structDraw.criticalInfoY	= bbox_top-60;
			structDraw.criticalInfoFinalY	= bbox_top-30;
			structDraw.criticalInfoAlpha	= 2;
			structDraw.criticalInfoColor	= c_white;
			structDraw.criticalInfoOrientation	= 0;//irandom(1);
			structDraw.criticalInfoScale	= _targetCategory == "enemy" ? 6 : 3;
		}
	}
	return true;
}