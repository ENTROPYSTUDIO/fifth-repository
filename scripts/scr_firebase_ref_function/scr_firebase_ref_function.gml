function method_firebase_ref_addChildren(_ref,_arrChild) {

	var _struct	= json_parse(_ref);
	if(is_struct(_struct))
	{
		var _size	= array_length(_arrChild);
		for(var _a=0; _a<_size; _a++)
		{
			variable_struct_set(_struct,string(variable_struct_names_count(_struct)),_arrChild[_a]);
		}
		var _return	= json_stringify(_struct);
		delete(_struct);
		return(_return);
	}
}

function firebase_ref(_arrayRef) {
	var _struct	= new refConstrutor(_arrayRef);
	var _json	= json_stringify(_struct);
    delete _struct;
	return(_json);
}

function refConstrutor(_arrayRef) constructor {
	var _size	= array_length(_arrayRef);
	for(var _i=0; _i<_size; ++_i)
	{
		variable_struct_set(self,string(_i),_arrayRef[_i]);
	}
}