function method_Scroll_callChildenMove(_list) {

	if(!ds_exists(_list,ds_type_list))
	{
		exit;
	}

	var a = 0;
	while(true)
	{
		var ins = _list[| a];
	
		if(is_undefined(ins))
		{
			break;
		}
	
		with(ins)
		{
			event_user(1);
		}
		
		a++;
	}
}
