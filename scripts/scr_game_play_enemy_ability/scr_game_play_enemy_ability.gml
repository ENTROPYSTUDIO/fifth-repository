function game_play_enemy_get_ability_attack(_stageKey,_type){
	var _statName	= "attack";
	var _level	= real(string_digits(_stageKey));
	var _ranGrade	= string(irandom(5));
	
	var _default	= method_struct_get_deep_value(global.structGameMeta,["stageData",_type,_statName]);
	var _base	= method_struct_get_deep_value(global.structGameMeta,["characterStatus",_statName,"amountBase_"+_ranGrade]);
	var _now	= power(_base,_level)*_default;
	return _now;
}
function game_play_enemy_get_ability_healthPointMax(_stageKey,_type){
	var _statName	= "healthPointMax";
	var _level	= real(string_digits(_stageKey));
	var _ranGrade	= string(irandom(5));
	
	var _default	= method_struct_get_deep_value(global.structGameMeta,["stageData",_type,_statName]);
	var _base	= method_struct_get_deep_value(global.structGameMeta,["characterStatus",_statName,"amountBase_"+_ranGrade]);
	var _now	= power(_base,_level)*_default;
	return _now;
}
function game_play_enemy_get_ability_critical(_stageKey,_type){
	var _statName	= "critical";
	var _level	= real(string_digits(_stageKey));
	
	var _default	= method_struct_get_deep_value(global.structGameMeta,["stageData",_type,_statName]);
	var _now		= 1+power(_level,1)*_default;
	return _now;
}
function game_play_enemy_get_ability_criticalPower(_stageKey,_type){
	var _statName	= "criticalPower";
	var _level		= real(string_digits(_stageKey));
	
	var _default	= method_struct_get_deep_value(global.structGameMeta,["stageData",_type,_statName]);
	var _now		= 2+power(_level,1)*_default;
	return _now;
}
function game_play_enemy_get_ability_attackSpeed(_stageKey,_type){
	var _statName	= "attackSpeed";
	var _level	= real(string_digits(_stageKey));
	
	var _default	= method_struct_get_deep_value(global.structGameMeta,["stageData",_type,_statName]);
	var _now	= min(1+power(_level,1)*_default,3);
	return _now;
}
function game_play_enemy_get_ability_moveSpeed(_stageKey,_type){
	var _statName	= "moveSpeed";
	var _level	= real(string_digits(_stageKey));
	
	var _default	= method_struct_get_deep_value(global.structGameMeta,["stageData",_type,_statName]);
	var _now	= min(5+power(_level,1)*_default,15);
	return _now;
}
function game_play_enemy_get_ability_multipleTarget(_stageKey,_type){
	var _statName	= "multipleTarget";
	var _level		= real(string_digits(_stageKey));
	
	var _default	= method_struct_get_deep_value(global.structGameMeta,["stageData",_type,_statName]);
	var _now		= floor(1+power(_level,1)*_default);
	return _now;
}
function game_play_enemy_get_ability_attackDelay(_stageKey,_type){
	var _statName	= "attackDelay";
	var _level		= real(string_digits(_stageKey));
	
	var _default	= method_struct_get_deep_value(global.structGameMeta,["stageData",_type,_statName]);
	var _now		= max(_default*0.1,_default-(_level*10));
	return _now;
}
function game_play_enemy_get_ability_attackWave(_stageKey,_type){
	var _statName	= "attackWave";
	var _default	= method_struct_get_deep_value(global.structGameMeta,["stageData",_type,_statName]);
	return _default;
}


//attack	healthPointMax	critical	criticalPower	attackSpeed	moveSpeed	multipleTarget	attackDelay	attackWave