/// @script
function method_show_debug(_arrArg) {
	if(global.debugMode)
	{
		var _i,_value,_debug_message;
		_debug_message	= "DEBUG MESSAGE";
		var _size	= array_length(_arrArg);
		for(_i=0; _i<_size; _i++)
		{
			_value		= string(_arrArg[_i]);
			_debug_message	+= " / "+_value;
		}
		show_debug_message(_debug_message);
	}
}
