function method_admob_show_rewarded_video(_rewardType) {
	with(Obj_AdMob)
	{
		if(Admob_RewardedVideoAd_isLoaded())
		{
			nowAdType	= _rewardType;
			Admob_RewardedVideoAd_Show();
			return true;
		}
		else
		{
			var _message	= method_struct_get_deep_value(global.structGameText,["common","adLoading",global.languageCode]);
			method_function_popup_message(_message,180);
			Admob_RewardedVideoAd_Load();
			return false;
		}
	}
}

function method_admob_load_rewarded_video() {
	with(Obj_AdMob)
	{
		if(!Admob_RewardedVideoAd_isLoaded())
		{
			Admob_RewardedVideoAd_Load();
		}
	}
}


function method_appodeal_show_rewarded_video(_rewardType) {
	with(OBJ_APPODEAL)
	{
		var _state	= appodealIsRewardedVideoLoaded();
		if(_state == "ready")
		{
			nowAdType	= _rewardType;
			appodealRewardedVideoShow();
			return true;
		}
		else 
		{
			if(_state == "notLoad")
			{
				appodealRewardedVideoLoad();
			}
			var _message	= method_struct_get_deep_value(global.structGameText,["common","adLoading",global.languageCode]);
			method_function_popup_message(_message,180);
			return false;
		}
	}
}

