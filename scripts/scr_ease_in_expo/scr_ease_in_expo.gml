/// @description method_ease_in_expo(time, start, change, duration)
/// @param time
/// @param  start
/// @param  change
/// @param  duration
function method_ease_in_expo(_t,_b,_c,_d) {

	/*
	 * Returns the easing function result
	 *
	 * @param   time      Time current position, real
	 * @param   start     Start time position, real
	 * @param   change    Change current amount, real
	 * @param   duration  Total duration, real
	 * 
	 * @return  Returns the easing function result, real
	 *
	 * @license http://robertpenner.com/easing_terms_of_use.html
	 */


	if (_t == 0) {
	    return _b;
	} else {
	    return (_c * power(2, 10 * (_t/_d - 1)) + _b);
	}



}
