function system_particle_hit_effect() {
	// Setup:
	global.part_type_hitEffect = part_type_create();
	part_type_shape(global.part_type_hitEffect, pt_shape_line);
	part_type_size(global.part_type_hitEffect, 0.15, 0.25, 0, 0);
	part_type_scale(global.part_type_hitEffect, 1, 2);
	part_type_color1(global.part_type_hitEffect, 16777215);
	part_type_alpha3(global.part_type_hitEffect, 0.21, 1, 0.26);
	part_type_speed(global.part_type_hitEffect, 3, 7, 0, 0.1);
	part_type_direction(global.part_type_hitEffect, 0, 360, 0, 0);
	part_type_gravity(global.part_type_hitEffect, 0, 0);
	part_type_orientation(global.part_type_hitEffect, 0, 360, -5, 0, 0);
	part_type_blend(global.part_type_hitEffect, 0);
	part_type_life(global.part_type_hitEffect, 5, 25);
	return global.part_type_hitEffect;
}

function system_particle_critical_effect() {
	// Setup:
	global.part_type_criticalEffect = part_type_create();
	part_type_shape(global.part_type_criticalEffect, pt_shape_ring);
	part_type_size(global.part_type_criticalEffect, 1.3,1.3,0.2,0.05);
	part_type_scale(global.part_type_criticalEffect,2,0.4);
	part_type_color1(global.part_type_criticalEffect, 16777215);
	part_type_alpha3(global.part_type_criticalEffect, 0,0.3,0);
	part_type_speed(global.part_type_criticalEffect, 0,0,0,0);
	part_type_direction(global.part_type_criticalEffect, 0,0, 0, 0);
	part_type_gravity(global.part_type_criticalEffect, 0, 0);
	part_type_orientation(global.part_type_criticalEffect, 0,0,0, 0, 0);
	part_type_blend(global.part_type_criticalEffect,1);
	part_type_life(global.part_type_criticalEffect, 5,10);
	return global.part_type_criticalEffect;
}

function system_particle_character_bottom_aura(_create) {
	if(_create)
	{
		partSystemAura = part_system_create();
		partTypeAura = part_type_create();
		part_type_shape(partTypeAura,pt_shape_ring);
		part_type_size(partTypeAura,1.3,1.3,0,0);
		part_type_scale(partTypeAura,0.8,3);
		part_type_color3(partTypeAura,16777215,16777215,16777215);
		part_type_alpha3(partTypeAura,0.1,0.05,0.02);
		part_type_speed(partTypeAura,0,0,0,0);
		part_type_direction(partTypeAura,90,90,0,0);
		part_type_gravity(partTypeAura,0.40,90);
		part_type_orientation(partTypeAura,0,0,0,0,1);
		part_type_blend(partTypeAura,1);
		part_type_life(partTypeAura,5,10);
		partEmitterAura = part_emitter_create(partSystemAura);
		part_emitter_region(partSystemAura,partEmitterAura,x,x,y,y,ps_shape_rectangle,1);
		part_emitter_stream(partSystemAura,partEmitterAura,partTypeAura,1);
		
		return partSystemAura;
	}
	else
	{
		if(part_type_exists(partTypeAura))
		{
			part_type_destroy(partTypeAura);
		}
		if(part_emitter_exists(partSystemAura,partEmitterAura))
		{
			part_emitter_destroy(partSystemAura,partEmitterAura)
		}
		if(part_system_exists(partSystemAura))
		{
			part_system_destroy(partSystemAura);
		}
	}
}
