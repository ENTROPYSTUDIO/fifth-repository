function method_Scroll_draw_childs(_list) {

	if(!ds_exists(_list,ds_type_list))
	{
		exit;
	}

	var _a = 0;
	while(true)
	{
		var _inst = _list[| _a];
	
		if(is_undefined(_inst))
		{
			break;
		}
	
		with(_inst)
		{
			if(bbox_left < owner.bbox_right && bbox_right > owner.bbox_left)
			{
				draw = true;
				event_perform(ev_draw,0);
			}
			draw	= false;
		
			//draw = true;
			//event_perform(ev_draw,0);
			//draw = false;
		}
		_a++;
	}



}
