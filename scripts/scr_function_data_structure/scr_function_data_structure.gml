function method_struct_draw_new_size(_struct) {
	var _drawMeta = function() constructor
	{
		type	= enumDrawGrid.typeSprite;
		text	= "";
		hAlignment = fa_center;
		vAlignment = fa_middle;
		alpha	= 1;
		color	= c_white;
		spriteScaleX	= 1;
		spriteScaleY	= 1;
		textScaleX	= mac_default_font_scale;
		textScaleY	= mac_default_font_scale;
		setScaleX	= 1;
		setScaleY	= 1;
		font	= font_debug;
		drawType	= 0;
		bmSrc	= bm_one;
		bmDest	= bm_inv_src_alpha;
		rot	= 0;
	}
	
	var _newName	= string(variable_struct_names_count(_struct));
	_struct[$ _newName]	= new _drawMeta();
	return _newName;
}

function method_struct_get_deep_value(_mapTarget,_arrArg) {
	var _i;
	var _size	= array_length(_arrArg);
	for(_i=0; _i<_size; _i++)
	{
		var _key	= _arrArg[_i];
		
		if(variable_struct_exists(_mapTarget,_key))
		{
			_mapTarget	= variable_struct_get(_mapTarget,_key);
		}
		else
		{
			return undefined;
		}
	}

	var _return	= _mapTarget;
	return(_return);
}

function method_struct_get_address_search_value(_mapTarget,_arg0,_finalKey) {
	var _i;
	var _address	= method_function_string_slice(_arg0);

	for(_i=0; _i<array_length(_address); _i++)
	{
		var _addressValue	= _address[_i];
		if(variable_struct_exists(_mapTarget,_addressValue))
		{
			_mapTarget	= variable_struct_get(_mapTarget,_addressValue);
		}
		else
		{
			return undefined;
		}
	}
	
	return(_mapTarget[$ _finalKey]);

}

function method_struct_increment_deep_value(_mapTarget,_arrArg,_setValue) {
	var _i;
	var _size	= array_length(_arrArg);
	for(_i=0; _i<_size; _i++)
	{
		var _key	= _arrArg[_i];
		
		if(variable_struct_exists(_mapTarget,_key))
		{
			if(_i == _size-1)
			{
				variable_struct_set(_mapTarget,_key,variable_struct_get(_mapTarget,_key)+_setValue);
			}
			else
			{
				_mapTarget	= variable_struct_get(_mapTarget,_key);
			}
		}

		else
		{
			return undefined;
		}
	}

	var _return	= _mapTarget;
	return(_return);
}

function method_struct_address_search_map(_mapTarget,_arg0) {
	var _i;
	var _address	= method_function_string_slice(_arg0);

	for(_i=0; _i<array_length(_address); _i++)
	{
		var _addressValue	= _address[_i];
		if(variable_struct_exists(_mapTarget,_addressValue))
		{
			_mapTarget	= variable_struct_get(_mapTarget,_addressValue);
		}
		else
		{
			return undefined;
		}
	}
	
	return(_mapTarget);

}
function method_draw_grid_create(_arg) {
	var _height	= _arg;
	var _gridDraw	= ds_grid_create(enumDrawGrid.size,_height);
	ds_grid_set_region(_gridDraw,enumDrawGrid.type,0,enumDrawGrid.type,ds_grid_height(_gridDraw)-1,enumDrawGrid.typeSprite);
	ds_grid_set_region(_gridDraw,enumDrawGrid.text,0,enumDrawGrid.text,ds_grid_height(_gridDraw)-1,"");
	ds_grid_set_region(_gridDraw,enumDrawGrid.hAlignment,0,enumDrawGrid.hAlignment,ds_grid_height(_gridDraw)-1,fa_center);
	ds_grid_set_region(_gridDraw,enumDrawGrid.vAlignment,0,enumDrawGrid.vAlignment,ds_grid_height(_gridDraw)-1,fa_middle);
	ds_grid_set_region(_gridDraw,enumDrawGrid.alpha,0,enumDrawGrid.alpha,ds_grid_height(_gridDraw)-1,1);
	ds_grid_set_region(_gridDraw,enumDrawGrid.color,0,enumDrawGrid.color,ds_grid_height(_gridDraw)-1,c_white);
	ds_grid_set_region(_gridDraw,enumDrawGrid.spriteScaleX,0,enumDrawGrid.spriteScaleX,ds_grid_height(_gridDraw)-1,1);
	ds_grid_set_region(_gridDraw,enumDrawGrid.spriteScaleY,0,enumDrawGrid.spriteScaleY,ds_grid_height(_gridDraw)-1,1);
	ds_grid_set_region(_gridDraw,enumDrawGrid.textScaleX,0,enumDrawGrid.textScaleX,ds_grid_height(_gridDraw)-1,mac_default_font_scale);
	ds_grid_set_region(_gridDraw,enumDrawGrid.textScaleY,0,enumDrawGrid.textScaleY,ds_grid_height(_gridDraw)-1,mac_default_font_scale);
	ds_grid_set_region(_gridDraw,enumDrawGrid.setScaleX,0,enumDrawGrid.setScaleX,ds_grid_height(_gridDraw)-1,1);
	ds_grid_set_region(_gridDraw,enumDrawGrid.setScaleY,0,enumDrawGrid.setScaleY,ds_grid_height(_gridDraw)-1,1);
	ds_grid_set_region(_gridDraw,enumDrawGrid.font,0,enumDrawGrid.font,ds_grid_height(_gridDraw)-1,font_gmarket_sans_medium);
	ds_grid_set_region(_gridDraw,enumDrawGrid.drawType,ds_grid_height(_gridDraw)-1,enumDrawGrid.drawType,ds_grid_height(_gridDraw)-1,0);
	ds_grid_set_region(_gridDraw,enumDrawGrid.bmSrc,ds_grid_height(_gridDraw)-1,enumDrawGrid.bmSrc,ds_grid_height(_gridDraw)-1,bm_one);
	ds_grid_set_region(_gridDraw,enumDrawGrid.bmDest,ds_grid_height(_gridDraw)-1,enumDrawGrid.bmDest,ds_grid_height(_gridDraw)-1,bm_inv_src_alpha);
	ds_grid_set_region(_gridDraw,enumDrawGrid.rot,ds_grid_height(_gridDraw)-1,enumDrawGrid.rot,ds_grid_height(_gridDraw)-1,0);

	return(_gridDraw);


}
function method_draw_grid_resize(_gridDraw) {
	ds_grid_resize(_gridDraw,enumDrawGrid.size,ds_grid_height(_gridDraw)+1);
	ds_grid_set_region(_gridDraw,enumDrawGrid.type,ds_grid_height(_gridDraw)-1,enumDrawGrid.type,ds_grid_height(_gridDraw)-1,enumDrawGrid.typeSprite);
	ds_grid_set_region(_gridDraw,enumDrawGrid.text,ds_grid_height(_gridDraw)-1,enumDrawGrid.text,ds_grid_height(_gridDraw)-1,"");
	ds_grid_set_region(_gridDraw,enumDrawGrid.hAlignment,ds_grid_height(_gridDraw)-1,enumDrawGrid.hAlignment,ds_grid_height(_gridDraw)-1,fa_center);
	ds_grid_set_region(_gridDraw,enumDrawGrid.vAlignment,ds_grid_height(_gridDraw)-1,enumDrawGrid.vAlignment,ds_grid_height(_gridDraw)-1,fa_middle);
	ds_grid_set_region(_gridDraw,enumDrawGrid.alpha,ds_grid_height(_gridDraw)-1,enumDrawGrid.alpha,ds_grid_height(_gridDraw)-1,1);
	ds_grid_set_region(_gridDraw,enumDrawGrid.color,ds_grid_height(_gridDraw)-1,enumDrawGrid.color,ds_grid_height(_gridDraw)-1,c_white);
	ds_grid_set_region(_gridDraw,enumDrawGrid.spriteScaleX,ds_grid_height(_gridDraw)-1,enumDrawGrid.spriteScaleX,ds_grid_height(_gridDraw)-1,1);
	ds_grid_set_region(_gridDraw,enumDrawGrid.spriteScaleY,ds_grid_height(_gridDraw)-1,enumDrawGrid.spriteScaleY,ds_grid_height(_gridDraw)-1,1);
	ds_grid_set_region(_gridDraw,enumDrawGrid.textScaleX,ds_grid_height(_gridDraw)-1,enumDrawGrid.textScaleX,ds_grid_height(_gridDraw)-1,mac_default_font_scale);
	ds_grid_set_region(_gridDraw,enumDrawGrid.textScaleY,ds_grid_height(_gridDraw)-1,enumDrawGrid.textScaleY,ds_grid_height(_gridDraw)-1,mac_default_font_scale);
	ds_grid_set_region(_gridDraw,enumDrawGrid.setScaleX,ds_grid_height(_gridDraw)-1,enumDrawGrid.setScaleX,ds_grid_height(_gridDraw)-1,1);
	ds_grid_set_region(_gridDraw,enumDrawGrid.setScaleY,ds_grid_height(_gridDraw)-1,enumDrawGrid.setScaleY,ds_grid_height(_gridDraw)-1,1);
	ds_grid_set_region(_gridDraw,enumDrawGrid.font,ds_grid_height(_gridDraw)-1,enumDrawGrid.font,ds_grid_height(_gridDraw)-1,font_debug);
	ds_grid_set_region(_gridDraw,enumDrawGrid.drawType,ds_grid_height(_gridDraw)-1,enumDrawGrid.drawType,ds_grid_height(_gridDraw)-1,0);
	ds_grid_set_region(_gridDraw,enumDrawGrid.bmSrc,ds_grid_height(_gridDraw)-1,enumDrawGrid.bmSrc,ds_grid_height(_gridDraw)-1,bm_one);
	ds_grid_set_region(_gridDraw,enumDrawGrid.bmDest,ds_grid_height(_gridDraw)-1,enumDrawGrid.bmDest,ds_grid_height(_gridDraw)-1,bm_inv_src_alpha);
	ds_grid_set_region(_gridDraw,enumDrawGrid.rot,ds_grid_height(_gridDraw)-1,enumDrawGrid.rot,ds_grid_height(_gridDraw)-1,0);
}
function method_ds_grid_delete_row(_grid,_row) {
	//ds_grid_delete_row(grid index, _row)
	//  deletes the _row in the given grid and resizes the grid appropriately
	//WILL NOT WORK IF THERE IS ONLY 1 ROW IN YOUR GRID.  Thanks for the stupid change, Yoyo.
	var _grid_width	= ds_grid_width(_grid);
	var _grid_height	= ds_grid_height(_grid);

	ds_grid_set_grid_region(_grid, _grid, 0, _row+1, _grid_width-1, _grid_height-1, 0,_row);
	ds_grid_resize(_grid,_grid_width,_grid_height-1);
}
function method_ds_grid_bt_box_input(_grid,_originX,_originY,_width,_height) {

	ds_grid_resize(_grid,ds_grid_width(_grid),ds_grid_height(_grid)+1);

	_grid[# 0,ds_grid_height(_grid)-1]	= _originX-_width/2;
	_grid[# 1,ds_grid_height(_grid)-1]	= _originY-_height/2;
	_grid[# 2,ds_grid_height(_grid)-1]	= _originX+_width/2;
	_grid[# 3,ds_grid_height(_grid)-1]	= _originY+_height/2;
}
