/// @description method_ease_inout_back(time, start, change, duration)
/// @param time
/// @param  start
/// @param  change
/// @param  duration
function method_ease_inout_back(_t,_b,_c,_d) {

	/*
	 * Returns the easing function result
	 *
	 * @param   time      Time current position, real
	 * @param   start     Start time position, real
	 * @param   change    Change current amount, real
	 * @param   duration  Total duration, real
	 * 
	 * @return  Returns the easing function result, real
	 *
	 * @license http://robertpenner.com/easing_terms_of_use.html
	 */


	//var _td  = _t/_d;
	var _td2 = _t/(_d/2);

	var _s = 1.70158;

	if ((_td2) < 1) {
	    return _c/2*(_td2*_td2*(((_s*(1.525))+1)*_td2 - (_s*(1.525)))) + _b;
	} else {
	    return _c/2*((_td2-2)*(_td2-2)*(((_s*(1.525))+1)*(_td2-2) + (_s*(1.525))) + 2) + _b;
	}




}
