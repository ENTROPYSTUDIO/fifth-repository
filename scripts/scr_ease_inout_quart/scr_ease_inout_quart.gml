/// @description method_ease_inout_quart(time, start, change, duration)
/// @param time
/// @param  start
/// @param  change
/// @param  duration
function method_ease_inout_quart(_t,_b,_c,_d) {

	/*
	 * Returns the easing function result
	 *
	 * @param   time      Time current position, real
	 * @param   start     Start time position, real
	 * @param   change    Change current amount, real
	 * @param   duration  Total duration, real
	 * 
	 * @return  Returns the easing function result, real
	 *
	 * @license http://robertpenner.com/easing_terms_of_use.html
	 */


	var _td2 = _t/(_d/2);

	if ((_td2) < 1) {
	    return _c/2*_td2*_td2*_td2*_td2 + _b;
	}

	return -_c/2 * ((_td2-2)*(_td2-2)*(_td2-2)*(_td2-2) - 2) + _b;



}
