function constrouctorResourceDraw() constructor {
	surfaceText	= noone;
	surfaceTextUpdate	= false;
	surfaceBack	= noone;
	surfaceBackUpdate	= false;
	resourceX	= other.x+170;
	resourceY	= other.y+(sprite_get_height(spr_stage_resource_bar_carrot)/2)+1;
	resourceInter	= 200;
	scale	= other.scale;
	
	arraySprite		= [spr_stage_resource_bar_carrot];
	spriteWidth		= sprite_get_width(spr_stage_resource_bar_carrot);
	spriteHeight	= sprite_get_height(spr_stage_resource_bar_carrot);
	
	structResource	= {
		carrot : {
			beforeAmount : 0,
			drawAmount : 0
		}
	};
	
	static calculateResource	= function(){
		var _names	= variable_struct_get_names(structResource);
		var _size	= array_length(_names);
		for(var _i=0; _i<_size; ++_i)
		{
			var _key	= _names[_i];
			var _struct	= structResource[$ _key];
			var _change	= string_number_resource_display(_struct,variable_struct_get(global.structUserData.resource,_key));
			if(_change) { surfaceTextUpdate	= true; }
		}	
	};
	
	static drawResource	= function() {
		static drawX	= spriteWidth-10;
		static drawY	= spriteHeight/2+1;
		preSetForDrawText(fa_right,fa_middle,c_white,1,font_stage_resource);
		//shader_premultiply(true,undefined,undefined);
		var _names	= variable_struct_get_names(structResource);
		var _size	= array_length(_names);
		for(var _i=0; _i<_size; ++_i)
		{
			var _key	= _names[_i];
			var _struct	= structResource[$ _key];
			draw_text(drawX+(resourceInter*_i),drawY,_struct.drawAmount);
		}
		//shader_premultiply(false,undefined,undefined);

	}
	
	static drawBack	= function() {
		for(var _i=0; _i<array_length(arraySprite); ++_i)
		{
			draw_sprite(arraySprite[_i],0,resourceInter*_i,0);
		}
	}
	
	
	static drawMain	= function() {
		if(!surface_exists(surfaceText))
		{
			surfaceText	= surface_create(spriteWidth*array_length(arraySprite),spriteHeight);
			surfaceTextUpdate	= true;
		}
		if(surfaceTextUpdate)
		{
			surfaceTextUpdate	= false;
			surface_set_target(surfaceText);
			draw_clear_alpha(c_black,0);
			draw_set_alpha(1);
			gpu_set_blendenable(false);
			drawResource();
			gpu_set_blendenable(true);
			surface_reset_target();
		}
		
		if(!surface_exists(surfaceBack))
		{
			surfaceBack	= surface_create(spriteWidth*array_length(arraySprite),spriteHeight);
			surfaceBackUpdate	= true;
		}
		if(surfaceBackUpdate)
		{
			surfaceBackUpdate	= false;
			surface_set_target(surfaceBack);
			draw_clear_alpha(c_black,0);
			draw_set_alpha(1);
			drawBack();
			surface_reset_target();
		}
		
		draw_surface_ext(surfaceBack,other.x,other.y,scale,scale,0,c_white,1);
		draw_surface_ext(surfaceText,other.x,other.y,scale,scale,0,c_white,1);
	}
}