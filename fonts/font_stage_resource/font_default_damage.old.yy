{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Montserrat Black",
  "styleName": "Black",
  "size": 22.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":9,"h":36,"character":32,"shift":9,"offset":0,},
    "48": {"x":205,"y":78,"w":20,"h":36,"character":48,"shift":20,"offset":0,},
    "49": {"x":227,"y":78,"w":11,"h":36,"character":49,"shift":12,"offset":0,},
    "50": {"x":2,"y":116,"w":19,"h":36,"character":50,"shift":18,"offset":-1,},
    "51": {"x":23,"y":116,"w":19,"h":36,"character":51,"shift":18,"offset":-1,},
    "52": {"x":44,"y":116,"w":21,"h":36,"character":52,"shift":21,"offset":0,},
    "53": {"x":67,"y":116,"w":18,"h":36,"character":53,"shift":18,"offset":0,},
    "54": {"x":87,"y":116,"w":20,"h":36,"character":54,"shift":19,"offset":0,},
    "55": {"x":109,"y":116,"w":19,"h":36,"character":55,"shift":19,"offset":0,},
    "56": {"x":130,"y":116,"w":20,"h":36,"character":56,"shift":20,"offset":0,},
    "57": {"x":152,"y":116,"w":19,"h":36,"character":57,"shift":19,"offset":0,},
    "65": {"x":173,"y":116,"w":26,"h":36,"character":65,"shift":24,"offset":-1,},
    "66": {"x":201,"y":116,"w":21,"h":36,"character":66,"shift":23,"offset":1,},
    "67": {"x":224,"y":116,"w":22,"h":36,"character":67,"shift":22,"offset":0,},
    "68": {"x":2,"y":154,"w":23,"h":36,"character":68,"shift":24,"offset":1,},
    "69": {"x":27,"y":154,"w":18,"h":36,"character":69,"shift":20,"offset":1,},
    "70": {"x":47,"y":154,"w":18,"h":36,"character":70,"shift":19,"offset":1,},
    "71": {"x":67,"y":154,"w":22,"h":36,"character":71,"shift":23,"offset":0,},
    "72": {"x":91,"y":154,"w":21,"h":36,"character":72,"shift":24,"offset":1,},
    "73": {"x":114,"y":154,"w":8,"h":36,"character":73,"shift":10,"offset":1,},
    "74": {"x":124,"y":154,"w":17,"h":36,"character":74,"shift":17,"offset":-1,},
    "75": {"x":143,"y":154,"w":23,"h":36,"character":75,"shift":22,"offset":1,},
    "76": {"x":168,"y":154,"w":17,"h":36,"character":76,"shift":18,"offset":1,},
    "77": {"x":187,"y":154,"w":26,"h":36,"character":77,"shift":28,"offset":1,},
    "78": {"x":215,"y":154,"w":21,"h":36,"character":78,"shift":24,"offset":1,},
    "79": {"x":2,"y":192,"w":25,"h":36,"character":79,"shift":25,"offset":0,},
    "80": {"x":29,"y":192,"w":20,"h":36,"character":80,"shift":22,"offset":1,},
    "81": {"x":51,"y":192,"w":26,"h":36,"character":81,"shift":25,"offset":0,},
    "82": {"x":79,"y":192,"w":21,"h":36,"character":82,"shift":22,"offset":1,},
    "83": {"x":102,"y":192,"w":19,"h":36,"character":83,"shift":19,"offset":0,},
    "84": {"x":183,"y":78,"w":20,"h":36,"character":84,"shift":19,"offset":0,},
    "85": {"x":160,"y":78,"w":21,"h":36,"character":85,"shift":23,"offset":1,},
    "86": {"x":133,"y":78,"w":25,"h":36,"character":86,"shift":23,"offset":-1,},
    "87": {"x":27,"y":40,"w":36,"h":36,"character":87,"shift":35,"offset":0,},
    "88": {"x":13,"y":2,"w":24,"h":36,"character":88,"shift":22,"offset":-1,},
    "89": {"x":39,"y":2,"w":23,"h":36,"character":89,"shift":21,"offset":-1,},
    "90": {"x":64,"y":2,"w":20,"h":36,"character":90,"shift":20,"offset":0,},
    "97": {"x":86,"y":2,"w":18,"h":36,"character":97,"shift":19,"offset":0,},
    "98": {"x":106,"y":2,"w":19,"h":36,"character":98,"shift":21,"offset":1,},
    "99": {"x":127,"y":2,"w":18,"h":36,"character":99,"shift":18,"offset":0,},
    "100": {"x":147,"y":2,"w":20,"h":36,"character":100,"shift":21,"offset":0,},
    "101": {"x":169,"y":2,"w":19,"h":36,"character":101,"shift":19,"offset":0,},
    "102": {"x":190,"y":2,"w":14,"h":36,"character":102,"shift":13,"offset":0,},
    "103": {"x":206,"y":2,"w":20,"h":36,"character":103,"shift":21,"offset":0,},
    "104": {"x":228,"y":2,"w":19,"h":36,"character":104,"shift":21,"offset":1,},
    "105": {"x":2,"y":40,"w":9,"h":36,"character":105,"shift":10,"offset":0,},
    "106": {"x":13,"y":40,"w":12,"h":36,"character":106,"shift":10,"offset":-3,},
    "107": {"x":65,"y":40,"w":21,"h":36,"character":107,"shift":21,"offset":1,},
    "108": {"x":123,"y":78,"w":8,"h":36,"character":108,"shift":10,"offset":1,},
    "109": {"x":88,"y":40,"w":29,"h":36,"character":109,"shift":31,"offset":1,},
    "110": {"x":119,"y":40,"w":19,"h":36,"character":110,"shift":21,"offset":1,},
    "111": {"x":140,"y":40,"w":20,"h":36,"character":111,"shift":20,"offset":0,},
    "112": {"x":162,"y":40,"w":19,"h":36,"character":112,"shift":21,"offset":1,},
    "113": {"x":183,"y":40,"w":20,"h":36,"character":113,"shift":21,"offset":0,},
    "114": {"x":205,"y":40,"w":12,"h":36,"character":114,"shift":13,"offset":1,},
    "115": {"x":219,"y":40,"w":17,"h":36,"character":115,"shift":17,"offset":0,},
    "116": {"x":238,"y":40,"w":14,"h":36,"character":116,"shift":13,"offset":0,},
    "117": {"x":2,"y":78,"w":18,"h":36,"character":117,"shift":20,"offset":1,},
    "118": {"x":22,"y":78,"w":21,"h":36,"character":118,"shift":19,"offset":-1,},
    "119": {"x":45,"y":78,"w":30,"h":36,"character":119,"shift":29,"offset":-1,},
    "120": {"x":77,"y":78,"w":21,"h":36,"character":120,"shift":19,"offset":-1,},
    "121": {"x":100,"y":78,"w":21,"h":36,"character":121,"shift":19,"offset":-1,},
    "122": {"x":123,"y":192,"w":17,"h":36,"character":122,"shift":17,"offset":0,},
    "9647": {"x":142,"y":192,"w":18,"h":36,"character":9647,"shift":28,"offset":5,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":48,"upper":57,},
    {"lower":65,"upper":90,},
    {"lower":97,"upper":122,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "font_default_damage",
  "tags": [],
  "resourceType": "GMFont",
}