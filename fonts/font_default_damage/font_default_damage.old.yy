{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Montserrat Black",
  "styleName": "Black",
  "size": 24.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "stageRoom",
    "path": "texturegroups/stageRoom",
  },
  "ascenderOffset": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":10,"h":39,"character":32,"shift":10,"offset":0,},
    "37": {"x":301,"y":43,"w":29,"h":39,"character":37,"shift":29,"offset":0,},
    "43": {"x":332,"y":43,"w":18,"h":39,"character":43,"shift":20,"offset":1,},
    "46": {"x":352,"y":43,"w":10,"h":39,"character":46,"shift":10,"offset":0,},
    "48": {"x":364,"y":43,"w":22,"h":39,"character":48,"shift":22,"offset":0,},
    "49": {"x":388,"y":43,"w":12,"h":39,"character":49,"shift":13,"offset":0,},
    "50": {"x":402,"y":43,"w":20,"h":39,"character":50,"shift":19,"offset":-1,},
    "51": {"x":424,"y":43,"w":20,"h":39,"character":51,"shift":20,"offset":-1,},
    "52": {"x":446,"y":43,"w":23,"h":39,"character":52,"shift":23,"offset":0,},
    "53": {"x":471,"y":43,"w":20,"h":39,"character":53,"shift":20,"offset":0,},
    "54": {"x":2,"y":84,"w":21,"h":39,"character":54,"shift":21,"offset":0,},
    "55": {"x":25,"y":84,"w":20,"h":39,"character":55,"shift":21,"offset":0,},
    "56": {"x":47,"y":84,"w":21,"h":39,"character":56,"shift":22,"offset":0,},
    "57": {"x":70,"y":84,"w":21,"h":39,"character":57,"shift":21,"offset":0,},
    "65": {"x":93,"y":84,"w":28,"h":39,"character":65,"shift":26,"offset":-1,},
    "66": {"x":146,"y":84,"w":23,"h":39,"character":66,"shift":25,"offset":1,},
    "67": {"x":2,"y":125,"w":24,"h":39,"character":67,"shift":24,"offset":0,},
    "68": {"x":171,"y":84,"w":25,"h":39,"character":68,"shift":26,"offset":1,},
    "69": {"x":198,"y":84,"w":20,"h":39,"character":69,"shift":22,"offset":1,},
    "70": {"x":220,"y":84,"w":20,"h":39,"character":70,"shift":21,"offset":1,},
    "71": {"x":242,"y":84,"w":24,"h":39,"character":71,"shift":25,"offset":0,},
    "72": {"x":268,"y":84,"w":23,"h":39,"character":72,"shift":26,"offset":1,},
    "73": {"x":293,"y":84,"w":9,"h":39,"character":73,"shift":11,"offset":1,},
    "74": {"x":304,"y":84,"w":18,"h":39,"character":74,"shift":18,"offset":-1,},
    "75": {"x":324,"y":84,"w":25,"h":39,"character":75,"shift":24,"offset":1,},
    "76": {"x":351,"y":84,"w":19,"h":39,"character":76,"shift":20,"offset":1,},
    "77": {"x":372,"y":84,"w":28,"h":39,"character":77,"shift":31,"offset":1,},
    "78": {"x":402,"y":84,"w":23,"h":39,"character":78,"shift":26,"offset":1,},
    "79": {"x":427,"y":84,"w":27,"h":39,"character":79,"shift":27,"offset":0,},
    "80": {"x":456,"y":84,"w":22,"h":39,"character":80,"shift":24,"offset":1,},
    "81": {"x":480,"y":84,"w":28,"h":39,"character":81,"shift":27,"offset":0,},
    "82": {"x":276,"y":43,"w":23,"h":39,"character":82,"shift":24,"offset":1,},
    "83": {"x":123,"y":84,"w":21,"h":39,"character":83,"shift":21,"offset":0,},
    "84": {"x":253,"y":43,"w":21,"h":39,"character":84,"shift":21,"offset":0,},
    "85": {"x":349,"y":2,"w":23,"h":39,"character":85,"shift":25,"offset":1,},
    "86": {"x":14,"y":2,"w":27,"h":39,"character":86,"shift":25,"offset":-1,},
    "87": {"x":43,"y":2,"w":39,"h":39,"character":87,"shift":39,"offset":0,},
    "88": {"x":84,"y":2,"w":26,"h":39,"character":88,"shift":24,"offset":-1,},
    "89": {"x":112,"y":2,"w":25,"h":39,"character":89,"shift":23,"offset":-1,},
    "90": {"x":139,"y":2,"w":21,"h":39,"character":90,"shift":22,"offset":1,},
    "97": {"x":162,"y":2,"w":19,"h":39,"character":97,"shift":20,"offset":0,},
    "98": {"x":183,"y":2,"w":21,"h":39,"character":98,"shift":22,"offset":1,},
    "99": {"x":206,"y":2,"w":20,"h":39,"character":99,"shift":20,"offset":0,},
    "100": {"x":228,"y":2,"w":21,"h":39,"character":100,"shift":23,"offset":0,},
    "101": {"x":251,"y":2,"w":21,"h":39,"character":101,"shift":21,"offset":0,},
    "102": {"x":274,"y":2,"w":15,"h":39,"character":102,"shift":14,"offset":0,},
    "103": {"x":291,"y":2,"w":22,"h":39,"character":103,"shift":23,"offset":0,},
    "104": {"x":315,"y":2,"w":20,"h":39,"character":104,"shift":22,"offset":1,},
    "105": {"x":337,"y":2,"w":10,"h":39,"character":105,"shift":10,"offset":0,},
    "106": {"x":374,"y":2,"w":14,"h":39,"character":106,"shift":11,"offset":-4,},
    "107": {"x":209,"y":43,"w":22,"h":39,"character":107,"shift":23,"offset":1,},
    "108": {"x":390,"y":2,"w":8,"h":39,"character":108,"shift":10,"offset":1,},
    "109": {"x":400,"y":2,"w":31,"h":39,"character":109,"shift":33,"offset":1,},
    "110": {"x":433,"y":2,"w":20,"h":39,"character":110,"shift":22,"offset":1,},
    "111": {"x":455,"y":2,"w":22,"h":39,"character":111,"shift":22,"offset":0,},
    "112": {"x":479,"y":2,"w":21,"h":39,"character":112,"shift":22,"offset":1,},
    "113": {"x":2,"y":43,"w":21,"h":39,"character":113,"shift":22,"offset":0,},
    "114": {"x":25,"y":43,"w":14,"h":39,"character":114,"shift":15,"offset":1,},
    "115": {"x":41,"y":43,"w":18,"h":39,"character":115,"shift":18,"offset":0,},
    "116": {"x":61,"y":43,"w":15,"h":39,"character":116,"shift":15,"offset":0,},
    "117": {"x":78,"y":43,"w":20,"h":39,"character":117,"shift":22,"offset":1,},
    "118": {"x":100,"y":43,"w":23,"h":39,"character":118,"shift":21,"offset":-1,},
    "119": {"x":125,"y":43,"w":33,"h":39,"character":119,"shift":31,"offset":-1,},
    "120": {"x":160,"y":43,"w":22,"h":39,"character":120,"shift":21,"offset":-1,},
    "121": {"x":184,"y":43,"w":23,"h":39,"character":121,"shift":21,"offset":-1,},
    "122": {"x":233,"y":43,"w":18,"h":39,"character":122,"shift":18,"offset":0,},
    "9647": {"x":28,"y":125,"w":19,"h":39,"character":9647,"shift":31,"offset":6,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":37,"upper":37,},
    {"lower":43,"upper":43,},
    {"lower":46,"upper":46,},
    {"lower":48,"upper":57,},
    {"lower":65,"upper":90,},
    {"lower":97,"upper":122,},
    {"lower":9647,"upper":9647,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "font_default_damage",
  "tags": [],
  "resourceType": "GMFont",
}