// Fragment Shader

varying vec2 v_vTexcoord;
varying vec4 v_vColour;
 
uniform vec4 gm_SpineTintBlackColour; // This is the uniform containing the tint-black colour
 
void main()
{
 vec4 tb = gm_SpineTintBlackColour;
 vec4 texcol = texture2D( gm_BaseTexture, v_vTexcoord );
 vec4 outcol;
 outcol.rgb = v_vColour.rgb * texcol.rgb;
 outcol.rgb += tb.rgb * ((tb.a * (texcol.a - 1.0)) + (1.0 - texcol.rgb));   // This line performs the tint-black blending logic
 outcol.a = v_vColour.a * texcol.a;
    gl_FragColor = outcol;
}