//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;
void main()
{
	vec4 hitColor	= v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	//hitColor.rgb	= vec3(0.8,0,0);
    gl_FragColor = vec4(1, 0.0, 0.0, hitColor.a);
}


